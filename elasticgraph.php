<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

use Itegration\ElasticGraph\Helper\Operation\Base as BaseOperation;

require realpath(__DIR__) . '/app/bootstrap.php';

define('DS', DIRECTORY_SEPARATOR);

require BP . DS . 'vendor' . DS . 'autoload.php';

$baseOperation = new BaseOperation();

if (false !== $baseOperation->execute()) {
    $baseOperation->sendResponse();
    return;
}

include_once 'index.php';