1.0.2
=====
* Fix bugs and improvements
    
1.0.1
=====
* Fix bugs and improvements
    + Support ajax functions on actions: add to card, add to compare, add ti wishlist.
    + Fixed compatible with Magento 2.3.2

1.0.0
=====
* First releases.    
    
