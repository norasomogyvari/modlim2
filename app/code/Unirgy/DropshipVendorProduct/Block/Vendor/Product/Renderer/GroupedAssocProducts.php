<?php

namespace Unirgy\DropshipVendorProduct\Block\Vendor\Product\Renderer;

use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Data\Form\Element\Renderer\RendererInterface;
use Magento\Framework\App\ObjectManager;

class GroupedAssocProducts extends \Magento\GroupedProduct\Block\Adminhtml\Product\Composite\Fieldset\Grouped implements RendererInterface
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('Unirgy_DropshipVendorProduct::unirgy/udprod/vendor/product/renderer/grouped.phtml');
    }

    public function render(AbstractElement $element)
    {
        $this->setElement($element);
        return $this->toHtml();
    }

    protected $_element;
    public function setElement(AbstractElement $element)
    {
        $this->_element = $element;
        return $this;
    }

    public function getElement()
    {
        return $this->_element;
    }

    public function getFieldName()
    {
        return $this->getData('field_name')
            ? $this->getData('field_name')
            : ($this->getElement() ? $this->getElement()->getName() : '');
    }

    protected $_idSuffix;
    public function resetIdSuffix()
    {
        $this->_idSuffix = null;
        return $this;
    }
    public function getIdSuffix()
    {
        if (null === $this->_idSuffix) {
            $this->_idSuffix = $this->prepareIdSuffix($this->getFieldName());
        }
        return $this->_idSuffix;
    }

    public function prepareIdSuffix($id)
    {
        return preg_replace('/[^a-zA-Z0-9\$]/', '_', $id);
    }

    public function suffixId($id)
    {
        return $id.$this->getIdSuffix();
    }
    public function getAssociatedProducts()
    {
        $product = $this->getProduct();
        $this->_hlp()->setScopeConfig('cataloginventory/options/show_out_of_stock',"1");
        $result = $product->getTypeInstance()->getAssociatedProductCollection($product)
            ->setFlag('product_children',false)
            ->setFlag('udskip_price_index',1)
            ->setFlag('has_group_entity', 1)
            ->setFlag('has_stock_status_filter', 1)
            ->addAttributeToSelect(
                ['name', 'price', 'special_price', 'special_from_date', 'special_to_date', 'tax_class_id', 'image']
            )
            ->addAttributeToFilter('required_options', [['neq' => 1], ['null' => true]], 'left')
            ->setPositionOrder()
            ;
        $result->getSelect()->order('position asc');
        $catHlp = \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Helper\Catalog');
        $catHlp->removePriceIndexFromProductCollection($this, $result->getSelect());

        $storeId = $product->getStoreId();
        foreach ($result as $item) {
            $item->setData('id', $item->getId());
            $item->setData('price', $this->_hlp()->formatPrice($item->getPrice(), false));
            $item->setStoreId($storeId);
        }

        return $result;
    }
    /**
     * @return \Unirgy\Dropship\Helper\Data
     */
    protected function _hlp()
    {
        return ObjectManager::getInstance()->get('\Unirgy\Dropship\Helper\Data');
    }
}