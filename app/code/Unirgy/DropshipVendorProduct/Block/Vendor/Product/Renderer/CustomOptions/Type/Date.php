<?php

namespace Unirgy\DropshipVendorProduct\Block\Vendor\Product\Renderer\CustomOptions\Type;

class Date extends \Magento\Catalog\Block\Adminhtml\Product\Edit\Tab\Options\Type\Date
{
    protected $_template = 'Unirgy_DropshipVendorProduct::catalog/product/edit/options/type/date.phtml';
}