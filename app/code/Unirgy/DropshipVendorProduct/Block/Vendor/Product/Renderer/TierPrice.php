<?php

namespace Unirgy\DropshipVendorProduct\Block\Vendor\Product\Renderer;

use Magento\Catalog\Block\Adminhtml\Product\Edit\Tab\Price\Tier;
use Magento\Framework\App\ObjectManager;
use Unirgy\Dropship\Helper\Data as DropshipHelper;

class TierPrice extends Tier
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('Unirgy_DropshipVendorProduct::unirgy/udprod/vendor/product/renderer/tier_price.phtml');
    }
    public function getAddButtonHtml()
    {
        $this->getChildBlock('add_button')->setTemplate('Unirgy_DropshipVendorProduct::widget/button.phtml');
        return $this->getChildHtml('add_button');
    }
    public function allowGroupTierPrice()
    {
        return $this->_hlp()->getScopeFlag('udprod/general/allow_group_tier_price');
    }
    /**
     * @return DropshipHelper
     */
    protected function _hlp()
    {
        return ObjectManager::getInstance()->get(DropshipHelper::class);
    }
}
