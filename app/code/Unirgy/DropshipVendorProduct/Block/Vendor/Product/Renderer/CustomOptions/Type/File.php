<?php

namespace Unirgy\DropshipVendorProduct\Block\Vendor\Product\Renderer\CustomOptions\Type;

class File extends \Magento\Catalog\Block\Adminhtml\Product\Edit\Tab\Options\Type\File
{
    protected $_template = 'Unirgy_DropshipVendorProduct::catalog/product/edit/options/type/file.phtml';
}