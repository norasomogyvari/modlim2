<?php

namespace Unirgy\DropshipVendorProduct\Block\Vendor\Product\Renderer\CustomOptions\Type;

class Text extends \Magento\Catalog\Block\Adminhtml\Product\Edit\Tab\Options\Type\Text
{
    protected $_template = 'Unirgy_DropshipVendorProduct::catalog/product/edit/options/type/text.phtml';
}