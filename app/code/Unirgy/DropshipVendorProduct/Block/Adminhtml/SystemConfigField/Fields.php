<?php

namespace Unirgy\DropshipVendorProduct\Block\Adminhtml\SystemConfigField;

use \Magento\Config\Block\System\Config\Form\Field;
use \Magento\Framework\Data\Form\Element\AbstractElement;
use \Zend_Json;

class Fields extends Field
{
    public function render(AbstractElement $element)
    {
        $html = parent::render($element);
        $elId = $element->getId();
        $value = $element->getValue();
        if (!is_array($value)) {
            $value = explode(',', $value);
        }
        $valueJson = \Magento\Framework\Serialize\JsonConverter::convert($value);
        $placeholder = $this->escapeJsQuote(__('* Please select'));
        $html .=<<<EOT
<script type="text/javascript">
require(["jquery", "select2", "prototype","domReady!"], function(jQuery) {
    if (jQuery('#{$elId}')) {
        jQuery('#{$elId}').select2({
            width: '250px',
            placeholder: '$placeholder',
            data: window['udprodFields']
        });
        jQuery('#{$elId}').val({$valueJson}).trigger("change");
    }
});
</script>
EOT;
        return $html;
    }
}