<?php

namespace {
    include_once __DIR__ . '/ProductFormModifierGeneralInc.php';
}

namespace Unirgy\DropshipVendorProduct\Ui\DataProvider {

    class ProductFormModifierGeneral extends \Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\General
    {
        protected function customizeStatusField(array $meta)
        {
            return $meta;
        }
    }
}