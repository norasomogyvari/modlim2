===== 2020-06-24 3.2.42 =====
  * Added 'Qty Uses Decimals', 'Notify for Quantity Below', 'Qty Increments' stock field in product edit form

===== 2020-06-23 3.2.41 =====
  * Fixed Duplicate entry 'xx' for key 'PRIMARY', query was: INSERT  INTO catalog_product_website ...

===== 2020-06-08 3.2.40 =====
  * Changed config form layout

===== 2020-05-05 3.2.39 =====
  * Added configuration to allow vendor select customer group in tier price
  * Fixed gallery not imported when product form too big

===== 2020-04-30 3.2.38 =====
  * Fixed depricated url addSessionParam

===== 2020-04-17 3.2.37 =====
  * Fixed empty selection in vendor > limit websites issue error

===== 2020-04-17 3.2.36 =====
  * Fixed 'Explicitly show media gallery in configurable product' have no effect

===== 2020-04-14 3.2.35 =====
  * Added 'Attribute set' layout for edit form setup (to use magento attribute set instead of custom fields setup)
  * Added wysiwyg support

===== 2020-04-13 3.2.34 =====
  * Added support for Limit Websites

===== 2020-03-27 3.2.33 =====
  * Fixed "Product Options" row remove doesn't return option to a dropdown

===== 2020-03-04 3.2.32 =====
  * Added support for dynamic vendor menu

===== 2019-11-19 3.2.31 =====
  * Fixed Unable to resolve the source file for 'Magento_Catalog/catalog/category-selector.css'

===== 2019-11-01 3.2.30 =====
  * Fixed 2.3.3 email sending compatibility

===== 2019-10-22 3.2.29 =====
  * Fixed check against magento version 2.3 instead of presence of Magento_Inventory module

===== 2019-10-18 3.2.28 =====
  * Fixed 2.3 MSI default source item not updated when create/update product from vendor portal

===== 2019-09-23 3.2.27 =====
  * Fixed vendor portal product manager downloadable products layout
  * Fixed vendor portal product manager grouped products

===== 2019-04-30 3.2.26 =====
  * Fixed configurable per simple product image upload doesn't save base image

===== 2019-04-30 3.2.24 =====
  * Fixed 'Cannot instantiate interface Magento\Backend\Model\Image\UploadResizeConfigInterface'

===== 2019-04-02 3.2.23 =====
  * Fixed backward compatibility with 2.0.x

===== 2018-12-18 3.2.22 =====
  * Fixed NoSuchEntityException when add new option to existing product in 2.2.6

===== 2018-12-17 3.2.21 =====
  * Fixed "Integrity constraint violation" in magento 2.2.6 when save product with custom options

===== 2018-12-05 3.2.19 =====
  * Workaround for varien/form removal
  * Fixed only one inventory item created for new configurable product in vendor portal (only for last simple)

===== 2018-12-03 3.2.18 =====
  * Fixed compilation errors on 2.3.0

===== 2018-10-30 3.2.17 =====
  * Fixed admin product edit miss custom statuses

===== 2018-10-23 3.2.16 =====
  * Fixed vendor product manager custom options form doesn't work on composer installed with Opalo theme

===== 2018-10-19 3.2.15 =====
  * Fixed massaction observer for magento>=2.2.5

===== 2018-10-15 3.2.14 =====
  * Fixed missing custom statuses in massaction in admin product listing
  * Fixed error on vendor portal edit of configurable product created from admin but not complete

===== 2018-05-28 3.2.13 =====
  * Fixed  Undefined property:$_hlp in Helper/Udcatalog.php

===== 2018-05-13 3.2.12 =====
  * Removed not used reference to \Magento\Catalog\Model\Product\Gallery\Processor

===== 2018-05-12 3.2.11 =====
  * Fixed 2.0.18, 2.0.13 compilation errors

===== 2018-05-08 3.2.8 =====
  * Modified layout of template sku configuration

===== 2018-04-24 3.2.7 =====
  * Fixed email templates don't use common header/footer templates

===== 2018-02-13 3.2.5 =====
  * Fixed "Integrity constraint violation" in magento 2.2.2 when save product with custom options
  * Added product manager per configurable attribute images upload

===== 3.2.4 =====
  * Fixed product collection use indexed vendor/product associations. Might not be available when indexer mode=update by schedule

===== 3.2.3 =====
  * Fixed can't use all configurable attributes when there are multiple attribute sets in type of product

===== 3.2.2 =====
  * Fixed "Unable to unserialize value"

===== 3.2.1 =====
  * Fixed compatibility with 2.2.0

===== 3.1.27 =====
  * Fixed price display in vendor portal products grid

===== 3.1.26 =====
  * Fixed pending/approved/fix notification emails
  * Fixed new product with custom options when 'Prefix vendor created products' SKUs with vendor id'=Yes

===== 3.1.25 =====
  * Fixed product custom options modifications in vendor portal

===== 3.1.24 =====
  * Fixed product custom options save in vendor portal
  * Fixed inventory default fields not propogated from template product
  * Fixed Call to a member function getMediaAttributes() on null in Block/Vendor/Product/VideoForm.php
  * Fixed stock status column "Not Available" in product manager grid when multivendor add-on used

===== 3.1.23 =====
  * Added support for custom options and dowloadable products in vendor product manager

===== 3.1.22 =====
  * Fixed usage of "Use In PreSelect"=Yes in
    "Template Sku > Product SKUs used as templates (per attribute set) > Default Configurable Attributes"

===== 3.1.21 =====
  * Fixed MEQP2 compatibility

===== 3.1.20 =====
  * Added "add video" functionality in vendor product manager

===== 3.1.19 =====
  * Fixed inconsistent status/qty display in vendor portal product manager

===== 3.1.18 =====
  * Fixed per vendor default inventory attributes values
  * Added missing * near required field label

===== 3.1.17 =====
  * Fixed date from/to validation on some locales fails

===== 3.1.16 =====
  * Fixed vendor portal product manager stock qty not updated
  * Fixed cannot delete out of stock products in vendor portal
  * Fixed vendor portal products delete in EE 2.1.0

===== 3.1.15 =====
  * Fixed LoadTemplateSku not working with custom admin permissions

===== 3.1.14 =====
  * Fixed intermittent vendor portal "product not found" error on EE 2.1

===== 3.1.13 =====
  * Fixed price and stock index filters hide vendor products in products grids in vendor portal

===== 3.1.12 =====
  * Fixed multiUpdateAttributes call when unpublish products on EE 2.1.
  * Fixed vendor portal product edit url contains vendor key which sometime lead to 404 pages when microsite installed

===== 3.1.11 =====
  * Fixed multiselect attributes not saved during initial product add in vendor portal

===== 3.1.10 =====
  * Fixed hide template product text in input when it's focused by default (vendor portal product edit)
  * Removed confusing field "product attributes / categories" from "Vendor Product Edit Form > Fieldsets"
  * Fixed categories field loader image

===== 3.1.9 =====
  * Fixed qty and stock status display in 2.1
  * Fixed https links in notification emails

===== 3.1.8 =====
  * Fixed google optimizer error in vendor portal product save

===== 3.1.7 =====
  * Fixed save of "Configuration > Vendor Products Edit > Vendor Product Edit Form > Fieldsets" with empty fields

===== 3.1.6 =====
  * Fixed vendor sku check for configurable in vendor portal product edit

===== 3.1.5 =====
  * Moved select2 to dropship module

===== 3.1.4 =====
  * Fixed tier_price edit in vendor portal

===== 3.1.3 =====
  * Fixed eliminate FPC for "Add Product Options" js calls

===== 3.1.2 =====
  * Fixed compatibility with magento 2.1

===== 3.0.10 =====
  * Fixed price html rendering

===== 3.0.7 =====
  * Fixed possible unirgyDomLoaded not defined error in vendor portal product edit

===== 3.0.6 =====
  * Fixed "template sku > Product SKUs used as templates (per attribute set)" js errors on some names of type of product

===== 2.4.0 =====
  * CE 1.9.2.2 compatibility
  * Fixed limit categories in admin

===== 2.1.0 =====
  * Central translations file

===== 1.2.26 =====
  * Fixed simples not linked when all attributes identify image
  * Added escapeJsQuote in config gallery
  * Fixed config gallery when all attributes identify image

===== 1.2.25 =====
  * Added ability to upload images per multiple identify image configurable attributes
  * Added option Disable product name "is empty" check
  * Fixed wysiwyg when admin have custom domain url
  * Fixed configurable swatches for CE 1.9.1

==== 1.2.24 =====
  * Added register js base url

==== 1.2.23 =====
  * Fixed storeview save in admin

==== 1.2.22 =====
  * Autohide template product texts on form element focus and require validation when template text not changed

==== 1.2.21 =====
  * Added 'Custom Options Changed' option to unpublish actions

==== 1.2.20 =====
  * Added hide quick create validation when add first row
  * Fixed delete product when there is quote in name

==== 1.2.19 =====
  * Added grouped products support
  * Fixed unpublish in 1.9 when no attributes changed
  * Fixed products grid toolbar pager

==== 1.2.18 =====
  * Added ability to hide add product button to have only virtual and only downloadable type of products

===== 1.2.17 =====
  * Added group price field

===== 1.2.16 =====
  * Added udmulti backorders field on form

===== 1.2.15 =====
  * Fixed pagination

===== 1.2.14 =====
  * Forced unpublish product when status=fix and attributes changed or image added

===== 1.2.13 =====
  * Fixed products grid when multiple websites selected for products

===== 1.2.12 =====
  * Added ability to remove product in vendor portal

===== 1.2.11 =====
  * Added check SKU when create product
  * Fixed udprod_attributes_changed save intermittent problem

===== 1.2.10 =====
  * Added stock_data qty correction field

===== 1.2.9 =====
  * Added virtual products support
  * Added functionality to setup configuration option prices
  * Added "new product" unpublish option

===== 1.2.8 =====
  * Added limit categories option
  * Fixed "Type of product" selector in "Configuration > Vendor Products Edit > Template Sku" when type of product contain slash char

===== 1.2.7 =====
  * Added optimization in product edit form configuration via usage of select2.js

===== 1.2.6 =====
  * Added per vendor default product inventory values (manage stock,backorders,min_qty,min_sale_qty,max_sale_qty)
    will override effect of "configuration > inventory > Product Stock Options" for vendor products
  * Added autogenerate configurable simple skus based on options
  * Fixed product attribute "is wysiwyg"=no have no effect

===== 1.2.5 =====
  * Fixed pending/approved/fix notifications
  * Added downloadable, virtual to filters/options
  * Fixed safeguard vendor portal product edit form from adding fields with same name

===== 1.2.4 =====
  * Added code to support vendor/product associations indexer
  * Added "Gallery upload is required" option to force vendors upload images
  * Fixed media attributes not saved for some scenarious

===== 1.2.3 =====
  * Fixed new notification related attributes definition

===== 1.2.2 =====
  * Fixed downloadble support missing files/changes

===== 1.2.1 =====
  * Added downloadable products support
  * Fixed "Template Sku" config section breaks when type of products have quote in name

===== 1.2.0 =====
  * Added pending/approved/fix notifications
  * Added configuration to show vendor sku column in product grid

===== 1.1.10 =====
  * Added tier price attribute support
  * Fixed "Use custom product zoom on frontend"=No affect price comparison add-on

===== 1.1.9 =====
  * Added support to use same attribute set differently within different type of products

===== 1.1.8 =====
  * Fixed "Check vendor SKU is unique within vendor products"=Yes

===== 1.1.7 =====
  * Fixed storage of template sku config when many attribute sets

===== 1.1.6 =====
  * Fixed redirect to admin login page on some pages when "Add Store Code to Urls"="Yes"

===== 1.1.5 =====
  * Added secure url for udprod/vendor
  * Changed order of "Add New Product", "Add New Product (with options)" buttons to eliminate confusions

===== 1.1.4 =====
  * Added functionality to add/edit custom options
  * Added unpublish on stock change
  * Fixed "quick create" configurable simples does not use configurable websites/categoires

===== 1.1.3 =====
  * Added extra configuration for media upload:
    "Upload images into configrable simples", "Explicitly show media gallery in configurable product"
  * Fixed price in configurable simples empty
  * Fixed sort order for configurable product options on frontend
  * Fixed uploader doesn't work when magento core "Use SID on Frontend"=No
  * Changes in uploader js

===== 1.1.2 =====
  * Fixed hidden value validation failure in quick create section
  * Added "Is product view info use tabbed layout" configuration option
  * Fixed price field in quick create form

===== 1.1.1 =====
  * Added "Show hidden categories to vendor if the field presented on product edit form" option
  * Fixed code config rewrites not added in cron

===== 1.1.0 =====
  * Added configuration to turn on/off custom product zoom on frontend

===== 1.0.1 =====

  * Added "allowed types of product" global and per vendor config
  * Fixed compatibility with IE9 dynamic rows js

===== 1.0 - Initial release =====
