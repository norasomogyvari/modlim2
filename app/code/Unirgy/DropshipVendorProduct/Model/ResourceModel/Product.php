<?php

namespace Unirgy\DropshipVendorProduct\Model\ResourceModel;

use Magento\Catalog\Model\ResourceModel\AbstractResource;
use Magento\Catalog\Model\ResourceModel\Product as ResourceModelProduct;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\DataObject;

class Product extends ResourceModelProduct
{
    protected function _beforeSave(DataObject $object)
    {
        if ($object->hasCategoryIds()) {
            $categoryIds = $this->_hlp()->getObj('\Magento\Catalog\Model\ResourceModel\Category')->verifyIds(
                $object->getCategoryIds()
            );
            $object->setCategoryIds($categoryIds);
        }
        $vId = $this->_hlp()->session()->getVendorId();
        if (!$vId && $this->_hlp()->isAdmin()
            && $this->_hlp()->isModuleActive('umicrosite')
            && ($v = $this->_hlp()->getObj('Unirgy\DropshipMicrosite\Helper\Data')->getAdminhtmlVendor())
        ) {
            $vId = $v->getId();
        } else {
            $vId = $object->getData('udropship_vendor');
        }
        $prefixSkuVid = $this->_hlp()->getScopeFlag('udprod/general/prefix_sku_vid');
        if (!$object->getSku() && $this->_hlp()->getScopeFlag('udprod/general/auto_sku')) {
            $adapter = $this->getConnection();
            $pidSuffix = $adapter->fetchOne($adapter->select()
                ->from($this->getEntityTable(), 'max(entity_id)'));
            do {
                $__checkSku = ++$pidSuffix;
                if ($prefixSkuVid && $vId) {
                    $__checkSku = $vId.'-'.$__checkSku;
                }
                $object->setSku($__checkSku);
            } while ($this->_ucatHlp()->getPidBySkuForUpdate($object->getSku(), $object->getId()));
        }
        if ($prefixSkuVid && $vId && 0 !== strpos($object->getSku(), $vId.'-')) {
            $object->setSku($vId.'-'.$object->getSku());
        }
        if ($this->_hlp()->getScopeFlag('udprod/general/unique_vendor_sku')
            && $vId
            && !$this->_hlp()->isUdmultiActive()
        ) {
            $vSkuAttr = $this->_hlp()->getScopeFlag('udropship/vendor/vendor_sku_attribute');
            if ($vSkuAttr && $vSkuAttr!='sku') {
                if (!$object->getData($vSkuAttr)) {
                    throw new \Exception('Vendor SKU attribute is empty');
                } elseif ($this->_ucatHlp()->getPidByVendorSku($object->getData($vSkuAttr), $vId, $object->getId())) {
                    throw new \Exception(__('Vendor SKU "%1" is already used', $object->getData($vSkuAttr)));
                }
            }
        }
        if ($this->_ucatHlp()->getPidBySku($object->getSku(), $object->getId())) {
            throw new \Exception(__('SKU "%1" is already used', $object->getSku()));
        }
        if ($object->getOptions()) {
            foreach ($object->getOptions() as $option) {
                $option->setProductSku($object->getSku());
            }
        }

        return AbstractResource::_beforeSave($object);
    }

    /**
     * @return \Unirgy\Dropship\Helper\Catalog
     */
    protected function _ucatHlp()
    {
        return ObjectManager::getInstance()->get('Unirgy\Dropship\Helper\Catalog');
    }

    /**
     * @return \Unirgy\Dropship\Helper\Data
     */
    protected function _hlp()
    {
        return ObjectManager::getInstance()->get('Unirgy\Dropship\Helper\Data');
    }
}