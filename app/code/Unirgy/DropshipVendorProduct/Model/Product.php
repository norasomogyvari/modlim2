<?php

namespace Unirgy\DropshipVendorProduct\Model;

use Magento\Catalog\Model\Product as ModelProduct;
use Magento\Framework\App\ObjectManager;

class Product extends ModelProduct
{
    protected function _construct()
    {
        $this->_init('Unirgy\DropshipVendorProduct\Model\ResourceModel\Product');
    }
    public function resetTypeInstance()
    {
        $this->_typeInstanceSingleton = null;
        $this->_typeInstance = null;
        return $this;
    }
    public function beforeSave()
    {
        if ($this->getName() !== false) {
            if (!$this->_hlp()->getScopeFlag('udprod/general/disable_name_check')) {
                $ufName = $this->formatUrlKey($this->getName());
                if (!trim($ufName)) {
                    throw new \Exception(__('Product name is invalid'));
                }
            }
        }
        return parent::beforeSave();
    }
    public function uclearOptions()
    {
        $this->getOptionInstance()->unsetOptions();
        $this->_options = [];
        return $this;
    }
    /**
     * @return \Unirgy\Dropship\Helper\Data
     */
    protected function _hlp()
    {
        return ObjectManager::getInstance()->get('Unirgy\Dropship\Helper\Data');
    }
}