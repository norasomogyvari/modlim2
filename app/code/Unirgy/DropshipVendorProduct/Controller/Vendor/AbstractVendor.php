<?php
/**
 * Unirgy LLC
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.unirgy.com/LICENSE-M1.txt
 *
 * @category   Unirgy
 * @package    Unirgy_Dropship
 * @copyright  Copyright (c) 2008-2009 Unirgy LLC (http://www.unirgy.com)
 * @license    http:///www.unirgy.com/LICENSE-M1.txt
 */

namespace Unirgy\DropshipVendorProduct\Controller\Vendor;

use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\HTTP\Header;
use Magento\Framework\Registry;
use Magento\Framework\View\DesignInterface;
use Magento\Framework\View\LayoutFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Store\Model\StoreManagerInterface;
use Unirgy\DropshipVendorProduct\Helper\Data as DropshipVendorProductHelperData;
use Unirgy\Dropship\Controller\Vendor\AbstractVendor as VendorAbstractVendor;
use Unirgy\Dropship\Helper\Data as HelperData;

abstract class AbstractVendor extends VendorAbstractVendor
{
    /**
     * @var DropshipVendorProductHelperData
     */
    protected $_prodHlp;

    /**
     * @var \Magento\MediaStorage\Helper\File\Storage\Database
     */
    protected $_storageDatabase;
    protected $_helperCatalog;
    protected $_modelProductFactory;

    public function __construct(
        DropshipVendorProductHelperData $helperData,
        Context $context,
        ScopeConfigInterface $scopeConfig, 
        DesignInterface $viewDesignInterface, 
        StoreManagerInterface $storeManager, 
        LayoutFactory $viewLayoutFactory, 
        Registry $registry, 
        ForwardFactory $resultForwardFactory, 
        HelperData $helper, 
        PageFactory $resultPageFactory, 
        RawFactory $resultRawFactory, 
        Header $httpHeader,
        \Unirgy\Dropship\Helper\Catalog $helperCatalog,
        \Unirgy\DropshipVendorProduct\Model\ProductFactory $modelProductFactory,
        \Magento\MediaStorage\Helper\File\Storage\Database $storageDatabase
    )
    {
        $this->_prodHlp = $helperData;
        $this->_helperCatalog = $helperCatalog;
        $this->_modelProductFactory = $modelProductFactory;
        $this->_storageDatabase = $storageDatabase;

        parent::__construct($context, $scopeConfig, $viewDesignInterface, $storeManager, $viewLayoutFactory, $registry, $resultForwardFactory, $helper, $resultPageFactory, $resultRawFactory, $httpHeader);
    }

    protected function _checkProduct($productId=null)
    {
        $this->_prodHlp->checkProduct($productId);
        return $this;
    }

    protected function _redirectUrl($url)
    {
        $this->_response->setRedirect($url);
    }
    
    protected function _redirectAfterPost($prod=null)
    {
        $session = ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session');
        $r = $this->getRequest();
        if (!$r->getParam('continue_edit')) {
            if ($session->getUdprodLastGridUrl()) {
                $this->_redirectUrl($session->getUdprodLastGridUrl());
            } else {
                $this->_redirect('udprod/vendor/products');
            }
        } else {
            if (isset($prod) && $prod->getId()) {
                $this->_redirect('udprod/vendor/productEdit', ['id'=>$prod->getId()]);
            } else {
                $this->_redirect('udprod/vendor/productNew', ['_current'=>true]);
            }
        }
    }
    protected function _initProduct()
    {
        $r = $this->getRequest();
        $v = ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendor();
        $productId  = (int) $this->getRequest()->getParam('id');
        $productData = $r->getPost('product');
        $product = $this->_prodHlp->initProductEdit([
            'id'   => $productId,
            'data' => $productData,
            'vendor' => $v
        ]);
        if (isset($productData['options'])) {
            $productOptions = $productData['options'];
            $product->setProductOptions($productOptions);
            $options = $this->mergeProductOptions(
                $productOptions,
                $r->getPost('options_use_default')
            );
            $customOptions = [];
            foreach ($options as $customOptionData) {
                if (empty($customOptionData['is_delete'])) {
                    if (empty($customOptionData['option_id'])) {
                        $customOptionData['option_id'] = null;
                        $customOptionData['id'] = null;
                    }
                    if (isset($customOptionData['values'])) {
                        $customOptionData['values'] = array_map(function ($valueData) {
                            if ($valueData['option_type_id']==-1) {
                                unset($valueData['option_type_id']);
                            }
                            return $valueData;
                        }, array_filter($customOptionData['values'], function ($valueData) {
                            return empty($valueData['is_delete']);
                        }));
                    }
                    $customOption = $this->getCustomOptionFactory()->create(['data' => $customOptionData]);
                    $customOption->setProductSku($product->getSku());
                    $customOptions[] = $customOption;
                }
            }
            $product->setOptions($customOptions);
        }
        if (isset($productData['links'])) {
            $this->setProductLinks($product, $productData['links']);
        }
        $product->setCanSaveCustomOptions(
            (bool)$this->getRequest()->getPost('affect_product_custom_options')
        );
        if (($lw = array_filter($v->getLimitWebsites()))) {
            $product->unsWebsiteIds();
            $product->setWebsiteIds(is_array($lw) ? $lw : explode(',', $lw));
        }
        return $product;
    }
    protected $customOptionFactory;
    protected function getCustomOptionFactory()
    {
        if (null === $this->customOptionFactory) {
            $this->customOptionFactory = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Catalog\Api\Data\ProductCustomOptionInterfaceFactory');
        }
        return $this->customOptionFactory;
    }
    public function mergeProductOptions($productOptions, $overwriteOptions)
    {
        if (!is_array($productOptions)) {
            return [];
        }

        if (!is_array($overwriteOptions)) {
            return $productOptions;
        }

        foreach ($productOptions as $optionIndex => $option) {
            $optionId = $option['option_id'];
            $option = $this->overwriteValue($optionId, $option, $overwriteOptions);

            if (isset($option['values']) && isset($overwriteOptions[$optionId]['values'])) {
                foreach ($option['values'] as $valueIndex => $value) {
                    if (isset($value['option_type_id'])) {
                        $valueId = $value['option_type_id'];
                        $value = $this->overwriteValue($valueId, $value, $overwriteOptions[$optionId]['values']);
                        $option['values'][$valueIndex] = $value;
                    }
                }
            }

            $productOptions[$optionIndex] = $option;
        }

        return $productOptions;
    }
    private function overwriteValue($optionId, $option, $overwriteOptions)
    {
        if (isset($overwriteOptions[$optionId])) {
            foreach ($overwriteOptions[$optionId] as $fieldName => $overwrite) {
                if ($overwrite && isset($option[$fieldName]) && isset($option['default_' . $fieldName])) {
                    $option[$fieldName] = $option['default_' . $fieldName];
                    if ('title' == $fieldName) {
                        $option['is_delete_store_title'] = 1;
                    }
                }
            }
        }

        return $option;
    }
    public function getCustomOptionValues($customOption)
    {
        $optValues = $customOption->getData('values');
        if (!is_array($optValues)) {
            $optValues = $customOption->getValues();
        }
        if (!is_array($optValues)) {
            $optValues = [];
        }
        return $optValues;
    }
    /*
    */
    public function returnResult($result)
    {
        $this->_resultRawFactory->create()->setContents($this->_hlp->jsonEncode($result));
    }

    /**
     * @return \Magento\Catalog\Model\Product\Initialization\Helper\ProductLinks
     */
    protected function productLinks()
    {
        return $this->_hlp->getObj('Magento\Catalog\Model\Product\Initialization\Helper\ProductLinks');
    }
    protected function linkTypeProvider()
    {
        return $this->_hlp->getObj('Magento\Catalog\Model\Product\LinkTypeProvider');
    }
    protected function productRepository()
    {
        return $this->_hlp->getObj('Magento\Catalog\Api\ProductRepositoryInterface\Proxy');
    }
    protected function productLinkFactory()
    {
        return $this->_hlp->getObj('Magento\Catalog\Api\Data\ProductLinkInterfaceFactory');
    }
    private function getLinkResolver()
    {
        return $this->_hlp->getObj('Magento\Catalog\Model\Product\Link\Resolver');
    }

    protected function setProductLinks(\Magento\Catalog\Model\Product $product, $links)
    {
        $product->setProductLinks([]);
        foreach ($links as $linkType => $subLinks) {
            foreach ($subLinks as $slKey => $linkData) {
                if (empty($linkData['id']) || !is_numeric($linkData['id'])) {
                    unset($links[$linkType][$slKey]);
                }
            }
        }

        $product = $this->productLinks()->initializeLinks($product, $links);
        $productLinks = $product->getProductLinks();
        $linkTypes = [];

        /** @var \Magento\Catalog\Api\Data\ProductLinkTypeInterface $linkTypeObject */
        foreach ($this->linkTypeProvider()->getItems() as $linkTypeObject) {
            $linkTypes[$linkTypeObject->getName()] = $product->getData($linkTypeObject->getName() . '_readonly');
        }

        // skip linkTypes that were already processed on initializeLinks plugins
        foreach ($productLinks as $productLink) {
            unset($linkTypes[$productLink->getLinkType()]);
        }

        foreach ($linkTypes as $linkType => $readonly) {
            if (isset($links[$linkType]) && !$readonly) {
                foreach ((array)$links[$linkType] as $linkData) {
                    if (empty($linkData['id']) || 1*$linkData['id']<=0) {
                        continue;
                    }

                    $linkProduct = $this->productRepository()->getById($linkData['id']);
                    $link = $this->productLinkFactory()->create();
                    $link->setSku($product->getSku())
                        ->setLinkedProductSku($linkProduct->getSku())
                        ->setLinkType($linkType)
                        ->setPosition(isset($linkData['position']) ? (int)$linkData['position'] : 0);
                    $productLinks[] = $link;
                }
            }
        }

        return $product->setProductLinks($productLinks);
    }
}