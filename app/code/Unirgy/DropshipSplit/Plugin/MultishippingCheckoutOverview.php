<?php

namespace Unirgy\DropshipSplit\Plugin;

class MultishippingCheckoutOverview
{
    static $_blockIter = 0;
    protected $_hlp;
    protected $_hlpPr;
    protected $_udsHlp;
    protected $_productFactory;

    public function __construct(
        \Unirgy\Dropship\Helper\Data $udropshipHelper,
        \Unirgy\Dropship\Helper\ProtectedCode $udropshipHelperProtected,
        \Unirgy\DropshipSplit\Helper\Data $udsplitHelper,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Unirgy\DropshipSplit\Model\Cart\VendorFactory $cartVendorFactory
    ) {
        $this->_hlp = $udropshipHelper;
        $this->_hlpPr = $udropshipHelperProtected;
        $this->_udsHlp = $udsplitHelper;
        $this->_productFactory = $productFactory;
        $this->_cartVendorFactory = $cartVendorFactory;
    }

    public function aroundGetShippingAddressItems(\Magento\Multishipping\Block\Checkout\Overview $subject, \Closure $next, $address)
    {
        if (!$this->_udsHlp->isActive()) {
            return $next($address);
        }

        $q = $subject->getCheckout()->getQuote();
        $methods = array();
        $details = $address->getUdropshipShippingDetails();
        if ($details) {
            $details = $this->_hlp->jsonDecode($details);
            $methods = isset($details['methods']) ? $details['methods'] : array();
        }

        $aItems = $address->getAllVisibleItems();
        $vendorItems = array();
        foreach ($aItems as $item) {
            $item->setQuoteItem($q->getItemById($item->getQuoteItemId()));
            $vendorItems[$item->getUdropshipVendor()][] = $item;
        }

        $items = array();

        foreach ($vendorItems as $vId=>$vItems) {
            $obj = $this->_cartVendorFactory->create()
                ->setQuote($q)
                ->setProduct($this->_productFactory->create());
            if (!$this->_hlp->getScopeFlag('carriers/udsplit/hide_vendor_name')) {
                $items[] = $obj->setQuoteItem($this->_cartVendorFactory->create()
                    ->setPart('header')
                    ->setVendor($this->_hlp->getVendor($vId)));
            }

            foreach ($vItems as $item) {
                if ($item->getParentItemId()) {
                    continue;
                }
                $items[] = $item;
            }

#echo "<pre>"; print_r($rates[$vId]); echo "</pre>";
            $obj = $this->_cartVendorFactory->create()
                ->setQuote($q)
                ->setProduct($this->_productFactory->create());
            $items[] = $obj->setQuoteItem($this->_cartVendorFactory->create()
                ->setPart('footer')
                ->setVendor($this->_hlp->getVendor($vId))
                ->setShowDropdowns(false)
                ->setEstimateRates(array())
                ->setErrorsOnly(false)
                ->setShippingMethod(isset($methods[$vId]) ? $methods[$vId] : null)
                ->setItems($vItems)
                ->setAddress($address)
                ->setQuote1($q));

        }
        return $items;
    }

    public function aroundGetItemHtml(\Magento\Multishipping\Block\Checkout\Overview $subject, \Closure $next, \Magento\Framework\DataObject $item)
    {
        if ($item instanceof \Unirgy\DropshipSplit\Model\Cart\Vendor
            || $item->getQuoteItem() instanceof \Unirgy\DropshipSplit\Model\Cart\Vendor
        ) {
            $qItem = !$item instanceof \Unirgy\DropshipSplit\Model\Cart\Vendor
                || $item->getProduct() && !$item->getProduct()->getId()
                    ? $item->getQuoteItem()
                    : $item;
            $blockName = "vendor_{$qItem->getVendor()->getId()}_{$qItem->getPart()}_".(self::$_blockIter++);
            return $subject->getLayout()->createBlock('\Unirgy\DropshipSplit\Block\Multishipping\Vendor', $blockName)
                ->addData($qItem->getData())
                ->setQuote($qItem->getQuote1())
                ->toHtml();
        }
        return $next($item);
    }

    public function aroundGetRowItemHtml(\Magento\Multishipping\Block\Checkout\Overview $subject, \Closure $next, \Magento\Framework\DataObject $item)
    {
        if ($item instanceof \Unirgy\DropshipSplit\Model\Cart\Vendor
            || $item->getQuoteItem() instanceof \Unirgy\DropshipSplit\Model\Cart\Vendor
        ) {
            return $subject->getItemHtml($item);
        }
        return $next($item);
    }

}