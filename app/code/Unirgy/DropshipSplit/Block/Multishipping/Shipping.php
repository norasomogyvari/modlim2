<?php

namespace Unirgy\DropshipSplit\Block\Multishipping;

class Shipping
    extends \Magento\Multishipping\Block\Checkout\Shipping
{
    static $_blockIter = 0;
    protected $_hlp;
    protected $_hlpPr;
    protected $_udsHlp;
    protected $_productFactory;

    public function __construct(
        \Unirgy\Dropship\Helper\Data $udropshipHelper,
        \Unirgy\Dropship\Helper\ProtectedCode $udropshipHelperProtected,
        \Unirgy\DropshipSplit\Helper\Data $udsplitHelper,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Unirgy\DropshipSplit\Model\Cart\VendorFactory $cartVendorFactory,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Filter\DataObject\GridFactory $filterGridFactory,
        \Magento\Multishipping\Model\Checkout\Type\Multishipping $multishipping,
        \Magento\Tax\Helper\Data $taxHelper,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        array $data = []
    ) {
        $this->_hlp = $udropshipHelper;
        $this->_hlpPr = $udropshipHelperProtected;
        $this->_udsHlp = $udsplitHelper;
        $this->_productFactory = $productFactory;
        $this->_cartVendorFactory = $cartVendorFactory;
        return parent::__construct($context, $filterGridFactory, $multishipping, $taxHelper, $priceCurrency, $data);
    }
    protected function _construct()
    {
        parent::_construct();
        $this->setData('module_name', 'Magento_Multishipping');
        $this->setTemplate('Magento_Multishipping::checkout/shipping.phtml');
    }

    public function getAddressItems($a)
    {
        if (!$this->_udsHlp->isActive()) {
            return parent::getAddressItems($a);
        }

        $q = $this->getCheckout()->getQuote();
        $methods = array();
        $details = $a->getUdropshipShippingDetails();
        if ($details) {
            $details = $this->_hlp->jsonDecode($details);
            $methods = isset($details['methods']) ? $details['methods'] : array();
        }

        $aItems = $a->getAllItems();
        $qItems = array();
        foreach ($aItems as $item) {
            $qItem = $q->getItemById($item->getQuoteItemId());
            if (!$qItem->getProduct()) {
                $qItem->setProduct($this->_productFactory->create());
            }
            $item->setQuoteItem($qItem);
            $qItems[] = $qItem;
        }

        $this->_hlpPr->prepareQuoteItems($qItems);

        $vendorItems = array();
        foreach ($aItems as $item) {
            $vendorItems[$item->getUdropshipVendor()][] = $item;
        }

        $rates = array();
        $qRates = $a->getGroupedAllShippingRates();
        foreach ($qRates as $cCode=>$cRates) {
            foreach ($cRates as $rate) {
                $vId = $rate->getUdropshipVendor();
                if (!$vId) {
                    continue;
                }
                $rates[$vId][$cCode][] = $rate;
            }
        }

        $items = array();

        foreach ($vendorItems as $vId=>$vItems) {
            $obj = $this->_cartVendorFactory->create();
            $obj->setQuote($q)
                ->setProduct($this->_productFactory->create());
            if (!$this->_hlp->getScopeFlag('carriers/udsplit/hide_vendor_name')) {
                $items[] = $obj->setQuoteItem($this->_cartVendorFactory->create()
                    ->setPart('header')
                    ->setVendor($this->_hlp->getVendor($vId)));
            }

            foreach ($vItems as $item) {
                if ($item->getParentItemId()) {
                    continue;
                }
                $item->setQty(sprintf('%d', $item->getQty()));
                $items[] = $item;
            }

            $errorsOnly = false;
            if (!empty($rates[$vId])) {
                $errorsOnly = true;
                foreach ($rates[$vId] as $cCode=>$rs) {
                    foreach ($rs as $r) {
                        if (!$r->getErrorMessage()) {
                            $errorsOnly = false;
                        }
                    }
                }
            }
#echo "<pre>"; print_r($rates[$vId]); echo "</pre>";
            $obj = $this->_cartVendorFactory->create();
            $obj->setQuote($q)
                ->setProduct($this->_productFactory->create());
            $items[] = $obj->setQuoteItem($this->_cartVendorFactory->create()
                ->setPart('footer')
                ->setVendor($this->_hlp->getVendor($vId))
                ->setShowDropdowns(true)
                ->setEstimateRates(isset($rates[$vId]) ? $rates[$vId] : array())
                ->setErrorsOnly($errorsOnly)
                ->setShippingMethod(isset($methods[$vId]) ? $methods[$vId] : null)
                ->setItems($vItems)
                ->setAddress($a)
                ->setQuote1($q));

        }
        return $items;
    }

    public function getShippingRates($address)
    {
        if (!$this->_udsHlp->isActive()) {
            return parent::getShippingRates($address);
        }

        $groups = $address->getGroupedAllShippingRates();
        $groups1 = array();
        foreach ($groups as $cCode=>$rates) {
            foreach ($rates as $rate) {
                if ($rate->getUdropshipVendor()) {
                    continue;
                }
                $groups1[$cCode][] = $rate;
            }
        }
        return $groups1;
    }

    public function getItemHtml(\Magento\Framework\DataObject $item)
    {
        if ($item instanceof \Unirgy\DropshipSplit\Model\Cart\Vendor) {
            $blockName = "vendor_{$item->getVendor()->getId()}_{$item->getPart()}_".(self::$_blockIter++);
            return $this->getLayout()->createBlock('Unirgy\DropshipSplit\Block\Multishipping\Vendor', $blockName)
                ->addData($item->getData())
                ->setQuote($item->getQuote1())
                ->toHtml();
        }
        return parent::getItemHtml($item);
    }
}