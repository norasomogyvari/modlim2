<?php
/**
 * Unirgy LLC
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.unirgy.com/LICENSE-M1.txt
 *
 * @category   Unirgy
 * @package    \Unirgy\Dropship
 * @copyright  Copyright (c) 2015-2016 Unirgy LLC (http://www.unirgy.com)
 * @license    http:///www.unirgy.com/LICENSE-M1.txt
 */

namespace Unirgy\DropshipMicrosite\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    const MEDIUMTEXT_SIZE = 16777216;
    const TEXT_SIZE = 65536;

    protected $_hlp;
    protected $rulesFactory;
    protected $encryptor;
    protected $mathRandom;
    protected $_moduleManager;

    public function __construct(
        \Unirgy\Dropship\Helper\Data $udropshipHelper,
        \Magento\Authorization\Model\RulesFactory $rulesFactory,
        \Magento\Framework\Encryption\Encryptor $encryptor,
        \Magento\Framework\Math\Random $mathRandom,
        \Magento\Framework\Module\Manager $moduleManager
    )
    {
        $this->_hlp = $udropshipHelper;
        $this->rulesFactory = $rulesFactory;
        $this->encryptor = $encryptor;
        $this->mathRandom = $mathRandom;
        $this->_moduleManager = $moduleManager;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
    }
}
