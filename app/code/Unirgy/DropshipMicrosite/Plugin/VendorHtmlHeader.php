<?php

namespace Unirgy\DropshipMicrosite\Plugin;

class VendorHtmlHeader
{
    /**
     * @var \Unirgy\Dropship\Helper\Data
     */
    protected $_hlp;

    /**
     * @var \Unirgy\DropshipMicrosite\Helper\Data
     */
    protected $_msHlp;

    public function __construct(
        \Unirgy\Dropship\Helper\Data $udropshipHelper,
        \Unirgy\DropshipMicrosite\Helper\Data $micrositeHelper
    ) {
        $this->_hlp = $udropshipHelper;
        $this->_msHlp = $micrositeHelper;
    }
    public function afterGetVendorMenu(
        \Unirgy\Dropship\Block\Vendor\HtmlHeader $subject,
        $result
    ) {
        /** @var \Magento\Framework\Data\Collection $result */
        if ($item = $result->getItemById('products_admin')) {
            $item->setUrl($this->_msHlp->getManageProductsUrl());
        }
        if (!$this->_hlp->session()->getVendor()->getShowProductsMenuItem()) {
            $result->removeItemByKey('products_admin');
        }
        return $result;
    }
}