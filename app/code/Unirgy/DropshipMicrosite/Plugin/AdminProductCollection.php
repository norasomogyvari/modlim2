<?php

namespace Unirgy\DropshipMicrosite\Plugin;

class AdminProductCollection
{
    /**
     * @var \Unirgy\DropshipMicrosite\Helper\Data
     */
    protected $_msHlp;

    public function __construct(
        \Unirgy\DropshipMicrosite\Helper\Data $micrositeHelper
    ) {
        $this->_msHlp = $micrositeHelper;
    }
    public function beforeLoad(
        \Magento\Catalog\Model\ResourceModel\Product\Collection $subject
    ) {
        if (!($vendor = $this->_getVendor()) || $subject->getFlag('skip_vendor_filter')) {
            return;
        }
        $subject->addAttributeToFilter('udropship_vendor', $vendor->getId());
    }
    protected function _getVendor()
    {
        return $this->_msHlp->getCurrentVendor();
    }
}
