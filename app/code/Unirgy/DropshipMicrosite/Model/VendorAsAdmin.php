<?php

namespace Unirgy\DropshipMicrosite\Model;

class VendorAsAdmin implements \Unirgy\Dropship\Model\VendorAsAdminInterface
{
    /**
     * @return \Unirgy\Dropship\Helper\Data
     */
    protected function _hlp()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Helper\Data');
    }
    protected $vendorId;
    public function getVendorId()
    {
        return $this->vendorId;
    }
    public function setVendorId($vendorId)
    {
        $this->vendorId = $vendorId;
        return $this;
    }
    public function loginAsAdmin($vendor)
    {
        return $this->_hlp()->aHlp()->loginAsAdmin($vendor);
    }
    public function logoutAsAdmin($vendor)
    {
        return $this->_hlp()->aHlp()->logoutAsAdmin($vendor);
    }
    public function loginAdminVendor($user)
    {
        return $this->_hlp()->aHlp()->loginAdminVendor($user);
    }
    public function logoutAdminVendor($vendor)
    {
        return $this->_hlp()->aHlp()->logoutAdminVendor($vendor);
    }
    public function redirectAdminVendor()
    {
        return $this->_hlp()->aHlp()->redirectAdminVendor();
    }
    public function hookBackendSessionStart($session)
    {
        return $this->_hlp()->aHlp()->hookBackendSessionStart($session);
    }
}
