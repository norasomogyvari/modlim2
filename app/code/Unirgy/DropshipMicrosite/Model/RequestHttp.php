<?php

namespace Unirgy\DropshipMicrosite\Model;

class RequestHttp extends \Magento\Framework\App\Request\Http
{
    public function umicrositeReset()
    {
        $this->originalPathInfo = '';
        $this->requestString = '';
        $this->pathInfo = '';
        return $this;
    }
    public function msPathInfo()
    {
        if (class_exists('\Magento\Framework\App\Request\PathInfo')) {
            return $this->msPathInfoService()->getPathInfo(
                $this->getRequestUri(),
                $this->getBaseUrl()
            );
        } else {
            $this->getPathInfo();
        }
    }
    protected function msPathInfoService()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get(
            '\Magento\Framework\App\Request\PathInfo'
        );
    }
}
