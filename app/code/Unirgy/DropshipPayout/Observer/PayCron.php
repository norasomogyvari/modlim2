<?php

namespace Unirgy\DropshipPayout\Observer;

use Psr\Log\LoggerInterface;
use Unirgy\DropshipPayout\Helper\Data as HelperData;
use Unirgy\DropshipPayout\Helper\ProtectedCode;
use Unirgy\DropshipPayout\Model\PayoutFactory;

class PayCron
{
    /**
     * @var PayoutFactory
     */
    protected $_payoutFactory;

    /**
     * @var HelperData
     */
    protected $_payoutHlp;

    /**
     * @var ProtectedCode
     */
    protected $_payoutHlpPr;

    protected $_hlp;

    public function __construct(
        PayoutFactory $modelPayoutFactory,
        HelperData $helperData,
        ProtectedCode $helperProtectedCode,
        \Unirgy\Dropship\Helper\Data $udropshipHelper
    )
    {
        $this->_hlp = $udropshipHelper;
        $this->_payoutFactory = $modelPayoutFactory;
        $this->_payoutHlp = $helperData;
        $this->_payoutHlpPr = $helperProtectedCode;
    }
    public function execute()
    {
        $this->processStandard();
    }
    public function processStandard()
    {
        session_write_close();
        ignore_user_abort(true);
        set_time_limit(0);
        ob_implicit_flush();

        $payouts = $this->_payoutFactory->create()->getCollection()
            ->setFlag('skip_offline', true)
            ->loadScheduledPayouts()
            ->addPendingPos(true)
            ->finishPayout()
            ->saveOrdersPayouts(true);

        try {
            $payouts->pay();
        } catch (\Exception $e) {
            $this->_hlp->logError($e);
        }


        $this->_payoutHlp->generateSchedules()->cleanupSchedules();
    }
}
