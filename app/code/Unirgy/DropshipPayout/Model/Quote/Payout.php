<?php
/**
 * Unirgy LLC
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.unirgy.com/LICENSE-M1.txt
 *
 * @category   Unirgy
 * @package    Unirgy_DropshipPayout
 * @copyright  Copyright (c) 2008-2009 Unirgy LLC (http://www.unirgy.com)
 * @license    http:///www.unirgy.com/LICENSE-M1.txt
 */

namespace Unirgy\DropshipPayout\Model\Quote;

use Magento\Tax\Helper\Data as TaxHelperData;
use Unirgy\DropshipPayout\Helper\Data as DropshipPayoutHelper;
use Unirgy\Dropship\Helper\Data as DropshipHelper;

/**
 * @api
 * @method int getVendorId()
 * @method Payout setVendorId($vendorId)
 * @method float getTotalPayment()
 * @method float getTotalPayout()
 * @method float getTotalInvoice()
 */

class Payout extends \Magento\Framework\DataObject
{
    /**
     * @var DropshipHelper
     */
    protected $_hlp;

    /**
     * @var DropshipPayoutHelper
     */
    protected $ptHlp;

    /**
     * @var
     */
    protected $taxHelper;

    public function __construct(
        DropshipHelper $uDropshipHelper,
        DropshipPayoutHelper $uPayoutHelper,
        TaxHelperData $taxHelper,
        array $data = []
    ) {
        $this->_hlp = $uDropshipHelper;
        $this->ptHlp = $uPayoutHelper;
        $this->taxHelper = $taxHelper;
        parent::__construct($data);
    }

    protected $_rows;
    protected $_totals;
    protected $_totals_amount;

    public function addItem($item, $onlySubtotal)
    {
        $quote = $item->getQuote();
        $quoteId = $this->_getObjectId($quote);
        $itemId = $this->_getObjectId($item);
        $row = $this->_addItem($item, $onlySubtotal);
        $row = $this->_calculateRow($row);
        $this->_totals_amount = $this->_accumulateRow($row, $this->_totals_amount);
        $this->_rows[$quoteId.'-'.$itemId] = $row;
    }

    protected function _getObjectId($object)
    {
        return $object->getId() ? $object->getId() : spl_object_hash($object);
    }

    public function getVendor()
    {
        return $this->_hlp->getVendor($this->getVendorId());
    }

    protected function _getVendorsCount($quote)
    {
        $vIds = [];
        foreach ($quote->getAllItems() as $item) {
            if ($item->isDeleted() || $item->getParentItemId() || $item->getParentItem()) continue;
            $vIds[$item->getUdropshipVendor()] = true;
        }
        return count($vIds);
    }

    public function addAdjustment($amount)
    {
        $this->initTotals();
        $this->_totals_amount['total_payment'] += $amount;
        $this->_totals_amount['total_payout'] += $amount;
        $this->_totals_amount['adj_amount']   += $amount;
        return $this;
    }

    protected function _addItem($item, $onlySubtotal)
    {
        $quote = $item->getQuote();
        $address = $quote->getShippingAddress() ? $quote->getShippingAddress() : $quote->getBillingAddress();
        $quoteId = $this->_getObjectId($quote);
        $itemId = $this->_getObjectId($item);
        $vId = $this->getVendorId();
        $vendor = $this->getVendor();
        $poId = $quoteId.'-'.$vId;
        $hlp = $this->_hlp;
        $qty = $item->getQty();
        $vendorsCount = $this->_getVendorsCount($quote);
        $vendorsCount = $vendorsCount ? $vendorsCount : 1;
        $order = [
            'id' => $itemId,
            'po_id' => $poId,
            'quote_id' => $quoteId,
            'quote_created_at' => $quote->getCreatedAt(),
            'quote_increment_id' => $quote->getIncrementId(),
            'date' => $quote->getCreatedAt(),
            'po_com_percent' => $quote->getCommissionPercent(),
            'com_percent' => $item->getCommissionPercent(),
            'trans_fee' => $item->getTransactionFee(),
            'adjustments' => [],
            'sku' => $item->getSku(),
            'vendor_sku' => $item->getVendorSku(),
            'product' => $item->getName(),
        ];
        $subtotal = $item->getBaseRowTotal();
        $iHiddenTax = $item->getBaseHiddenTaxAmount();
        $iTax = $item->getBaseTaxAmount();
        $iDiscount = $item->getBaseDiscountAmount();
        $iDiscTaxComp = $item->getBaseDiscountTaxCompensationAmount();
        $subtotal += $iDiscTaxComp;

        $vendorRates = $this->_hlp->hlpPr()->getOrderVendorRates($address);
        $shippingAmount = $address->getBaseShippingAmount()/$vendorsCount;
        $shippingTax = $address->getBaseShippingTax()/$vendorsCount;
        $shippingDiscount = $address->getBaseShippingDiscountAmount()/$vendorsCount;
        if (isset($vendorRates[$vId])) {
            $vendorRate = $vendorRates[$vId];
            if (isset($vendorRate['price_excl'])) {
                $shippingAmount = $vendorRate['price_excl'];
            } elseif (isset($vendorRate['price'])) {
                $shippingAmount = $vendorRate['price'];
            }
            if (isset($vendorRate['tax'])) {
                $shippingTax = $vendorRate['tax'];
            }
        }
        $shippingAmount = $this->_hlp->roundPrice($shippingAmount);
        $shippingTax = $this->_hlp->roundPrice($shippingTax);

        if ($this->getVendor()->getIsShippingTaxInShipping()) {
            $shippingAmount += $shippingTax;
        } else {
            if ($onlySubtotal) {
                $iTax += $shippingTax;
            }
        }
        $iTax = $this->_deltaRound($iTax, $quoteId);
        if ($shippingDiscount && !$onlySubtotal && in_array($this->getVendor()->getStatementShippingInPayout(),['', 'include'])) {
            $iDiscount += $shippingDiscount;
        }
        $amountRow = [
            'subtotal' => $subtotal,
            'shipping' => $onlySubtotal ? 0 : $shippingAmount,
            'tax' => $iTax,
            'hidden_tax' => $iHiddenTax,
            'discount' => $iDiscount,
            'handling' => 0,
            'trans_fee' => $onlySubtotal ? 0 : $quote->getTransactionFee(),
            'adj_amount' => 0,
        ];
        foreach ($amountRow as &$_ar) {
            $_ar = is_null($_ar) ? 0 : $_ar;
        }
        unset($_ar);
        $order['amounts'] = array_merge($this->_getEmptyTotals(), $amountRow);
        return $order;
    }

    protected function _calculateRow($row)
    {
        /** @var \Magento\Tax\Helper\Data $taxHelper */
        $taxHelper = $this->_hlp->createObj('\Magento\Tax\Helper\Data');
        $taxInSubtotal = $taxHelper->displaySalesBothPrices() || $taxHelper->displaySalesPriceInclTax();
        if (is_null($row['com_percent'])) {
            $row['com_percent'] = (float)$this->getVendor()->getCommissionPercent();
        }
        $row['com_percent'] *= 1;
        if (is_null($row['po_com_percent'])) {
            $row['po_com_percent'] = (float)$this->getVendor()->getCommissionPercent();
        }
        $row['po_com_percent'] *= 1;

        if (isset($row['amounts']['tax']) && in_array($this->getVendor()->getStatementTaxInPayout(),
                ['', 'include'])
        ) {
            if ($taxInSubtotal) {
                if ($this->getVendor()->getApplyCommissionOnTax()) {
                    $row['amounts']['subtotal'] += $row['amounts']['tax'];
                    $row['amounts']['subtotal'] += $row['amounts']['hidden_tax'];
                    $row['amounts']['com_amount'] = $row['amounts']['subtotal'] * $row['com_percent'] / 100;
                } else {
                    $row['amounts']['com_amount'] = $row['amounts']['subtotal'] * $row['com_percent'] / 100;
                    $row['amounts']['subtotal'] += $row['amounts']['tax'];
                    $row['amounts']['subtotal'] += $row['amounts']['hidden_tax'];
                }
            } else {
                $row['amounts']['com_amount'] = $row['amounts']['subtotal'] * $row['com_percent'] / 100;
                $row['amounts']['total_payout'] += $row['amounts']['tax'];
                $row['amounts']['total_payout'] += $row['amounts']['hidden_tax'];
                $row['amounts']['total_payment'] += $row['amounts']['tax'];
                $row['amounts']['total_payment'] += $row['amounts']['hidden_tax'];
                if ($this->getVendor()->getApplyCommissionOnTax()) {
                    $taxCom = round($row['amounts']['tax'] * $row['com_percent'] / 100, 2);
                    $row['amounts']['com_amount'] += $taxCom;
                    //$order['amounts']['total_payout'] -= $taxCom;
                }
            }
        } else {
            $row['amounts']['com_amount'] = $row['amounts']['subtotal'] * $row['com_percent'] / 100;
        }

        $row['amounts']['com_amount'] = round($row['amounts']['com_amount'], 2);

        $row['amounts']['total_payout'] += $row['amounts']['subtotal'] - $row['amounts']['com_amount'] - $row['amounts']['trans_fee'] + $row['amounts']['adj_amount'];
        $row['amounts']['total_payment'] += $row['amounts']['subtotal'] + $row['amounts']['adj_amount'];

        if (isset($row['amounts']['discount']) && in_array($this->getVendor()->getStatementDiscountInPayout(),
                ['', 'include'])
        ) {
            if ($this->getVendor()->getApplyCommissionOnDiscount()) {
                $discountCom = round($row['amounts']['discount'] * $row['com_percent'] / 100, 2);
                $row['amounts']['com_amount'] -= $discountCom;
                $row['amounts']['total_payout'] += $discountCom;
            }
            $row['amounts']['total_payout'] -= $row['amounts']['discount'];
        }
        $row['amounts']['total_payment'] -= $row['amounts']['discount'];

        if (isset($row['amounts']['shipping']) && in_array($this->getVendor()->getStatementShippingInPayout(),
                ['', 'include'])
        ) {
            if ($this->getVendor()->getApplyCommissionOnShipping()) {
                $shipCom = round($row['amounts']['shipping'] * $row['po_com_percent'] / 100, 2);
                $row['amounts']['com_amount'] += $shipCom;
                $row['amounts']['total_payout'] -= $shipCom;
            }
            $row['amounts']['total_payout'] += $row['amounts']['shipping'];
        }
        $row['amounts']['total_payment'] += $row['amounts']['shipping'];
        $row['amounts']['total_invoice'] = $row['amounts']['com_amount'] + $row['amounts']['trans_fee'] + $row['amounts']['adj_amount'];

        return $row;
    }
    protected function _accumulateRow($order, $totals_amount)
    {
        foreach ($this->_getEmptyTotals() as $k => $v) {
            if (isset($order['amounts'][$k])) {
                $totals_amount[$k] += $order['amounts'][$k];
            }
        }
        return $totals_amount;
    }

    public function finishPayout()
    {
        $this->_calculateTotalDue();
        $this->_formatRows();
        $this->_formatTotals();
        $this->_compactTotals();
        return $this;
    }

    protected function _compactTotals()
    {
        $this->initTotals();
        $this->setPayoutData(\Zend_Json::encode([
            'rows' => $this->getRows(),
            'totals' => $this->getTotals(),
            'totals_amount' => $this->getTotalsAmount(),
        ]));
        $this->setTotalOrders(count($this->getRows()));
        return $this;
    }

    protected function _formatRows()
    {
        $this->initTotals();
        foreach ($this->_rows as &$row) {
            if (!empty($row['amounts'])) {
                $this->_hlp->formatAmounts($row, $row['amounts'], 'merge');
            }
        }
        unset($row);
        return $this;
    }
    protected function _formatTotals()
    {
        $this->initTotals();
        $this->_hlp->formatAmounts($this->_totals, $this->_totals_amount, 'merge');
        return $this;
    }

    protected function _calculateTotalDue()
    {
        $this->initTotals();
        $this->_totals_amount['payment_paid'] = $this->getPaymentPaid();
        $this->_totals_amount['payment_due']  = $this->_totals_amount['total_payment'] - $this->_totals_amount['payment_paid'];
        $this->_totals_amount['invoice_paid'] = $this->getInvoicePaid();
        $this->_totals_amount['invoice_due']  = $this->_totals_amount['total_invoice'] - $this->_totals_amount['invoice_paid'];
        $this->_totals_amount['total_paid'] = $this->getTotalPaid();
        $this->_totals_amount['total_due']  = $this->_totals_amount['total_payout'] - $this->_totals_amount['total_paid'];
        $this->setTotalAdjustment($this->_totals_amount['adj_amount']);
        $this->setTotalPayout($this->_totals_amount['total_payout']);
        $this->setTotalPayment($this->_totals_amount['total_payment']);
        $this->setTotalInvoice($this->_totals_amount['total_invoice']);
        $this->setTotalDue($this->_totals_amount['total_due']);
        $this->setInvoiceDue($this->_totals_amount['invoice_due']);
        $this->setPaymentDue($this->_totals_amount['payment_due']);
        return $this;
    }

    public function getTotals()
    {
        $this->initTotals();
        return $this->_totals;
    }
    public function getTotalsAmount()
    {
        $this->initTotals();
        return $this->_totals_amount;
    }
    public function getRows()
    {
        $this->initTotals();
        return $this->_rows;
    }

    protected $_totalsInit = false;
    public function initTotals()
    {
        if (!$this->_totalsInit) {
            $this->_resetRows();
            $this->_resetTotals();
            $this->_extractTotals();
            $this->_totalsInit = true;
        }
        return $this;
    }
    protected function _resetRows()
    {
        $this->_rows = [];
        return $this;
    }
    protected function _resetTotals($soft = false)
    {
        if ($soft) {
            $this->_totals = array_merge($this->_totals, $this->_getEmptyTotals(true));
            $this->_totals_amount = array_merge($this->_totals_amount, $this->_getEmptyTotals());
        } else {
            $this->_totals = $this->_getEmptyTotals(true);
            $this->_totals_amount = $this->_getEmptyTotals();
        }
        return $this;
    }

    protected function _getEmptyTotals($format=false)
    {
        return $this->ptHlp->getEmptyPayoutTotals($format);
    }

    protected $_totalsExtracted = false;
    protected function _extractTotals()
    {
        if (!$this->_totalsExtracted) {
            $payoutData = $this->getPayoutData();
            $payoutData = $this->_hlp->unserializeArr($payoutData);
            if (!empty($payoutData['totals'])) {
                $this->_totals = $payoutData['totals'];
            }
            if (!empty($payoutData['totals_amount'])) {
                $this->_totals_amount = $payoutData['totals_amount'];
            }
            if (!empty($payoutData['rows'])) {
                $this->_rows = $payoutData['rows'];
            }
            $this->_totalsExtracted = true;
        }
        return $this;
    }

    protected $_roundingDeltas = [];
    protected function _deltaRound($price, $id)
    {
        if ($price) {
            $delta = isset($this->_roundingDeltas[$id]) ? $this->_roundingDeltas[$id] : 0;
            $price += $delta;
            $this->_roundingDeltas[$id] = $price - round($price, 2);
            $price = round($price, 2);
        }
        return $price;
    }
}