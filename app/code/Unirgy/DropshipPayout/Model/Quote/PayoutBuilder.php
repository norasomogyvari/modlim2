<?php
/**
 * Unirgy LLC
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.unirgy.com/LICENSE-M1.txt
 *
 * @category   Unirgy
 * @package    Unirgy_DropshipPayout
 * @copyright  Copyright (c) 2008-2009 Unirgy LLC (http://www.unirgy.com)
 * @license    http:///www.unirgy.com/LICENSE-M1.txt
 */

namespace Unirgy\DropshipPayout\Model\Quote;

use Unirgy\Dropship\Helper\Data as DropshipHelper;
use Unirgy\DropshipPayout\Model\Quote\PayoutFactory as QuotePayoutFactory;

class PayoutBuilder extends \Magento\Framework\DataObject
{
    /**
     * @var DropshipHelper
     */
    protected $_hlp;
    /**
     * @var QuotePayoutFactory
     */
    protected $quotePayoutFactory;

    public function __construct(
        DropshipHelper $uDropshipHelper,
        QuotePayoutFactory $quotePayoutFactory,
        array $data = []
    ) {
        $this->_hlp = $uDropshipHelper;
        $this->quotePayoutFactory = $quotePayoutFactory;
        parent::__construct($data);
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @return Payout[]
     */
    public function build(\Magento\Quote\Model\Quote $quote)
    {
        $itemsByVendor = [];
        foreach ($quote->getAllItems() as $item) {
            if ($item->isDeleted() || $item->getParentItemId() || $item->getParentItem()) continue;
            $vId = $item->getUdropshipVendor();
            $itemsByVendor[$vId][] = $item;
        }
        $oldQuoteVid = $quote->getUdropshipVendor();
        foreach ($itemsByVendor as $vId => $vendorItems) {
            $quote->setUdropshipVendor($vId);
            if ($this->_hlp->isModulesActive('Unirgy_DropshipTierCommission')) {
                $this->tierComHlp()->processPo($quote, $vendorItems);
            }
            $payout = $this->_createPayout($vId);
            $this->payouts[$vId] = $payout;
            $onlySubtotal = false;
            foreach ($vendorItems as $item) {
                $payout->addItem($item, $onlySubtotal);
                $onlySubtotal = true;
            }
            $payout->finishPayout();
        }
        $quote->setUdropshipVendor($oldQuoteVid);
        return $this->payouts;
    }

    /**
     * @return \Unirgy\DropshipTierCommission\Helper\Data
     */
    protected function tierComHlp()
    {
        return $this->_hlp->getObj('Unirgy\DropshipTierCommission\Helper\Data');
    }

    /**
     * @var \Unirgy\DropshipPayout\Model\Quote\Payout[]
     */
    protected $payouts = [];
    public function getPayouts()
    {
        return $this->payouts;
    }
    public function resetPayouts()
    {
        $this->payouts = [];
        return $this;
    }

    /**
     * @param $vId
     * @return \Unirgy\DropshipPayout\Model\Quote\Payout
     */
    protected function _createPayout($vId)
    {
        $payout = $this->quotePayoutFactory->create()
            ->setVendorId($vId)->initTotals();
        return $payout;
    }

}