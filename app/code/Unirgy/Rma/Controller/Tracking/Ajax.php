<?php

namespace Unirgy\Rma\Controller\Tracking;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\Result\RawFactory;
use Unirgy\Rma\Model\RmaFactory;

class Ajax extends AbstractTracking
{
    public function execute()
    {
        if ($rma = $this->_initRma()) {
            $response = '';
            $tracks = $rma->getTracksCollection();

            $block = $this->_hlp->createObj('\Magento\Framework\View\Element\Template');
            $block->setType('core/template')
                ->setIsAnonymous(true)
                ->setTemplate('sales/order/trackinginfo.phtml');

            foreach ($tracks as $track){
                $trackingInfo = $track->getNumberDetail();
                $block->setTrackingInfo($trackingInfo);
                $response .= $block->toHtml()."\n<br />";
            }

            return $this->resultFactory->create(ResultFactory::TYPE_RAW)->setContents($response);
        }
    }
}
