<?php
/**
 * Unirgy LLC
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.unirgy.com/LICENSE-M1.txt
 *
 * @category   Unirgy
 * @package    \Unirgy\Rma
 * @copyright  Copyright (c) 2015-2016 Unirgy LLC (http://www.unirgy.com)
 * @license    http:///www.unirgy.com/LICENSE-M1.txt
 */

namespace Unirgy\Rma\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $connection = $setup->getConnection();

        if (version_compare($context->getVersion(), '3.2.9', '<')) {
            $rmaTable = $setup->getTable('urma_rma');
            $rmaCommentTable = $setup->getTable('urma_rma_comment');
            $connection->modifyColumn($rmaTable, 'created_at', ['TYPE'=>Table::TYPE_TIMESTAMP,'nullable' => false,'default' => Table::TIMESTAMP_INIT,'COMMENT'=>'created_at']);
            $connection->modifyColumn($rmaTable, 'updated_at', ['TYPE'=>Table::TYPE_TIMESTAMP,'nullable' => false,'default' => Table::TIMESTAMP_INIT_UPDATE,'COMMENT'=>'updated_at']);
            $connection->modifyColumn($rmaCommentTable, 'created_at', ['TYPE'=>Table::TYPE_TIMESTAMP,'nullable' => false,'default' => Table::TIMESTAMP_INIT,'COMMENT'=>'created_at']);
        }

        $setup->endSetup();
    }
}