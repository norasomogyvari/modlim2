<?php

namespace Unirgy\Dropship\Plugin;

class ProductTypeConfigurable
{
    protected $_hlp;
    public function __construct(
        \Unirgy\Dropship\Helper\Data $udropshipHelper
    ) {
        $this->_hlp = $udropshipHelper;
    }
    public function aroundGetUsedProductCollection(
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $subject,
        \Closure $next,
        $product
    ) {
        if ($this->_hlp->isVendorPortalAction()) {
            $cfgProdColFac = $this->_hlp->getObj('Unirgy\Dropship\Model\ResourceModel\ConfigurableProductCollectionFactory');
            $this->_hlp->setObjectPrivateProperty($subject, '_productCollectionFactory', $cfgProdColFac);
        }
        return $next($product);
    }
}