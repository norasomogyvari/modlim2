<?php

namespace Unirgy\DropshipTierShipping\Plugin;

class VendorHtmlHeader
{
    /**
     * @var \Unirgy\Dropship\Helper\Data
     */
    protected $_hlp;

    /**
     * @var \Unirgy\DropshipTierShipping\Helper\Data
     */
    protected $_tsHlp;

    public function __construct(
        \Unirgy\Dropship\Helper\Data $udropshipHelper,
        \Unirgy\DropshipTierShipping\Helper\Data $tiershipHelper
    ) {
        $this->_hlp = $udropshipHelper;
        $this->_tsHlp = $tiershipHelper;
    }
    public function afterGetVendorMenu(
        \Unirgy\Dropship\Block\Vendor\HtmlHeader $subject,
        $result
    ) {
        /** @var \Magento\Framework\Data\Collection $result */
        if ($item = $result->getItemById('tiership_v2_rates')) {
            $item->setUrl($this->_tsHlp->getVendorEditUrl());
        }
        if (!$this->_hlp->session()->getVendor()->getAllowTiershipModify()) {
            $result->removeItemByKey('tiership_v2_rates');
        }
        return $result;
    }
}