<?php

namespace Unirgy\DropshipTierCommission\Block\Adminhtml;

class VendorStatementEditTabRefundRows extends \Unirgy\Dropship\Block\Adminhtml\Vendor\Statement\Edit\Tab\RefundRows
{
    protected function _prepareColumns()
    {
        $this->addColumn('sku', [
            'header'    => __('SKU'),
            'index'     => 'sku'
        ]);
        $this->addColumn('vendor_sku', [
            'header'    => __('Vendor SKU'),
            'index'     => 'vendor_sku'
        ]);
        $this->addColumn('product', [
            'header'    => __('Product'),
            'index'     => 'product'
        ]);
        $this->addColumnsOrder('sku', 'refund_increment_id');
        $this->addColumnsOrder('sku', 'vendor_sku');
        $this->addColumnsOrder('product', 'sku');
        return parent::_prepareColumns();
    }
}