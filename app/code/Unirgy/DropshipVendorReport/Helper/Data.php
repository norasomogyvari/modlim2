<?php
/**
 * Unirgy LLC
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.unirgy.com/LICENSE-M1.txt
 *
 * @category   Unirgy
 * @package    Unirgy_DropshipVendorReport
 * @copyright  Copyright (c) 2008-2009 Unirgy LLC (http://www.unirgy.com)
 * @license    http:///www.unirgy.com/LICENSE-M1.txt
 */

namespace Unirgy\DropshipVendorReport\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\DataObject;
use Magento\Framework\Data\Collection;
use Magento\Framework\Event\ManagerInterface;
use Magento\Sales\Model\Order\Shipment;
use Unirgy\DropshipPo\Model\Po;
use Unirgy\DropshipVendorReport\Model\PoTotalFactory;
use Unirgy\DropshipVendorReport\Model\ShipmentTotalFactory;
use Unirgy\Dropship\Helper\Data as HelperData;
use Unirgy\Dropship\Model\Vendor\StatementFactory;

class Data extends AbstractHelper
{
    /**
     * @var PototalFactory
     */
    protected $_poTotalFactory;

    /**
     * @var ShipmenttotalFactory
     */
    protected $_shipmentTotalFactory;

    /**
     * @var HelperData
     */
    protected $_hlp;

    /**
     * @var StatementFactory
     */
    protected $_vendorStatementFactory;

    public function __construct(Context $context,
        PototalFactory $modelPototalFactory, 
        ShipmentTotalFactory $modelShipmenttotalFactory,
        HelperData $udropshipHelper,
        StatementFactory $vendorStatementFactory)
    {
        $this->_poTotalFactory = $modelPototalFactory;
        $this->_shipmentTotalFactory = $modelShipmenttotalFactory;
        $this->_hlp = $udropshipHelper;
        $this->_vendorStatementFactory = $vendorStatementFactory;

        parent::__construct($context);
    }

    public function updatePoTotals($posData, $poType)
    {
        $rHlp = $this->_hlp->rHlp();
        $conn = $rHlp->getConnection();
        $obj = $this->getPoTotalObj($poType);
        if (!$obj || empty($posData)) return $this;
        $table = $obj->getResource()->getMainTable();
        $fieldsData = $rHlp->myPrepareDataForTable($table, [], true);
        foreach ($posData as $poId=>$poData) {
            $poData = array_intersect_key($poData, $fieldsData);
            $poData['is_aggregated'] = 0;
            $conn->update($table, $poData, ['po_id=?'=>$poId]);
        }
    }
    public function addPoTotalsToCollection($pos)
    {
        foreach ($pos as $po) {
            $this->addPoTotals($po);
        }
    }
    public function addPoTotals($po)
    {
        if (!($poTotals = $this->getPoTotals($po))) {
            $this->initTotals($po);
            $poTotals = $this->getPoTotals($po);
        }
        $po->setPoTotals(new DataObject($poTotals));
    }
    public function getPoTable($poType)
    {
        $poTable = false;
        $rHlp = $this->_hlp->rHlp();
        if ($poType=='po' || $poType instanceof Po) {
            $poTable = $rHlp->getTable('udropship_po');
        } elseif ($poType=='shipment' || $poType instanceof Shipment) {
            $poTable = $rHlp->getTable('sales_shipment');
        }
        return $poTable;
    }
    public function getPoTotalObj($poType)
    {
        $poTotalObj = false;
        if ($poType=='po' || $poType instanceof Po) {
            $poTotalObj = $this->_poTotalFactory->create();
        } elseif ($poType=='shipment' || $poType instanceof Shipment) {
            $poTotalObj = $this->_shipmentTotalFactory->create();
        }
        return $poTotalObj;
    }
    public function getAllPoTotalsById($poIds, $poType)
    {
        $poTotalObj = $this->getPoTotalObj($poType);
        if (!isset($poTotalObj)) return [];
        return $this->_getPoTotals($poTotalObj, $poIds, true);
    }
    public function getPoTotalsById($poId, $poType)
    {
        $poTotalObj = $this->getPoTotalObj($poType);
        if (!isset($poTotalObj)) return [];
        return $this->_getPoTotals($poTotalObj, $poId);
    }
    public function getPoTotals($po)
    {
        $poTotalObj = $this->getPoTotalObj($po);
        if (!isset($poTotalObj)) return [];
        return $this->_getPoTotals($poTotalObj, $po->getId());
    }
    protected function _getPoTotals($poTotalObj, $poId, $all=false)
    {
        $rHlp = $this->_hlp->rHlp();
        $conn = $rHlp->getConnection();
        $table = $poTotalObj->getResource()->getMainTable();
        $fieldsData = $rHlp->myPrepareDataForTable($table, [], true);
        $fields = array_keys($fieldsData);
        $extraCondition = $conn->quoteInto('po_id in (?)', $poId);
        $existing = $rHlp->loadDbColumns($poTotalObj, true, $fields, $extraCondition);
        reset($existing);
        return empty($existing) ? [] : ($all ? $existing : current($existing));
    }
    public function initTotals($po, $force=false)
    {
        if (!$po instanceof Po && !$po instanceof Shipment) return $this;
        $rHlp = $this->_hlp->rHlp();
        $conn = $rHlp->getConnection();
        $obj = $this->getPoTotalObj($po);
        if (!$obj) return $this;
        $table = $obj->getResource()->getMainTable();
        $fieldsData = $rHlp->myPrepareDataForTable($table, [], true);
        $fields = array_keys($fieldsData);
        $extraCondition = $conn->quoteInto('po_id=?', $po->getId());
        $existing = $rHlp->loadDbColumns($obj, true, $fields, $extraCondition);
        $insert = [];
        if (empty($existing) || $force) {
            $hlp = $this->_hlp;
            $isTierCom = $hlp->isModuleActive('Unirgy_DropshipTierCommission');
            $vendor = $hlp->getVendor($po->getUdropshipVendor());
            $order = $po->getOrder();
            $statement = $this->_vendorStatementFactory->create()->setVendor($vendor)->setVendorId($vendor->getId());
            $totals = $statement->getEmptyTotals(true);
            $totals_amount = $statement->getEmptyTotals();
            $pos = $this->_hlp->createObj('\Magento\Framework\Data\Collection');
            $pos->addItem($po);
            $hlp->collectPoAdjustments([$po], true);
            $this->_eventManager->dispatch('udropship_vendor_statement_pos', [
                'statement'=>$statement,
                'pos'=>$pos
            ]);
            $stOrders = [];
            if ($isTierCom) {
                $onlySubtotal = false;
                foreach ($po->getAllItems() as $item) {
                    if ($item->getOrderItem()->getParentItem()) continue;
                    $stOrder = $statement->initPoItem($item, $onlySubtotal);
                    $onlySubtotal = true;
                    $this->_eventManager->dispatch('udropship_vendor_statement_item_row', [
                        'statement'=>$statement,
                        'po'=>$po,
                        'po_item'=>$item,
                        'order'=>&$stOrder
                    ]);
                    $stOrder = $statement->calculateOrder($stOrder);
                    $totals_amount = $statement->accumulateOrder($stOrder, $totals_amount);
                    if (!isset($totals_amount['total_paid'])) {
                        $totals_amount['total_paid'] = 0;
                        $totals_amount['total_due'] = 0;
                    }
                    if (!empty($stOrder['paid'])) {
                        $totals_amount['total_paid'] += @$stOrder['amounts']['total_payout'];
                    }
                    $stOrders[$item->getId()] = $stOrder;
                }
            } else {
                $stOrder = $statement->initOrder($po);
                $this->_eventManager->dispatch('udropship_vendor_statement_row', [
                    'statement'=>$statement,
                    'po'=>$po,
                    'order'=>&$stOrder
                ]);
                $stOrder = $statement->calculateOrder($stOrder);
                $totals_amount = $statement->accumulateOrder($stOrder, $totals_amount);
                if (!isset($totals_amount['total_paid'])) {
                    $totals_amount['total_paid'] = 0;
                    $totals_amount['total_due'] = 0;
                }
                if (!empty($stOrder['paid'])) {
                    $totals_amount['total_paid'] += @$stOrder['amounts']['total_payout'];
                }
            }
            $totals_amount['po_id'] = $po->getId();
            $totals_amount['udropship_vendor'] = $po->getUdropshipVendor();
            $totals_amount['udropship_status'] = $po->getUdropshipStatus();
            $totals_amount['total_due'] = @$totals_amount['total_payout']-@$totals_amount['total_paid'];
            $totals_amount['is_aggregated'] = 0;
            $insert[] = $rHlp->myPrepareDataForTable($table, $totals_amount, true);
        }
        if (!empty($insert)) {
            $rHlp->multiInsertOnDuplicate($table, $insert);
        }
        return $this;
    }
}