<?php

namespace Unirgy\DropshipVendorReport\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class PoTotal extends AbstractDb
{
    protected $_eventPrefix = 'udreport_po_total_resource';

    protected function _construct()
    {
        $this->_init('udreport_udpo_total', 'id');
    }

}

