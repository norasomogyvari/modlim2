<?php

namespace Unirgy\DropshipVendorReport\Model\ResourceModel\PoTotal;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'udreport_po_total_collection';
    protected $_eventObject = 'po_total_collection';

    protected function _construct()
    {
        $this->_init('Unirgy\DropshipVendorReport\Model\PoTotal', 'Unirgy\DropshipVendorReport\Model\ResourceModel\PoTotal');
    }

}