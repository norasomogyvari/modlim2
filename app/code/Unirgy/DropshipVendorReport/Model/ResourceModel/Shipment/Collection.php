<?php

namespace Unirgy\DropshipVendorReport\Model\ResourceModel\ShipmentTotal;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'udreport_shipment_total_collection';
    protected $_eventObject = 'shipment_total_collection';

    protected function _construct()
    {
        $this->_init('Unirgy\DropshipVendorReport\Model\ShipmentTotal', 'Unirgy\DropshipVendorReport\Model\ResourceModel\ShipmentTotal');
    }

}