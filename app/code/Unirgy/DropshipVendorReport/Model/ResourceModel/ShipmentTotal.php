<?php

namespace Unirgy\DropshipVendorReport\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class ShipmentTotal extends AbstractDb
{
    protected $_eventPrefix = 'udreport_shipment_total_resource';

    protected function _construct()
    {
        $this->_init('udreport_shipment_total', 'id');
    }

}