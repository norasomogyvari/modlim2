<?php

namespace Unirgy\DropshipVendorReport\Model\ResourceModel\Report\PoTotal\Collection;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Select;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface;
use Magento\Framework\Data\Collection\EntityFactory;
use Magento\Framework\Db\Adapter\AdapterInterface as AdapterAdapterInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Sales\Model\ResourceModel\Report;
use Magento\Sales\Model\ResourceModel\Report\Collection\AbstractCollection;
use Psr\Log\LoggerInterface;

class Order extends AbstractCollection
{

    protected $_periodFormat;

    protected $_aggregationTable = 'udreport_udpo_aggregated_order';

    protected $_selectedColumns    = [];

    protected $_vendorIdsFilter;
    protected $_poStatusIdsFilter;

    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactory $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Sales\Model\ResourceModel\Report $resource,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null
    ) {
        $resource->init($this->_aggregationTable);
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $resource, $connection);
    }

    protected function _getSelectedColumns()
    {
        $adapter = $this->getConnection();
        if ('month' == $this->_period) {
            $this->_periodFormat = $adapter->getDateFormatSql('period', '%Y-%m');
        } elseif ('year' == $this->_period) {
            $this->_periodFormat =
                $adapter->getDateExtractSql('period', AdapterAdapterInterface::INTERVAL_YEAR);
        } else {
            $this->_periodFormat = $adapter->getDateFormatSql('period', '%Y-%m-%d');
        }

        if (!$this->isTotals() && !$this->isSubTotals()) {
            $this->_selectedColumns = [
                'period'                => $this->_periodFormat,
                'pos_count'             => new \Zend_Db_Expr('sum(pos_count)'),
                'subtotal'              => new \Zend_Db_Expr(
                    "SUM(subtotal)"),
                'trans_fee' => new \Zend_Db_Expr(
                    "SUM(trans_fee)"),
                'com_amount' => new \Zend_Db_Expr(
                    "SUM(com_amount)"),
                'adj_amount' => new \Zend_Db_Expr(
                    "SUM(adj_amount)"),
                'tax' => new \Zend_Db_Expr(
                    "SUM(tax)"),
                'discount' => new \Zend_Db_Expr(
                    "SUM(discount)"),
                'shipping' => new \Zend_Db_Expr(
                    "SUM(shipping)"),
                'total_payout' => new \Zend_Db_Expr(
                    "SUM(total_payout)"),
                'total_paid' => new \Zend_Db_Expr(
                    "SUM(total_paid)"),
                'total_due' => new \Zend_Db_Expr(
                    "SUM(total_due)"),
            ];
            if (!empty($this->_vendorIdsFilter) && is_array($this->_vendorIdsFilter)) {
                $this->_selectedColumns[] = 'udropship_vendor';
            }
        }

        if ($this->isTotals()) {
            $this->_selectedColumns = $this->getAggregatedColumns();
        }

        if ($this->isSubTotals()) {
            $this->_selectedColumns =
                $this->getAggregatedColumns() +
                ['period' => $this->_periodFormat];
        }

        return $this->_selectedColumns;
    }

    protected function _initSelect()
    {
        $this->getSelect()->from(['main_table' => $this->getResource()->getMainTable()], []);
        return $this;
    }

    protected function _beforeLoad()
    {
        $this->getSelect()->columns($this->_getSelectedColumns(), 'main_table');

        if (!$this->isTotals() && !$this->isSubTotals()) {
            $this->getSelect()->group([$this->_periodFormat, 'udropship_vendor']);
        }
        if ($this->isSubTotals()) {
            $this->getSelect()->group([$this->_periodFormat]);
        }
        return parent::_beforeLoad();
    }

    public function addVendorFilter($vIds)
    {
        $this->_vendorIdsFilter = $vIds;
        return $this;
    }
    public function addPoStatusFilter($sIds)
    {
        $this->_poStatusIdsFilter = $sIds;
        return $this;
    }

    protected function _applyVendorFilter()
    {
        if (empty($this->_vendorIdsFilter) || !is_array($this->_vendorIdsFilter)) {
            $this->getSelect()->where('udropship_vendor=0');
            return $this;
        }

        $vendorFilterSqlParts = [];

        foreach ($this->_vendorIdsFilter as $vId) {
            $vendorFilterSqlParts[] = $this->getConnection()->quoteInto('udropship_vendor = ?', $vId);
        }

        if (!empty($vendorFilterSqlParts)) {
            $this->getSelect()->where(implode($vendorFilterSqlParts, ' OR '));
        }
    }

    protected function _applyPoStatusFilter()
    {
        if (empty($this->_poStatusIdsFilter) || !is_array($this->_poStatusIdsFilter)) {
            return $this;
        }

        $poStatusFilterSqlParts = [];

        foreach ($this->_poStatusIdsFilter as $vId) {
            $poStatusFilterSqlParts[] = $this->getConnection()->quoteInto('udropship_status = ?', $vId);
        }

        if (!empty($poStatusFilterSqlParts)) {
            $this->getSelect()->where(implode($poStatusFilterSqlParts, ' OR '));
        }
    }

    protected function _applyCustomFilter()
    {
        $this->_applyVendorFilter();
        $this->_applyPoStatusFilter();
        return parent::_applyCustomFilter();
    }

    protected function _applyStoresFilterToSelect(Select $select)
    {
        return $this;
    }
}
