<?php

namespace Unirgy\DropshipVendorReport\Model\ResourceModel\Report;

use Magento\Sales\Model\ResourceModel\Report\AbstractReport;
use Unirgy\DropshipVendorReport\Model\PoTotal as ModelPoTotal;

class PoTotal extends AbstractReport
{
    protected function _construct()
    {
        $this->_setResource('sales');
    }

    public function aggregate($from = null, $to = null)
    {
        $this->_aggregateByOrderCreatedAt($from, $to);
        $this->_aggregateByPoCreatedAt($from, $to);
        $this->_aggregateByStatementReadyAt($from, $to);
        $this->_setFlagData(\Unirgy\DropshipVendorReport\Model\Flag::REPORT_UDPO_FLAG_CODE);
        return $this;
    }

    protected function _aggregateByOrderCreatedAt($from, $to)
    {
        $table       = $this->getTable('udreport_udpo_aggregated_order');
        $sourceTable = $this->getTable('sales_order');
        $orderTable  = $this->getTable('sales_order');
        $poTable     = $this->getTable('udropship_po');
        $poTotalTable= $this->getTable('udreport_udpo_total');
        $adapter     = $this->getConnection();
        $adapter->beginTransaction();

        try {
            if ($from !== null || $to !== null) {
                $subSelect = $this->_getTableDateRangeSelect($orderTable, 'created_at', 'updated_at', $from, $to);
            } else {
                $subSelect = null;
            }

            $this->_clearTableByDateRange($table, $from, $to, $subSelect);
            // convert dates from UTC to current admin timezone
            $periodExpr = $adapter->getDatePartSql(
                $this->getStoreTZOffsetQuery(['order_table'=>$orderTable], 'order_table.created_at', $from, $to)
            );
            $columns = [
                'period'                => $periodExpr,
                'udropship_vendor'      => 'po_table.udropship_vendor',
                'udropship_status'      => 'po_table.udropship_status',
                'pos_count'             => new \Zend_Db_Expr('COUNT(po_table.entity_id)'),
                'subtotal'              => new \Zend_Db_Expr(
                    "SUM(po_total_table.subtotal)"),
                'trans_fee' => new \Zend_Db_Expr(
                    "SUM(po_total_table.trans_fee)"),
                'com_amount' => new \Zend_Db_Expr(
                    "SUM(po_total_table.com_amount)"),
                'adj_amount' => new \Zend_Db_Expr(
                    "SUM(po_total_table.adj_amount)"),
                'tax' => new \Zend_Db_Expr(
                    "SUM(po_total_table.tax)"),
                'discount' => new \Zend_Db_Expr(
                    "SUM(po_total_table.discount)"),
                'shipping' => new \Zend_Db_Expr(
                    "SUM(po_total_table.shipping)"),
                'total_payout' => new \Zend_Db_Expr(
                    "SUM(po_total_table.total_payout)"),
                'total_paid' => new \Zend_Db_Expr(
                    "SUM(po_total_table.total_paid)"),
                'total_due' => new \Zend_Db_Expr(
                    "SUM(po_total_table.total_due)"),
            ];

            $select = $adapter->select();
            $select->from(['order_table' => $orderTable], $columns)
                ->joinInner(
                    ['po_table' => $poTable],
                    'po_table.order_id = order_table.entity_id',
                    []
                )
                ->joinInner(
                    ['po_total_table' => $poTotalTable],
                    'po_total_table.po_id = po_table.entity_id',
                    []
                );

            if ($subSelect !== null) {
                $select->having($this->_makeConditionFromDateRangeSelect($subSelect, 'period'));
            }

            $select->group([
                $periodExpr,
                'udropship_vendor',
                'udropship_status',
            ]);

            $select->having('pos_count > 0');

            $insertQuery   = $adapter->insertFromSelect($select, $table, array_keys($columns));
            $adapter->query($insertQuery);

            $select->reset();

            $columns = [
                'period'                => 'period',
                'udropship_vendor'      => new \Zend_Db_Expr('0'),
                'udropship_status'      => 'udropship_status',
                'pos_count'             => new \Zend_Db_Expr('sum(pos_count)'),
                'subtotal'              => new \Zend_Db_Expr(
                    "SUM(subtotal)"),
                'trans_fee' => new \Zend_Db_Expr(
                    "SUM(trans_fee)"),
                'com_amount' => new \Zend_Db_Expr(
                    "SUM(com_amount)"),
                'adj_amount' => new \Zend_Db_Expr(
                    "SUM(adj_amount)"),
                'tax' => new \Zend_Db_Expr(
                    "SUM(tax)"),
                'discount' => new \Zend_Db_Expr(
                    "SUM(discount)"),
                'shipping' => new \Zend_Db_Expr(
                    "SUM(shipping)"),
                'total_payout' => new \Zend_Db_Expr(
                    "SUM(total_payout)"),
                'total_paid' => new \Zend_Db_Expr(
                    "SUM(total_paid)"),
                'total_due' => new \Zend_Db_Expr(
                    "SUM(total_due)"),
            ];

            $select
                ->from($table, $columns)
                ->where('udropship_vendor != ?', 0);

            if ($subSelect !== null) {
                $select->where($this->_makeConditionFromDateRangeSelect($subSelect, 'period'));
            }

            $select->group([
                'period',
                'udropship_status',
            ]);

            $insertQuery = $adapter->insertFromSelect($select, $table, array_keys($columns));
            $adapter->query($insertQuery);
        } catch (\Exception $e) {
            $adapter->rollBack();
            throw $e;
        }

        $adapter->commit();
        return $this;
    }

    protected function _aggregateByPoCreatedAt($from, $to)
    {
        $table       = $this->getTable('udreport_udpo_aggregated');
        $sourceTable = $this->getTable('udropship_po');
        $orderTable  = $this->getTable('sales_order');
        $poTable     = $this->getTable('udropship_po');
        $poTotalTable= $this->getTable('udreport_udpo_total');
        $adapter     = $this->getConnection();
        $adapter->beginTransaction();

        try {
            if ($from !== null || $to !== null) {
                $subSelect = $this->_getTableDateRangeSelect($poTable, 'created_at', 'updated_at', $from, $to);
            } else {
                $subSelect = null;
            }

            $this->_clearTableByDateRange($table, $from, $to, $subSelect);
            // convert dates from UTC to current admin timezone
            $periodExpr = $adapter->getDatePartSql(
                $this->getStoreTZOffsetQuery(['po_table'=>$poTable], 'po_table.created_at', $from, $to)
            );
            $columns = [
                'period'                => $periodExpr,
                'udropship_vendor'      => 'po_table.udropship_vendor',
                'udropship_status'      => 'po_table.udropship_status',
                'pos_count'             => new \Zend_Db_Expr('COUNT(po_table.entity_id)'),
                'subtotal'              => new \Zend_Db_Expr(
                    "SUM(po_total_table.subtotal)"),
                'trans_fee' => new \Zend_Db_Expr(
                    "SUM(po_total_table.trans_fee)"),
                'com_amount' => new \Zend_Db_Expr(
                    "SUM(po_total_table.com_amount)"),
                'adj_amount' => new \Zend_Db_Expr(
                    "SUM(po_total_table.adj_amount)"),
                'tax' => new \Zend_Db_Expr(
                    "SUM(po_total_table.tax)"),
                'discount' => new \Zend_Db_Expr(
                    "SUM(po_total_table.discount)"),
                'shipping' => new \Zend_Db_Expr(
                    "SUM(po_total_table.shipping)"),
                'total_payout' => new \Zend_Db_Expr(
                    "SUM(po_total_table.total_payout)"),
                'total_paid' => new \Zend_Db_Expr(
                    "SUM(po_total_table.total_paid)"),
                'total_due' => new \Zend_Db_Expr(
                    "SUM(po_total_table.total_due)"),
            ];

            $select = $adapter->select();
            $select->from(['order_table' => $orderTable], $columns)
                ->joinInner(
                    ['po_table' => $poTable],
                    'po_table.order_id = order_table.entity_id',
                    []
                )
                ->joinInner(
                    ['po_total_table' => $poTotalTable],
                    'po_total_table.po_id = po_table.entity_id',
                    []
                );

            if ($subSelect !== null) {
                $select->having($this->_makeConditionFromDateRangeSelect($subSelect, 'period'));
            }

            $select->group([
                $periodExpr,
                'udropship_vendor',
                'udropship_status',
            ]);

            $select->having('pos_count > 0');

            $insertQuery   = $adapter->insertFromSelect($select, $table, array_keys($columns));
            $adapter->query($insertQuery);

            $select->reset();

            $columns = [
                'period'                => 'period',
                'udropship_vendor'      => new \Zend_Db_Expr('0'),
                'udropship_status'      => 'udropship_status',
                'pos_count'             => new \Zend_Db_Expr('sum(pos_count)'),
                'subtotal'              => new \Zend_Db_Expr(
                    "SUM(subtotal)"),
                'trans_fee' => new \Zend_Db_Expr(
                    "SUM(trans_fee)"),
                'com_amount' => new \Zend_Db_Expr(
                    "SUM(com_amount)"),
                'adj_amount' => new \Zend_Db_Expr(
                    "SUM(adj_amount)"),
                'tax' => new \Zend_Db_Expr(
                    "SUM(tax)"),
                'discount' => new \Zend_Db_Expr(
                    "SUM(discount)"),
                'shipping' => new \Zend_Db_Expr(
                    "SUM(shipping)"),
                'total_payout' => new \Zend_Db_Expr(
                    "SUM(total_payout)"),
                'total_paid' => new \Zend_Db_Expr(
                    "SUM(total_paid)"),
                'total_due' => new \Zend_Db_Expr(
                    "SUM(total_due)"),
            ];

            $select
                ->from($table, $columns)
                ->where('udropship_vendor != ?', 0);

            if ($subSelect !== null) {
                $select->where($this->_makeConditionFromDateRangeSelect($subSelect, 'period'));
            }

            $select->group([
                'period',
                'udropship_status',
            ]);

            $insertQuery = $adapter->insertFromSelect($select, $table, array_keys($columns));
            $adapter->query($insertQuery);
        } catch (\Exception $e) {
            $adapter->rollBack();
            throw $e;
        }

        $adapter->commit();
        return $this;
    }

    protected function _aggregateByStatementReadyAt($from, $to)
    {
        $table       = $this->getTable('udreport_udpo_aggregated_statement');
        $sourceTable = $this->getTable('udropship_po');
        $orderTable  = $this->getTable('sales_order');
        $poTable     = $this->getTable('udropship_po');
        $poTotalTable= $this->getTable('udreport_udpo_total');
        $adapter     = $this->getConnection();
        $adapter->beginTransaction();

        try {
            if ($from !== null || $to !== null) {
                $subSelect = $this->_getTableDateRangeSelect($poTable, 'statement_date', 'updated_at', $from, $to);
            } else {
                $subSelect = null;
            }

            $this->_clearTableByDateRange($table, $from, $to, $subSelect);
            // convert dates from UTC to current admin timezone
            $periodExpr = $adapter->getDatePartSql(
                $this->getStoreTZOffsetQuery(['po_table'=>$poTable], 'po_table.statement_date', $from, $to)
            );
            $columns = [
                'period'                => $periodExpr,
                'udropship_vendor'      => 'po_table.udropship_vendor',
                'udropship_status'      => 'po_table.udropship_status',
                'pos_count'             => new \Zend_Db_Expr('COUNT(po_table.entity_id)'),
                'subtotal'              => new \Zend_Db_Expr(
                    "SUM(po_total_table.subtotal)"),
                'trans_fee' => new \Zend_Db_Expr(
                    "SUM(po_total_table.trans_fee)"),
                'com_amount' => new \Zend_Db_Expr(
                    "SUM(po_total_table.com_amount)"),
                'adj_amount' => new \Zend_Db_Expr(
                    "SUM(po_total_table.adj_amount)"),
                'tax' => new \Zend_Db_Expr(
                    "SUM(po_total_table.tax)"),
                'discount' => new \Zend_Db_Expr(
                    "SUM(po_total_table.discount)"),
                'shipping' => new \Zend_Db_Expr(
                    "SUM(po_total_table.shipping)"),
                'total_payout' => new \Zend_Db_Expr(
                    "SUM(po_total_table.total_payout)"),
                'total_paid' => new \Zend_Db_Expr(
                    "SUM(po_total_table.total_paid)"),
                'total_due' => new \Zend_Db_Expr(
                    "SUM(po_total_table.total_due)"),
            ];

            $select = $adapter->select();
            $select->from(['order_table' => $orderTable], $columns)
                ->joinInner(
                    ['po_table' => $poTable],
                    'po_table.order_id = order_table.entity_id',
                    []
                )
                ->joinInner(
                    ['po_total_table' => $poTotalTable],
                    'po_total_table.po_id = po_table.entity_id',
                    []
                );

            if ($subSelect !== null) {
                $select->having($this->_makeConditionFromDateRangeSelect($subSelect, 'period'));
            }

            $select->group([
                $periodExpr,
                'udropship_vendor',
                'udropship_status',
            ]);

            $select->having('pos_count > 0');

            $insertQuery   = $adapter->insertFromSelect($select, $table, array_keys($columns));
            $adapter->query($insertQuery);

            $select->reset();

            $columns = [
                'period'                => 'period',
                'udropship_vendor'      => new \Zend_Db_Expr('0'),
                'udropship_status'      => 'udropship_status',
                'pos_count'             => new \Zend_Db_Expr('sum(pos_count)'),
                'subtotal'              => new \Zend_Db_Expr(
                    "SUM(subtotal)"),
                'trans_fee' => new \Zend_Db_Expr(
                    "SUM(trans_fee)"),
                'com_amount' => new \Zend_Db_Expr(
                    "SUM(com_amount)"),
                'adj_amount' => new \Zend_Db_Expr(
                    "SUM(adj_amount)"),
                'tax' => new \Zend_Db_Expr(
                    "SUM(tax)"),
                'discount' => new \Zend_Db_Expr(
                    "SUM(discount)"),
                'shipping' => new \Zend_Db_Expr(
                    "SUM(shipping)"),
                'total_payout' => new \Zend_Db_Expr(
                    "SUM(total_payout)"),
                'total_paid' => new \Zend_Db_Expr(
                    "SUM(total_paid)"),
                'total_due' => new \Zend_Db_Expr(
                    "SUM(total_due)"),
            ];

            $select
                ->from($table, $columns)
                ->where('udropship_vendor != ?', 0);

            if ($subSelect !== null) {
                $select->where($this->_makeConditionFromDateRangeSelect($subSelect, 'period'));
            }

            $select->group([
                'period',
                'udropship_status',
            ]);

            $insertQuery = $adapter->insertFromSelect($select, $table, array_keys($columns));
            $adapter->query($insertQuery);
        } catch (\Exception $e) {
            $adapter->rollBack();
            throw $e;
        }

        $adapter->commit();
        return $this;
    }
}
