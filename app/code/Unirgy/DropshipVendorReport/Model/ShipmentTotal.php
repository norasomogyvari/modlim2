<?php

namespace Unirgy\DropshipVendorReport\Model;

use Magento\Framework\Model\AbstractModel;

class ShipmentTotal extends AbstractModel
{
    const REPORT_FLAG_CODE    = 'udreport_shipment_total_aggregated';
    protected $_eventPrefix = 'udreport_shipment_total';
    protected $_eventObject = 'shipment_total';

    protected function _construct()
    {
        $this->_init('Unirgy\DropshipVendorReport\Model\ResourceModel\ShipmentTotal');
    }
}