<?php

namespace Unirgy\DropshipVendorReport\Model;

use Magento\Framework\Model\AbstractModel;

class PoTotal extends AbstractModel
{
    const REPORT_FLAG_CODE    = 'udreport_po_total_aggregated';
    protected $_eventPrefix = 'udreport_po_total';
    protected $_eventObject = 'po_total';

    protected function _construct()
    {
        $this->_init('Unirgy\DropshipVendorReport\Model\ResourceModel\PoTotal');
    }
}