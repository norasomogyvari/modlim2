<?php

namespace Unirgy\DropshipVendorReport\Model;

class Flag extends \Magento\Framework\Flag
{
    const REPORT_UDPO_FLAG_CODE = 'udreport_udpo_aggregated';

    const REPORT_SHIPMENT_FLAG_CODE = 'udreport_shipment_aggregated';

    public function setReportFlagCode($code)
    {
        $this->_flagCode = $code;
        return $this;
    }
}
