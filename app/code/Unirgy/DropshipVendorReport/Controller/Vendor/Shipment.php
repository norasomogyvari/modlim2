<?php

namespace Unirgy\DropshipVendorReport\Controller\Vendor;

class Shipment extends AbstractVendor
{
    public function execute()
    {
        $this->_renderPage(null, 'udrshipment');
    }
}
