<?php

namespace Unirgy\DropshipVendorReport\Controller\Adminhtml\Index;

use Magento\Framework\View\LayoutFactory;

class ExportCsv extends AbstractIndex
{
    /**
     * @var LayoutFactory
     */
    protected $_viewLayoutFactory;

    public function __construct(LayoutFactory $viewLayoutFactory)
    {
        $this->_viewLayoutFactory = $viewLayoutFactory;

    }

    public function execute()
    {
        $fileName   = 'shipment.csv';
        $grid       = $this->_viewLayoutFactory->create()->createBlock('');
        $this->_initReportAction($grid);
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }
}
