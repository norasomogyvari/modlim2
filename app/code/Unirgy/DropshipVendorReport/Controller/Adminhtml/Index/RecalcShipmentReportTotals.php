<?php

namespace Unirgy\DropshipVendorReport\Controller\Adminhtml\Index;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\Order\Pdf\Shipment;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory;

class RecalcShipmentReportTotals extends \Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction
{
    const ADMIN_RESOURCE = 'Unirgy_DropshipVendorReport::shiptotals';

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context, $filter);
    }
    protected function massAction(AbstractCollection $collection)
    {
        $countRecalTotals = 0;
        foreach ($collection as $shipment) {
            $this->_rptHlp()->initTotals(
                $shipment,
                true
            );
            $countRecalTotals++;
        }
        $countNonRecalTotals = $collection->count() - $countRecalTotals;

        if ($countNonRecalTotals && $countRecalTotals) {
            $this->messageManager->addError(__('%1 shipments(s) cannot be recalculated.', $countNonRecalTotals));
        } elseif ($countNonRecalTotals) {
            $this->messageManager->addError(__('You cannot recalculate the shipment(s).'));
        }

        if ($countRecalTotals) {
            $this->messageManager->addSuccess(__('We recalculated %1 shipment(s).', $countRecalTotals));
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('sales/shipment/');
        return $resultRedirect;
    }

    /**
     * @return \Unirgy\DropshipVendorReport\Helper\Data
     */
    protected function _rptHlp()
    {
        return ObjectManager::getInstance()->get('Unirgy\DropshipVendorReport\Helper\Data');
    }
}
