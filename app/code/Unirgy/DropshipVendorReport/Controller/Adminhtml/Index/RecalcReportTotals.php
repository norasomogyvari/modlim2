<?php

namespace Unirgy\DropshipVendorReport\Controller\Adminhtml\Index;

use Unirgy\DropshipVendorReport\Helper\Data as HelperData;

class RecalcReportTotals extends AbstractIndex
{
    /**
     * @var HelperData
     */
    protected $_rptHlp;

    protected $_hlp;

    public function __construct(
        \Unirgy\Dropship\Helper\Data $udropshipHelper,
        \Unirgy\DropshipVendorReport\Helper\Data $helperData,
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Stdlib\DateTime\Filter\Date $dateFilter,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
    )
    {
        parent::__construct($context, $fileFactory, $dateFilter, $timezone);
        $this->_hlp = $udropshipHelper;
        $this->_rptHlp = $helperData;

    }

    protected function _poFactory()
    {
        return $this->_hlp->getObj('Unirgy\DropshipPo\Model\PoFactory');
    }

    public function execute()
    {
        $poIds = (array)$this->getRequest()->getParam('udpo_ids');
        if (empty($poIds)) {
            $this->_redirect('adminhtml');
            return;
        }

        try {
            foreach ($poIds as $modelId) {
                $this->_rptHlp->initTotals(
                    $this->_poFactory()->create()->load($modelId),
                    true
                );
            }
            $modelIdsCnt = count($poIds);
            $this->messageManager->addSuccess(
                __('Total of %1 record(s) were successfully updated', $modelIdsCnt)
            );
        }
        catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        }
        catch (\Exception $e) {
            $this->messageManager->addException($e, __('There was an error while recalculating totals'));
        }

        if (!empty($poIds)) {
            $this->_redirect('udpo/po/');
        } else {
            $this->_redirect('adminhtml');
        }
    }
}
