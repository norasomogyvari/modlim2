<?php


namespace Unirgy\DropshipVendorReport\Controller\Adminhtml\Index;

abstract class AbstractIndex extends \Magento\Reports\Controller\Adminhtml\Report\AbstractReport
{
    public function _initAction()
    {
        parent::_initAction();
        $this->_addBreadcrumb(__('Dropship'), __('Dropship'));
        return $this;
    }
    protected function _isAllowed()
    {
        switch ($this->getRequest()->getActionName()) {
            case 'advanced':
            case 'exportAdvancedCsv':
            case 'exportAdvancedExcel':
                return $this->_authorization->isAllowed('Unirgy_DropshipVendorReport::pototals');
                break;
            case 'index':
            case 'exportCsv':
            case 'exportExcel':
                return $this->_authorization->isAllowed('Unirgy_DropshipVendorReport::shiptotals');
                break;
            default:
                return $this->_authorization->isAllowed('Unirgy_DropshipVendorReport::udropship');
        }
        return false;
    }
}
