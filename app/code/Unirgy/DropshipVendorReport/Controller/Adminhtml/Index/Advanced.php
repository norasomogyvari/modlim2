<?php

namespace Unirgy\DropshipVendorReport\Controller\Adminhtml\Index;

use Unirgy\DropshipVendorReport\Model\Flag;
use Magento\Framework\App\ObjectManager;

class Advanced extends AbstractIndex
{
    public function execute()
    {
        if ($this->_udHlp()->isUdpoActive()) {
            $this->_showLastExecutionTime(Flag::REPORT_UDPO_FLAG_CODE, 'udreport_po_total');

            $this->_initAction()->_setActiveMenu(
                'Unirgy_DropshipVendorReport::pototals'
            )->_addBreadcrumb(
                __('PO Totals'),
                __('PO Totals')
            );
            $this->_view->getPage()->getConfig()->getTitle()->prepend(__('PO Totals Report'));

            $gridBlock = $this->_view->getLayout()->getBlock('adminhtml_poTotal.grid');
            $filterFormBlock = $this->_view->getLayout()->getBlock('grid.filter.form');

            $this->_initReportAction([$gridBlock, $filterFormBlock]);

            $this->_view->renderLayout();
        } else {
            $this->_redirect('udreport/index');
        }
    }

    /**
     * @return \Unirgy\Dropship\Helper\Data
     */
    protected function _udHlp()
    {
        return ObjectManager::getInstance()->get('Unirgy\Dropship\Helper\Data');
    }

}
