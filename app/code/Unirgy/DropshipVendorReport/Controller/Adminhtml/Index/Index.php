<?php

namespace Unirgy\DropshipVendorReport\Controller\Adminhtml\Index;

use Magento\Framework\View\LayoutFactory;
use Unirgy\DropshipVendorReport\Model\ShipmentTotal;
use Unirgy\DropshipVendorReport\Model\Flag;

class Index extends AbstractIndex
{
    public function execute()
    {
        $this->_showLastExecutionTime(Flag::REPORT_SHIPMENT_FLAG_CODE, 'udreport_shipment_total');

        $this->_initAction()->_setActiveMenu(
            'Unirgy_DropshipVendorReport::shiptotals'
        )->_addBreadcrumb(
            __('Shipment Totals'),
            __('Shipment Totals')
        );
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Shipment Totals Report'));

        $gridBlock = $this->_view->getLayout()->getBlock('adminhtml_shipmentTotal.grid');
        $filterFormBlock = $this->_view->getLayout()->getBlock('grid.filter.form');

        $this->_initReportAction([$gridBlock, $filterFormBlock]);

        $this->_view->renderLayout();
    }
}
