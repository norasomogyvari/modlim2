<?php

namespace Unirgy\DropshipVendorReport\Plugin;

class ReportsRefreshCollection
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_localeDate;

    /**
     * @var \Magento\Reports\Model\FlagFactory
     */
    protected $_reportsFlagFactory;

    /**
     * @param \Magento\Framework\Data\Collection\EntityFactory $entityFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Reports\Model\FlagFactory $reportsFlagFactory
     */
    public function __construct(
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Reports\Model\FlagFactory $reportsFlagFactory,
        \Magento\Framework\Module\Manager $moduleManager
    ) {
        $this->_localeDate = $localeDate;
        $this->_reportsFlagFactory = $reportsFlagFactory;
        $this->_moduleManager = $moduleManager;
    }
    public function aroundLoadData(\Magento\Reports\Model\ResourceModel\Refresh\Collection $subject, \Closure $next, $printQuery, $logQuery)
    {
        $next($printQuery, $logQuery);
        $data = [
            [
                'id' => 'udreport_shipment_total',
                'report' => __('Shipment Totals'),
                'comment' => __('Shipment Totals Report'),
                'updated_at' => $this->_getUpdatedAt(\Unirgy\DropshipVendorReport\Model\Flag::REPORT_SHIPMENT_FLAG_CODE),
            ],
        ];
        if ((bool)$this->_moduleManager->isEnabled('Unirgy_DropshipPo')) {
            $data[] = [
                'id' => 'udreport_po_total',
                'report' => __('PO Totals'),
                'comment' => __('PO Totals Report'),
                'updated_at' => $this->_getUpdatedAt(\Unirgy\DropshipVendorReport\Model\Flag::REPORT_UDPO_FLAG_CODE),
            ];
        }
        foreach ($data as $value) {
            $item = new \Magento\Framework\DataObject();
            $item->setData($value);
            try {
                $subject->addItem($item);
            } catch (\Exception $e) {}
        }
        return $subject;
    }
    protected function _getUpdatedAt($reportCode)
    {
        $flag = $this->_reportsFlagFactory->create()->setReportFlagCode($reportCode)->loadSelf();
        return $flag->hasData() ? $flag->getLastUpdate() : '';
    }
}