<?php

namespace Unirgy\DropshipVendorReport\Block\Vendor\Shipment;

use Magento\Reports\Block\Adminhtml\Grid\AbstractGrid;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\DataObject;
use Unirgy\DropshipVendorReport\Block\Adminhtml\ShipmentTotal\Grid as ShipmentTotalGrid;

class Grid extends ShipmentTotalGrid
{
    /**
     * @return \Unirgy\Dropship\Helper\Data
     */
    protected function _hlp()
    {
        return ObjectManager::getInstance()->get('Unirgy\Dropship\Helper\Data');
    }

    protected function _construct()
    {
        parent::_construct();

        $this->setCountTotals(true);
        $this->setCountSubTotals(true);
    }

    protected function _prepareColumns()
    {
        parent::_prepareColumns();
        if ('po' == ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendor()->getStatementPoType()) {
            $this->removeColumn('total_paid');
            $this->removeColumn('total_due');
        }
        return $this;
    }
    protected function _addCustomFilter($collection, $filterData)
    {
        $collection->addVendorFilter([ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId()]);
        if ($filterData->getShowPoStatuses()) {
            $statusList = $filterData->getData('po_statuses');
            if (isset($statusList[0])) {
                $statusIds = explode(',', $statusList[0]);
                $collection->addPoStatusFilter($statusIds);
            }
        }

        return AbstractGrid::_addCustomFilter($filterData, $collection);
    }
    protected $_filterData;
    public function getFilterData()
    {
        if ($this->_filterData === null) {
            $filter=(array)$this->getRequest()->getParam('filter');
            $filterInput = new \Zend_Filter_LocalizedToNormalized([
                'date_format' => $this->_hlp()->getDateFormat(\IntlDateFormatter::SHORT)
            ]);
            $filterInternal = new \Zend_Filter_NormalizedToLocalized([
                'date_format' => \Magento\Framework\Stdlib\DateTime::DATE_INTERNAL_FORMAT
            ]);

            foreach (['from','to'] as $dateField) {
                if (array_key_exists($dateField, $filter) && !empty($dateField)) {
                    $filter[$dateField] = $filterInput->filter($filter[$dateField]);
                    $filter[$dateField] = $filterInternal->filter($filter[$dateField]);
                }
            }
            $this->_filterData = new DataObject($filter);
        }
        return $this->_filterData;
    }
    public function getShowPoStatusJs()
    {
        return $this->getDependSelectJs('filter-show_po_statuses', ['depend_fields' => ['filter-po_statuses'=>'1']]);
    }
    public function getDependSelectJs($htmlId, $config)
    {
        $html = '';
        $fc = (array)@$config;
        if (isset($fc['depend_fields']) && ($dependFields = (array)$fc['depend_fields'])
            || isset($fc['hide_depend_fields']) && ($hideDependFields = (array)$fc['hide_depend_fields'])
        ) {
            if (!empty($dependFields)) {
                foreach ($dependFields as &$dv) {
                    $dv = $dv!='' ? explode(',', $dv) : [''];
                }
                unset($dv);
                $dfJson = $this->_hlp()->serialize($dependFields);
            } else {
                $dfJson = '{}';
            }
            if (!empty($hideDependFields)) {
                foreach ($hideDependFields as &$dv) {
                    $dv = $dv!='' ? explode(',', $dv) : [''];
                }
                unset($dv);
                $hideDfJson = $this->_hlp()->serialize($hideDependFields);
            } else {
                $hideDfJson = '{}';
            }
            $html .=<<<EOT
<script type="text/javascript">
require(["jquery", "prototype"], function(jQuery) {

document.observe("dom:loaded", function() {
	var df = \$H($dfJson);
	var hideDf = \$H($hideDfJson);
	var enableDisable = function (pair, flag) {
        if ($(pair.key) && (trElem = $(pair.key).up("li"))) {
            if (flag == (\$A(pair.value).indexOf($('{$htmlId}').value) != -1)) {
                trElem.show()
                trElem.select('select').invoke('enable')
                trElem.select('input').invoke('enable')
                trElem.select('textarea').invoke('enable')
            } else {
                trElem.hide()
                trElem.select('select').invoke('disable')
                trElem.select('input').invoke('disable')
                trElem.select('textarea').invoke('disable')
            }
        }
    }
	var syncDependFields = function() {
		df.each(function(pair){
			enableDisable(pair, true);
		});
		hideDf.each(function(pair){
			enableDisable(pair, false);
		});
	}
    $('{$htmlId}').observe('change', syncDependFields)
    syncDependFields()
})

});
</script>
EOT;
        }
        return $html;
    }
}