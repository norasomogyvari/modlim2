<?php

namespace Unirgy\DropshipVendorReport\Block\Adminhtml\ShipmentTotal;

use Magento\Framework\Data\Form\Element\Fieldset;
use Magento\Reports\Block\Adminhtml\Filter\Form;
use Magento\Framework\App\ObjectManager;

class FilterForm extends Form
{
    /**
     * @return \Unirgy\Dropship\Helper\Data
     */
    protected function _hlp()
    {
        return ObjectManager::getInstance()->get('Unirgy\Dropship\Helper\Data');
    }

    protected function _prepareForm()
    {
        parent::_prepareForm();
        $form = $this->getForm();
        $htmlIdPrefix = $form->getHtmlIdPrefix();
        /** @var Fieldset $fieldset */
        $fieldset = $this->getForm()->getElement('base_fieldset');

        if (is_object($fieldset) && $fieldset instanceof Fieldset) {

            $fieldset->addField('show_po_statuses', 'select', [
                'name'      => 'show_po_statuses',
                'label'     => __('Filter Shipment Status'),
                'options'   => [
                    '0' => __('Any'),
                    '1' => __('Specified'),
                ],
                'note'      => __('Applies to Any of the Specified Order Statuses'),
            ], 'show_vendors');

            $fieldset->addField('po_statuses', 'multiselect', [
                'name'      => 'po_statuses',
                'label'     => __('Shipment Status'),
                'values'    => $this->_hlp()->src()->setPath('shipment_statuses')->toOptionArray(),
                'display'   => 'none'
            ], 'show_po_statuses');

            $fieldset->addField('show_vendors', 'select', [
                'name'      => 'show_vendors',
                'label'     => __('Filter Vendors'),
                'options'   => [
                    '0' => __('Any'),
                    '1' => __('Specified'),
                ],
                'note'      => __('Applies to Any of the Specified Vendors'),
            ], 'to');

            $fieldset->addField('vendors', 'multiselect', [
                'label'     => __('Vendors'),
                'name'      => 'vendors',
                'values'    => $this->_hlp()->src()->setPath('vendors')->toOptionArray(),
                'display'   => 'none'
            ], 'show_vendors');

            // define field dependencies
            if ($this->getFieldVisibility('show_po_statuses') && $this->getFieldVisibility('po_statuses')) {
                $this->setChild('form_after', $this->getLayout()->createBlock('Magento\Backend\Block\Widget\Form\Element\Dependence')
                        ->addFieldMap("{$htmlIdPrefix}show_po_statuses", 'show_po_statuses')
                        ->addFieldMap("{$htmlIdPrefix}po_statuses", 'po_statuses')
                        ->addFieldMap("{$htmlIdPrefix}show_vendors", 'show_vendors')
                        ->addFieldMap("{$htmlIdPrefix}vendors", 'vendors')
                        ->addFieldDependence('po_statuses', 'show_po_statuses', '1')
                        ->addFieldDependence('vendors', 'show_vendors', '1')
                );
            }
        }

        return $this;
    }
}
