<?php

namespace Unirgy\DropshipVendorReport\Block\Adminhtml\ShipmentTotal;

use Magento\Reports\Block\Adminhtml\Grid\AbstractGrid;
use Magento\Framework\App\ObjectManager;

class Grid extends AbstractGrid
{
    protected $_columnGroupBy = 'period';

    /**
     * @return \Unirgy\Dropship\Helper\Data
     */
    protected function _hlp()
    {
        return ObjectManager::getInstance()->get('Unirgy\Dropship\Helper\Data');
    }

    public function getResourceCollectionName()
    {
        if (($this->getFilterData()->getData('report_type') == 'created_at_order')) {
            return 'Unirgy\DropshipVendorReport\Model\ResourceModel\Report\ShipmentTotal\Collection\Order';
        } elseif (($this->getFilterData()->getData('report_type') == 'created_at_shipment')) {
            return 'Unirgy\DropshipVendorReport\Model\ResourceModel\Report\ShipmentTotal\Collection\Po';
        } else {
            return 'Unirgy\DropshipVendorReport\Model\ResourceModel\Report\ShipmentTotal\Collection\Statement';
        }
    }

    protected function _prepareColumns()
    {
        $this->addColumn('period', [
            'header'            => __('Period'),
            'index'             => 'period',
            'width'             => 100,
            'sortable'          => false,
            'period_type'       => $this->getPeriodType(),
            'renderer'          => 'Magento\Reports\Block\Adminhtml\Sales\Grid\Column\Renderer\Date',
            'totals_label'      => __('Total'),
            'subtotals_label'   => __('Subtotal'),
            'html_decorators' => ['nobr'],
        ]);

        if ($this->getFilterData()->getShowVendors()) {
            $this->setCountSubTotals(true);
            $this->addColumn('udropship_vendor', [
                'header'    => __('Vendor'),
                'sortable'  => false,
                'index'     => 'udropship_vendor',
                'type' => 'options',
                'options' => $this->_hlp()->src()->setPath('vendors')->toOptionHash(),
            ]);
        } else {
            $this->setCountSubTotals(false);
        }

        $this->addColumn('pos_count', [
            'header'    => __('Number of POs'),
            'sortable'  => false,
            'index'     => 'pos_count',
            'total'     => 'sum',
            'type'      => 'number'
        ]);

        $this->addColumn('subtotal', [
            'header'        => __('Subtotal'),
            'sortable'      => false,
            'type'          => 'price',
            'currency_code' => $this->_hlp()->getScopeConfig('currency/options/base'),
            'total'         => 'sum',
            'index'         => 'subtotal',
        ]);
        $this->addColumn('trans_fee', [
            'header'        => __('Transaction Fee'),
            'sortable'      => false,
            'type'          => 'price',
            'currency_code' => $this->_hlp()->getScopeConfig('currency/options/base'),
            'total'         => 'sum',
            'index'         => 'trans_fee',
        ]);
        $this->addColumn('com_amount', [
            'header'        => __('Commission Amount'),
            'sortable'      => false,
            'type'          => 'price',
            'currency_code' => $this->_hlp()->getScopeConfig('currency/options/base'),
            'total'         => 'sum',
            'index'         => 'com_amount',
        ]);
        $this->addColumn('adj_amount', [
            'header'        => __('Adjustment'),
            'sortable'      => false,
            'type'          => 'price',
            'currency_code' => $this->_hlp()->getScopeConfig('currency/options/base'),
            'total'         => 'sum',
            'index'         => 'adj_amount',
        ]);
        $this->addColumn('tax', [
            'header'        => __('Tax Amount'),
            'sortable'      => false,
            'type'          => 'price',
            'currency_code' => $this->_hlp()->getScopeConfig('currency/options/base'),
            'total'         => 'sum',
            'index'         => 'tax',
        ]);
        $this->addColumn('discount', [
            'header'        => __('Discount'),
            'sortable'      => false,
            'type'          => 'price',
            'currency_code' => $this->_hlp()->getScopeConfig('currency/options/base'),
            'total'         => 'sum',
            'index'         => 'discount',
        ]);
        $this->addColumn('shipping', [
            'header'        => __('Shipping'),
            'sortable'      => false,
            'type'          => 'price',
            'currency_code' => $this->_hlp()->getScopeConfig('currency/options/base'),
            'total'         => 'sum',
            'index'         => 'shipping',
        ]);
        $this->addColumn('total_payout', [
            'header'        => __('Total Payout'),
            'sortable'      => false,
            'type'          => 'price',
            'currency_code' => $this->_hlp()->getScopeConfig('currency/options/base'),
            'total'         => 'sum',
            'index'         => 'total_payout',
        ]);
        if ($this->_hlp()->isModuleActive('Unirgy_DropshipPayout')) {
        $this->addColumn('total_paid', [
            'header'        => __('Total Paid'),
            'sortable'      => false,
            'type'          => 'price',
            'currency_code' => $this->_hlp()->getScopeConfig('currency/options/base'),
            'total'         => 'sum',
            'index'         => 'total_paid',
        ]);
        $this->addColumn('total_due', [
            'header'        => __('Total Due'),
            'sortable'      => false,
            'type'          => 'price',
            'currency_code' => $this->_hlp()->getScopeConfig('currency/options/base'),
            'total'         => 'sum',
            'index'         => 'total_due',
        ]);
        }

        $this->addExportType('*/*/exportCsv', __('CSV'));
        $this->addExportType('*/*/exportExcel', __('Excel XML'));

        return parent::_prepareColumns();
    }

    protected function _addCustomFilter($collection, $filterData)
    {
        if ($filterData->getShowVendors()) {
            $vendorsList = $filterData->getData('vendors');
            if (isset($vendorsList[0])) {
                $vendorsIds = explode(',', $vendorsList[0]);
                $collection->addVendorFilter($vendorsIds);
            }
        }
        if ($filterData->getShowPoStatuses()) {
            $statusList = $filterData->getData('po_statuses');
            if (isset($statusList[0])) {
                $statusIds = explode(',', $statusList[0]);
                $collection->addPoStatusFilter($statusIds);
            }
        }

        return parent::_addCustomFilter($collection, $filterData);
    }
}
