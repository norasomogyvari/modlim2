<?php

namespace Unirgy\DropshipVendorReport\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Container;

class ShipmentTotal extends Container
{
    protected $_template = 'Magento_Reports::report/grid/container.phtml';
    public function _construct()
    {
        $this->_blockGroup = 'Unirgy_DropshipVendorReport';
        $this->_controller = 'adminhtml_shipmentTotal';
        $this->_headerText = __('Shipment Totals');
        parent::_construct();
        $this->removeButton('add');
        $this->addButton('filter_form_submit', [
            'label'     => __('Show Report'),
            'onclick'   => 'filterFormSubmit()'
        ]);
    }

    public function getFilterUrl()
    {
        $this->getRequest()->setParam('filter', null);
        return $this->getUrl('*/*/*', ['_current' => true]);
    }
}
