<?php

namespace Unirgy\DropshipVendorReport\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class UdpoPoLoadAfter extends AbstractObserver implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        $this->_rptHlp->addPoTotals($observer->getPo());
    }
}
