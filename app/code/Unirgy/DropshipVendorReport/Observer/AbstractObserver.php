<?php

namespace Unirgy\DropshipVendorReport\Observer;

abstract class AbstractObserver
{
    /**
     * @var \Unirgy\DropshipVendorReport\Helper\Data
     */
    protected $_rptHlp;

    /**
     * @var \Unirgy\Dropship\Helper\Data
     */
    protected $_hlp;

    /**
     * @var \Magento\Framework\Locale\Resolver
     */
    protected $_localeResolver;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_dateTimeTimezoneInterface;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_modelStoreManagerInterface;

    /**
     * @var \Unirgy\DropshipVendorReport\Model\ResourceModel\Report\Pototal
     */
    protected $_reportPoTotal;

    /**
     * @var \Unirgy\DropshipVendorReport\Model\ResourceModel\Report\Shipmenttotal
     */
    protected $_reportShipmentTotal;

    public function __construct(
        \Unirgy\DropshipVendorReport\Helper\Data $helperData,
        \Unirgy\Dropship\Helper\Data $dropshipHelperData, 
        \Magento\Framework\Locale\Resolver $localeResolver, 
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $dateTimeTimezoneInterface, 
        \Magento\Store\Model\StoreManagerInterface $modelStoreManagerInterface, 
        \Unirgy\DropshipVendorReport\Model\ResourceModel\Report\PoTotal $reportPototal,
        \Unirgy\DropshipVendorReport\Model\ResourceModel\Report\ShipmentTotal $reportShipmenttotal)
    {
        $this->_rptHlp = $helperData;
        $this->_hlp = $dropshipHelperData;
        $this->_localeResolver = $localeResolver;
        $this->_dateTimeTimezoneInterface = $dateTimeTimezoneInterface;
        $this->_modelStoreManagerInterface = $modelStoreManagerInterface;
        $this->_reportPoTotal = $reportPototal;
        $this->_reportShipmentTotal = $reportShipmenttotal;

    }

    public function processPoStatusChange($po)
    {
        if ($po->dataHasChangedFor('udropship_status')) {
            $hlp = $this->_rptHlp;
            $poTotals = $hlp->getPoTotals($po);
            if ($poTotals) {
                $hlp->updatePoTotals([$po->getId()=>['udropship_status'=>$po->getUdropshipStatus()]], $po);
            }
        }
    }
    public function _addRecalcGridMassaction($grid)
    {
        $grid->getMassactionBlock()->addItem('recalcReportTotals', [
            'label'=> __('Recalculate Report Totals'),
            'url'  => $grid->getUrl('udreport/index/recalcReportTotals'),
        ]);
    }
    public function aggregatePoTotalReportData()
    {
        if (!$this->_hlp->isModuleActive('Unirgy_DropshipPo')) return $this;
        $this->_localeResolver->emulate(0);
        $rHlp = $this->_hlp->rHlp();
        $conn = $rHlp->getConnection();
        $firstDateSel = $conn->select()
            ->from(['po_total'=>$rHlp->getTable('udreport_udpo_total')], [])
            ->joinInner(['po'=>$rHlp->getTable('udropship_po')], 'po_total.po_id=po.entity_id', [])
            ->joinInner(['o'=>$rHlp->getTable('sales_order')], 'po.order_id=o.entity_id', ['o.created_at'])
            ->where('po_total.is_aggregated=0')
            ->order('o.created_at');
        $firstDate = $conn->fetchOne(
            $firstDateSel
        );
        $dateObj = $this->_dateTimeTimezoneInterface->date();
        $dateObj->setTimezone(\Magento\Framework\Model\Locale::DEFAULT_TIMEZONE);
        $dateObj->set($firstDate, \Magento\Framework\Date::DATETIME_INTERNAL_FORMAT);
        if ($timezone = $this->_modelStoreManagerInterface->getStore()->getConfig(\Magento\Framework\Model\Locale::XML_PATH_DEFAULT_TIMEZONE)) {
            $dateObj->setTimezone($timezone);
        }
        $date = $dateObj->subHour(25);
        $this->_reportPoTotal->aggregate($date);
        $this->_localeResolver->revert();
        return $this;
    }
    public function aggregateShipmentTotalReportData()
    {
        $this->_localeResolver->emulate(0);
        $rHlp = $this->_hlp->rHlp();
        $conn = $rHlp->getConnection();
        $firstDateSel = $conn->select()
            ->from(['po_total'=>$rHlp->getTable('udreport_shipment_total')], [])
            ->joinInner(['po'=>$rHlp->getTable('sales_shipment')], 'po_total.po_id=po.entity_id', [])
            ->joinInner(['o'=>$rHlp->getTable('sales_order')], 'po.order_id=o.entity_id', ['o.created_at'])
            ->where('po_total.is_aggregated=0')
            ->order('o.created_at');
        $firstDate = $conn->fetchOne(
            $firstDateSel
        );
        $dateObj = $this->_dateTimeTimezoneInterface->date();
        $dateObj->setTimezone(\Magento\Framework\Model\Locale::DEFAULT_TIMEZONE);
        $dateObj->set($firstDate, \Magento\Framework\Date::DATETIME_INTERNAL_FORMAT);
        if ($timezone = $this->_modelStoreManagerInterface->getStore()->getConfig(\Magento\Framework\Model\Locale::XML_PATH_DEFAULT_TIMEZONE)) {
            $dateObj->setTimezone($timezone);
        }
        $date = $dateObj->subHour(25);
        $this->_reportShipmentTotal->aggregate($date);
        $this->_localeResolver->revert();
        return $this;
    }
}