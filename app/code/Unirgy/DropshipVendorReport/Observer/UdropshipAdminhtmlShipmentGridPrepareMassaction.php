<?php

namespace Unirgy\DropshipVendorReport\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class UdropshipAdminhtmlShipmentGridPrepareMassaction extends AbstractObserver implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        $this->_addRecalcGridMassaction($observer->getGrid());
    }
}
