<?php

namespace Unirgy\DropshipVendorReport\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Unirgy\DropshipPayout\Model\Payout;

class UdpayoutPayoutSaveCommitAfter extends AbstractObserver implements ObserverInterface
{

    public function execute(Observer $observer)
    {
        $hlp = $this->_rptHlp;
        $payout = $observer->getEvent()->getPayout();
        if ($payout->dataHasChangedFor('payout_status')
            && ($payout->getPayoutStatus() == Payout::STATUS_PAYPAL_IPN
                || $payout->getPayoutStatus() == Payout::STATUS_PAID)
        ) {
            $orders = $payout->getOrders();
            $poIdByPoType = [];
            $orderIds = [];
            foreach ($orders as $order) {
                $orderIds[] = $order['order_id'];
                $poIdByPoType[$order['po_type']][] = $order['po_id'];
            }
            foreach (['po', 'shipment'] as $poType) {
                if (!empty($poIdByPoType[$poType])) {
                    $allPoTotals = $hlp->getAllPoTotalsById($poIdByPoType[$poType], $poType);
                    $update = [];
                    $poIds = [];
                    foreach ($allPoTotals as $__idx=>$poTotal) {
                        $poIds[] = $poTotal['po_id'];
                        $update[$poTotal['po_id']] = [
                            'total_paid'=>$poTotal['total_payout'],
                            'total_due'=>0,
                        ];
                    }
                    if (!empty($update)) $hlp->updatePoTotals($update, $poType);
                    $rHlp = $this->_hlp->rHlp();
                    $conn = $rHlp->getConnection();
                    $conn->update($hlp->getPoTable($poType), ['updated_at'=>$this->_hlp->now()], ['entity_id in (?)'=>$poIds]);
                    $conn->update($rHlp->getTable('sales_order'), ['updated_at'=>$this->_hlp->now()], ['entity_id in (?)'=>$orderIds]);
                }
            }
        }
    }
}
