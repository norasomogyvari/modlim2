<?php

namespace Unirgy\DropshipVendorReport\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class UdropshipShipmentStatusSaveAfter extends AbstractObserver implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        $po = $observer->getEvent()->getShipment();
        $this->processPoStatusChange($po);
    }
}
