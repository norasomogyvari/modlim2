<?php

namespace Unirgy\DropshipVendorReport\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    const MEDIUMTEXT_SIZE=16777216;
    const TEXT_SIZE=65536;
    protected $_moduleManager;
    public function __construct(\Magento\Framework\Module\Manager $moduleManager)
    {
        $this->_moduleManager = $moduleManager;
    }
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        // TODO: Implement install() method.
        $installer = $setup;
        $installer->startSetup();
        $connection = $installer->getConnection();

        $tableNames = ['udreport_shipment_aggregated', 'udreport_shipment_aggregated_statement', 'udreport_shipment_aggregated_order', 'udreport_udpo_aggregated', 'udreport_udpo_aggregated_statement', 'udreport_udpo_aggregated_order'];
        foreach ($tableNames as $_tblName) {
            $aggrTable = $installer->getTable($_tblName);
            $table = $connection->newTable($aggrTable)
                ->addColumn('id', Table::TYPE_INTEGER, 10, [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true
                ])
                ->addColumn('period', Table::TYPE_DATE, null, ['nullable' => true])
                ->addColumn('udropship_vendor', Table::TYPE_INTEGER, null, ['unsigned' => true,'nullable' => true])
                ->addColumn('udropship_status', Table::TYPE_SMALLINT, null, ['unsigned' => true,'nullable' => true])
                ->addColumn('pos_count', Table::TYPE_INTEGER, null, ['nullable' => true])
                ->addColumn('subtotal', Table::TYPE_DECIMAL, [12, 4], ['nullable' => true])
                ->addColumn('trans_fee', Table::TYPE_DECIMAL, [12, 4], ['nullable' => true])
                ->addColumn('com_amount', Table::TYPE_DECIMAL, [12, 4], ['nullable' => true])
                ->addColumn('adj_amount', Table::TYPE_DECIMAL, [12, 4], ['nullable' => true])
                ->addColumn('tax', Table::TYPE_DECIMAL, [12, 4], ['nullable' => true])
                ->addColumn('discount', Table::TYPE_DECIMAL, [12, 4], ['nullable' => true])
                ->addColumn('shipping', Table::TYPE_DECIMAL, [12, 4], ['nullable' => true])
                ->addColumn('total_payout', Table::TYPE_DECIMAL, [12, 4], ['nullable' => true])
                ->addColumn('total_paid', Table::TYPE_DECIMAL, [12, 4], ['nullable' => true])
                ->addColumn('total_due', Table::TYPE_DECIMAL, [12, 4], ['nullable' => true])
                ->addIndex(
                    $installer->getIdxName(
                        $aggrTable,
                        ['period', 'udropship_vendor', 'udropship_status'],
                        AdapterInterface::INDEX_TYPE_UNIQUE
                    ),
                    ['period', 'udropship_vendor', 'udropship_status'],
                    ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
                )
                ->setComment('Vendor Shipment Aggregated Report Table')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');
            $connection->createTable($table);
        }

        $tableNames = ['udreport_shipment_total'=>[$installer->getTable('sales_shipment'), 'entity_id']];
        if ((bool)$this->_moduleManager->isEnabled('Unirgy_DropshipPo')) {
            $tableNames['udreport_udpo_total'] = [$installer->getTable('udropship_po'), 'entity_id'];
        }

        foreach ($tableNames as $_tblName=>$foreignKey) {
            $totalTable = $installer->getTable($_tblName);
            $table = $connection->newTable($totalTable)
                ->addColumn('id', Table::TYPE_INTEGER, 10, [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true
                ])
                ->addColumn('po_id', Table::TYPE_INTEGER, null, ['unsigned' => true,'nullable' => true])
                ->addColumn('udropship_vendor', Table::TYPE_INTEGER, null, ['unsigned' => true,'nullable' => true])
                ->addColumn('udropship_status', Table::TYPE_SMALLINT, null, ['unsigned' => true,'nullable' => true])
                ->addColumn('subtotal', Table::TYPE_DECIMAL, [12, 4], ['nullable' => true])
                ->addColumn('trans_fee', Table::TYPE_DECIMAL, [12, 4], ['nullable' => true])
                ->addColumn('com_amount', Table::TYPE_DECIMAL, [12, 4], ['nullable' => true])
                ->addColumn('adj_amount', Table::TYPE_DECIMAL, [12, 4], ['nullable' => true])
                ->addColumn('tax', Table::TYPE_DECIMAL, [12, 4], ['nullable' => true])
                ->addColumn('discount', Table::TYPE_DECIMAL, [12, 4], ['nullable' => true])
                ->addColumn('shipping', Table::TYPE_DECIMAL, [12, 4], ['nullable' => true])
                ->addColumn('total_payout', Table::TYPE_DECIMAL, [12, 4], ['nullable' => true])
                ->addColumn('total_paid', Table::TYPE_DECIMAL, [12, 4], ['nullable' => true])
                ->addColumn('total_due', Table::TYPE_DECIMAL, [12, 4], ['nullable' => true])
                ->addColumn('is_aggregated', Table::TYPE_SMALLINT, null, ['nullable' => true])
                ->addIndex(
                    $installer->getIdxName(
                        $totalTable,
                        ['is_aggregated'],
                        AdapterInterface::INDEX_TYPE_INDEX
                    ),
                    ['is_aggregated'],
                    ['type' => AdapterInterface::INDEX_TYPE_INDEX]
                )
                ->addIndex(
                    $installer->getIdxName(
                        $totalTable,
                        ['po_id'],
                        AdapterInterface::INDEX_TYPE_UNIQUE
                    ),
                    ['po_id'],
                    ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
                )
                ->setComment('Vendor Shipment Aggregated Report Table')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');
            $connection->createTable($table);
            $connection->addForeignKey(
                $installer->getFkName($totalTable, 'po_id', $foreignKey[0], $foreignKey[1]),
                $totalTable, 'po_id', $foreignKey[0], $foreignKey[1]
            );
        }

        $installer->endSetup();
    }
}