<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */


namespace Wyomind\ElasticsearchMultifacetedAutocomplete\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;


/**
 * Class UpgradeData
 * @package Wyomind\ElasticsearchCore\Setup
 */
class UpgradeData implements UpgradeDataInterface
{

    /**
     * @var \Magento\Store\Model\ResourceModel\Store\CollectionFactory
     */
    protected $storeCollectionFactory;
    /**
     * @var \Wyomind\Core\Helper\DataFactory
     */
    protected $coreHelperFactory;

    /**
     * UpgradeData constructor.
     * @param \Magento\Store\Model\ResourceModel\Store\CollectionFactory $storeCollectionFactory
     * @param \Wyomind\Core\Helper\DataFactory $coreHelperFactory
     */
    public function __construct(
        \Magento\Store\Model\ResourceModel\Store\CollectionFactory $storeCollectionFactory,
        \Wyomind\Core\Helper\DataFactory $coreHelperFactory
    )
    {
        $this->storeCollectionFactory = $storeCollectionFactory;
        $this->coreHelperFactory = $coreHelperFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    )
    {


        if (version_compare($context->getVersion(), '2.3.0') < 0) {
            $setup->startSetup();

            $stores = $this->storeCollectionFactory->create();

            $storesIds = [0];

            foreach ($stores as $store) {
                $storesIds[] = $store->getStoreId();
            }

            $config = $this->coreHelperFactory->create();

            foreach ($storesIds as $storeId) {
                $layers = $config->getStoreConfig("wyomind_elasticsearchmultifacetedautocomplete/settings/display/layer/attributes", $storeId);

                if (strlen($layers) > 1 && (substr($layers, 0, 1) == "{" || substr($layers, 0, 1) == "[")) {
                    $methodEncode = "json_encode";
                    $methodDecode = "json_decode";
                    $layers = $methodDecode($layers, true);
                } elseif (strlen($layers) > 1) {
                    $methodEncode = "serialize";
                    $methodDecode = "unserialize";
                    $layers = $methodDecode($layers);
                } else {
                    continue;
                }
                foreach ($layers as $id => $layer) {

                    if (!isset($layers[$id]['show_more_less'])) {
                        $layers[$id]['show_more_less'] = 0;
                    }
                    if (!isset($layers[$id]['show_more_less_nb_values'])) {
                        $layers[$id]['show_more_less_nb_values'] = 5;
                    }
                    if (!isset($layers[$id]['show_layer_filter'])) {
                        $layers[$id]['show_layer_filter'] = 0;
                    }

                    if (!isset($layers[$id]['closed'])) {
                        $layers[$id]['closed'] = 0;
                    }
                }
                $layers = $methodEncode($layers) . "\n";
                $config->setStoreConfig("wyomind_elasticsearchmultifacetedautocomplete/settings/display/layer/attributes", $layers, $storeId);
            }

            $setup->endSetup();
        }
    }
}
