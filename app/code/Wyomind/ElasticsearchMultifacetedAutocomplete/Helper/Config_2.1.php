<?php

/**
 * Copyright © 2018 Elastic-Ecommerce. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\ElasticsearchMultifacetedAutocomplete\Helper;

/**
 * Config utilities
 */
class Config extends \Magento\Framework\App\Helper\AbstractHelper
{

    const MULTIFACETEDAUTOCOMPLETE = "wyomind_elasticsearchmultifacetedautocomplete/settings/display";

    const UPDATE_SEARCH_TERM = "wyomind_elasticsearchmultifacetedautocomplete/settings/update_search_term";

    /* LABELS */
    const LABELS = self::MULTIFACETEDAUTOCOMPLETE . "/labels";

    /* LAYER */
    const LAYER_ATTRIBUTES = self::MULTIFACETEDAUTOCOMPLETE . "/layer/attributes";
    const LAYER_UPDATE_ENABLE = self::MULTIFACETEDAUTOCOMPLETE . "/layer/update/enable";
    const LAYER_UPDATE_METHOD = self::MULTIFACETEDAUTOCOMPLETE . "/layer/update/method";
    const LAYER_DISPLAY_INPUT_BOXES_PRICE_FILTER = self::MULTIFACETEDAUTOCOMPLETE . "/layer/sliders/display_input_boxes_price_filter";

    /* BREADCRUMB */
    const BREADCRUMB_ENABLE = self::MULTIFACETEDAUTOCOMPLETE . "/breadcrumb/enable";

    /* HIGHLIGHT */
    const LISTING_HIGHLIGHT_ENABLE = self::MULTIFACETEDAUTOCOMPLETE . "/listing/highlight/enable";

    /* BACK TO TOP */
    const LISTING_BACKTOTOP_ENABLE = self::MULTIFACETEDAUTOCOMPLETE . "/listing/backtotop/enable";

    /* SORTING */
    const LISTING_SORTING_ENABLE = self::MULTIFACETEDAUTOCOMPLETE . "/listing/sorting/enable";
    const LISTING_SORTING_RATINGS = self::MULTIFACETEDAUTOCOMPLETE . "/listing/sorting/ratings";

    const LISTING_DISPLAY_REVIEWS = self::MULTIFACETEDAUTOCOMPLETE . "/listing/display_rating";
    const LISTING_DISPLAY_ADDTOCART = self::MULTIFACETEDAUTOCOMPLETE . "/listing/display_addtocart";
    const LISTING_DISPLAY_ADDTOCOMPARE = self::MULTIFACETEDAUTOCOMPLETE . "/listing/display_addtocompare";
    const LISTING_DISPLAY_ADDTOWISHLIST = self::MULTIFACETEDAUTOCOMPLETE . "/listing/display_addtowishlist";
    const LISTING_DISPLAY_SORTER = self::MULTIFACETEDAUTOCOMPLETE . "/listing/display_sorter";
    const LISTING_DISPLAY_EMAIL_TO_A_FRIEND = self::MULTIFACETEDAUTOCOMPLETE . "/listing/display_emailtoafriend";
    const LISTING_DISPLAY_CONFIGURABLE_OPTIONS = self::MULTIFACETEDAUTOCOMPLETE . "/listing/display_config_attributes";


    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface|null
     */
    private $_scopeConfig = null;


    /**
     * @var \Magento\Catalog\Model\Config
     */
    private $_catalogConfig = null;

    /**
     * @var \Magento\Wishlist\Helper\Data|null
     */
    private $_wishlistHelper = null;


    /**
     * Config constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Catalog\Model\Config $catalogConfig
     * @param \Magento\Wishlist\Helper\Data $wishlistHelper
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Model\Config $catalogConfig,
        \Magento\Wishlist\Helper\Data $wishlistHelper
    )
    {
        parent::__construct($context);
        $this->_scopeConfig = $context->getScopeConfig();
        $this->_catalogConfig = $catalogConfig;
        $this->_wishlistHelper = $wishlistHelper;
    }

    /**
     * Get the configuration for the filterable product attributes
     * @param int $store
     * @return string
     */
    public function getFilterableAttributesJson($store = null)
    {
        $serializedData = $this->getStoreConfig(self::LAYER_ATTRIBUTES, $store);
        if ($serializedData != "") {
            if (strlen($serializedData) > 2 && substr($serializedData,0,2) == "a:") {
                $data = unserialize($serializedData);
            } else {
                $data = json_decode($serializedData,true);
            }
            $newData = [];
            foreach ($data as $uid => $filter) {
                $newData[$filter['position']] = [
                    $filter['attribute_code'],
                    $filter['show_results_count'],
                    $filter['show_more_less'],
                    $filter['show_more_less_nb_values'],
                    $filter['show_layer_filter'],
                    $filter['closed']
                ];
            }
            ksort($newData);
            return json_encode($newData);
        } else {
            return "{}";
        }
    }

    /**
     * @param $key
     * @param null $scopeId
     * @return mixed
     */
    public function getStoreConfig($key, $scopeId = null)
    {
        $scope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;
        if (!$scopeId) {
            $scope = \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT;
        }

        return $this->_scopeConfig->getValue($key, $scope, $scopeId);
    }


    /**
     * @param null $store
     * @return mixed
     */
    public function getEaConfig($store = null) {
        return $this->getStoreConfig("wyomind_elasticsearchautocomplete/settings", $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getUpdateSearchTerm($store = null) {
        return $this->getStoreConfig(self::UPDATE_SEARCH_TERM, $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getLabels($store = null) {
        return $this->getStoreConfig(self::LABELS, $store);
    }


    /**
     * @param null $store
     * @return mixed
     */
    public function isHighlightEnabled($store = null) {
        return $this->getStoreConfig(self::LISTING_HIGHLIGHT_ENABLE, $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function isLayerUpdateEnabled($store = null) {
        return $this->getStoreConfig(self::LAYER_UPDATE_ENABLE, $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getLayerUpdateMethod($store = null) {
        return $this->getStoreConfig(self::LAYER_UPDATE_METHOD, $store);
    }

    /**
     * Get all settings
     * @param null $store
     * @return mixed
     */
    public function getConfig($store = null)
    {
        return $this->getStoreConfig(self::MULTIFACETEDAUTOCOMPLETE, $store);
    }

    /**
     * Should we display the reviews in the product grid
     * @param int $store
     * @return int (0 or 1)
     */
    public function getDisplayReviews($store = null)
    {
        return $this->getStoreConfig(self::LISTING_DISPLAY_REVIEWS, $store);
    }


    /**
     * @param null $store
     * @return mixed
     */
    public function isSortingRatingsEnabled($store = null)
    {
        return $this->getStoreConfig(self::LISTING_SORTING_RATINGS, $store);
    }


    /**
     * @param null $store
     * @return mixed
     */
    public function isBreadcrumbEnabled($store = null)
    {
        return $this->getStoreConfig(self::BREADCRUMB_ENABLE, $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function isBackToTopEnabled($store = null)
    {
        return $this->getStoreConfig(self::LISTING_BACKTOTOP_ENABLE, $store);
    }

    /**
     * Should we display the add to cart button in the product grid?
     * @param int $store
     * @return int (0 or 1)
     */
    public function getDisplayAddToCart($store = null)
    {
        return $this->getStoreConfig(self::LISTING_DISPLAY_ADDTOCART, $store);
    }


    /**
     * Should we display the input boxes for the price filter?
     * @param null $store
     * @return mixed
     */
    public function getDisplayInputBoxesForPriceFilter($store = null)
    {
        return $this->getStoreConfig(self::LAYER_DISPLAY_INPUT_BOXES_PRICE_FILTER, $store);
    }

    /**
     * Should we display the add to compare button in the product grid
     * @param int $store
     * @return int (0 or 1)
     */
    public function getDisplayAddToCompare($store = null)
    {
        return $this->getStoreConfig(self::LISTING_DISPLAY_ADDTOCOMPARE, $store);
    }

    /**
     * Should we display the add to wishlist button in the product grid?
     * @param int $store
     * @return int (0 or 1)
     */
    public function getDisplayAddToWishlist($store = null)
    {
        $isAllow = $this->_wishlistHelper->isAllow();
        return $isAllow && $this->getStoreConfig(self::LISTING_DISPLAY_ADDTOWISHLIST, $store);
    }

    /**
     * Should we display the sorter in the toolbar of the product grid
     * @param int $store
     * @return int (0 or 1)
     */
    public function getDisplaySorter($store = null)
    {
        return $this->getStoreConfig(self::LISTING_SORTING_ENABLE, $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getDisplayEmailToAFriend($store = null)
    {
        return $this->getStoreConfig(self::LISTING_DISPLAY_EMAIL_TO_A_FRIEND, $store);
    }

    /**
     * Should we display the configurable options in the product grid
     * @param int $store
     * @return int (0 or 1)
     */
    public function getDisplayConfigurableOptions($store = null)
    {
        return $this->getStoreConfig(self::LISTING_DISPLAY_CONFIGURABLE_OPTIONS, $store);
    }

    /**
     * Get the available orders for the sorter
     * @return array (0 or 1)
     */
    public function getAvailableOrders()
    {
        $availableOrders = [];
        $orders = $this->_catalogConfig->getAttributeUsedForSortByArray();
        foreach ($orders as $key => $order) {
            if ($key == "position") {
                $key = "score";
                $order = "Relevance";
            }
            $availableOrders[$key] = $order;
        }

        if ($this->getDisplayReviews() && $this->isSortingRatingsEnabled()) {
            $availableOrders["rating"] = "Rating";
        }

        return $availableOrders;
    }

}
