<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */


namespace Wyomind\ElasticsearchMultifacetedAutocomplete\Helper;


use Magento\Framework\App\Response\Http;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\App\RequestInterface;
use Magento\Store\Model\StoreManager;
use Wyomind\ElasticsearchCore\Helper\Currency;
use Wyomind\ElasticsearchCore\Helper\Data;
use Wyomind\ElasticsearchCore\Helper\Tax;

class Frontend extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * Frontend constructor.
     * @param StoreManager $storeManager
     */
    public function __construct(
        StoreManager $storeManager
    )
    {
        $this->storeManager = $storeManager;
    }

    public function getBaseMediaUrl() {
        return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . \Magento\Swatches\Helper\Media::SWATCH_MEDIA_PATH;
    }

}