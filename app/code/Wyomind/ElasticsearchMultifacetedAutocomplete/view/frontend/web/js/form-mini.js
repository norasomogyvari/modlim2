/**
 * Copyright © 2018 Elastic-Ecommerce. All rights reserved.
 */
define([
    'jquery',
    'underscore',
    'mage/template',
    'Magento_Ui/js/modal/alert',
    'jquery/ui',
    'mage/translate'
], function ($, _, mageTemplate, alert) {
    'use strict';

    /**
     * Check wether the incoming string is not empty or if doesn't consist of spaces.
     *
     * @param {String} value - Value to check.
     * @returns {Boolean}
     */
    function isEmpty(value) {
        return (value.length === 0) || (value == null) || /^\s+$/.test(value);
    }

    $.widget('mage.wyoema', {

        firstLoad: true,

        /**
         * Widget options
         */
        options: {
            autocomplete: 'off',
            minSearchLength: 3,
            responseFieldElements: 'ul li.item',
            selectClass: 'selected',
            submitBtn: 'button[type="submit"]',
            searchLabel: '[data-role=minisearch-label]',
            searchUrl: '',
            cgiUrl: '',
            filterableAttributes: {},
            displayReviews: false,
            displayAddToCart: false,
            displayAddToCompare: false,
            displayAddToWishlist: false,
            displayEmailToAFriend: false,
            displayConfigurableOptions: false,
            displayInputBoxesForPriceFilter: false,
            storeCode: 'default',
            eaConfig: {},
            labels: {},
            customerGroupId: 0,
            layerUpdateMethod: 1, // 1: disabled, 0: hidden, -1: no update,
            noProductMessage: "",
            updateSearchTerm: 0,
            updateSearchTermUrl: ''
        },

        /**
         * Elasticsearch requesters cache (contains the results of the requests)
         */
        cache: [],
        /**
         * Current search term
         */
        searchTerm: '',

        /**
         * Size of an item in the product grid
         */
        itemWidth: 200,
        itemHeight: 200,

        /**
         * Current filters applied
         */
        filters: {},

        /**
         * Configurable products data cached
         */
        configurableIndex: {},
        configurableImages: {},

        /**
         * Paging parameters
         */
        paging: {
            from: 0,
            size: 10
        },

        /**
         * Sorting parameters
         */
        sorter: {
            order: 'score',
            direction: 'desc'
        },

        /**
         * Price sliders limit
         */
        prices: {
            min: 0,
            max: 0
        },

        /**
         * No more products to load for the infinite scroll
         */
        noMoreInfiniteScrollToLoad: false,

        /**
         * A request is perforing for adding a product to the cart
         */
        ajaxCartInProgress: 0,

        /**
         * Are template hints enabled?
         */
        templateHintsEnabled: false,

        /**
         * Underscore.js templates selectors
         */
        templateSelectors: {
            productsGrid: '#ma-products-grid-template',
            pricesFilter: "#ma-product-prices-filter-template",
            ratingsFilter: "#ma-product-ratings-filter-template",
            textSwatchFilter: "#ma-product-text-swatch-filter-template",
            visualSwatchFilter: "#ma-product-visual-swatch-filter-template",
            attributeFilter: "#ma-product-attribute-filter-template",
            categoryFilter: "#ma-product-category-filter-template",
            categoryItemFilter: "#ma-product-category-item-filter-template",
            breadcrumb: "#ma-breadcrumb-template"
        },
        /**
         * Underscore.js containers target
         */
        contentSelectors: {
            productsGrid: 'ul[data-role=ma-products-grid]',
            productsCount: '#ma-results-count',
            filters: 'div[data-role=ma-products-layer]',
            breadcrumb: "#ma-breadcrumb",
            ratingsFilter: "[data-role=ma-product-ratings-filter-content]",
            attributeFilter: "[data-role=ma-product-attribute-filter-content]",
            categoryFilter: "[data-role=ma-product-category-filter-content]",
            categoryItemFilter: "[data-role=ma-product-category-filter-item-filter-content]",
            textSwatchFilter: "[data-role=ma-product-text-swatch-filter-content]",
            visualSwatchFilter: "[data-role=ma-product-visual-swatch-filter-content]",
            pricesFilter: "[data-role=ma-product-prices-filter-content]"
        },
        /**
         * Containers phtml references (only for template hints)
         */
        contentTemplateFiles: {
            productsGrid: 'view/frontend/templates/autocomplete/grid.phtml',
            pricesFilter: "view/frontend/templates/layer/renderer/price.phtml",
            ratingsFilter: "view/frontend/templates/layer/renderer/rating.phtml",
            textSwatchFilter: "view/frontend/templates/layer/renderer/textSwatch.phtml",
            visualSwatchFilter: "view/frontend/templates/layer/renderer/visualSwatch.phtml",
            attributeFilter: "view/frontend/templates/layer/renderer/attribute.phtml",
            categoryFilter: "view/frontend/templates/layer/renderer/categories.phtml",
            categoryItemFilter: "view/frontend/templates/layer/renderer/categories.phtml",
            breadcrumb: "view/frontend/templates/autocomplete/breadcrumb.phtml",
            filters: "view/frontend/templates/autocomplete/search-box.phtml"
        },

        alreadyInitialized: false,

        /**
         * Widget intialisation method
         * @private
         */
        _create: function () {
            $.ajax({
                url: this.options.cgiUrl,
                method: "POST",
                global: false,
                data: {},
                dataType: "json"
            }).done(function (data) {
                if (typeof data.cgi !== "undefined") {
                    this.options.customerGroupId = data.cgi;
                    this.init();
                }
            }.bind(this));
        },


        init: function () {
            $('div[data-role=ma-products-layer]').css({left: ($(window).width() + 20) + "px"});


            this.responseList = {
                indexList: null,
                currentIndex: null
            };
            this.autoComplete = $(this.options.destinationSelector);
            this.searchForm = $(this.options.formSelector);
            this.submitBtn = this.searchForm.find(this.options.submitBtn)[0];
            this.searchLabel = $(this.options.searchLabel);
            this.overlay = $("#ma-overlay");

            _.bindAll(this, '_onKeyDown', '_onKeyUp', '_onPropertyChange');
            this.element.on('input propertychange', this._onPropertyChange);

            this.submitBtn.disabled = true;

            this.element.attr('autocomplete', this.options.autocomplete);

            this.element.on('focus', $.proxy(function () {
                this.searchLabel.addClass('active');
            }, this));
            this.element.on('keydown', this._onKeyDown);
            this.element.on('keyup', this._onKeyUp);

            this.searchForm.on('submit', $.proxy(function () {
                if ('true' === this.element.attr('aria-haspopup') && null !== this.responseList.currentIndex) {
                    return false;
                }
                $("#" + this.searchForm.attr('id') + " input:not(.input-text)").attr('name', '');
                this._updateAriaHasPopup(false);
            }, this));


            this.computeItemSize();

            if (!this.alreadyInitialized) {
                this.initObservers();
                this.alreadyInitialized = true;
            }

        },
        /**
         * Compute the width and the height of an item in the grid (useful to know how many product must be fetched by the infinite scroll)
         */
        computeItemSize: function () {

            // add an empty item in the grid
            $('ul[data-role=ma-products-grid]').html("<li class='item'>&nbsp;</li>");

            // get the width of the item

            var width = parseInt($($('#ma-grid-container .item')[0]).css('min-width').replace('px', ''));
            var pleft = parseInt($($('#ma-grid-container .item')[0]).css('padding-left').replace('px', ''));
            var pright = parseInt($($('#ma-grid-container .item')[0]).css('padding-right').replace('px', ''));
            var bleft = parseInt($($('#ma-grid-container .item')[0]).css('border-left-width').replace('px', ''));
            var bright = parseInt($($('#ma-grid-container .item')[0]).css('border-right-width').replace('px', ''));
            var mleft = parseInt($($('#ma-grid-container .item')[0]).css('margin-left').replace('px', ''));
            var mright = parseInt($($('#ma-grid-container .item')[0]).css('margin-right').replace('px', ''));
            this.itemWidth = width + pleft + pright + bleft + bright + mleft + mright;

            // get the height of the item (height+paddingtop+paddingbottom)
            var height = parseInt($($('#ma-grid-container .item')[0]).css('min-height').replace('px', ''));
            var ptop = parseInt($($('#ma-grid-container .item')[0]).css('padding-top').replace('px', ''));
            var pbottom = parseInt($($('#ma-grid-container .item')[0]).css('padding-bottom').replace('px', ''));
            this.itemHeight = height + ptop + pbottom;
        },

        /**
         * Initialize the debug popup observers
         */
        initDebugObservers: function () {
            $(document).on('click', '#ma-debug-enable-template-hints', function () {
                this.toggleTemplateHints(true);
            }.bind(this));
            $(document).on('click', '#ma-debug-disable-template-hints', function () {
                this.toggleTemplateHints(false);
            }.bind(this));
            $(document).on('click', '#ma-debug-show-last-request-data', function () {
                require(['jquery', 'jquery/ui', 'Magento_Ui/js/modal/modal', "elasticsearchcore_jsonview"], function ($) {
                    $("#ma-debug-show-last-request-data-modal").modal({
                        "type": "slide",
                        "title": "Last Request Data",
                        "modalClass": "mage-new-category-dialog form-inline",
                        buttons: []
                    });
                    $("#ma-debug-show-last-request-data-modal").html("");
                    $("#ma-debug-show-last-request-data-modal").JSONView(JSON.stringify(this.lastRequestData));
                    $("#ma-debug-show-last-request-data-modal").modal("openModal");
                }.bind(this));
            }.bind(this));
        },

        /**
         * Init observers for the smartphone view
         */
        initSmartPhoneObservers: function () {
            $(document).on('click', 'span#filters-smartphones', function () {
                $('div[data-role=ma-products-layer]').css({left: "0px"});
                return false;
            });
            // iphone fix
            $(document).on('touchstart', '#filters-smartphones, #filters-smartphones span', function () {
                $('div[data-role=ma-products-layer]').css({left: "0px"});
                return false;
            });


            $(document).on('keyup keydown paste', "#search-smartphones", function (e) {
                if ($("#search-smartphones").is(":focus")) {
                    var keyCode = e.keyCode || e.which;
                    if (keyCode === $.ui.keyCode.ENTER) {
                        this.searchForm.trigger('submit');
                    } else {
                        this.element.val($('#search-smartphones').val());
                        this.loadProducts();
                    }
                }
            }.bind(this));
        },

        /**
         * Init the overlay observers (click on the overlay = hide the autocomplete)
         */
        initOverlayObservers: function () {
            $(document).on('click', function (event) {
                if ($(event.target).parents(".searchbox-header").length == 0) { // smarket theme
                    if ($(event.target).parents(".block-search").length == 0) {
                        this._updateAriaHasPopup(false);
                    }
                }
            }.bind(this));
        },

        /**
         * Init the breadcrumb observers
         */
        initBreadcrumbObservers: function () {


            // remove filter
            $(document).on('click', 'div.ma-breadcrumb .action.remove', function (evt) {
                var a = $(evt.target);
                var attributeCode = a.attr("attribute-code");
                //var optionId = a.attr("option-id");
                this.removeFilter(attributeCode);
                return false;
            }.bind(this));

            // - remove filters after
            $(document).on('click', 'div.ma-breadcrumb a.remove-after', function (evt) {
                var target = evt.target;
                if (target.tagName !== "A") {
                    target = $(evt.target).parents("a.remove-after");
                } else {
                    target = $(target);
                }

                var attributeCode = target.attr("attribute-code");
                var after = false;
                _.each(this.filters, function (values, key) {
                    if (after) {
                        delete(this.filters[key]);
                    }
                    if (key === attributeCode) {
                        after = true;
                    }
                }.bind(this));
                this.paging.from = 0;
                this.loadProducts();
                return false;
            }.bind(this));

            // - remove filters after and others values
            $(document).on('click', 'div.ma-breadcrumb a.remove-after-and-others', function (evt) {

                var target = evt.target;
                if (target.tagName !== "A") {
                    target = $(evt.target).parents("a.remove-after");
                } else {
                    target = $(target);
                }

                var attributeCode = target.attr("attribute-code");
                var optionId = target.attr("option-id");
                var after = false;
                _.each(this.filters, function (values, key) {
                    if (after) {
                        delete(this.filters[key]);
                    }
                    if (key === attributeCode) {
                        after = true;
                    }
                }.bind(this));
                this.filters[attributeCode] = [optionId];
                this.paging.from = 0;
                this.loadProducts();
                return false;
            }.bind(this));
        },

        /**
         * Initialize the sorter observers (sort field, sort order)
         */
        initSortingObservers: function () {
            // grid direction
            $(document).on('click', '.ma-toolbar a.sorter-action', function (evt) {
                var elt = $(evt.target);
                elt.toggleClass('active');
                if (elt.hasClass('sort-desc')) {
                    $('div.ma-order a.sorter-action.sort-asc').toggleClass('active');
                    this.sorter.direction = 'asc';
                }
                if (elt.hasClass('sort-asc')) {
                    $('div.ma-order a.sorter-action.sort-desc').toggleClass('active');
                    this.sorter.direction = 'desc';
                }
                this.loadProducts();
                return false;
            }.bind(this));

            // grid order
            $(document).on('change', '.ma-toolbar select#sorter', function (evt) {
                var elt = $(evt.target);
                this.sorter.order = elt.val();
                this.loadProducts();
            }.bind(this));
        },

        /**
         * Initialize observers on the configurable product options
         */
        initConfigurableProductObservers: function () {
            // configurable products options
            $(document).on('click', 'ul.ma-grid li.item .swatch-option', function (evt) {
                var elt = $(evt.target);

                // remove error message
                var errorMsg = elt.parents('li').find('.swatch-error');
                $('#ma-message-options-error').slideUp();

                // unselect other option values
                elt.parent().find('.swatch-option').removeClass('selected');
                // select this value
                elt.addClass('selected');
                // product id
                var productId = elt.attr('product-id');
                // attribute id
                var attributeId = elt.attr('attribute-id');
                // option id
                var optionId = elt.attr('option-id');


                $('#ma-cart-super-attribute-' + productId + '-' + attributeId).val(optionId);

                // all option selected
                var selected = $(elt.parents("li.item")[0]).find(".swatch-option.selected");
                var selection = {};
                selected.each(function () {
                    selection[$(this).attr('attribute-id')] = $(this).attr('option-id');
                });
                // search the simple product id
                var simpleProductId = -1;
                _.each(this.configurableIndex[productId], function (data, key) {
                    if (_.isEqual(data, selection)) {
                        simpleProductId = key;
                    }
                }.bind(this));
                // simple product id found
                if (simpleProductId !== -1
                    && typeof this.configurableImages[productId] !== "undefined"
                    && typeof this.configurableImages[productId][simpleProductId] !== "undefined"
                    && typeof this.configurableImages[productId][simpleProductId]['img'] !== "undefined") {
                    // display the image matching the simple product (if it exists)
                    $('#product-image-' + productId).attr('src', this.configurableImages[productId][simpleProductId]['img']);

                }

            }.bind(this));
        },

        initAjaxAddToWishlistObservers: function () {
            let form_key = this.getCookie("form_key");
            $(document).on('click', '.ma-ajax-add-to-wishlist', function (evt) {
                $('<form method="post" action="'+$(this).attr('href')+'"><input type="hidden" name="productId" value="'+$(this).attr('product')+'"/><input type="hidden" name="form_key" value="'+form_key+'"/>></form>').appendTo('body').submit();
                return false;
            });
        },

        /**
         * Initialize the ajax add to cart observers
         */
        initAjaxAddToCartObservers: function () {

            // Add To Cart button
            $(document).on('click', 'button.ma-ajax-add-to-cart', function (evt) {
                var elt = $(evt.target);
                var form = $($(elt).parents("div[data-role=tocart-form]")[0]);
                var data = {};
                var error = false;
                form.find("input").each(function () {
                    if (!error && $(this).hasClass("swatch-input") && $(this).val() === "") {
                        error = true;
                    }
                    data[$(this).attr('name')] = $(this).val();
                });
                var action = form.attr('action');

                if (error) {
                    var errorMsg = form.parents('li').find('.swatch-error');
                    $('#ma-message-options-error').slideDown();
                    setTimeout(function () {
                        $('#ma-message-options-error').slideUp();
                    }, 3000);
                    return;
                }

                form.parents('li').find('.loader').addClass('active');
                this.ajaxCartInProgress++;
                $.ajax({
                    url: action,
                    method: "post",
                    global: true, // allow to reload the mini cart
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        if (typeof data.backUrl !== "undefined") {
                            document.location.href = data.backUrl;
                        } else {
                            form.parents('li').find('.loader').removeClass('active');
                        }
                    },
                    error: function () {
                        form.parents('li').find('.loader').removeClass('active');
                    }
                });
            }.bind(this));

            // get add to cart messages
            $(document).on('ajaxComplete', function (event, xhr, settings) {
                if (this.ajaxCartInProgress > 0) {
                    // after add to cart
                    if (settings.url.includes("checkout/cart")) {
                        if (typeof xhr.responseText !== "undefined" && typeof xhr.responseJSON === "undefined" && xhr.responseText !== "[]" && !document.location.href.endsWith("checkout/cart/")) {
                            var message = "An error happened.";
                            var hash = this.hash(message);
                            $("#ma-messages").append("<div id='" + hash + "' style='display:none' class='message-error error message'><div>" + message + "</div></div>");
                            $("#" + hash).slideDown();
                            setTimeout(function () {
                                $("#" + hash).slideUp(function () {
                                    $("#" + hash).remove();
                                });
                            }, 3000);
                        }
                    }
                    // retrieve add to cart messages
                    else if (settings.url.includes("sections=cart%2Cmessages")) {
                        if (!_.isObject(xhr.responseJSON)) { //eslint-disable-line
                            return;
                        }
                        var json = xhr.responseJSON;
                        if (typeof json.messages !== "undefined") {
                            _.each(json.messages.messages, function (message) {
                                var hash = this.hash(message.text);
                                $("#ma-messages").append("<div id='" + hash + "' style='display:none' class='message-" + message.type + " " + message.type + " message'><div>" + message.text + "</div></div>");
                                $("#" + hash).slideDown();
                                setTimeout(function () {
                                    $("#" + hash).slideUp(function () {
                                        $("#" + hash).remove();
                                    });
                                }, 3000);
                            }.bind(this));
                        }
                        this.ajaxCartInProgress--;
                    }
                }

            }.bind(this));
        },

        /**
         * Add infinite scroll observer to the products list
         */
        initInfiniteScrollObservers: function () {
            // scroll in the grid => infiniteScroll
            $('#ma-grid-container').bind('scroll', function () {
                if (!this.noMoreInfiniteScrollToLoad) {
                    var elt = $('#ma-grid-container');
                    if (elt.scrollTop() + elt.innerHeight() >= elt[0].scrollHeight - 150) {
                        if (this.xhr.readyState === 4) {
                            this.loadInfiniteScrollProducts();
                        }
                    }
                }
            }.bind(this));
        },

        initLayerFilterObservers: function () {
            $(document).on('keyup', '.elma-layer-filter', function () {
                if ($(this).val() == "") {
                    $(this).parent().find('a div').parent().removeClass('hidden');
                    $(this).parent().parent().addClass('hide-more');
                    $(this).parent().parent().find('.ma-show-more, .ma-show-less').css({'display': ''});
                } else {
                    $(this).parent().parent().removeClass('hide-more');
                    $(this).parent().parent().find('.ma-show-more, .ma-show-less').css({'display': 'none'});
                    $(this).parent().find('a div:not([data-filter*=' + $(this).val() + '])').parent().addClass('hidden');
                    $(this).parent().find('a div[data-filter*=' + $(this).val() + ']').parent().removeClass('hidden');
                }
            });
        },

        /**
         * Init observers for the mode switcher (grid/list)
         */
        initModeSwitcherObservers: function () {
            // switch between grid mode and list mode
            $(document).on('click', 'a.ela.modes-mode', function (evt) {
                if (evt.target.id === "mode-grid") {
                    $('a#mode-list.mode-list.ela.active').removeClass('active').removeClass("wyomind-btn-primary").addClass("wyomind-btn-primary-inverted");
                    $('a#mode-grid.mode-grid.ela').addClass('active').addClass("wyomind-btn-primary").removeClass("wyomind-btn-primary-inverted");
                    $('#ma-grid').removeClass('list');
                } else if (evt.target.id === "mode-list") {
                    $('a#mode-grid.mode-grid.ela.active').removeClass('active').removeClass("wyomind-btn-primary").addClass("wyomind-btn-primary-inverted");
                    $('a#mode-list.mode-list.ela').addClass('active').addClass("wyomind-btn-primary").removeClass("wyomind-btn-primary-inverted");
                    $('#ma-grid').addClass('list');
                }
                $.cookie('ma-mode', evt.target.id.replace('mode-', ''), {path: '/'});
            });
        },

        /**
         * Init observers on filters
         */
        initFilterObservers: function () {

            // clear all filters action (breadcrumb)
            $(document).on('click', ".ma-layer-clear-all", function () {
                this.filters = {};
                this.paging.from = "0";
                this.loadProducts(true);
                return false;
            }.bind(this));

            // open/close a filter block
            $(document).on('click', 'div.ma-filter-item', function (evt) {
                var target = $(evt.target);
                if (target.hasClass("ma-filter-content")) {
                    return;
                }
                if (!target.hasClass("ma-filter-item")) {
                    target = target.parent("div.ma-filter-item");
                }
                target.toggleClass('active');
            });


            // open /close a category in the treeview
            $(document).on('click', 'div.ma-filter-item[attribute-code=categories] li.children span.tree', function (evt) {
                $($(this).parents('li')[0]).toggleClass('active');
            });

            // price range modified
            $(document).on('ma-price-range-changed', function (evt, valMin, valMax, min, max) {
                if (valMin === min && valMax === max) {
                    delete(this.filters.final_price);
                } else {
                    this.filters.final_price = {min: valMin, max: valMax};
                }
                this.paging.from = "0";
                this.loadProducts(false);
                return false;
            }.bind(this));

            // prices text input
            $(document).on('keydown', '.ma-filter-content.price .ma-price.input-text', function (evt) {
                if (evt.keyCode == 13) { // "Enter" key => do not trigger the form submission
                    $(evt.target).trigger("change");
                    return false;
                }
            });
            $(document).on('change', '.ma-filter-content.price .ma-price.input-text', function (evt) {
                var elt = $(evt.target).parent();
                var selectedMin = elt.find(".input-text.min").val();
                var selectedMax = elt.find(".input-text.max").val();
                if (isNaN(selectedMin) || selectedMin === "" || parseFloat(selectedMin) < this.prices.min) {
                    selectedMin = this.prices.min;
                }
                selectedMin = parseFloat(selectedMin);
                if (selectedMin > this.prices.max) {
                    selectedMin = this.prices.max;
                }
                if (isNaN(selectedMax) || selectedMax === "" || parseFloat(selectedMax) > this.prices.max) {
                    selectedMax = this.prices.max;
                }
                selectedMax = parseFloat(selectedMax);
                if (selectedMax < this.prices.min) {
                    selectedMax = this.prices.min
                }
                if (selectedMax < selectedMin) {
                    selectedMax = selectedMin;
                }
                $(document).trigger("ma-price-range-changed", [selectedMin, selectedMax]);
                return false;
            }.bind(this));

            // select a filter option
            $(document).on('click', 'div.ma-layer a.filter-option', function (evt) {
                // retrieve attribute code + option id
                var target = evt.target;
                if (target.tagName !== "A") {
                    target = $(evt.target).parents("a.filter-option");
                } else {
                    target = $(target);
                }


                if (target.hasClass('disabled')) {
                    return false;
                }

                var attributeCode = target.attr("attribute-code");
                var optionId = target.attr("option-id");
                // swatch
                if (target.hasClass("swatch-option-link-layered")) {
                    if (target.find(".swatch-option").hasClass("selected")) {
                        target.find(".swatch-option").removeClass("selected");
                        this.removeFilter(attributeCode, optionId);
                    } else {
                        target.find(".swatch-option").addClass("selected");
                        this.addFilter(attributeCode, optionId);
                    }
                }
                // ratings
                else if (target.hasClass("rating")) {
                    if (target.find(".title").hasClass("selected")) {
                        this.removeFilter(attributeCode, optionId);
                        target.find(".title-option").removeClass("selected");
                    } else {
                        this.addFilter(attributeCode, optionId);
                        target.find(".title").addClass("selected");
                    }
                }
                // click on the checkbox
                else if (evt.target.tagName === "INPUT") {
                    var checked = $(evt.target).attr('checked');
                    if (typeof checked === "undefined") { // unchecked => remove filter
                        // categories
                        if (attributeCode === "categories_ids") {
                            target.parent().find("a.ma-filter-option:not(.disabled) span.title").removeClass('selected');
                            this.updateCategoriesFilter();
                            // others
                        } else {
                            this.removeFilter(attributeCode, optionId);
                        }
                    } else {
                        // categories
                        if (attributeCode === "categories_ids") {
                            target.parent().find("a.ma-filter-option:not(.disabled) span.title").addClass('selected');
                            this.updateCategoriesFilter();
                            // others
                        } else {
                            this.addFilter(attributeCode, optionId);
                        }
                    }
                } else { // click on the label + count
                    var cbx = target.find('input[type="checkbox"]');
                    var checked = cbx.attr('checked');
                    if (typeof checked === "undefined") { // unchecked => add filter
                        // categories
                        if (attributeCode === "categories_ids") {
                            target.parent().find("a.ma-filter-option:not(.disabled) input[type=checkbox]").attr('checked', true);
                            target.parent().find("a.ma-filter-option:not(.disabled) span.title").addClass('selected');
                            this.updateCategoriesFilter();
                            // others
                        } else {
                            target.find("input[type=checkbox]").attr('checked', true);
                            target.find("span.title").addClass('selected');
                            this.addFilter(attributeCode, optionId);
                        }
                    } else {
                        // categories
                        if (attributeCode === "categories_ids") {
                            target.parent().find("a.ma-filter-option:not(.disabled) input[type=checkbox]").attr('checked', false);
                            target.parent().find("a.ma-filter-option:not(.disabled) span.title").removeClass('selected');
                            this.updateCategoriesFilter();
                            // others
                        } else {
                            target.find("input[type=checkbox]").attr('checked', false);
                            target.find("span.title").removeClass('selected');
                            this.removeFilter(attributeCode, optionId);
                        }
                    }

                }
                return false;
            }.bind(this));

            // - show more links
            $(document).on('click', '.ma-show-more', function () {
                $(this).parent().removeClass('hide-more');
                return false;
            });
            $(document).on('click', '.ma-show-less', function () {
                $(this).parent().addClass('hide-more');
                return false;
            });
        },

        /**
         * Add observers when the window is resized
         */
        initWindowResizeObservers: function () {
            $(window).resize(function () {
                if (this.element.attr('aria-haspopup') == 'true') {
                    this.loadProducts();
                }
            }.bind(this));

        },

        /**
         * Add observers to close the autocomplete on the right top crosses
         */
        initAutocompleteCloseObservers: function () {

            // smartphone view
            $(document).on('click', 'div.ma-layer span.back', function () {
                $('div[data-role=ma-products-layer]').css({left: ($(window).width() + 20) + "px"});
                return false;
            });
            // fix iphone
            $(document).on('touchstart', 'div.ma-layer span.back', function () {
                $('div[data-role=ma-products-layer]').css({left: ($(window).width() + 20) + "px"});
                return false;
            });

            // desktop/tablet view
            $(document).on('click', '#ma-close', function () {
                this._updateAriaHasPopup(false);

            }.bind(this));
        },
        initAutocompleteDisabledObservers: function () {

            $(document).on('mouseover', '.ma-search-autocomplete', function () {
                if (this.xhr === undefined && $("#search_autocomplete").css("opacity") == 0) {
                    $("#search_autocomplete").css({height: "0px"});
                }


            }.bind(this));
        },

        initBackToTopObservers: function () {
            $('#ma-grid-container').bind('scroll', function () {
                if ($('#ma-grid-container').scrollTop() > 450) {
                    $('.ma-back-to-top').removeClass('hidden');
                } else {
                    $('.ma-back-to-top').addClass('hidden');
                }
            });
            $(document).on('click', '.ma-back-to-top', function () {
                $('#ma-grid-container').animate({scrollTop: 0}, "slow");
            });
        },

        /**
         * Initialize all observers
         */
        initObservers: function () {

            this.initDebugObservers();
            this.initSmartPhoneObservers();
            this.initOverlayObservers();
            this.initBreadcrumbObservers();
            this.initSortingObservers();
            this.initConfigurableProductObservers();
            this.initAjaxAddToCartObservers();
            this.initAjaxAddToWishlistObservers();
            this.initInfiniteScrollObservers();
            this.initModeSwitcherObservers();
            this.initFilterObservers();
            this.initWindowResizeObservers();
            this.initAutocompleteCloseObservers();
            this.initAutocompleteDisabledObservers();
            this.initBackToTopObservers();
            this.initLayerFilterObservers();

            $(document).on('wyo-search-term-updated-finish', function () {
                this.element.removeClass('in-progress');
            }.bind(this));
        },


        initPriceSlider: function (valMin, valMax) {
            $(".ma-filter-content .ma-price-slider").slider({
                range: true,
                min: this.prices.min,
                max: this.prices.max,
                values: [valMin, valMax],
                stop: function (event, ui) {
                    var selectedMin = parseInt(ui.values[0]);
                    if (isNaN(selectedMin)) {
                        selectedMin = 0;
                    }
                    var selectedMax = parseInt(ui.values[1]);
                    $(document).trigger("ma-price-range-changed", [selectedMin, selectedMax, this.min, this.max]);
                }.bind(this),
                slide: function (event, ui) {
                    $(".ma-filter-content .price-range .from").html(_.getFormattedPrice(ui.values[0]));
                    $(".ma-filter-content .price-range .to").html(_.getFormattedPrice(ui.values[1]));
                    $(".ma-filter-content .ma-price-slider").slider("option", "values", [ui.values[0], ui.values[1]]);
                }
            });
            $(".ma-filter-content .ma-price-slider .ui-slider-handle").addClass("wyomind-primary-bgcolor");
        },

        /**
         * Update the categories selected in the filters then reload the products
         */
        updateCategoriesFilter: function () {
            this.filters["categories_ids"] = [];
            var elts = $('#ma-product-category-filter span.selected');
            _.each(elts, function (elt) {
                this.filters["categories_ids"].push($(elt).attr('category-id'));
            }.bind(this));
            this.paging.from = "0";
            this.loadProducts();
        },

        /**
         * Add a filter to the current filters list, then reload the products list
         * @param attributeCode
         * @param optionId
         */
        addFilter: function (attributeCode, optionId) {
            if (typeof this.filters[attributeCode] === "undefined") {
                this.filters[attributeCode] = [optionId];
            } else {
                this.filters[attributeCode].push(optionId);
            }
            this.paging.from = "0";
            this.loadProducts();
        },

        /**
         * Remove a filter from the current filters list, then reload the products list
         * @param attributeCode
         * @param optionId
         */
        removeFilter: function (attributeCode, optionId) {
            if (attributeCode === "rating") {
                $("#rating-filter-" + optionId).removeClass("selected");
            }
            this.paging.from = "0";
            if (attributeCode === "final_price" || typeof optionId == "undefined") {
                delete(this.filters[attributeCode]);
                this.loadProducts();
            } else {
                this.filters[attributeCode] = _.without(this.filters[attributeCode], optionId);
                if (this.filters[attributeCode].length === 0) {
                    delete(this.filters[attributeCode]);
                }

                this.loadProducts();
            }
        },


        /**
         * Make a request to get products list when the infinite scroll is triggered, according to the current filters, page, order, sorting field, etc...
         */
        loadInfiniteScrollProducts: function () {
            this.element.addClass('in-progress');

            var fullStartTime = Date.now();

            this.paging.from += this.paging.size;

            var data = {
                "store": this.options.storeCode,
                "categoryId": -1,
                "searchTerm": this.searchTerm,
                "from": this.paging.from,
                "size": this.paging.size,
                "order": this.sorter.order,
                "direction": this.sorter.direction,
                "filters": this.filters,
                "loadAggregations": false,
                "loadSelectedFilters": false,
                "highlightEnabled": this.options.highlightEnabled,
                "customerGroupId": this.options.customerGroupId
            };

            var hash = this.hash(JSON.stringify(data));
            var requestStartTime = Date.now();

            if (typeof this.cache[hash] !== "undefined") {
                data = this.cache[hash];
                this.lastRequestData = data;
                this.updateContentInfiniteScroll(data, fullStartTime, requestStartTime, hash);
                this.element.removeClass('in-progress');
            } else {

                this.xhr = $.ajax({
                    url: this.options.url,
                    method: "POST",
                    global: false,
                    data: data,
                    dataType: "json"
                }).done(function (data) {
                    if (this.searchTerm === this.element.val()) {
                        _.each(data['products'], function (product) {
                            if (typeof product.configurable_index !== "undefined") {
                                this.configurableIndex[product.id] = $.parseJSON(product.configurable_index);
                                this.configurableImages[product.id] = $.parseJSON(product.configurable_images);
                            }
                        }.bind(this));
                        this.updateContentInfiniteScroll(data, fullStartTime, requestStartTime);
                        this.lastRequestData = data;
                        this.cache[hash] = data;
                        this.cache[hash].time = 0;
                        this.element.removeClass('in-progress');
                    }
                }.bind(this));
            }
        },

        /**
         * Update the products list contains in the results of the request for the infinite scroll
         * No need to refresh the filters
         * @param data
         * @param fullStartTime
         * @param requestStartTime
         * @param cacheHash
         */
        updateContentInfiniteScroll: function (data, fullStartTime, requestStartTime, cacheHash) {
            var requestTime = Date.now() - requestStartTime;
            var renderingStartTime = Date.now();

            if (data.products.length < this.paging.size) {
                this.noMoreInfiniteScrollToLoad = true;
            }

            var productGrid = mageTemplate(this.templateSelectors.productsGrid, {
                "products": data.products,
                "displayReviews": this.options.displayReviews,
                "displayAddToCart": this.options.displayAddToCart,
                "displayAddToCompare": this.options.displayAddToCompare,
                "displayAddToWishlist": this.options.displayAddToWishlist,
                "displayEmailToAFriend": this.options.displayEmailToAFriend,
                "displayConfigurableOptions": this.options.displayConfigurableOptions,
                "customerGroupId": this.options.customerGroupId
            });
            // complete the grid with empty items
            var totalWidth = $('#ma-grid-container').innerWidth();

            var itemWidth = this.itemWidth;
            var rowItemsNumber = parseInt(totalWidth / itemWidth);

            var loadedItems = data.products.length + 1; //@todo check cms/categories/suggests

            if (this.noMoreInfiniteScrollToLoad) {
                productGrid += "<li class='item no-more'>" + this.options.labels.no_more_product + "</li>";
                loadedItems++;
            }

            while (loadedItems % rowItemsNumber !== 0) {
                productGrid += "<li class='item empty'></li>";
                loadedItems++;
            }

            $(this.contentSelectors.productsGrid).find('li.item.empty').remove();
            $(this.contentSelectors.productsGrid).append(productGrid.replace(/<b>/ig, '').replace(/<\/b>/ig, ''));

            if ($("#ma-debug-template").length >= 1) {
                var fullEndTime = Date.now();
                var debug = mageTemplate("#ma-debug-template", {
                    "phpTime": data.time,
                    "requestTime": requestTime,
                    "renderingTime": (fullEndTime - renderingStartTime),
                    "totalTime": (fullEndTime - fullStartTime),
                    "fromCache": typeof cacheHash !== "undefined",
                    "cacheHash": cacheHash,
                    "infiniteScroll": true,
                    "templateHintsEnabled": this.templateHintsEnabled,
                    "data": data
                });
                $('#ma-debug').html(debug);
                this.toggleTemplateHints(this.templateHintsEnabled);
            }

            this.updateFormKey();
        },

        /**
         * Make a request to get products list according to the current filters, page, order, sorting field, etc...
         */
        loadProducts: function (resetFilters, loadAggregations) {

            this.element.addClass('in-progress');

            if (typeof loadAggregations == "undefined") {
                loadAggregations = this.options.layerUpdateMethod >= 0;
            }


            var fullStartTime = Date.now();


            if (typeof resetFilters !== "undefined" && resetFilters === true) {
                this.filters = {};
            }

            var searchField = this.element;

            var value = this.element.val();
            this.submitBtn.disabled = isEmpty(value);

            if (value.length >= parseInt(this.options.minSearchLength, 10)) {

                if ($('body').hasClass('catalogsearch-result-index') && $('body').hasClass('eln-auto-refresh') && $('.eln-main-content').length > 0) {
                    $(document).trigger('wyo-search-term-updated', value);
                    return;
                }


                if (this.xhr !== undefined) {
                    this.xhr.abort();
                }

                this.noMoreInfiniteScrollToLoad = false;

                if ($(window).width() < 768/* || $(window).height() <= 704*/) { // ipad mini dimensions
                    $("#search_autocomplete").css({
                        display: "flex",
                        height: ($(window).height() + 1) + "px",
                        width: $(window).width() + "px",
                        top: "-1px",
                        bottom: "0px",
                        right: "0px",
                        left: "0px",
                        position: "fixed"
                    });

                } else {
                    $("#search_autocomplete").css({
                        position: "absolute",
                        display: "flex",
                        left: (-1 * (searchField.offset().left - 60)) + 'px',
                        height: ($(window).height() - (searchField.offset().top - $(document).scrollTop()) - searchField.outerHeight() - 51) + 'px',
                        width: (window.outerWidth - (2 * 60)),
                        top: searchField.outerHeight() + 13
                    });
                }


                // opened filter divs
                var openedFilters = [];
                var openedShowMore = [];
                var layerFilters = {};
                $('div.ma-filter-item.active').each(function () {
                    openedFilters.push($(this).attr('attribute-code'));
                    if ($(this).find('.ma-show-more').length > 0) {
                        if ($(this).find('.hide-more').length == 0) {
                            openedShowMore.push($(this).attr('attribute-code'));
                        }
                    }

                    if ($(this).find('.elma-layer-filter').length == 1) {
                        let filter = $(this).find('.elma-layer-filter').val();
                        if (filter !== "") {
                            layerFilters[$(this).attr('attribute-code')] = filter;
                        }
                    }

                });

                // opened categories
                var openedCategories = [];
                $('#ma-product-category-filter li.children.active').each(function () {
                    openedCategories.push($(this).attr('category-id'));
                });

                var totalWidth = $('#ma-grid-container').innerWidth();

                var totalHeight = $('#ma-grid-container').innerHeight();

                var itemWidth = this.itemWidth;
                var itemHeight = this.itemHeight;

                var rowItemsNumber = parseInt(totalWidth / itemWidth);
                var colItemsNumber = parseInt(totalHeight / itemHeight) + 2;

                this.searchTerm = value;
                this.paging.from = 0;
                this.paging.size = rowItemsNumber * colItemsNumber;

                var data = {
                    "store": this.options.storeCode,
                    "categoryId": -1,
                    "searchTerm": this.searchTerm,
                    "from": this.paging.from,
                    "size": this.paging.size,
                    "order": this.sorter.order,
                    "direction": this.sorter.direction,
                    "filters": this.filters,
                    "loadAggregations": loadAggregations,
                    "eaConfig": this.options.eaConfig,
                    "highlightEnabled": this.options.highlightEnabled,
                    "customerGroupId": this.options.customerGroupId
                };

                if ($("#ea-suggests-template").length == 1) {
                    data.suggest = true;
                }
                if ($("#ea-categories-template").length == 1) {
                    data.categories = true;
                }
                if ($("#ea-cms-template").length == 1) {
                    data.cms = true;
                }

                var hash = this.hash(JSON.stringify(data));
                var requestStartTime = Date.now();

                if (typeof this.cache[hash] !== "undefined") {
                    var data = this.cache[hash];
                    this.lastRequestData = data;
                    this.updateContent(data, openedFilters, openedShowMore, layerFilters, openedCategories, rowItemsNumber, fullStartTime, requestStartTime, hash);
                    this.element.removeClass('in-progress');

                    $('#search-smartphones').focus();
                } else {
                    this.xhr = $.ajax({
                        url: this.options.url,
                        method: "POST",
                        global: false,
                        data: data,
                        dataType: "json"
                    }).done(function (data) {
                        if (value === this.element.val()) {
                            _.each(data['products'], function (product) {
                                if (typeof product.configurable_index !== "undefined") {
                                    this.configurableIndex[product.id] = $.parseJSON(product.configurable_index);
                                    this.configurableImages[product.id] = $.parseJSON(product.configurable_images);
                                }
                            }.bind(this));
                            this.updateContent(data, openedFilters, openedShowMore, layerFilters, openedCategories, rowItemsNumber, fullStartTime, requestStartTime);
                            this.lastRequestData = data;
                            this.cache[hash] = data;
                            this.cache[hash].time = 0;
                            this.element.removeClass('in-progress');

                            $('#search-smartphones').focus();
                        }
                    }.bind(this));
                }
            } else {
                this._resetResponseList();

                this._updateAriaHasPopup(false);
                this.element.removeClass('in-progress');
            }
        },


        /**
         * Update the content of the products list and the filters (not infinite scroll)
         * @param data
         * @param openedFilters
         * @param openedCategories
         * @param fullStartTime
         * @param requestStartTime
         * @param cacheHash
         */
        updateContent: function (data, openedFilters, openedShowMore, layerFilters, openedCategories, rowItemsNumber, fullStartTime, requestStartTime, cacheHash) {

            this.noMoreInfiniteScrollToLoad = false;

            if (data.products.length < this.paging.size) {
                this.noMoreInfiniteScrollToLoad = true;
            }

            var htmlSuggests = "";
            if ($("#ea-suggests-template").length == 1 && typeof data.suggest != "undefined" && data.suggest.count > 0) {
                var suggests = data.suggest;
                htmlSuggests = mageTemplate("#ea-suggests-template", {
                    suggests: suggests.docs,
                    count: suggests.count,
                    enabled: 1,
                    title: this.options.eaConfig.general.labels.didyoumean,
                });
            }

            var htmlCategories = "";
            if ($("#ea-categories-template").length == 1 && typeof data.category != "undefined") {
                var categories = data.category;
                htmlCategories = mageTemplate("#ea-categories-template", {
                    categories: categories.docs,
                    count: categories.count,
                    enabled: 1,
                    title: this.options.eaConfig.general.labels.categories,
                    displayEmpty: this.options.eaConfig.category.display_empty_autocomplete
                });
            }

            var htmlCms = "";
            if ($("#ea-cms-template").length == 1 && typeof data.cms != "undefined") {
                var cms = data.cms;
                htmlCms = mageTemplate("#ea-cms-template", {
                    cms: cms.docs,
                    count: cms.count,
                    enabled: 1,
                    title: this.options.eaConfig.general.labels.cms,
                    displayEmpty: this.options.eaConfig.cms.display_empty_autocomplete
                });

            }
            var eaHtml = htmlSuggests + htmlCategories + htmlCms;

            if ((typeof data[0] !== "undefined" || data.products.length === 0) && _.isEmpty(this.filters)) {
                if (eaHtml == "") {
                    $('#no-product-message').html(this.options.noProductMessage);
                } else {
                    $('#no-product-message').html("");
                }
                $('#ea-alone').html(eaHtml);

                this._updateAriaHasPopup(true, true);
                var padding = parseInt($("#search_autocomplete-no-result").css("padding-left").replace("px", "")) + parseInt($("#search_autocomplete-no-result").css("padding-right").replace("px", ""));
                $("#search_autocomplete-no-result").css({
                    width: ($(this.element).innerWidth() - padding) + 'px'
                });
                $("#search_autocomplete").css({
                    height: '0px'
                });
                this.element.focus();

                return;
            } else {

                this._updateAriaHasPopup(true, false);
            }

            var requestTime = Date.now() - requestStartTime;
            var renderingStartTime = Date.now();

            // selected filters
            var selectedFilters = data['selectedFilters'];

            var filtersCount = _.size(selectedFilters);
            if (filtersCount > 0) {
                $('#filters-smartphones-count').html(filtersCount);
            } else {
                $('#filters-smartphones-count').html("");
            }

            // back to top of the grid
            $('#ma-grid-container').scrollTop(0);
            // empty the grid
            $('ul[data-role=ma-products-grid]').html();

            // grid html


            var productItems = "";
            if (eaHtml != "") {
                productItems += "<li class='item ea" + (data.products.length + 1 < rowItemsNumber ? " last" : "") + "'>" + htmlSuggests + htmlCategories + htmlCms + "</li>";
            }

            if (this.options.updateSearchTerm == 1) {
                $.ajax({
                    url: this.options.updateSearchTermUrl,
                    method: "POST",
                    global: false,
                    data: {"term": this.searchTerm, "num_results": data.amount.total},
                    dataType: "json"
                }).done(function (data) {
                }.bind(this));
            }


            productItems += mageTemplate(this.templateSelectors.productsGrid, {
                "products": data.products,
                "displayReviews": this.options.displayReviews,
                "displayAddToCart": this.options.displayAddToCart,
                "displayAddToCompare": this.options.displayAddToCompare,
                "displayAddToWishlist": this.options.displayAddToWishlist,
                "displayEmailToAFriend": this.options.displayEmailToAFriend,
                "displayConfigurableOptions": this.options.displayConfigurableOptions,
                "itemsPerRow": eaHtml != "" ? rowItemsNumber : -1,
                "customerGroupId": this.options.customerGroupId
            });

            // products count
            $(this.contentSelectors.productsCount).html(this.options.labels.nb_products_found.replace("{{nbItems}}", data.amount.total));

            var loadedItems = data.products.length + (eaHtml != "" ? 1 : 0);

            if (this.noMoreInfiniteScrollToLoad) {
                productItems += "<li class='item no-more'>" + this.options.labels.no_more_product + "</li>";
                loadedItems++;
            }

            // complete the grid with empty items if needed
            while (loadedItems % rowItemsNumber !== 0) {
                productItems += "<li class='item empty'></li>";
                loadedItems++;
            }

            // render the grid
            $('ul[data-role=ma-products-grid]').html(productItems);


            // filters
            var filters = data['aggregations'];

            if (!_.isEmpty(filters)) {
                var initPriceSlider = false;
                var min = 0;
                var max = 0;
                var valMin = 0;
                var valMax = 0;

                // empty the filters
                $('div[data-role=ma-products-layer]').html("<span class='back'></span>");
                // for each filter to display
                _.each(this.options.filterableAttributes, function (attributeInfo) {

                    var attributeCode = attributeInfo[0];
                    var showResultsCount = attributeInfo[1];
                    var showMoreLess = attributeInfo[2];
                    var showMoreLessNbValues = attributeInfo[3];
                    var showLayerFilter = attributeInfo[4];
                    var closed = attributeInfo[5];

                    var code = "";
                    if (typeof filters[attributeCode] !== "undefined") {
                        code = attributeCode;
                    } else if (typeof filters[attributeCode + "_ids"] !== "undefined") {
                        code = attributeCode + "_ids";
                    }
                    if (code !== "") {

                        var filter = filters[code];
                        var attributeFilter = "";

                        var goahead = 0;

                        // filter PRICE
                        if (code === "final_price") {
                            if (filter['min'] != filter['max']) {
                                attributeFilter = mageTemplate(this.templateSelectors.pricesFilter, {
                                    "min": filter['min'],
                                    "max": filter['max'],
                                    "valMin": filter['values']['min'],
                                    "valMax": filter['values']['max'],
                                    "displayInputBoxesForPriceFilter": this.options.displayInputBoxesForPriceFilter
                                });
                                min = filter['min'];
                                max = filter['max'];
                                valMin = filter['values']['min'];
                                valMax = filter['values']['max'];
                                initPriceSlider = true;
                            } else {
                                attributeFilter = "";
                            }
                            // Ratings
                        } else if (code === "rating") {
                            _.each(filter, function (count, rating) {
                                goahead += count;
                            });
                            if (goahead > 0 || this.options.layerUpdateMethod > 0) {
                                attributeFilter = mageTemplate(this.templateSelectors.ratingsFilter, {
                                    "ratings": filter,
                                    "selected": (typeof selectedFilters[code] !== "undefined") ? selectedFilters[code]['values'] : [],
                                    "showResultsCount": showResultsCount,
                                    "closed": closed
                                });
                            } else {
                                attributeFilter = "";
                            }

                            // categories
                        } else if (code === "categories") { // filter header + first treeview level
                            _.each(filter, function (category) {
                                goahead += ((category.count == -1) ? 0 : category.count);
                            });
                            if (goahead > 0 || this.options.layerUpdateMethod > 0) {
                                attributeFilter = mageTemplate(this.templateSelectors.categoryFilter, {
                                    "showResultsCount": showResultsCount,
                                    "closed": closed
                                });
                            } else {
                                attributeFilter = "";
                            }
                        } else {
                            _.each(filter.values, function (data, optionId) {
                                goahead += data.count;
                            });
                            if (goahead > 0 || this.options.layerUpdateMethod > 0) { // layerUpdateMethod > 0 => visible and disabled
                                // Text Swatch
                                if (filter.textSwatch) {
                                    attributeFilter = mageTemplate(this.templateSelectors.textSwatchFilter, {
                                        "filter": filter,
                                        "attributeCode": code,
                                        "selected": (typeof selectedFilters[code] !== "undefined") ? selectedFilters[code]['values'] : [],
                                        "showResultsCount": showResultsCount,
                                        "showMoreLess": showMoreLess,
                                        "showMoreLessNbValues": showMoreLessNbValues,
                                        "showLayerFilter": showLayerFilter
                                    });

                                    // Visual Swatch
                                } else if (filter.visualSwatch) {
                                    attributeFilter = mageTemplate(this.templateSelectors.visualSwatchFilter, {
                                        "filter": filter,
                                        "attributeCode": code,
                                        "selected": (typeof selectedFilters[code] !== "undefined") ? selectedFilters[code]['values'] : [],
                                        "showResultsCount": showResultsCount,
                                        "showMoreLess": showMoreLess,
                                        "showMoreLessNbValues": showMoreLessNbValues,
                                        "showLayerFilter": showLayerFilter
                                    });

                                    // all other filters
                                } else {
                                    attributeFilter = mageTemplate(this.templateSelectors.attributeFilter, {
                                        "filter": filter,
                                        "attributeCode": code,
                                        "selected": (typeof selectedFilters[code] !== "undefined") ? selectedFilters[code]['values'] : [],
                                        "showResultsCount": showResultsCount,
                                        "showMoreLess": showMoreLess,
                                        "showMoreLessNbValues": showMoreLessNbValues,
                                        "showLayerFilter": showLayerFilter
                                    });
                                }
                            } else {
                                attributeFilter = "";
                            }
                        }

                        // append the filter block
                        $(this.contentSelectors.filters).append(attributeFilter);

                        $('div.ma-filter-item[attribute-code=' + code + ']').removeClass('active');

                        // open previously opened filters
                        if (_.contains(openedFilters, code) || (this.firstLoad === true && closed != "1")) {
                            $('div.ma-filter-item[attribute-code=' + code + ']').addClass('active');
                        }

                        _.each(openedShowMore, function (attributeCode, id) {
                            $("[attribute-code=" + attributeCode + "]").find('.hide-more').removeClass('hide-more');
                        });


                        _.each(layerFilters, function (term, attributeCode) {
                            let container = $("[attribute-code=" + attributeCode + "]");
                            container.find('.elma-layer-filter').val(term);
                            container.find('.swatch-attribute').removeClass('hide-more');
                            container.find('.ma-show-more, .ma-show-less').css({'display': 'none'});
                            container.find('a div:not([data-filter*=' + term + '])').parent().addClass('hidden');
                            container.find('a div[data-filter*=' + term + ']').parent().removeClass('hidden');
                        });

                        // if (this.firstLoad === true) {
                        //     if (closed) {
                        //         $('div.ma-filter-item[attribute-code=' + code + ']').removeClass('active');
                        //     }
                        // }

                        // post-process : categories
                        if (code === "categories") {
                            if (goahead > 0 || this.options.layerUpdateMethod > 0) {
                                var values = [];
                                if ((typeof selectedFilters["categories_ids"] !== "undefined")) {
                                    values = _.keys(selectedFilters["categories_ids"]['values']);
                                }
                                _.each(filter, function (category) {
                                    var categoryContent = mageTemplate(this.templateSelectors.categoryItemFilter, {
                                        "category": category,
                                        "selected": _.contains(values, category.id),
                                        "showResultsCount": showResultsCount
                                    });
                                    var parent = $("ul#ma-category-" + category.parent);
                                    if (parent.length === 0) {
                                        $("ul#ma-product-category-filter").append(categoryContent);
                                    } else {
                                        parent.append(categoryContent);
                                    }
                                }.bind(this));
                            }
                        }

                        // post-process : prices
                        if (initPriceSlider) {
                            this.prices = {min: parseFloat(min), max: parseFloat(max)};
                            this.initPriceSlider(valMin, valMax);
                        }
                    }
                }.bind(this));

                // re-open categories
                _.each(openedCategories, function (categoryId) {
                    $('#ma-product-category-filter li.children[category-id=' + categoryId + ']').addClass('active');
                });
            }

            // breadcrumb
            if ($(this.contentSelectors.breadcrumb).length != 0) {
                var breadcrumb = mageTemplate(this.templateSelectors.breadcrumb, {
                    "filters": selectedFilters
                });
                $(this.contentSelectors.breadcrumb).html(breadcrumb);
            }

            if (selectedFilters.length > 0 || _.size(selectedFilters) > 0) {
                $('.ma-layer-clear-all').removeClass('hide');
            } else {
                $('.ma-layer-clear-all').addClass('hide');
            }


            if ($("#ma-debug-template").length >= 1) {
                var fullEndTime = Date.now();
                var debug = mageTemplate("#ma-debug-template", {
                    "phpTime": data.time,
                    "requestTime": requestTime,
                    "renderingTime": (fullEndTime - renderingStartTime),
                    "totalTime": (fullEndTime - fullStartTime),
                    "fromCache": typeof cacheHash !== "undefined",
                    "cacheHash": cacheHash,
                    "infiniteScroll": true,
                    "templateHintsEnabled": this.templateHintsEnabled
                });
                $('#ma-debug').html(debug);
                this.toggleTemplateHints(this.templateHintsEnabled);
            }

            this.firstLoad = false;


            this.updateFormKey();
        },

        /**
         * Executes when the value of the search input field changes. Executes a GET request
         * to populate a suggestion list based on entered text. Handles click (select), hover,
         * and mouseout events on the populated suggestion list dropdown.
         * @private
         */
        _onPropertyChange: function (evt) {
            this.loadProducts(true, true);

        },


        ////////////////////////////////////////////////////////////////////////
        // STANDARD AUTOCOMPLETE FUNCTIONS
        ////////////////////////////////////////////////////////////////////////

        /**
         * @private
         * @return {Element} The current element in the suggestion list.
         */
        _current: function () {
            return this.responseList.indexList
                ? $(this.responseList.indexList[this.responseList.currentIndex])
                : null;
        },
        /**
         * @private
         */
        _updateCurrent: function () {
            var selectClass = this.options.selectClass;
            this.responseList.indexList.removeClass(selectClass);
            var current = this._current();
            if (current) {
                current.addClass(selectClass);

                this._updateAriaHasPopup(true);
            }
        },
        /**
         * @private
         */
        _previous: function () {
            if (this.responseList.indexList) {
                var currentIndex = this.responseList.currentIndex;
                var listLength = this.responseList.indexList.length;
                if (--currentIndex < 0) {
                    currentIndex = listLength - 1;
                }
                this.responseList.currentIndex = currentIndex;
                this._updateCurrent();
            }
        },
        /**
         * @private
         */
        _next: function () {
            if (this.responseList.indexList) {
                var currentIndex = this.responseList.currentIndex;
                var listLength = this.responseList.indexList.length;
                if (null === currentIndex || ++currentIndex >= listLength) {
                    currentIndex = 0;
                }
                this.responseList.currentIndex = currentIndex;
                this._updateCurrent();
            }
        },
        /**
         * @private
         */
        _first: function () {
            if (this.responseList.indexList) {
                this.responseList.currentIndex = 0;
                this._updateCurrent();
            }
        },
        /**
         * @private
         */
        _last: function () {
            if (this.responseList.indexList) {
                var listLength = this.responseList.indexList.length;
                this.responseList.currentIndex = listLength - 1;
                this._updateCurrent();
            }
        },
        /**
         * @private
         * @param {Boolean} show Set attribute aria-haspopup to "true/false" for element.
         */
        _updateAriaHasPopup: function (show, noResult) {
            if (show) {
                this.element.attr('aria-haspopup', 'true');

                if (typeof noResult != "undefined" && noResult === true) {
                    $("body").addClass("ma-no-result").removeClass("ma-autocomplete");

                }
                else {
                    $("body").removeClass("ma-no-result").addClass("ma-autocomplete");

                }
                $('#search-smartphones').val(this.element.val());
                $('#search-smartphones').focus();
            } else {

                if (this.xhr === undefined || this.xhr.readyState == 4 || this.xhr.readyState == 0
                ) {
                    this.element.attr('aria-haspopup', 'false');
                    if ($('body').hasClass("ma-autocomplete")) {
                        this.element.focus();
                    }
                    $("body").removeClass("ma-autocomplete").removeClass("ma-no-result");
                    $("#search_autocomplete").css({display: "none"});
                }
            }

        },
        /**
         * Clears the item selected from the suggestion list and resets the suggestion list.
         * @private
         */
        _resetResponseList: function () {
            this.responseList.indexList = null;
            this.responseList.currentIndex = null;
        },

        /**
         * @private
         * @param {Event} e - The key down event
         * @return {Boolean} Default return type for any unhandled keys
         */
        _onKeyUp: function (e) {
            $('#search-smartphones').val(this.element.val());
            if (this.element.val() === "") {
                this._updateAriaHasPopup(false, false);
            }
        },
        /**
         * Executes when keys are pressed in the search input field. Performs specific actions
         * depending on which keys are pressed.
         * @private
         * @param {Event} e - The key down event
         * @return {Boolean} Default return type for any unhandled keys
         */
        _onKeyDown: function (e) {
            $('#search-smartphones').val(this.element.val());
            var keyCode = e.keyCode || e.which;

            switch (keyCode) {
                case $.ui.keyCode.ESCAPE:

                    this._updateAriaHasPopup(false);

                    break;
                case $.ui.keyCode.ENTER:
                    var current = this._current();
                    if (current) {
                        var links = current.find('a');
                        if (links.length) {
                            location.href = links[0].href;

                            this._updateAriaHasPopup(false);

                        }
                    } else {

                        this._updateAriaHasPopup(false);

                        this.autoComplete.find("input").remove();
                        this.searchForm.trigger('submit');
                    }
                    break;

                default:

                    return true;
            }
        },


        //######################################################################
        // GENERAL TOOLS
        //######################################################################
        /**
         * Get a unique hash of a string
         * @param string
         * @returns {number}
         */
        hash: function (string) {
            var hash = 0;
            if (string.length === 0) {
                return hash;
            }
            for (var i = 0; i < string.length; i++) {
                var char = string.charCodeAt(i);
                hash = ((hash << 5) - hash) + char;
                hash = hash & hash; // Convert to 32bit integer
            }
            return hash;
        },


        //######################################################################
        // DEBUG TOOLS
        //######################################################################

        /**
         * Enable the template hints
         * @param on true => enable, false => disable
         */
        toggleTemplateHints: function (on) {
            // add a hint for all Underscore.js template
            _.each(this.templateSelectors, function (elt, id) {
                // remove all hints
                $(elt + "-th").remove();
                if (on) { // enabling
                    var div = $("<div>").addClass("ma-template-hint").addClass("template-hint").attr('id', elt.replace("#", "") + "-th");
                    div.html("<a name='" + id.toUpperCase() + "'>" + id.toUpperCase() + "</a><br/>elt: " + elt + "<br/>file: " + this.contentTemplateFiles[id]);
                    // the template dom elt is a script node => adding the hint before the node
                    div.insertBefore($(elt));
                }
            }.bind(this));

            // add a hint for all blocks updated using uUnderscore.js template
            _.each(this.contentSelectors, function (elt, id) {
                // remove all hints
                $(elt + " > .ma-template-hint").remove();
                if (on) { // enabling template hints
                    this.templateHintsEnabled = true;
                    $('#ma-debug-disable-template-hints').show();
                    $('#ma-debug-enable-template-hints').hide();
                    var div = $("<div>").addClass("ma-template-hint");
                    if (typeof this.templateSelectors[id] != "undefined") {
                        // an underscore.js template exists => add an anchor link to the hint of the underscore.js template
                        div.html(id.toUpperCase() + "<br/>elt: " + elt + "<br/>_tpl: <a onClick='jQuery(\".template-hint.selected\").removeClass(\"selected\");jQuery(\"" + this.templateSelectors[id] + "-th\").addClass(\"selected\")' href='#" + id.toUpperCase() + "'>" + this.templateSelectors[id] + "</a>");
                    } else {
                        div.html(id.toUpperCase() + "<br/>elt: " + elt + "<br/>phtml: " + this.contentTemplateFiles[id]);
                    }
                    $(elt).prepend(div);
                    $(elt).addClass("ma-template-hint-container");
                } else { // disabling templateh hints
                    this.templateHintsEnabled = false;
                    // remove hint class to the container
                    $(elt).removeClass("ma-template-hint-container");
                    $('#ma-debug-disable-template-hints').hide();
                    $('#ma-debug-enable-template-hints').show();
                }
            }.bind(this));
        },
        getCookie: function (cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        },

        updateFormKey: function () {
            let formKey = this.getCookie("form_key");
            $('input[name=form_key]').val(formKey);
            var as = $('a[href*=\\/form_key\\/]');
            as.each(function (i) {
                let link = jQuery(as[i]);
                let href = link.attr('href').replace(/form_key\/[^\/]+\//, "form_key/" + formKey + "/");
                link.attr("href", href);

            });
        }

    });

    return $.mage.wyoema;
});
