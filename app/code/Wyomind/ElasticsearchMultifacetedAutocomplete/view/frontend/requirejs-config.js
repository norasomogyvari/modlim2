/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * https://www.wyomind.com
 */
var config = {
    map: {
        '*': {
            'wyoema': 'Wyomind_ElasticsearchMultifacetedAutocomplete/js/form-mini'
        }
    }
}; 
