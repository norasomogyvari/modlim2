<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\ElasticsearchCore\Block\Adminhtml\System\Config\Form\Field;


/**
 * Class FilterOptionsSortingMethod
 * @package Wyomind\ElasticsearchCore\Block\Adminhtml\System\Config\Form\Field
 */
class FilterOptionsSortingMethod implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 1, 'label' => __('Magento native sort')], ['value' => 2, 'label' => __('Alphabetical')], ['value' => 3, 'label' => __('Results count')]];
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [1 => __('Magento native sort'), 2 => __('Alphabetical'), 3 => __('Results count')];
    }
}