<?php
/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\ElasticsearchCore\Model;

class ToReindex extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Wyomind\ElasticsearchCore\Model\ResourceModel\ToReindex');
    }

    /**
     * The indexer ID is the unique identifier for this collection
     *
     * @param string $indexerId
     *
     * @return \Wyomind\ElasticsearchCore\Model\ResourceModel\ToReindex\Collection
     */
    public function loadByIndexerId($indexerId)
    {
        $collection = $this->getCollection()->addFieldToFilter(
            'indexer_id',
            ['eq' => $indexerId]
        );

        return $collection;
    }

    /**
     * @param $indexerId
     * @param $entityId
     *
     * @return \Magento\Framework\DataObject
     */
    public function getEntityByIndexerId($indexerId, $entityId)
    {
        return $this->getCollection()->addFieldToFilter(
            'indexer_id',
            ['eq' => $indexerId]
        )->addFieldToFilter(
            'to_reindex',
            ['eq' => $entityId]
        )->getFirstItem();
    }

    /**
     * Get the id to reindex and the last entry created_at
     *
     * @param string          $indexerId
     * @param null|\DateTime $dateTime
     *
     * @return array
     */
    public function getIndexerLastEntries($indexerId, $dateTime = null)
    {
        $collection = $this->loadByIndexerId($indexerId)
            ->addFieldToSelect('to_reindex')
            ->addFieldToSelect(
                new \Zend_Db_Expr('MAX(created_at) AS last_entry')
            )->addFieldToSelect(
                new \Zend_Db_Expr('COUNT(id) AS counter')
            );

        if (null !== $dateTime) {
            $collection->getSelect()
                ->where('DATE(created_at) < ?', $dateTime);
        }

        return $collection->getSelect()
            ->group('to_reindex')
            ->order('created_at ASC')
            ->query()
            ->fetchAll();
    }

    /**
     * @param string $indexerId
     *
     * @return int
     */
    public function getCountOfEntries($indexerId)
    {
        return $this->loadByIndexerId($indexerId)->getSize();
    }

    /**
     * Delete all entries related to an indexer type
     *
     * @param string         $indexerId
     * @param int[]          $ids
     * @param null|\DateTime $dateTime
     */
    public function deleteIndexerToReindex(
        $indexerId,
        $ids = [],
        $dateTime = null
    ) {
        $this->getResource()->deleteMultiple($indexerId, $ids, $dateTime);
    }

    public function deleteEntityToReindex($indexerId, $id)
    {
        $this->deleteIndexerToReindex($indexerId, [$id]);
    }

    /**
     * @param string $indexerId
     * @param int[]  $ids
     */
    public function insertMultiple($indexerId, $ids)
    {
        $this->getResource()->insertMultiple($indexerId, $ids);
    }
}
