<?php
/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\ElasticsearchCore\Model\ResourceModel;

class ToReindex extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('wyomind_elasticsearchcore_to_reindex', 'id');
    }

    /**
     * @param string $indexerId
     * @param int[]  $ids
     */
    public function insertMultiple($indexerId, $ids)
    {
        $this->getConnection()->beginTransaction();
        foreach ($ids as $id) {
            try {
                $this->getConnection()->insert(
                    $this->getMainTable(),
                    ['indexer_id' => $indexerId, 'to_reindex' => $id]
                );
            } catch (\Exception $e) {
            }
        }
        $this->commit();
    }

    /**
     * @param string           $indexerId
     * @param int[]            $ids
     * @param null | \DateTime $dateTime
     */
    public function deleteMultiple($indexerId, $ids, $dateTime = null)
    {
        try {
            $wheres = [
                $this->getConnection()->prepareSqlCondition(
                    'indexer_id',
                    ['eq' => $indexerId]
                ),
                $this->getConnection()->prepareSqlCondition(
                    'to_reindex',
                    ['in' => $ids]
                ),
            ];

            if (null !== $dateTime) {
                $wheres[] = $this->getConnection()->prepareSqlCondition(
                    'created_at',
                    ['lt' => $dateTime]
                );
            }

            $this->getConnection()->delete(
                $this->getMainTable(),
                join(' AND ', $wheres)
            );
        } catch (\Exception $e) {
        }
    }
}
