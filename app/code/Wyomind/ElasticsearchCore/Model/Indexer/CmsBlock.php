<?php
/**
 * Copyright © 2020 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\ElasticsearchCore\Model\Indexer;

class CmsBlock extends AbstractIndexer
{
    /**
     * @var string
     */
    public $type = 'cms_block';

    /**
     * @var string
     */
    public $name = 'Cms Blocks';

    /**
     * @var string
     */
    public $comment = 'CMS block indexer';

    /**
     * @var array
     */
    protected $allowedTypes = [
        'char', 'varchar', 'tinytext', 'text', 'mediumtext', 'longtext',
    ];

    protected $_searchableAttributes = [];

    /**
     * @var array
     */
    protected $_blocks = [];

    /**
     * @param \Wyomind\ElasticsearchCore\Model\Indexer\Context $context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    public function export($storeId, $ids = [])
    {
        $this->handleLog('');
        $this->handleLog(
            '<comment>' . __(
                'Indexing cms blocks for store id: '
            ) . $storeId . '</comment>'
        );

        $this->_eventManager->dispatch(
            'wyomind_elasticsearchcore_cms_block_export_before',
            [
                'store_id' => $storeId,
                'ids'      => $ids,
            ]
        );

        $this->_blocks = [];
        $attributesConfig = [];
        $attributes = $this->_configHelper->getEntitySearchableAttributes(
            $this->type,
            $storeId
        );

        foreach ($attributes as $attributeCode => $attributeInfo) {
            if ($attributeInfo['c'] == "1") {
                $attributesConfig[] = $attributeCode;
            }
        }

        $collection = $this->createBlockCollection()->addStoreFilter($storeId);

        // Searchable attributes
        if (count(array_filter($attributesConfig)) != 0) {
            $collection->addFieldToSelect($attributesConfig);
        }

        // Specific documents
        if (false === empty($ids)) {
            $collection->addFieldToFilter('block_id', ['in' => $ids]);
        }

        // Excluded blocks
        $excluded = explode(
            ',',
            $this->_configHelper->getStoreConfig(
                'wyomind_elasticsearchcore/types/cms_block/excluded_blocks',
                $storeId
            )
        );

        if ($excluded) {
            $collection->addFieldToFilter('block_id', ['nin' => $excluded]);
        }
        // Only active blocks
        $collection->addFieldToFilter(
            'is_active',
            \Magento\Cms\Model\Block::STATUS_ENABLED
        );

        $this->handleLog(
            '<info>' . count($collection) . ' cms blocks found</info>'
        );
        /** @var \Magento\Cms\Model\Block $block */
        foreach ($collection as $block) {
            $block->setContent(html_entity_decode($block->getContent()));
            $this->_blocks[$block->getId()] = array_merge(
                ['id' => (int)$block->getId()],
                $block->toArray($attributesConfig)
            );

            if (isset($this->_blocks[$block->getId()]['title'])) {
                $this->_blocks[$block->getId(
                )]['title_suggester'] = $this->_blocks[$block->getId(
                )]['title'];
            }
        }

        $this->_eventManager->dispatch(
            'wyomind_elasticsearchcore_cmsblock_export',
            [
                'store_id' => $storeId,
                'indexer'  => $this,
            ]
        );

        $this->handleLog(
            '<info>' . count($collection) . __(
                ' cms blocks indexed'
            ) . '</info>'
        );

        yield $this->_blocks;

        $this->_eventManager->dispatch(
            'wyomind_elasticsearchcore_cms_block_export_after',
            [
                'store_id' => $storeId,
                'ids'      => $ids,
            ]
        );
    }

    /**
     * @param array $blocks
     *
     * @return $this
     */
    public function setBlocks($blocks)
    {
        $this->_blocks = $blocks;

        return $this;
    }

    /**
     * @return array
     */
    public function getBlocks()
    {
        return $this->_blocks;
    }

    /**
     * @return Magento\Cms\Model\ResourceModel\Block\Collection
     */
    protected function createBlockCollection()
    {
        return $this->_objectManager->create(
            \Magento\Cms\Model\ResourceModel\Block\Collection::class
        );
    }

    protected function getSearchableAttributes($store)
    {
        if (!isset($this->_searchableAttributes[$store])) {
            $this->_searchableAttributes[$store] = [];
            $atts = $this->_configHelper->getEntitySearchableAttributes(
                $this->type,
                $store
            );

            foreach ($atts as $attributeCode => $attributeInfo) {
                if ($attributeInfo['c'] === "1") {
                    if ($attributeInfo['b'] == "varchar" || $attributeInfo['b'] == "text") {
                        $attributeInfo['b'] = "string";
                    }
                    $this->_searchableAttributes[$store][$attributeCode] = $attributeInfo;
                }
            }
        }

        return $this->_searchableAttributes[$store];
    }

    /**
     * {@inheritdoc}
     */
    public function getProperties($store = null, $withBoost = false)
    {
        $data = [];
        $charType = 'text';

        $compatibility = $this->_adapter->getConfigHelper()->getCompatibility(
            $store
        );
        $searchableAttributes = $this->getSearchableAttributes($store);

        foreach ($searchableAttributes as $attributeCode => $attributeInfo) {
            $field = $attributeCode;
            if ($compatibility >= 6) {
                $data[$field] = [
                    'type'     => 'text',
                    'analyzer' => $this->getLanguageAnalyzer($store),
                    'copy_to'  => 'all',
                ];
            } elseif ($compatibility < 6) {
                $data[$field] = [
                    'type'           => 'string',
                    'analyzer'       => $this->getLanguageAnalyzer($store),
                    'include_in_all' => true,
                ];

                $charType = 'string';
            }

            $data[$field]['fields']['prefix'] = [
                'type'            => $charType,
                'analyzer'        => 'text_prefix',
                'search_analyzer' => 'std',
            ];
            $data[$field]['fields']['suffix'] = [
                'type'            => $charType,
                'analyzer'        => 'text_suffix',
                'search_analyzer' => 'std',
            ];


            if ($field == 'title') {
                $data[$field . '_suggester'] = [
                    'type'            => 'completion',
                    'analyzer'        => 'std',
                    'search_analyzer' => 'std',
                ];
            }
        }

        if ($compatibility >= 6) {
            $data['all'] = ['type' => 'text'];
        }

        $data['id'] = ['type' => 'long'];

        $properties = new \Magento\Framework\DataObject($data);

        $this->_eventManager->dispatch(
            'wyomind_elasticsearchcore_cms_block_index_properties',
            [
                'indexer'    => $this,
                'store'      => $store,
                'properties' => $properties,
            ]
        );

        return $properties->getData();
    }

    /**
     * {@inheritdoc}
     */
    public function getDynamicConfigGroups()
    {
        $dynamicConfigFields['enable'] = [
            'id'            => 'enable',
            'translate'     => 'label comment',
            'type'          => 'select',
            'sortOrder'     => '10',
            'showInDefault' => '1',
            'showInWebsite' => '1',
            'showInStore'   => '1',
            'label'         => __('Enable CMS Block Index'),
            'source_model'  => 'Magento\Config\Model\Config\Source\Yesno',
            'comment'       => __(
                'If enabled, CMS blocks will be indexed in Elasticsearch.'
            ),
            '_elementType'  => 'field',
            'path'          => 'wyomind_elasticsearchcore/types/cms_block',
        ];


        // Indexable attributes
        $dynamicConfigFields['attributes'] = [
            'id'             => 'attributes',
            'translate'      => 'label comment',
            'type'           => 'hidden',
            'sortOrder'      => '30',
            'showInDefault'  => '1',
            'showInWebsite'  => '1',
            'showInStore'    => '1',
            'label'          => 'Attributes to index',
            '_elementType'   => 'field',
            'frontend_model' => 'Wyomind\ElasticsearchCore\Block\Adminhtml\System\Config\Form\Field\CmsBlockAttributes',
            'path'           => 'wyomind_elasticsearchcore/types/cms_block',
            'depends'        => [
                'fields' => [
                    'enable' => [
                        'id'           => 'wyomind_elasticsearchcore/types/cms_block/enable',
                        'value'        => '1',
                        '_elementType' => 'field',
                        'dependPath'   => [
                            0 => 'wyomind_elasticsearchcore',
                            1 => 'types',
                            2 => 'cms_block',
                            3 => 'enable',
                        ],
                    ],
                ],
            ],
        ];

        $dynamicConfigFields['excluded_blocks'] = [
            'id'            => 'excluded_blocks',
            'translate'     => 'label comment',
            'type'          => 'multiselect',
            'sortOrder'     => '60',
            'showInDefault' => '1',
            'showInWebsite' => '1',
            'showInStore'   => '1',
            'can_be_empty'  => true,
            'label'         => __('Excluded Blocks'),
            'source_model'  => 'Wyomind\ElasticsearchCore\Model\Indexer\CmsBlock::excludedBlocks',
            'comment'       => __('Selected CMS blocks will be excluded.'),
            'depends'       => [
                'fields' => [
                    'enable' => [
                        'id'           => 'wyomind_elasticsearchcore/types/cms_block/enable',
                        'value'        => '1',
                        '_elementType' => 'field',
                        'dependPath'   => [
                            0 => 'wyomind_elasticsearchcore',
                            1 => 'types',
                            2 => 'cms_block',
                            3 => 'enable',
                        ],
                    ],
                ],
            ],
            '_elementType'  => 'field',
            'path'          => 'wyomind_elasticsearchcore/types/cms_block',
        ];

        $dynamicConfigGroups['cms_block'] = [
            'id'            => 'cms_block',
            'translate'     => 'label',
            'sortOrder'     => '60',
            'showInDefault' => '1',
            'showInWebsite' => '0',
            'showInStore'   => '1',
            'label'         => 'CMS Block',
            'children'      => $dynamicConfigFields,
            '_elementType'  => 'group',
            'path'          => 'wyomind_elasticsearchcore/types',
        ];

        return $dynamicConfigGroups;
    }

    /**
     * {@inheritdoc}
     */
    public function getEvents()
    {
        return [
            'cms_block_save_after'                                   => [
                [
                    'indexer' => $this->type, 'action' => 'executeRow',
                ],
            ],
            'cms_block_delete_after'                                 => [
                [
                    'indexer' => $this->type, 'action' => 'deleteRow',
                ],
            ],
            'wyomind_elasticsearchcore_full_reindex_after_cms_block' => [
                [
                    'indexer' => $this->type, 'action' => 'reindex',
                ],
            ],
        ];
    }

    public function excludedBlocks()
    {
        $options = [];

        $collection = $this->createBlockCollection();
        foreach ($collection as $block) {
            $options[] = [
                'value' => $block->getId(),
                'label' => $block->getTitle(),
            ];
        }

        return $options;
    }

    public function getBrowseColumns($storeId)
    {
        $columns = parent::getBrowseColumns($storeId);
        if (isset($columns['content'])) {
            $columns['content']['arguments']['data']['config']['bodyTmpl'] = 'Wyomind_ElasticsearchCore/listing/browse/bightml';
        }

//        $columns = [];
//
//        $columns['image'] = $this->addBrowseColumn('image', 'image', 'Image', 20);
//        $columns['sku'] = $this->addBrowseColumn('html', 'sku', 'Sku', 30);
//        $columns['name'] = $this->addBrowseColumn('html', 'name', 'Product Name', 40);
//        $columns['url'] = $this->addBrowseColumn('url', 'url', 'Url', 50);
//        $columns['type_id'] = $this->addBrowseColumn('html', 'type_id', 'Type', 60);
//        $columns['visibility'] = $this->addBrowseColumn('html', 'visibility', 'Visibility', 70);
//        $columns['price'] = $this->addBrowseColumn('price', 'price', 'Price', 80);

        return $columns;
    }

    public function getBrowseActions(&$item, $name)
    {
        $item[$name]['edit'] = [
            'href'  => $this->_urlBuilder->getUrl(
                'cms/block/edit',
                ['block_id' => $item['id']]
            ),
            'label' => __('Edit'),
        ];
    }

    public function execute($ids)
    {
        $storeIds = array_keys($this->_storeManager->getStores());
        foreach ($storeIds as $storeId) {
            $documents = $this->export($storeId, $ids);
            $this->_adapter->addDocs($documents, $this->type, false, $storeId);
        }
    }
}
