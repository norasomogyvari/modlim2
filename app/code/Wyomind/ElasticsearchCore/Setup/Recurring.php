<?php
/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\ElasticsearchCore\Setup;

class Recurring implements \Magento\Framework\Setup\InstallSchemaInterface
{
    /**
     * @var \Symfony\Component\Console\Output\ConsoleOutput
     */
    public $output = null;

    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList $directoryList
     */
    protected $_directoryList = null;

    /**
     * @var string
     */
    public $magentoVersion = '';
    /**
     * @var \Wyomind\Core\Helper\Data
     */
    protected $coreHelper;

    /**
     * @param \Symfony\Component\Console\Output\ConsoleOutput $output
     * @param \Magento\Framework\App\Filesystem\DirectoryList $directoryList
     * @param \Magento\Framework\App\ProductMetadata $productMetaData
     * @param \Wyomind\Core\Helper\Data $coreHelper
     */
    public function __construct(
        \Symfony\Component\Console\Output\ConsoleOutput $output,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\App\ProductMetadata $productMetaData,
        \Wyomind\Core\Helper\Data $coreHelper
    )
    {
        $this->output = $output;
        $this->_directoryList = $directoryList;
        $explodedVersion = explode('-', $productMetaData->getVersion()); // remove all after "-" (eg: 2.2.3-beta => 2.2.3)
        $this->magentoVersion = $explodedVersion[0];
        $this->coreHelper = $coreHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function install(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    )
    {
        $this->updateAutocompleteFile();
        $this->copyAutocompleteFile();
        $this->copyFilesByMagentoVersion();
    }

    public function updateAutocompleteFile()
    {
        $files = [
            "elastic.php" => [
                "from" => "// @require", "to" => "require"
            ],
            "pub/elastic.php" => [
                "from" => "// @include", "to" => "include"
            ]
        ];
        $this->coreHelper->replaceInFiles(__FILE__, $files);
    }

    public function copyAutocompleteFile()
    {
        $this->output->writeln('');
        $this->output->writeln('<comment>Copying the elastic.php file</comment>');

        $path = str_replace('Setup' . DIRECTORY_SEPARATOR . 'Recurring.php', '', __FILE__);
        $file = 'elastic.php';
        $rootPath = $this->_directoryList->getRoot();

        copy($path . DIRECTORY_SEPARATOR . $file, $rootPath . DIRECTORY_SEPARATOR . $file);
        copy($path . DIRECTORY_SEPARATOR . 'pub' . DIRECTORY_SEPARATOR . $file, $rootPath . DIRECTORY_SEPARATOR . 'pub' . DIRECTORY_SEPARATOR . $file);
    }

    public function copyFilesByMagentoVersion()
    {
        $files = [
            'Block/Product/Renderer/Configurable.php',
            'Model/Product/Type/Configurable.php',
            'Ui/Model/Manager.php',
            'etc/adminhtml/di.xml',
            'view/base/web/js/jquery.touch.min.js'
        ];


        $this->coreHelper->copyFilesByMagentoVersion(__FILE__, $files);
    }
}