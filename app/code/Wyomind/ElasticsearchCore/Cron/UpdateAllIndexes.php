<?php
/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\ElasticsearchCore\Cron;

use Wyomind\ElasticsearchCore\Model\Indexer\Product as ProductIndexer;

/**
 * Class UpdateAllIndexes
 */
class UpdateAllIndexes
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_coreDate = null;

    /**
     * @var \Wyomind\ElasticsearchCore\Helper\IndexerFactory
     */
    protected $_indexerHelperFactory;

    /**
     * @var \Wyomind\ElasticsearchCore\Model\Index
     */
    protected $_indexModel = null;

    /**
     * @var \Wyomind\ElasticsearchCore\Model\ToReindexFactory
     */
    protected $_toReindexModelFactory = null;

    /**
     * Class constructor
     *
     * @param \Magento\Framework\Stdlib\DateTime\DateTime       $coreDate
     * @param \Wyomind\ElasticsearchCore\Helper\IndexerFactory  $indexerHelperFactory
     * @param \Wyomind\ElasticsearchCore\Model\Index            $indexModel
     * @param \Wyomind\ElasticsearchCore\Model\ToReindexFactory $toReindexModelFactory
     */
    public function __construct(
        \Magento\Framework\Stdlib\DateTime\DateTime $coreDate,
        \Wyomind\ElasticsearchCore\Helper\IndexerFactory $indexerHelperFactory,
        \Wyomind\ElasticsearchCore\Model\Index $indexModel,
        \Wyomind\ElasticsearchCore\Model\ToReindexFactory $toReindexModelFactory
    ) {
        $this->_coreDate = $coreDate;
        $this->_indexerHelperFactory = $indexerHelperFactory;
        $this->_indexModel = $indexModel;
        $this->_toReindexModelFactory = $toReindexModelFactory;
    }

    /**
     * @param \Magento\Cron\Model\Schedule $schedule
     */
    public function execute(\Magento\Cron\Model\Schedule $schedule)
    {
        $indexers = $this->_indexerHelperFactory->create()->getAllIndexers();

        /** @var \Wyomind\ElasticsearchCore\Model\Indexer\AbstractIndexer $indexer */
        foreach ($indexers as $indexer) {
            $type = $indexer->getType();

            /** @var \Wyomind\ElasticsearchCore\Model\ToReindex $toReindexModel */
            $toReindexModel = $this->_toReindexModelFactory->create();

            $toReindexModel->deleteEntityToReindex($type, 0);

            $now = new \DateTime();

            $indexerLastEntries = $toReindexModel->getIndexerLastEntries($type, $now);

            $itemsToReindex = [];
            $repeatedIndex = [];
            foreach ($indexerLastEntries as $toReindex) {
                $itemsToReindex[] = $toReindex['to_reindex'];
                if ($toReindex['counter'] > 1) {
                    $repeatedIndex[] = $toReindex['to_reindex'];
                }
            }

            $chunkSize = count($itemsToReindex);
            if ($indexer instanceof ProductIndexer) {
                $chunkSize = $indexer->getProductChunkSize() * 4;
            }

            if (count($itemsToReindex)) {
                while($items = array_splice($itemsToReindex,0, $chunkSize)){
                    $indexer->execute($items);
                    $toReindexModel->deleteIndexerToReindex($type, $items, $now);
                }
            }

            $index = $this->_indexModel->loadByIndexerId($type);
            $index->setIndexerId($type);
            $index->setUpdateMode('schedule');
            $index->setReindexed(1);
            $datetime = $this->_coreDate->date(
                'Y-m-d H:i:s',
                $this->_coreDate->gmtTimestamp()
            );
            $index->setLastIndexDate($datetime);
            $index->save();

            if ($repeatedIndex && $indexer instanceof ProductIndexer) {
                $indexer->reindexAfterProductSave($repeatedIndex);
            }
        }
    }
}
