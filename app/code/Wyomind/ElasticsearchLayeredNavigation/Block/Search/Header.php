<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\ElasticsearchLayeredNavigation\Block\Search;

/**
 * Class Header : in order to display the search results page title
 * @package Wyomind\ElasticsearchLayeredNavigation\Block\Search
 */
class Header extends \Magento\Framework\View\Element\Template
{

    const TITLE = "Search results for: '%1'";

    /**
     * @return \Magento\Framework\View\Element\Template
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function _prepareLayout()
    {
        $title = $this->getSearchQueryText();
        $this->pageConfig->getTitle()->set($title);
        // add Home breadcrumb
        $breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
        if ($breadcrumbs) {
            $breadcrumbs->addCrumb(
                'home', [
                'label' => __('Home'),
                'title' => __('Go to Home Page'),
                'link' => $this->_storeManager->getStore()->getBaseUrl()
                ]
            )->addCrumb(
                'search', ['label' => $title, 'title' => $title]
            );
        }

        return parent::_prepareLayout();
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getSearchQueryText()
    {
        return __(self::TITLE, $this->getRequest()->getParam('q'));
    }

}
