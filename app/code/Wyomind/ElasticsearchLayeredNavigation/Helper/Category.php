<?php

/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\ElasticsearchLayeredNavigation\Helper;

use Magento\Catalog\Model\CategoryRepository;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Registry;

/**
 * Category Helper
 * Mainly to know the current category on the frontend
 * @package Wyomind\ElasticsearchLayeredNavigation\Helper
 */
class Category extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var Registry|null
     */
    protected $_registry = null;

    /**
     * Category constructor.
     * @param Context $context
     * @param Registry $registry
     */
    public function __construct(
        Context $context,
        Registry $registry
    )
    {
        parent::__construct($context);
        $this->_registry = $registry;
    }

    /**
     * Get the current category or -1
     * @return int
     */
    public function getCurrentCategoryId()
    {
        /** @var \Magento\Catalog\Model\Category $category */
        $category = $this->_registry->registry('current_category');
        if ($category) {
            return $category->getId();
        }
        return -1;
    }

    public function getCurrentCategoryUrl()
    {
        /** @var \Magento\Catalog\Model\Category $category */
        $category = $this->_registry->registry('current_category');
        if ($category) {
            return $category->getUrl();
        }
        return "/";
    }

    /**
     * @return null
     */
    public function getCurrentCategoryDisplayMode()
    {
        $category = $this->_registry->registry('current_category');
        if ($category) {
            return $category->getDisplayMode();
        }
        return null;
    }

}
