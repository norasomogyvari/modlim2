<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\ElasticsearchLayeredNavigation\Helper;

/**
 * Config helper : retrieve all config information
 * @package Wyomind\ElasticsearchLayeredNavigation\Helper
 */
class Config extends \Magento\Framework\App\Helper\AbstractHelper
{

    const XML_PATH_CATALOG_FRONTEND_GRID_PER_PAGE = "catalog/frontend/grid_per_page";
    const XML_PATH_CATALOG_FRONTEND_LIST_PER_PAGE = "catalog/frontend/list_per_page";
    const XML_PATH_WELN_SETTINGS_DISPLAY = "wyomind_elasticsearchlayerednavigation/settings/display/";
    const XML_PATH_LABELS = "wyomind_elasticsearchlayerednavigation/settings/display/labels";
    const XML_PATH_SETTINGS = "wyomind_elasticsearchlayerednavigation/settings";

    /* PRODUCTS LISTING */
    const SEARCH_AUTO_REFRESH = "/listing/enable_auto_refresh";
    const UPDATE_SEARCH_TERM = "/listing/update_search_term";
    const ADD_TO_COMPARE_ENABLE = "/listing/enable_addtocompare";
    const AJAX_CART_ENABLE = "/listing/enable_ajaxcart";
    const RATING_SORT_ENABLE = "/listing/enable_rating_sort";

    /* HIGHLIGHT */
    const DISPLAY_HIGHLIGHT_ENABLE = "/highlight/enable";

    /* LAYER UPDATE */
    const LAYER_UPDATE_HIDE_ONE_VALUE = "/layer/update/hide_one_value";
    const LAYER_UPDATE_ENABLED = "/layer/update/enable";
    const LAYER_UPDATE_METHOD = "/layer/update/method";
    const LAYER_UPDATE_SORTING = "/layer/update/sorting";

    /* LAYER */
    const STICKY_LAYER_ENABLED = "/layer/sticky";
    const DISPLAY_INPUT_BOXES_PRICE_FILTER = "/layer/sliders/display_input_boxes_price_filter";
    const ENABLE_BREADCRUMB = "/layer/breadcrumb/enable";
    const ATTRIBUTES_TOP_LAYER = "/layer/top/attributes";
    const ATTRIBUTES_LEFT_LAYER = "/layer/left/attributes";
    const ATTRIBUTES_RIGHT_LAYER = "/layer/right/attributes";
    const ATTRIBUTES_MOBILE_LAYER = "/layer/mobile/attributes";

    /* SCROLLING */
    const INFINITE_SCROLL_ENABLE = "/scrolling/enable";
    const INFINITE_SCROLL_STEP = "/scrolling/step";
    const INFINITE_SCROLL_AUTOMATIC = "/scrolling/automatic";
    const INFINITE_SCROLL_OFFSET = "/scrolling/offset";

    const SEO_CATEGORY_URL_SUFFIX = "catalog/seo/category_url_suffix";

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface|null
     */
    private $_scopeConfig = null;

    /**
     * @var \Magento\Framework\Serialize\SerializerInterface|null
     */
    private $_serializer = null;

    /**
     * Config constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\Serialize\SerializerInterface $serializer
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Serialize\SerializerInterface $serializer
    )
    {
        parent::__construct($context);
        $this->_scopeConfig = $context->getScopeConfig();
        $this->_serializer = $serializer;
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getEaConfig($store = null)
    {
        return $this->_scopeConfig->getValue("wyomind_elasticsearchautocomplete/settings", \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param $type
     * @param null $store
     * @return mixed
     */
    public function getSEOCategoryUrlSuffix($store = null)
    {
        return $this->_scopeConfig->getValue(self::SEO_CATEGORY_URL_SUFFIX, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }


    /**
     * @param $type
     * @param null $store
     * @return mixed
     */
    public function isLayerHideOneValue($type, $store = null)
    {
        return $this->_scopeConfig->getValue(self::XML_PATH_WELN_SETTINGS_DISPLAY . $type . self::LAYER_UPDATE_HIDE_ONE_VALUE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param $type
     * @param null $store
     * @return mixed
     */
    public function isLayerUpdateEnabled($type, $store = null)
    {
        return $this->_scopeConfig->getValue(self::XML_PATH_WELN_SETTINGS_DISPLAY . $type . self::LAYER_UPDATE_ENABLED, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param $type
     * @param null $store
     * @return mixed
     */
    public function isStickyLayerEnabled($type, $store = null)
    {
        return $this->_scopeConfig->getValue(self::XML_PATH_WELN_SETTINGS_DISPLAY . $type . self::STICKY_LAYER_ENABLED, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param $type
     * @param null $store
     * @return mixed
     */
    public function getLayerUpdateMethod($type, $store = null)
    {
        return $this->_scopeConfig->getValue(self::XML_PATH_WELN_SETTINGS_DISPLAY . $type . self::LAYER_UPDATE_METHOD, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param $type
     * @param null $store
     * @return mixed
     */
    public function getLayerUpdateSorting($type, $store = null)
    {
        return $this->_scopeConfig->getValue(self::XML_PATH_WELN_SETTINGS_DISPLAY . $type . self::LAYER_UPDATE_SORTING, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getConfig($store = null)
    {
        return $this->_scopeConfig->getValue(self::XML_PATH_SETTINGS, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getLabels($store = null)
    {
        return $this->_scopeConfig->getValue(self::XML_PATH_LABELS, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }


    /**
     * Get the number of products to display per page
     * @param null $store
     * @return array
     */
    public function getPerPageDefaultValue($store = null)
    {
        return [
            'grid' => $this->_scopeConfig->getValue(self::XML_PATH_CATALOG_FRONTEND_GRID_PER_PAGE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store),
            'list' => $this->_scopeConfig->getValue(self::XML_PATH_CATALOG_FRONTEND_LIST_PER_PAGE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store)
        ];
    }

    /**
     * Is the infinite scroll enabled
     * @param $type category/search
     * @param null $store
     * @return mixed
     */
    public function isInfiniteScrollEnabled($type,
                                            $store = null)
    {
        return $this->_scopeConfig->getValue(self::XML_PATH_WELN_SETTINGS_DISPLAY . $type . self::INFINITE_SCROLL_ENABLE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param $type
     * @param null $store
     * @return mixed
     */
    public function isBreadcrumbEnabled($type, $store = null)
    {
        return $this->_scopeConfig->getValue(self::XML_PATH_WELN_SETTINGS_DISPLAY . $type . self::ENABLE_BREADCRUMB, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * Is the ajax cart enabled
     * @param $type category/search
     * @param null $store
     * @return mixed
     */
    public function isAjaxCartEnabled($type,
                                      $store = null)
    {
        return $this->_scopeConfig->getValue(self::XML_PATH_WELN_SETTINGS_DISPLAY . $type . self::AJAX_CART_ENABLE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function isAutoRefreshEnabled($store = null)
    {
        return $this->_scopeConfig->getValue(self::XML_PATH_WELN_SETTINGS_DISPLAY . "search" . self::SEARCH_AUTO_REFRESH, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getUpdateSearchTerm($store = null)
    {
        return $this->_scopeConfig->getValue(self::XML_PATH_WELN_SETTINGS_DISPLAY . "search" . self::UPDATE_SEARCH_TERM, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * Is the sort on ratings enabled
     * @param $type category/search
     * @param null $store
     * @return mixed
     */
    public function isRatingSortEnabled($type,
                                        $store = null)
    {
        return $this->_scopeConfig->getValue(self::XML_PATH_WELN_SETTINGS_DISPLAY . $type . self::RATING_SORT_ENABLE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * Infinite Scroll step (number of products to load for each scroll step)
     * @param $type
     * @param null $store
     * @return mixed
     */
    public function getInfiniteScrollStep($type, $store = null)
    {
        return $this->_scopeConfig->getValue(self::XML_PATH_WELN_SETTINGS_DISPLAY . $type . self::INFINITE_SCROLL_STEP, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * Is Infinite Scroll Automatic?
     * @param $type
     * @param null $store
     * @return mixed
     */
    public function isInfiniteScrollAutomatic($type, $store = null)
    {
        return $this->_scopeConfig->getValue(self::XML_PATH_WELN_SETTINGS_DISPLAY . $type . self::INFINITE_SCROLL_AUTOMATIC, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * Should we display a "Add To Compare" button?
     * @param $type
     * @param null $store
     * @return mixed
     */
    public function isAddToCompareEnabled($type,
                                          $store = null)
    {
        return $this->_scopeConfig->getValue(self::XML_PATH_WELN_SETTINGS_DISPLAY . $type . self::ADD_TO_COMPARE_ENABLE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }


    /**
     * Get the infinite scroll offset (limit from which the infinite scroll is triggered when scrolling down)
     * @param $type category/search
     * @param null $store
     * @return mixed
     */
    public function getInfiniteScrollOffset($type,
                                            $store = null)
    {
        return $this->_scopeConfig->getValue(self::XML_PATH_WELN_SETTINGS_DISPLAY . $type . self::INFINITE_SCROLL_OFFSET, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * Should we display the inputs for the price filters
     * @param $type category/search
     * @param null $store
     * @return mixed
     */
    public function displayInputBoxesForPriceFilter($type,
                                                    $store = null)
    {
        return $this->_scopeConfig->getValue(self::XML_PATH_WELN_SETTINGS_DISPLAY . $type . self::DISPLAY_INPUT_BOXES_PRICE_FILTER, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }


    /**
     * @param null $store
     * @return mixed
     */
    public function isHighlightEnabled($store = null)
    {
        return $this->_scopeConfig->getValue(self::XML_PATH_WELN_SETTINGS_DISPLAY . "search" . self::DISPLAY_HIGHLIGHT_ENABLE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * Get the product attributes used for the display of the layers
     * @param $type category/search
     * @param null $store
     * @return string
     */
    public function getFilterableAttributesJson($type,
                                                $store = null)
    {

        $dataTop = $this->_scopeConfig->getValue(self::XML_PATH_WELN_SETTINGS_DISPLAY . $type . self::ATTRIBUTES_TOP_LAYER, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
        if (!$dataTop) {
            $dataTop = "[]";
        }
        $top = $this->_serializer->unserialize($dataTop);
        $newTop = [];
        foreach ($top as $uid => $filter) {
            $newTop[$filter['position']] = [
                $filter['attribute_code'],
                (int)($filter['show_results_count']??0),
                (int)($filter['show_more_less']??0),
                (int)($filter['show_more_less_nb_values']??5),
                (int)($filter['show_layer_filter']??0),
                (int)($filter['closed']??0),
                (int)($filter['generate_url']??0),
                (int)($filter['no_follow']??0)
            ];
        }
        ksort($newTop);

        $dataLeft = $this->_scopeConfig->getValue(self::XML_PATH_WELN_SETTINGS_DISPLAY . $type . self::ATTRIBUTES_LEFT_LAYER, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
        if (!$dataLeft) {
            $dataLeft = "[]";
        }
        $left = $this->_serializer->unserialize($dataLeft);
        $newLeft = [];
        foreach ($left as $uid => $filter) {
            $newLeft[$filter['position']] = [
                $filter['attribute_code'],
                (int)($filter['show_results_count']??0),
                (int)($filter['show_more_less']??0),
                (int)($filter['show_more_less_nb_values']??5),
                (int)($filter['show_layer_filter']??0),
                (int)($filter['closed']??0),
                (int)($filter['generate_url']??0),
                (int)($filter['no_follow']??0)
            ];
        }
        ksort($newLeft);

        $dataRight = $this->_scopeConfig->getValue(self::XML_PATH_WELN_SETTINGS_DISPLAY . $type . self::ATTRIBUTES_RIGHT_LAYER, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
        if (!$dataRight) {
            $dataRight = "[]";
        }
        $right = $this->_serializer->unserialize($dataRight);
        $newRight = [];
        foreach ($right as $uid => $filter) {
            $newRight[$filter['position']] = [
                $filter['attribute_code'],
                (int)($filter['show_results_count']??0),
                (int)($filter['show_more_less']??0),
                (int)($filter['show_more_less_nb_values']??5),
                (int)($filter['show_layer_filter']??0),
                (int)($filter['closed']??0),
                (int)($filter['generate_url']??0),
                (int)($filter['no_follow']??0)
            ];
        }
        ksort($newRight);

        $dataMobile = $this->_scopeConfig->getValue(self::XML_PATH_WELN_SETTINGS_DISPLAY . $type . self::ATTRIBUTES_MOBILE_LAYER, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
        if (!$dataMobile) {
            $dataMobile = "[]";
        }
        $mobile = $this->_serializer->unserialize($dataMobile);
        $newMobile = [];
        foreach ($mobile as $uid => $filter) {
            $newMobile[$filter['position']] = [
                $filter['attribute_code'],
                (int)($filter['show_results_count']??0),
                (int)($filter['show_more_less']??0),
                (int)($filter['show_more_less_nb_values']??5),
                (int)($filter['show_layer_filter']??0),
                (int)($filter['closed']??0),
                (int)($filter['generate_url']??0),
                (int)($filter['no_follow']??0)
            ];
        }
        ksort($newMobile);

        return json_encode([
            "top" => $newTop,
            "left" => $newLeft,
            "right" => $newRight,
            "mobile" => $newMobile
        ]);
    }

}
