<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_ProductLabels
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\ProductLabels\Helper;

use Exception;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Phrase;
use Magento\Framework\View\Element\Template;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;
use Mageplaza\Core\Helper\AbstractData;
use Mageplaza\ProductLabels\Model\Meta;
use Mageplaza\ProductLabels\Model\MetaFactory;
use Mageplaza\ProductLabels\Model\Rule;
use Mageplaza\ProductLabels\Model\RuleFactory;

/**
 * Class Data
 * @package Mageplaza\ProductLabels\Helper
 */
class Data extends AbstractData
{
    const CONFIG_MODULE_PATH = 'productlabels';

    /**
     * @var RuleFactory
     */
    protected $ruleFactory;

    /**
     * @var MetaFactory
     */
    protected $metaFactory;

    /**
     * @var CollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var ProductFactory
     */
    protected $productLoader;

    /**
     * @var Template
     */
    protected $template;

    /**
     * List product id stop process
     *
     * @var array
     */
    protected $listIdStopProcess = [];

    /**
     * Data constructor.
     *
     * @param Context $context
     * @param ObjectManagerInterface $objectManager
     * @param StoreManagerInterface $storeManager
     * @param RuleFactory $ruleFactory
     * @param MetaFactory $metaFactory
     * @param CollectionFactory $productCollectionFactory
     * @param ProductFactory $productLoader
     * @param Template $template
     */
    public function __construct(
        Context $context,
        ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager,
        RuleFactory $ruleFactory,
        MetaFactory $metaFactory,
        CollectionFactory $productCollectionFactory,
        ProductFactory $productLoader,
        Template $template
    ) {
        $this->ruleFactory              = $ruleFactory;
        $this->metaFactory              = $metaFactory;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->productLoader            = $productLoader;
        $this->template                 = $template;

        parent::__construct($context, $objectManager, $storeManager);
    }

    /**
     * @param Rule $rule
     * @param Meta $metaModel
     *
     * @return $this
     */
    public function applyRuleId($rule, $metaModel)
    {
        $listId = $this->getProductIds($rule);

        if (is_array($listId)) {
            $metaData = [];

            foreach ($listId as $proId) {
                if (!in_array($proId, $this->listIdStopProcess)) {
                    $metaData['rule_id']            = $rule->getId();
                    $metaData['product_id']         = $proId;
                    $metaData['template_url']       = $rule->getListTemplate();
                    $metaData['img_url']            = $this->getSrcImg($rule);
                    $metaData['label']              = $rule->getListLabel();
                    $metaData['label_style']        = $this->getLabelStyle($rule);
                    $metaData['label_fontsize']     = $rule->getListFontSize();
                    $metaData['label_position']     = $rule->getListPosition();
                    $metaData['custom_css']         = $this->getCategoryCustomCss($rule, $proId);
                    $metaData['from_date']          = strtotime($rule->getFromDate());
                    $metaData['to_date']            = strtotime($rule->getToDate());
                    $metaData['customer_group_ids'] = $rule->getCustomerGroupIds();
                    $metaData['priority']           = $rule->getPriority();

                    if (count($metaData)) {
                        $metaModel->applyRule($metaData);
                        $metaData = [];
                    }
                }
            }
        }

        if ($rule->getStopProcess()) {
            $this->listIdStopProcess = $listId;
        }

        return $this;
    }

    /**
     * @param Rule $rule
     *
     * @return string|null
     */
    public function getSrcImg($rule)
    {
        if ($listImage = $rule->getListImage()) {
            return $this->getImageUrl($listImage, 'listing');
        }

        return null;
    }

    /**
     * @param string $label
     * @param int $productId
     *
     * @return string
     */
    public function getCategoryProductLabel($label, $productId)
    {
        $attrCode = $this->getAttrInLabel($label);
        $search   = [
            '{{discount}}',
            '{{discount_percent}}',
            '{{current_price}}',
        ];
        $replace  = [
            $this->getDiscount($productId),
            $this->getPercentDiscount($productId),
            $this->getCurrentPrice($productId),
        ];
        $label    = str_replace($search, $replace, $label);

        if ($attrCode && !in_array('{{' . $attrCode . '}}', $search)) {
            $label = str_replace('{{' . $attrCode . '}}', $this->getAttributeProduct($attrCode, $productId), $label);
        }

        return $label;
    }

    /**
     * @param Rule $rule
     *
     * @return string
     */
    public function getLabelStyle($rule)
    {
        $font  = $rule->getListFont();
        $color = $rule->getListColor();

        return 'font-family:' . $font . '; color:' . $color;
    }

    /**
     * @param Rule $rule
     * @param int $id
     *
     * @return string
     */
    public function getCategoryCustomCss($rule, $id)
    {
        $customCss = $rule->getListCss();
        $search    = [
            'design-labels-list',
            'design-label-image-list',
            'design-label-text-list',
        ];
        $replace   = [
            'design-labels-' . $rule->getId() . '-' . $id,
            'design-label-image-' . $rule->getId() . '-' . $id,
            'design-label-text-' . $rule->getId() . '-' . $id,
        ];

        return str_replace($search, $replace, $customCss);
    }

    /**
     * @param int $productId
     *
     * @return Product
     */
    public function getProductById($productId)
    {
        return $this->productLoader->create()->load($productId);
    }

    /**
     * @param int $productId
     *
     * @return string
     */
    public function getDiscount($productId)
    {
        $product        = $this->getProductById($productId);
        $originalPrice  = $product->getPrice();
        $finalPrice     = $product->getFinalPrice();
        $currencySymbol = $this->getCurrentCySymbol();
        $discount       = 0;

        if ($originalPrice > $finalPrice) {
            $discount = $originalPrice - $finalPrice;
        }

        return $currencySymbol . $discount;
    }

    /**
     * @return string
     */
    public function getCurrentCySymbol()
    {
        try {
            return $this->storeManager->getStore()->getCurrentCurrencyCode();
        } catch (NoSuchEntityException $e) {
            $this->_logger->critical($e->getMessage());

            return '';
        }
    }

    /**
     * Get discount percent by product id
     *
     * @param $productId
     *
     * @return int|string
     */
    public function getPercentDiscount($productId)
    {
        $product       = $this->getProductById($productId);
        $originalPrice = $product->getPrice();
        $finalPrice    = $product->getFinalPrice();

        $percentage = 0;
        if ($originalPrice > $finalPrice) {
            $percentage = number_format(($originalPrice - $finalPrice) * 100 / $originalPrice, 0);
        }

        return $percentage;
    }

    /**
     * Get Current price by product id
     *
     * @param $productId
     *
     * @return float
     */
    public function getCurrentPrice($productId)
    {
        return number_format($this->getProductById($productId)->getFinalPrice(), 2);
    }

    /**
     * Get product attribute name
     *
     * @param string $attributeCode
     * @param int $productId
     *
     * @return string
     */
    public function getAttributeProduct($attributeCode, $productId)
    {
        $product = $this->getProductById($productId);

        if (is_object($product->getCustomAttribute($attributeCode))) {
            $option = $product->getCustomAttribute($attributeCode);

            if ($option !== null) {
                $optionId = $option->getValue();
            }

            $_attributeId = $product->getResource()->getAttribute($attributeCode);
            if ($_attributeId->usesSource()) {
                $label = $_attributeId->getSource()->getOptionText($optionId);
                if (is_array($label)) {
                    return implode(', ', $label);
                }

                return $label;
            }
        }

        return null;
    }

    /**
     * Get attribute code in variables label
     *
     * @param $label
     *
     * @return bool|string
     */
    public function getAttrInLabel($label)
    {
        $start = strpos($label, '{{');
        $end   = strpos($label, '}}');

        return substr($label, $start + 2, $end - ($start + 2));
    }

    /**
     * @param Rule $rule
     *
     * @return array
     */
    public function getProductIds($rule)
    {
        $productIds = $rule->getMatchingProductIds();

        if ($rule->getBestseller() != 1) {
            return $productIds;
        }

        $bestSellerProductIds = [];
        $collection           = $this->productCollectionFactory->create()
            ->addIdFilter($productIds)
            ->setPageSize($rule->getLimit());

        $collection->getSelect()->joinLeft(
            ['soi' => $collection->getTable('sales_order_item')],
            'e.entity_id = soi.product_id',
            ['qty_ordered' => 'SUM(soi.qty_ordered)']
        )->group('e.entity_id')->where('soi.qty_ordered', ['gt' => 0])->order('qty_ordered DESC');

        $collection->addStoreFilter();

        foreach ($collection->getData() as $item) {
            $bestSellerProductIds[] = $item['entity_id'];
        }

        return $bestSellerProductIds;
    }

    /**
     * @param $rule
     *
     * @return Phrase
     */
    public function getState($rule)
    {
        if ($rule->getId()) {
            $toDate      = strtotime($rule->getToDate());
            $fromDate    = strtotime($rule->getFromDate());
            $currentDate = strtotime(date('d-m-Y H:i:s'));

            if (($toDate >= $currentDate && $fromDate <= $currentDate) || (!$toDate && $fromDate <= $currentDate)) {
                return __('Running');
            }
            if ($fromDate > $currentDate) {
                return __('Queue');
            }
            if ($toDate < $currentDate) {
                return __('Done');
            }
        }

        return __('None');
    }

    /**
     * @param $image
     * @param string $type
     *
     * @return string
     */
    public function getImageUrl($image, $type = Image::TEMPLATE_MEDIA_LABEL)
    {
        try {
            $imageHelper = $this->getImageHelper();
            $imageFile   = $imageHelper->getMediaPath($image, $type);

            return $imageHelper->getMediaUrl($imageFile);
        } catch (Exception $e) {
            return '#';
        }
    }

    /**
     * @return Image
     */
    public function getImageHelper()
    {
        return $this->objectManager->get(Image::class);
    }

    /**
     * @param null $storeId
     *
     * @return bool|StoreInterface
     */
    public function getStore($storeId = null)
    {
        try {
            return $this->storeManager->getStore($storeId);
        } catch (NoSuchEntityException $e) {
            return false;
        }
    }
}
