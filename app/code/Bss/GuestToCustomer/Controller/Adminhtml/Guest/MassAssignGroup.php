<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_GuestToCustomer
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\GuestToCustomer\Controller\Adminhtml\Guest;

use Bss\GuestToCustomer\Helper\ConfigAdmin;
use Bss\GuestToCustomer\Helper\ConfigCustomerAdmin;
use Bss\GuestToCustomer\Helper\Customer\SaveCustomer;
use Bss\GuestToCustomer\Helper\MassActionHelper;
use Bss\GuestToCustomer\Model\Guest;
use Bss\GuestToCustomer\Model\ResourceModel\Guest\CollectionFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Class MassAssignGroup
 *
 * @package Bss\GuestToCustomer\Controller\Adminhtml\Guest
 */
class MassAssignGroup extends Action
{
    /**
     * BSS BILLING ADDRESS TYPE
     */
    const BSS_BILLING_ADDRESS_TYPE = true;

    /**
     * BSS SHIPPING ADDRESS TYPE
     */
    const BSS_SHIPPING_ADDRESS_TYPE = false;

    /**
     * Helper Config Admin
     * @var ConfigAdmin $helperConfigAdmin
     */
    protected $helperConfigAdmin;

    /**
     * CollectionFactory
     * @var CollectionFactory $collectionFactory
     */
    protected $collectionFactory;

    /**
     * Guest
     * @var Guest
     */
    protected $modelGuest;

    /**
     * Helper Save Customer
     * @var SaveCustomer
     */
    protected $helperSaveCustomer;

    /**
     * Config Customer Admin
     *
     * @var ConfigCustomerAdmin $helperConfigCustomerAdmin
     */
    protected $helperConfigCustomerAdmin;

    /**
     * @var MassActionHelper
     */
    protected $massActionHelper;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * MassAssignGroup constructor.
     * @param Context $context
     * @param SerializerInterface $serializer
     * @param SaveCustomer $helperSaveCustomer
     * @param Guest $modelGuest
     * @param CollectionFactory $collectionFactory
     * @param ConfigAdmin $helperConfigAdmin
     * @param ConfigCustomerAdmin $helperConfigCustomerAdmin
     * @param MassActionHelper $massActionHelper
     */
    public function __construct(
        Context $context,
        SerializerInterface $serializer,
        SaveCustomer $helperSaveCustomer,
        Guest $modelGuest,
        CollectionFactory $collectionFactory,
        ConfigAdmin $helperConfigAdmin,
        ConfigCustomerAdmin $helperConfigCustomerAdmin,
        MassActionHelper $massActionHelper
    ) {
        $this->serializer = $serializer;
        $this->modelGuest = $modelGuest;
        $this->helperSaveCustomer = $helperSaveCustomer;
        $this->collectionFactory = $collectionFactory;
        $this->helperConfigAdmin = $helperConfigAdmin;
        $this->helperConfigCustomerAdmin = $helperConfigCustomerAdmin;
        $this->massActionHelper = $massActionHelper;
        parent::__construct($context);
    }

    /**
     * Execute
     *
     * @return Redirect|ResponseInterface|ResultInterface
     * @throws LocalizedException
     */
    public function execute()
    {
        $this->massActionHelper->getCoreSession()->start();
        $this->massActionHelper->getCoreSession()->setData('bss_guest_to_customer_type', 1);

        $groupId = $this->getRequest()->getParam('group');
        $collection=$this->massActionHelper->getFilter()->getCollection($this->collectionFactory->create());
        $collectionSize = $collection->getSize();
        foreach ($collection as $guest) {
            try {
                if ($this->helperConfigAdmin->getConfigEnableModule()) {
                    $addressBilling = $this->serializer->unserialize($guest->getBillingAddress());
                    $addressShipping = $this->serializer->unserialize($guest->getShippingAddress());
                    $this->helperConfigAdmin->getConfigTelephoneRequire($addressShipping);
                    $this->helperConfigAdmin->getConfigTelephoneRequire($addressBilling);
                    $customerData =
                        [
                            "website_id" => $guest['website_id'],
                            "store_id" => $guest['store_id'],
                            "group_id" => $groupId,
                            "disable_auto_group_change" => 0,
                            "prefix" => $addressBilling['prefix'],
                            "firstname" => $addressBilling['firstname'],
                            "lastname" => $addressBilling['lastname'],
                            "suffix"=> $addressBilling['suffix'],
                            "email" => $addressBilling['email'],
                            "fax" => $addressBilling['fax'],
                            'telephone' => $addressBilling['telephone'],
                            'company' => $addressBilling['company'],
                            "sendemail_store_id" => 1
                        ];
                    $this->saveOneCustomer($customerData, $addressBilling, $addressShipping);
                    $this->deleteGuest($guest);
                }
            } catch (\Exception $exception) {
                $this->messageManager->addErrorMessage($exception->getMessage());
            }
        }
        $indexer = $this->massActionHelper->getIndexer()->get("customer_grid");
        $indexer->reindexAll();
        $this->messageManager->addSuccessMessage(
            __(
                'A total of %1 record(s) have been assign.',
                $collectionSize
            )
        );
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $this->massActionHelper->getCoreSession()->setData('bss_guest_to_customer_type', 0);
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Get Bind Data
     *
     * @param array $addressData
     * @param number $idCustomer
     * @return array
     */
    protected function getBindData($addressData, $idCustomer)
    {
        $bind = [
            'parent_id' => $idCustomer,
            'firstname' => $addressData['firstname'],
            'middlename' => $addressData['middlename'],
            'lastname' => $addressData['lastname'],
            'country_id' => $addressData['country_id'],
            'postcode' => $addressData['postcode'],
            'city' => $addressData['city'],
            'telephone' => $addressData['telephone'],
            'company' => $addressData['company'],
            'street' => $addressData['street'],
            'region' => $addressData['region'],
            'region_id' => $addressData['region_id'],
            "suffix" => $addressData['suffix'],
            "fax" => $addressData['fax'],
            "prefix" => $addressData['prefix']
        ];

        return $bind;
    }

    /**
     * Is async address
     *
     * @param array $shippingAddress
     * @param array $billingAddress
     * @return bool
     */
    public function isAsyncAddress($shippingAddress, $billingAddress)
    {
        unset($shippingAddress['address_type']);
        unset($billingAddress['address_type']);
        unset($shippingAddress['quote_address_id']);
        unset($billingAddress['quote_address_id']);
        if (!empty(array_diff($shippingAddress, $billingAddress))) {
            $sameAddress = false;
        } else {
            $sameAddress = true;
        }
        if ($sameAddress && $this->helperConfigAdmin->isAsyncAddress()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Save One Customer
     *
     * @param array $dataCustomer
     * @param array $dataAddressBilling
     * @param array $dataAddressShipping
     * @return void
     */
    protected function saveOneCustomer($dataCustomer = [], $dataAddressBilling = [], $dataAddressShipping = [])
    {
        $idCustomer = $this->helperSaveCustomer->saveCustomer($dataCustomer);
        if ($idCustomer) {
            if (!$this->isAsyncAddress($dataAddressShipping, $dataAddressBilling)) {
                if (isset($dataAddressBilling['address_type'])) {
                    $dataAddressBilling = $this->getBindData($dataAddressBilling, $idCustomer);

                    $this->helperSaveCustomer->saveAddress(
                        $dataAddressBilling,
                        $idCustomer,
                        self::BSS_BILLING_ADDRESS_TYPE
                    );
                }
                if (isset($dataAddressShipping['address_type'])) {
                    $dataAddressShipping = $this->getBindData($dataAddressShipping, $idCustomer);

                    $this->helperSaveCustomer->saveAddress(
                        $dataAddressShipping,
                        $idCustomer,
                        self::BSS_SHIPPING_ADDRESS_TYPE
                    );
                }
            } else {
                if (isset($dataAddressShipping['address_type'])) {
                    $dataAddressShipping = $this->getBindData($dataAddressShipping, $idCustomer);

                    $this->helperSaveCustomer->saveAddress(
                        $dataAddressShipping,
                        $idCustomer,
                        self::BSS_SHIPPING_ADDRESS_TYPE,
                        true
                    );
                }
            }
            $config = $this->helperConfigAdmin->getConfigAssignOrders();
            if ($config) {
                $this->helperSaveCustomer->saveOrders($idCustomer, $dataCustomer);
            }
        }
    }

    /**
     * Delete Guest
     *
     * @param Guest $guest
     * @return void
     */
    private function deleteGuest($guest)
    {
        try {
            $this->modelGuest->load($guest->getId());
            $this->modelGuest->delete();
        } catch (\Exception $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
        }
    }

    /**
     * Check Rule
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed("Bss_GuestToCustomer::assign");
    }
}
