<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ProductsGraphQl\Model\Resolver;

use Itegration\ProductsGraphQl\Model\ResourceModel\Prices as PricesResource;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Store\Model\StoreManagerInterface;

class Prices implements ResolverInterface
{
    /**
     * @var PricesResource
     */
    private $pricesResource;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * Prices constructor.
     *
     * @param PricesResource        $pricesResource
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        PricesResource $pricesResource,
        StoreManagerInterface $storeManager
    ) {
        $this->pricesResource = $pricesResource;
        $this->storeManager = $storeManager;
    }

    /**
     * @param Field            $field
     * @param ContextInterface $context
     * @param ResolveInfo      $info
     * @param array|null       $value
     * @param array|null       $args
     *
     * @return Value|mixed|void
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $store = $this->storeManager->getStore();
        $websiteId = $store->getWebsiteId();

        $prices = $this->pricesResource->getProductPrices(
            $value['entity_id'],
            $websiteId
        );

        $returnArray = [];
        foreach ($prices as $price) {
            $returnArray[] = $this->buildPricesResponse(
                $price,
                $store->getBaseCurrencyCode()
            );
        }

        return $returnArray;
    }

    private function buildPricesResponse($price, $currencyCode)
    {
        return [
            'customer_group_id' => $price['customer_group_id'],
            'price'             => $this->buildPriceResponse(
                $price['price'],
                $currencyCode
            ),
            'finalPrice'        => $this->buildPriceResponse(
                $price['final_price'],
                $currencyCode
            ),
            'minPrice'          => $this->buildPriceResponse(
                $price['min_price'],
                $currencyCode
            ),
            'maxPrice'          => $this->buildPriceResponse(
                $price['max_price'],
                $currencyCode
            )
        ];
    }

    /**
     * @param float  $amount
     * @param string $currencyCode
     *
     * @return array
     */
    private function buildPriceResponse($amount, $currencyCode)
    {
        return [
            'amount' => [
                'value'    => $amount,
                'currency' => $currencyCode,
            ],
        ];
    }
}
