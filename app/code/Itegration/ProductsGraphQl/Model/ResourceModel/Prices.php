<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ProductsGraphQl\Model\ResourceModel;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\ProductFactory;

class Prices
{
    /**
     * @var ProductFactory
     */
    private $productFactory;

    /**
     * Prices constructor.
     *
     * @param ProductFactory             $productFactory
     * @param ProductRepositoryInterface $productRepository
     * @param GroupCollectionFactory     $groupCollectionFactory
     */
    public function __construct(
        ProductFactory $productFactory
    ) {
        $this->productFactory = $productFactory;
    }

    /**
     * @param int $productId
     * @param int $websiteId
     *
     * @return array
     */
    public function getProductPrices($productId, $websiteId)
    {
        $resource = $this->productFactory->create()->getResource();
        $connection = $resource->getConnection();

        // Add prices
        $least = $connection->getLeastSql(
            ['prices.min_price', 'prices.tier_price']
        );
        $minimalExpr = $connection->getCheckSql(
            'prices.tier_price IS NOT NULL',
            $least,
            'prices.min_price'
        );
        $cols = [
            'customer_group_id', 'entity_id', 'price', 'final_price',
            'minimal_price' => $minimalExpr, 'min_price', 'max_price',
            'tier_price',
        ];
        $select = $connection->select()
            ->from(
                [
                    'prices' => $resource->getTable(
                        'catalog_product_index_price'
                    ),
                ],
                $cols
            )
            ->where('prices.entity_id IN (?)', [$productId])
            ->where('prices.website_id = ?', $websiteId);
        $select->joinLeft(
            [
                'price_rule' => $resource->getTable(
                    'catalogrule_product_price'
                ),
            ],
            'price_rule.product_id = prices.entity_id AND '
            . $connection->quoteInto(
                'price_rule.website_id = ?',
                $websiteId
            ) . ' AND '
            . 'price_rule.customer_group_id = prices.customer_group_id',
            ['rule_price']
        );

        $query = $connection->query($select);

        try {
            return $query->fetchAll();
        } catch (\Exception $e) {
            return [];
        }
    }
}
