<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\StoreViewGraphQl\Model\Resolver;

use Magento\Directory\Model\Currency;
use Magento\Directory\Model\CurrencyFactory;
use Magento\Directory\Model\ResourceModel\Country\CollectionFactory as CountryCollectionFactory;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Store\Model\Store as StoreModel;
use Magento\Store\Model\StoreManagerInterface;
use Nosto\Object\Signup\Account;
use Nosto\Tagging\Helper\Account as NostoAccountHelper;

class StoreViews implements ResolverInterface
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var CountryCollectionFactory
     */
    private $countryCollectionFactory;

    /**
     * @var NostoAccountHelper
     */
    private $nostoAccountHelper;

    /**
     * @var CurrencyFactory
     */
    private $currencyFactory;

    /**
     * StoreViews constructor.
     *
     * @param StoreManagerInterface    $storeManager
     * @param CountryCollectionFactory $countryCollectionFactory
     * @param NostoAccountHelper       $nostoAccountHelper
     * @param CurrencyFactory          $currencyFactory
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        CountryCollectionFactory $countryCollectionFactory,
        NostoAccountHelper $nostoAccountHelper,
        CurrencyFactory $currencyFactory
    ) {
        $this->storeManager = $storeManager;
        $this->countryCollectionFactory = $countryCollectionFactory;
        $this->nostoAccountHelper = $nostoAccountHelper;
        $this->currencyFactory = $currencyFactory;
    }

    /**
     * @param Field            $field
     * @param ContextInterface $context
     * @param ResolveInfo      $info
     * @param array|null       $value
     * @param array|null       $args
     *
     * @return array
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $stores = $this->storeManager->getStores(false);

        $storeArray = [];
        /** @var StoreModel $store */
        foreach ($stores as $store) {

            $currencies = [];
            foreach ($store->getAvailableCurrencyCodes(true) as $currencyCode) {
                $currencies[] = $this->_getCurrency($currencyCode);
            }

            $storeArray[] = [
                'id'               => $store->getId(),
                'name'             => $store->getName(),
                'code'             => $store->getCode(),
                'nosto_code'       => $this->getNostoCodeToSite($store),
                'base_currency'    => $this->_getCurrency($store->getBaseCurrencyCode()),
                'default_currency' => $this->_getCurrency($store->getDefaultCurrencyCode()),
                'currencies'       => $currencies,
            ];
        }

        $countryArray = [];
        $countries = $this->countryCollectionFactory->create()->toOptionArray(
            false
        );
        foreach ($countries as $country) {
            $countryArray[] = [
                'code' => $country['value'],
                'name' => $country['label'],
            ];
        }

        return [
            'items'     => $storeArray,
            'countries' => $countryArray,
        ];
    }

    /**
     * @param string $currencyCode
     *
     * @return array
     */
    private function _getCurrency($currencyCode)
    {
        /** @var Currency $currency */
        $currency = $this->currencyFactory->create()->load($currencyCode);

        return [
            'code'          => $currency->getCode(),
            'symbol'        => $currency->getCurrencySymbol(),
            'output_format' => $currency->getOutputFormat(),
        ];
    }

    /**
     * @param StoreModel $store
     *
     * @return string
     */
    private function getNostoCodeToSite($store)
    {
        /** @var Account $account */
        $account = $this->nostoAccountHelper->findAccount($store);
        if ($account instanceof Account) {
            return $account->getName();
        }
        return null;
    }
}
