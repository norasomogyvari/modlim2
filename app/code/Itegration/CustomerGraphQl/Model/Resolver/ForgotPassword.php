<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\CustomerGraphQl\Model\Resolver;

use Exception;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\AccountManagement;
use Magento\Framework\Escaper;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\SecurityViolationException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\Validator\EmailAddress;
use Zend_Validate;

class ForgotPassword implements ResolverInterface
{
    /**
     * @var AccountManagementInterface
     */
    private $accountManagement;

    /**
     * @var Escaper
     */
    private $escaper;

    /**
     * @var string
     */
    private $message = '';

    /**
     * @var bool
     */
    private $success = true;

    /**
     * ForgotPassword constructor.
     *
     * @param AccountManagementInterface $accountManagement
     * @param Escaper                    $escaper
     */
    public function __construct(
        AccountManagementInterface $accountManagement,
        Escaper $escaper
    ) {
        $this->accountManagement = $accountManagement;
        $this->escaper = $escaper;
    }

    /**
     * @param Field $field
     * @param ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     *
     * @return Value|mixed|void
     * @throws \Zend_Validate_Exception
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!array_key_exists('email', $args)) {
            return $this
                ->setMessage(__('E-mail not specified'))
                ->setSuccess(false)
                ->handleReturn();
        }

        $email = $args['email'];

        if (!Zend_Validate::is($email, EmailAddress::class)) {
            return $this
                ->setMessage(
                    __(
                        'The email address is incorrect. Verify the email address and try again.'
                    )
                )
                ->setSuccess(false)
                ->handleReturn();
        }

        try {
            $this->accountManagement->initiatePasswordReset(
                $email,
                AccountManagement::EMAIL_RESET
            );
        } catch (NoSuchEntityException $exception) {
            // Do nothing, we don't want anyone to use this action to determine which email accounts are registered.
        } catch (SecurityViolationException $exception) {
            return $this
                ->setMessage(__($exception->getMessage()))
                ->setSuccess(false)
                ->handleReturn();
        } catch (Exception $exception) {
            return $this
                ->setMessage(
                    __('We\'re unable to send the password reset email.')
                )
                ->setSuccess(false)
                ->handleReturn();
        }

        return $this->setMessage(
            __(
                'If there is an account associated with %1 you will receive an email with a link to reset your password.',
                $this->escaper->escapeHtml($email)
            ))
            ->setSuccess(true)
            ->handleReturn();
    }

    /**
     * @param bool $success
     *
     * @return $this
     */
    private function setSuccess($success)
    {
        $this->success = $success;
        return $this;
    }

    /**
     * @param string $message
     *
     * @return $this
     */
    private function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return array
     */
    private function handleReturn()
    {
        return [
            'message' => $this->message,
            'success' => $this->success,
        ];
    }
}
