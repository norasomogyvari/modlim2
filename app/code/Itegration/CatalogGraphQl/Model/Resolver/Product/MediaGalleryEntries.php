<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\CatalogGraphQl\Model\Resolver\Product;

use Magento\Catalog\Model\Product;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

/**
 * @inheritdoc
 *
 * Format a product's media gallery information to conform to GraphQL schema representation
 * @deprecated 100.3.3
 */
class MediaGalleryEntries implements ResolverInterface
{
    /**
     * @inheritdoc
     *
     * Format product's media gallery entry data to conform to GraphQL schema
     *
     * @param Field            $field
     * @param ContextInterface $context
     * @param ResolveInfo      $info
     * @param array|null       $value
     * @param array|null       $args
     *
     * @return array
     * @throws \Exception
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!isset($value['model'])) {
            throw new LocalizedException(
                __('"model" value should be specified')
            );
        }

        /** @var Product $product */
        $product = $value['model'];

        $mediaGalleryEntries = [];
        if (!empty($product->getMediaGalleryEntries())) {
            foreach ($product->getMediaGalleryEntries() as $key => $entry) {
                $label = $entry->getLabel();
                if ($label && substr($label, -7) == '-swatch') {
                    continue;
                }

                $mediaGalleryEntries[$key] = $entry->getData();
                if ($entry->getExtensionAttributes(
                    ) && $entry->getExtensionAttributes()->getVideoContent()) {
                    $mediaGalleryEntries[$key]['video_content']
                        = $entry->getExtensionAttributes()
                        ->getVideoContent()
                        ->getData();
                }
            }
        }
        return $mediaGalleryEntries;
    }
}
