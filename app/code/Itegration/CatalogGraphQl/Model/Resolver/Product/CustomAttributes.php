<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\CatalogGraphQl\Model\Resolver\Product;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class CustomAttributes implements ResolverInterface
{
    /**
     * @param Field            $field
     * @param ContextInterface $context
     * @param ResolveInfo      $info
     * @param array|null       $value
     * @param array|null       $args
     *
     * @return Value|mixed|void
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $returnArray = [];

        if (array_key_exists('model', $value)) {
            /** @var Product $product */
            $product = $value['model'];
            $product->load($product->getId());

            /** @var Attribute $attribute */
            foreach ($product->getAttributes() as $attribute) {
                if ($attribute->getIsUserDefined()) {
                    $attributeValue = $attribute->getFrontend()->getValue(
                        $product
                    );
                    if (is_array($attributeValue)) {
                        $attributeValues = [];
                        foreach ($attributeValue as $attributeVal) {
                            $attributeValues[] = $attributeVal . '';
                        }
                        $attributeValue = implode(', ', $attributeValues);
                    }

                    $returnArray[] = [
                        'code'  => $attribute->getAttributeCode(),
                        'label' => $attribute->getFrontend()->getLabel(),
                        'value' => trim($attributeValue . ''),
                    ];
                }
            }
        }

        return $returnArray;
    }

}
