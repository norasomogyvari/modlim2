<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\UrlRewriteGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Magento\UrlRewrite\Model\UrlFinderInterface;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;
use Wyomind\ElasticsearchCore\Helper\Config as ConfigHelper;

class Alternatives implements ResolverInterface
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var UrlFinderInterface
     */
    private $urlFinder;

    /**
     * @var ConfigHelper
     */
    private $configHelper;

    /**
     * Alternatives constructor.
     *
     * @param StoreManagerInterface $storeManager
     * @param UrlFinderInterface    $urlFinder
     * @param ConfigHelper          $configHelper
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        UrlFinderInterface $urlFinder,
        ConfigHelper $configHelper
    ) {
        $this->storeManager = $storeManager;
        $this->urlFinder = $urlFinder;
        $this->configHelper = $configHelper;
    }

    /**
     * @param Field            $field
     * @param ContextInterface $context
     * @param ResolveInfo      $info
     * @param array|null       $value
     * @param array|null       $args
     *
     * @return Value|mixed|void
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!array_key_exists(
                'relative_url',
                $value
            ) || empty($value['relative_url'])) {
            return [];
        }

        $actualStore = $this->storeManager->getStore();
        $stores = $this->storeManager->getStores(false, true);

        $returnArray = [];

        /** @var UrlRewrite $url */
        $url = $this->urlFinder->findOneByData(
            [
                'store_id'     => $actualStore->getId(),
                'request_path' => $value['relative_url'],
            ]
        );

        if (!($url instanceof UrlRewrite) || !$url->getUrlRewriteId()) {
            return [];
        }

        /** @var Store $store */
        foreach ($stores as $store) {
            if ($store->getId() != $actualStore->getId()) {
                $storeUrl = $this->getAlternativeUrl($url, $store);

                if ($storeUrl) {
                    $returnArray[] = $storeUrl;
                }
            }
        }

        return $returnArray;
    }

    /**
     * @param UrlRewrite $url
     * @param Store      $store
     *
     * @return null|array
     */
    private function getAlternativeUrl($url, $store)
    {
        $rewriteUrl = $this->urlFinder->findOneByData(
            [
                'target_path' => $url->getTargetPath(),
                'store_id'    => $store->getId(),
            ]
        );

        if (($rewriteUrl instanceof UrlRewrite) &&
            $rewriteUrl->getUrlRewriteId()
        ) {
            $lang = $this->convertLangToISO6391(
                $this->configHelper->getLocaleCode($store->getId())
            );
            return [
                'lang' => $lang,
                'code' => $store->getCode(),
                'url'  => $rewriteUrl->getRequestPath(),
            ];
        }

        return null;
    }

    /**
     * @param string $langCode
     *
     * @return string
     */
    private function convertLangToISO6391($langCode)
    {
        return strtolower(str_replace('_', '-', $langCode));
    }
}
