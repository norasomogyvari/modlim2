/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'Magento_Ui/js/form/element/textarea',
    'mage/adminhtml/wysiwyg/widget'
], function (Textarea) {
    'use strict';

    var NOSTO_ID_PLACEHOLDER = 'NOSTO_ID_PLACEHOLDER';

    return Textarea.extend({
        defaults: {
            elementTmpl: 'Itegration_PageBuilder/form/element/nosto-code'
        },

        /**
         * Click event for Insert Widget Button
         */
        clickInsertWidget: function () {
            return widgetTools.openDialog(
                this.widgetUrl.replace(NOSTO_ID_PLACEHOLDER, this.uid)
            );
        },

        /**
         * Click event for Insert Image Button
         */
        clickInsertImage: function () {
            return MediabrowserUtility.openDialog(
                this.imageUrl.replace(NOSTO_ID_PLACEHOLDER, this.uid)
            );
        },

        /**
         * Click event for Insert Variable Button
         */
        clickInsertVariable: function () {
            return MagentovariablePlugin.loadChooser(
                this.variableUrl,
                this.uid
            );
        },
    });
});
