<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\Dropship\Plugin\QuoteGraphQl\Model\Cart;

use Magento\GraphQl\Model\Query\ContextInterface;
use Magento\Quote\Api\Data\CartInterface;
use Magento\QuoteGraphQl\Model\Cart\SetShippingAddressesOnCart as OriginalSetShippingAddressesOnCart;
use Itegration\Dropship\Helper\Region as RegionHelper;

class SetShippingAddressesOnCart
{
    /**
     * @var RegionHelper
     */
    private $regionHelper;

    /**
     * SetShippingAddressesOnCart constructor.
     *
     * @param RegionHelper $regionHelper
     */
    public function __construct(
        RegionHelper $regionHelper
    ){
        $this->regionHelper = $regionHelper;
    }

    /**
     * @param OriginalSetShippingAddressesOnCart $subject
     * @param ContextInterface                   $context
     * @param CartInterface                      $cart
     * @param array                              $shippingAddressesInput
     *
     * @return array
     */
    public function beforeExecute(
        OriginalSetShippingAddressesOnCart $subject,
        ContextInterface $context,
        CartInterface $cart,
        array $shippingAddressesInput
    ) {
        $shippingAddressInput = current($shippingAddressesInput);
        $addressInput = $shippingAddressInput['address'] ?? [];

        if (array_key_exists('region', $addressInput) &&
            is_array($addressInput['region'])) {
            $region = $addressInput['region'];
            $addressInput = array_merge($addressInput, $region);
            if (array_key_exists('region_id', $addressInput) &&
                $addressInput['region_id']
            ) {
                $regionCode = $this->regionHelper->getRegionCodeFromId($addressInput['region_id']);
                $addressInput['region'] = $regionCode;
            }
            $shippingAddressInput['address'] = $addressInput;
            $shippingAddressesInput[0] = $shippingAddressInput;
        }

        return [$context, $cart, $shippingAddressesInput];
    }
}
