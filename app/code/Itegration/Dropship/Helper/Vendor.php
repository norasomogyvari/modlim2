<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\Dropship\Helper;

use Unirgy\Dropship\Model\ResourceModel\Vendor\Collection as VendorCollection;
use Unirgy\Dropship\Model\ResourceModel\Vendor\CollectionFactory as VendorCollectionFactory;
use Unirgy\Dropship\Model\Vendor as VendorModel;

class Vendor
{
    /**
     * @var VendorCollectionFactory
     */
    private $vendorCollectionFactory;

    /**
     * Vendor constructor.
     *
     * @param VendorCollectionFactory $vendorCollectionFactory
     */
    public function __construct(
        VendorCollectionFactory $vendorCollectionFactory
    ) {
        $this->vendorCollectionFactory = $vendorCollectionFactory;
    }

    /**
     * @param int $dropShipVendorId
     *
     * @return string
     */
    public function getDropShipNameFromId($dropShipVendorId)
    {
        /** @var VendorCollection $collection */
        $vendorCollection = $this->vendorCollectionFactory->create();
        $vendorCollection->addVendorFilter([$dropShipVendorId]);
        $vendorModel = $vendorCollection->getFirstItem();
        if ($vendorModel instanceof VendorModel && $vendorModel->getId()) {
            return (string)$vendorModel->getData('vendor_name');
        }
        return '';
    }
}
