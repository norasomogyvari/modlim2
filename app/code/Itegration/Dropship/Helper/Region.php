<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\Dropship\Helper;

use Magento\Directory\Model\ResourceModel\Region\CollectionFactory as RegionCollectionFactory;

class Region
{
    /**
     * @var RegionCollectionFactory
     */
    private $regionCollectionFactory;

    /**
     * @var array
     */
    private $regionCodes = [];

    /**
     * Region constructor.
     *
     * @param RegionCollectionFactory $regionCollectionFactory
     */
    public function __construct(
        RegionCollectionFactory $regionCollectionFactory
    ) {
        $this->regionCollectionFactory = $regionCollectionFactory;
    }

    /**
     * @param int $regionId
     *
     * @return string
     */
    public function getRegionCodeFromId($regionId)
    {
        $regionId = (int)$regionId;

        if (!array_key_exists($regionId, $this->regionCodes)) {
            $regionCollection = $this->regionCollectionFactory->create();
            $select = "SELECT code FROM " . $regionCollection->getTable('directory_country_region') . " WHERE region_id = ?";
            $regionCode = $regionCollection->getConnection()->fetchOne($select, $regionId);
            if (!$regionCode) {
                $regionCode = $regionId;
            }
            $this->regionCodes[$regionId] = $regionCode;
        }

        return $this->regionCodes[$regionId];
    }
}
