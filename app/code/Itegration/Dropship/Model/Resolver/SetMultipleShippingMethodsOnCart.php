<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\Dropship\Model\Resolver;

use Itegration\Dropship\Model\Cart\SetMultipleShippingMethodsOnCartInterface;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\QuoteGraphQl\Model\Cart\CheckCartCheckoutAllowance;
use Magento\QuoteGraphQl\Model\Cart\GetCartForUser;

class SetMultipleShippingMethodsOnCart implements ResolverInterface
{
    /**
     * @var GetCartForUser
     */
    private $getCartForUser;

    /**
     * @var SetMultipleShippingMethodsOnCartInterface
     */
    private $setMultipleShippingMethodsOnCart;

    /**
     * @var CheckCartCheckoutAllowance
     */
    private $checkCartCheckoutAllowance;

    /**
     * SetShippingMethodsOnCart constructor.
     *
     * @param GetCartForUser                            $getCartForUser
     * @param SetMultipleShippingMethodsOnCartInterface $setMultipleShippingMethodsOnCart
     * @param CheckCartCheckoutAllowance                $checkCartCheckoutAllowance
     */
    public function __construct(
        GetCartForUser $getCartForUser,
        SetMultipleShippingMethodsOnCartInterface $setMultipleShippingMethodsOnCart,
        CheckCartCheckoutAllowance $checkCartCheckoutAllowance
    ) {
        $this->getCartForUser = $getCartForUser;
        $this->setMultipleShippingMethodsOnCart = $setMultipleShippingMethodsOnCart;
        $this->checkCartCheckoutAllowance = $checkCartCheckoutAllowance;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (empty($args['input']['cart_id'])) {
            throw new GraphQlInputException(
                __('Required parameter "cart_id" is missing')
            );
        }
        $maskedCartId = $args['input']['cart_id'];

        if (empty($args['input']['shipping_method_all'])) {
            throw new GraphQlInputException(
                __('Required parameter "shipping_method_all" is missing')
            );
        }
        $shippingData = $args['input'];

        $storeId = (int)$context->getExtensionAttributes()->getStore()->getId();
        $cart = $this->getCartForUser->execute(
            $maskedCartId,
            $context->getUserId(),
            $storeId
        );
        $this->checkCartCheckoutAllowance->execute($cart);
        $this->setMultipleShippingMethodsOnCart->execute(
            $context,
            $cart,
            $shippingData
        );

        return [
            'cart' => [
                'model' => $cart,
            ],
        ];
    }
}
