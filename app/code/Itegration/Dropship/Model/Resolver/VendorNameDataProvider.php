<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\Dropship\Model\Resolver;

use Itegration\Dropship\Helper\Vendor as VendorHelper;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Unirgy\Dropship\Helper\Data as HelperData;

class VendorNameDataProvider implements ResolverInterface
{
    /**
     * @var HelperData
     */
    private $helperData;

    /**
     * @var VendorHelper
     */
    private $vendorHelper;

    /**
     * VendorNameDataProvider constructor.
     *
     * @param HelperData $helperData
     * @param VendorHelper $vendorHelper
     */
    public function __construct(
        HelperData $helperData,
        VendorHelper $vendorHelper
    ) {
        $this->helperData = $helperData;
        $this->vendorHelper = $vendorHelper;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!$this->helperData->isActive()) {
            return '';
        }

        if (!array_key_exists('model', $value) ||
            !$value['model'] instanceof ProductInterface) {
            throw new LocalizedException(
                __('"model" value should be specified')
            );
        }

        /* @var $product ProductInterface */
        $product = $value['model'];
        $product->load($product->getId());

        if ($product->getData('udropship_vendor_name')) {
            return $product->getData('udropship_vendor_name');
        }

        $dropShipVendorId = $product->getData('udropship_vendor');
        if ($dropShipVendorId) {
            return $this->vendorHelper->getDropShipNameFromId(
                $dropShipVendorId
            );
        }

        return null;
    }
}
