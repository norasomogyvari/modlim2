<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\Dropship\Model\Resolver;

use Magento\Directory\Model\Currency;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Quote\Model\Quote\Address\RateResult\Error as RateError;
use Magento\Store\Api\Data\StoreInterface;
use Unirgy\DropshipSplit\Api\ShippingMethodInterface as DropShipShippingMethodInterface;
use Unirgy\DropshipSplit\Api\ShippingMethodInterfaceFactory as DropShipShippingMethodInterfaceFactory;
use Unirgy\DropshipSplit\Api\ShippingMethodManagementInterface;
use Unirgy\DropshipSplit\Model\Quote\Address as QuoteAddress;
use Unirgy\DropshipSplit\Model\Quote\RateFactory as DropShipQuoteRateRateFactory;
use Unirgy\DropshipSplit\Model\ShippingMethod as DropShipShippingMethod;

class AvailableShippingMethods implements ResolverInterface
{
    /**
     * @var ShippingMethodManagementInterface
     */
    private $shippingMethodManagement;

    /**
     * @var ExtensibleDataObjectConverter
     */
    private $dataObjectConverter;

    /**
     * @var DropShipQuoteRateRateFactory
     */
    private $dropShipQuoteRateFactory;

    /**
     * @var DropShipShippingMethodInterfaceFactory
     */
    private $dropShipShippingMethodFactory;

    /**
     * AvailableShippingMethods constructor.
     *
     * @param ShippingMethodManagementInterface      $shippingMethodManagement
     * @param ExtensibleDataObjectConverter          $dataObjectConverter
     * @param DropShipQuoteRateRateFactory           $dropShipQuoteRateFactory
     * @param DropShipShippingMethodInterfaceFactory $dropShipShippingMethodFactory
     */
    public function __construct(
        ShippingMethodManagementInterface $shippingMethodManagement,
        ExtensibleDataObjectConverter $dataObjectConverter,
        DropShipQuoteRateRateFactory $dropShipQuoteRateFactory,
        DropShipShippingMethodInterfaceFactory $dropShipShippingMethodFactory
    ) {
        $this->shippingMethodManagement = $shippingMethodManagement;
        $this->dataObjectConverter = $dataObjectConverter;
        $this->dropShipQuoteRateFactory = $dropShipQuoteRateFactory;
        $this->dropShipShippingMethodFactory = $dropShipShippingMethodFactory;
    }

    /**
     * @param Field            $field
     * @param ContextInterface $context
     * @param ResolveInfo      $info
     * @param array|null       $value
     * @param array|null       $args
     *
     * @return Value|mixed|void
     * @throws LocalizedException
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!isset($value['model'])) {
            throw new LocalizedException(
                __('"model" values should be specified')
            );
        }

        /** @var QuoteAddress $address */
        $address = $value['model'];

        $address->setCollectShippingRates(true);
        $address->collectShippingRates();

        if (!$address->getQuote()) {
            throw new LocalizedException(__('Quote not exists'));
        }

        $quote = $address->getQuote();

        $shippingMethods = $this->shippingMethodManagement->estimateByExtendedAddress(
            $quote->getId(),
            $address
        );

        $returnData = [];

        /** @var DropShipShippingMethod $shippingMethod */
        foreach ($shippingMethods as $shippingMethod) {
            if ($shippingMethod instanceof RateError) {
                $methodData = $shippingMethod->getData();
            } else {
                $methodData = $this->dataObjectConverter->toFlatArray(
                    $shippingMethod,
                    [],
                    DropShipShippingMethodInterface::class
                );
            }
            $methodData = $this->processMoneyTypeData(
                $methodData,
                $quote->getQuoteCurrencyCode(),
                $context->getExtensionAttributes()->getStore()
            );

            $returnData[] = $methodData;
        }

        return $returnData;
    }

    /**
     * Process money type data
     *
     * @param array          $data
     * @param string         $quoteCurrencyCode
     * @param StoreInterface $store
     *
     * @return array
     */
    private function processMoneyTypeData($data, $quoteCurrencyCode, $store)
    {
        if (isset($data['amount'])) {
            $data['amount'] = [
                'value' => $data['amount'], 'currency' => $quoteCurrencyCode,
            ];
        }

        if (isset($data['base_amount'])) {
            /** @var Currency $currency */
            $currency = $store->getBaseCurrency();
            $data['base_amount'] = [
                'value'    => $data['base_amount'],
                'currency' => $currency->getCode(),
            ];
        }

        if (isset($data['price_excl_tax'])) {
            $data['price_excl_tax'] = [
                'value'    => $data['price_excl_tax'],
                'currency' => $quoteCurrencyCode,
            ];
        }

        if (isset($data['price_incl_tax'])) {
            $data['price_incl_tax'] = [
                'value'    => $data['price_incl_tax'],
                'currency' => $quoteCurrencyCode,
            ];
        }
        return $data;
    }
}
