<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\Dropship\Model\Cart;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Quote\Api\Data\AddressInterface;
use Magento\Quote\Api\Data\CartInterface;
use Unirgy\DropshipSplit\Api\ShippingInformationInterface;
use Unirgy\DropshipSplit\Api\ShippingInformationInterfaceFactory;
use Unirgy\DropshipSplit\Api\ShippingInformationManagementInterface;
use Unirgy\DropshipSplit\Api\ShippingMethodInterface;
use Unirgy\DropshipSplit\Api\ShippingMethodInterfaceFactory;

class AssignMultipleShippingMethodToCart
{
    /**
     * @var ShippingInformationInterfaceFactory
     */
    private $shippingInformationFactory;

    /**
     * @var ShippingInformationManagementInterface
     */
    private $shippingInformationManagement;

    /**
     * @var ShippingMethodInterfaceFactory
     */
    private $shippingMethodFactory;

    /**
     * @var DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * AssignMultipleShippingMethodToCart constructor.
     *
     * @param ShippingInformationInterfaceFactory    $shippingInformationFactory
     * @param ShippingInformationManagementInterface $shippingInformationManagement
     * @param ShippingMethodInterfaceFactory         $shippingMethodFactory
     * @param DataObjectHelper                       $dataObjectHelper
     */
    public function __construct(
        ShippingInformationInterfaceFactory $shippingInformationFactory,
        ShippingInformationManagementInterface $shippingInformationManagement,
        ShippingMethodInterfaceFactory $shippingMethodFactory,
        DataObjectHelper $dataObjectHelper
    ) {
        $this->shippingInformationFactory = $shippingInformationFactory;
        $this->shippingInformationManagement = $shippingInformationManagement;
        $this->shippingMethodFactory = $shippingMethodFactory;
        $this->dataObjectHelper = $dataObjectHelper;
    }

    /**
     * @param CartInterface    $cart
     * @param AddressInterface $quoteAddress
     * @param string           $carrierCode
     * @param string           $methodCode
     * @param array            $shippingMethodAll
     *
     * @throws GraphQlInputException
     * @throws GraphQlNoSuchEntityException
     */
    public function execute(
        CartInterface $cart,
        AddressInterface $quoteAddress,
        $carrierCode,
        $methodCode,
        $shippingMethodAll
    ) {
        $shippingMethodAllObjectData = [];
        foreach ($shippingMethodAll as $shippingMethod) {
            /** @var ShippingMethodInterface $shippingMethodObject */
            $shippingMethodObject = $this->shippingMethodFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $shippingMethodObject,
                $shippingMethod,
                ShippingMethodInterface::class
            );
            $shippingMethodAllObjectData[] = $shippingMethodObject;
        }

        /** @var ShippingInformationInterface $shippingInformation */
        $shippingInformation = $this->shippingInformationFactory->create(
            [
                'data' => [
                    ShippingInformationInterface::SHIPPING_ADDRESS      => $quoteAddress,
                    ShippingInformationInterface::SHIPPING_CARRIER_CODE => $carrierCode,
                    ShippingInformationInterface::SHIPPING_METHOD_CODE  => $methodCode,
                    ShippingInformationInterface::SHIPPING_METHOD_ALL   => $shippingMethodAllObjectData,
                ],
            ]
        );

        try {
            $this->shippingInformationManagement->saveAddressInformation(
                $cart->getId(),
                $shippingInformation
            );
        } catch (NoSuchEntityException $e) {
            throw new GraphQlNoSuchEntityException(__($e->getMessage()), $e);
        } catch (LocalizedException $e) {
            throw new GraphQlInputException(__($e->getMessage()), $e);
        }
    }
}
