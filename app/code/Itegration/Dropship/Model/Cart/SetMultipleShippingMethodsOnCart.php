<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\Dropship\Model\Cart;

use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Quote\Api\Data\CartInterface;

class SetMultipleShippingMethodsOnCart implements SetMultipleShippingMethodsOnCartInterface
{
    /**
     * @var AssignMultipleShippingMethodToCart
     */
    private $assignMultipleShippingMethodToCart;

    /**
     * SetMultipleShippingMethodsOnCart constructor.
     *
     * @param AssignMultipleShippingMethodToCart $assignMultipleShippingMethodToCart
     */
    public function __construct(
        AssignMultipleShippingMethodToCart $assignMultipleShippingMethodToCart
    ) {
        $this->assignMultipleShippingMethodToCart = $assignMultipleShippingMethodToCart;
    }

    /**
     * @param ContextInterface $context
     * @param CartInterface    $cart
     * @param array            $shippingData
     *
     * @return void
     * @throws GraphQlInputException
     * @throws GraphQlNoSuchEntityException
     */
    public function execute(
        ContextInterface $context,
        CartInterface $cart,
        array $shippingData
    ) {
        if (!isset($shippingData['shipping_carrier_code']) || empty($shippingData['shipping_carrier_code'])) {
            throw new GraphQlInputException(
                __('Required parameter "shipping_carrier_code" is missing.')
            );
        }
        $carrierCode = $shippingData['shipping_carrier_code'];

        if (!isset($shippingData['shipping_method_code']) || empty($shippingData['shipping_method_code'])) {
            throw new GraphQlInputException(
                __('Required parameter "shipping_method_code" is missing.')
            );
        }
        $methodCode = $shippingData['shipping_method_code'];

        if (!isset($shippingData['shipping_method_all']) || empty($shippingData['shipping_method_all'])) {
            throw new GraphQlInputException(
                __('Required parameter "shipping_method_all" is missing.')
            );
        }
        $shippingMethodAll = $shippingData['shipping_method_all'];

        $shippingAddress = $cart->getShippingAddress();
        $this->assignMultipleShippingMethodToCart->execute(
            $cart,
            $shippingAddress,
            $carrierCode,
            $methodCode,
            $shippingMethodAll
        );
    }
}
