<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\Dropship\Model\ResourceModel\VendorName;

use Itegration\Dropship\Helper\Vendor as VendorHelper;
use Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend;
use Magento\Framework\DataObject;
use Unirgy\Dropship\Helper\Data as HelperData;

class Backend extends AbstractBackend
{
    /**
     * @var HelperData
     */
    private $helperData;

    /**
     * @var VendorHelper
     */
    private $vendorHelper;

    /**
     * Backend constructor.
     *
     * @param HelperData $helperData
     * @param VendorHelper $vendorHelper
     */
    public function __construct(
        HelperData $helperData,
        VendorHelper $vendorHelper
    ) {
        $this->helperData = $helperData;
        $this->vendorHelper = $vendorHelper;
    }

    /**
     * @return bool
     */
    protected function _isEnabled()
    {
        return $this->helperData->isActive();
    }

    /**
     * @return string
     */
    public function getDefaultValue()
    {
        return parent::getDefaultValue();
    }

    /**
     * @param DataObject $object
     *
     * @return $this
     */
    public function beforeSave($object)
    {
        if (!$this->_isEnabled()) {
            parent::beforeSave($object);
            return $this;
        }
        $dropShipVendorId = $object->getData('udropship_vendor');
        $attrCode = $this->getAttribute()->getAttributeCode();
        $object->setData(
            $attrCode,
            $this->getDropShipNameFromId($dropShipVendorId)
        );

        return $this;
    }

    /**
     * @param DataObject $object
     *
     * @return $this
     */
    public function afterLoad($object)
    {
        parent::afterLoad($object);
        if (!$this->_isEnabled()) {
            return $this;
        }
        $dropShipVendorId = $object->getData('udropship_vendor');
        $attrCode = $this->getAttribute()->getAttributeCode();
        $dropShipName = $object->getData($attrCode);

        if (!$dropShipName && $dropShipVendorId) {
            $object->setData(
                $attrCode,
                $this->getDropShipNameFromId($dropShipVendorId)
            );
        }

        return $this;
    }

    /**
     * @param int $dropShipVendorId
     *
     * @return string
     */
    protected function getDropShipNameFromId($dropShipVendorId)
    {
        return $this->vendorHelper->getDropShipNameFromId($dropShipVendorId);
    }
}
