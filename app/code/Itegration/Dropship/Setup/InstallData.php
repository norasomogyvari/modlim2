<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\Dropship\Setup;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Setup\CategorySetup;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Unirgy\Dropship\Setup\QuoteSetupFactory;
use Unirgy\Dropship\Setup\SalesSetupFactory;

class InstallData implements InstallDataInterface
{
    /**
     * @var CategorySetupFactory
     */
    private $categorySetupFactory;

    /**
     * @var QuoteSetupFactory
     */
    private $quoteSetupFactory;

    /**
     * @var SalesSetupFactory
     */
    private $salesSetupFactory;

    /**
     * InstallData constructor.
     *
     * @param CategorySetupFactory $categorySetupFactory
     * @param QuoteSetupFactory    $quoteSetupFactory
     * @param SalesSetupFactory    $salesSetupFactory
     */
    public function __construct(
        CategorySetupFactory $categorySetupFactory,
        QuoteSetupFactory $quoteSetupFactory,
        SalesSetupFactory $salesSetupFactory
    ) {
        $this->categorySetupFactory = $categorySetupFactory;
        $this->quoteSetupFactory = $quoteSetupFactory;
        $this->salesSetupFactory = $salesSetupFactory;
    }

    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {

        /** @var CategorySetup $catalogSetup */
        $catalogSetup = $this->categorySetupFactory->create(
            ['setup' => $setup]
        );

        $catalogSetup->addAttribute(
            Product::ENTITY,
            'udropship_vendor_name',
            [
                'type'                    => 'varchar',
                'input'                   => 'text',
                'length'                  => 255,
                'label'                   => 'Dropship Vendor Name',
                'group'                   => 'General',
                'global'                  => ScopedAttributeInterface::SCOPE_GLOBAL,
                'user_defined'            => 0,
                'required'                => 0,
                'visible'                 => 1,
                'backend'                 => '\Itegration\Dropship\Model\ResourceModel\VendorName\Backend',
                'visible_on_front'        => false,
                'is_used_in_grid'         => false,
                'is_visible_in_grid'      => false,
                'is_filterable_in_grid'   => false,
                'used_in_product_listing' => true,
                'is_used_for_price_rules' => false,
            ]
        );
    }
}
