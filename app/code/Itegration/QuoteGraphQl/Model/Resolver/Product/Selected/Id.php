<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\QuoteGraphQl\Model\Resolver\Product\Selected;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Quote\Model\Quote\Item as QuoteItem;

class Id implements ResolverInterface
{
    /**
     * @param Field            $field
     * @param ContextInterface $context
     * @param ResolveInfo      $info
     * @param array|null       $value
     * @param array|null       $args
     *
     * @return Value|mixed|void
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $returnArray = [];
        if (array_key_exists('cart_item', $value)) {
            /** @var QuoteItem $cartItem */
            $cartItem = $value['cart_item'];
            foreach ($cartItem->getChildren() as $childrenItem) {
                $returnArray[] = $childrenItem->getProduct()->getId();
            }
        }

        return $returnArray;
    }

}
