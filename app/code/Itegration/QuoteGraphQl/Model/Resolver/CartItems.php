<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\QuoteGraphQl\Model\Resolver;

use Magento\Catalog\Model\Product\Configuration\Item\ItemResolverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Quote\Model\Quote\Item as QuoteItem;

class CartItems implements ResolverInterface
{
    /**
     * @var ItemResolverInterface
     */
    private $itemResolver;

    /**
     * CartItems constructor.
     *
     * @param ItemResolverInterface $itemResolver
     */
    public function __construct(
        ItemResolverInterface $itemResolver
    ) {
        $this->itemResolver = $itemResolver;
    }

    /**
     * @param Field            $field
     * @param ContextInterface $context
     * @param ResolveInfo      $info
     * @param array|null       $value
     * @param array|null       $args
     *
     * @return array|Value|mixed
     * @throws LocalizedException
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!isset($value['model'])) {
            throw new LocalizedException(
                __('"model" value should be specified')
            );
        }
        $cart = $value['model'];

        $itemsData = [];
        /** @var QuoteItem $cartItem */
        foreach ($cart->getAllVisibleItems() as $cartItem) {
            $productData = $cartItem->getProduct()->getData();
            $productData['model'] = $cartItem->getProduct();
            $productData['child_model'] = $this->itemResolver->getFinalProduct($cartItem);
            $productData['cart_item'] = $cartItem;

            $itemsData[] = [
                'id'       => $cartItem->getItemId(),
                'quantity' => $cartItem->getQty(),
                'product'  => $productData,
                'model'    => $cartItem,
            ];
        }
        return $itemsData;
    }

}
