<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\QuoteGraphQl\Plugin\Model\Cart;

use Itegration\Dropship\Helper\Region as RegionHelper;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\GraphQl\Model\Query\ContextInterface;
use Magento\Quote\Api\BillingAddressManagementInterface;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Model\Quote\Address as QuoteAddress;
use Magento\Quote\Model\Quote\AddressFactory as QuoteAddressFactory;
use Magento\Quote\Model\ShippingAddressManagementInterface;
use Magento\QuoteGraphQl\Model\Cart\SetBillingAddressOnCart as OriginalSetBillingAddressOnCart;

class SetBillingAddressesOnCart
{
    /**
     * @var RegionHelper
     */
    private $regionHelper;

    /**
     * @var ShippingAddressManagementInterface
     */
    private $shippingAddressManagement;

    /**
     * @var BillingAddressManagementInterface
     */
    private $billingAddressManagement;

    /**
     * @var QuoteAddressFactory
     */
    private $quoteAddressFactory;

    private $_addressFields = [
        'customer_id',
        'customer_address_id',
        'email',
        'prefix',
        'firstname',
        'middlename',
        'lastname',
        'suffix',
        'company',
        'street',
        'city',
        'region',
        'region_id',
        'postcode',
        'country_id',
        'telephone',
        'fax',
    ];

    /**
     * SetBillingAddressesOnCart constructor.
     *
     * @param RegionHelper                       $regionHelper
     * @param ShippingAddressManagementInterface $shippingAddressManagement
     * @param BillingAddressManagementInterface  $billingAddressManagement
     * @param QuoteAddressFactory                $quoteAddressFactory
     */
    public function __construct(
        RegionHelper $regionHelper,
        ShippingAddressManagementInterface $shippingAddressManagement,
        BillingAddressManagementInterface $billingAddressManagement,
        QuoteAddressFactory $quoteAddressFactory
    ) {
        $this->regionHelper = $regionHelper;
        $this->shippingAddressManagement = $shippingAddressManagement;
        $this->billingAddressManagement = $billingAddressManagement;
        $this->quoteAddressFactory = $quoteAddressFactory;
    }

    /**
     * @param OriginalSetBillingAddressOnCart $subject
     * @param ContextInterface                $context
     * @param CartInterface                   $cart
     * @param array                           $billingAddressInput
     *
     * @return array
     */
    public function beforeExecute(
        OriginalSetBillingAddressOnCart $subject,
        ContextInterface $context,
        CartInterface $cart,
        array $billingAddressInput
    ) {
        $addressInput = $billingAddressInput['address'] ?? [];

        if (array_key_exists('region', $addressInput) &&
            is_array($addressInput['region'])) {
            $region = $addressInput['region'];
            $addressInput = array_merge($addressInput, $region);
            if (array_key_exists('region_id', $addressInput) &&
                $addressInput['region_id']
            ) {
                $regionCode = $this->regionHelper->getRegionCodeFromId(
                    $addressInput['region_id']
                );
                $addressInput['region'] = $regionCode;
            }
            $billingAddressInput['address'] = $addressInput;
        }

        return [$context, $cart, $billingAddressInput];
    }

    /**
     * @param OriginalSetBillingAddressOnCart $subject
     * @param callable                        $proceed
     * @param ContextInterface                $context
     * @param CartInterface                   $cart
     * @param array                           $billingAddressInput
     *
     * @return mixed
     * @throws InputException
     * @throws NoSuchEntityException
     */
    public function aroundExecute(
        OriginalSetBillingAddressOnCart $subject,
        callable $proceed,
        ContextInterface $context,
        CartInterface $cart,
        array $billingAddressInput
    ) {
        $sameAsShipping = isset($billingAddressInput['use_for_shipping'])
            ? (bool)$billingAddressInput['use_for_shipping'] : false;
        $sameAsShipping = isset($billingAddressInput['same_as_shipping'])
            ? (bool)$billingAddressInput['same_as_shipping'] : $sameAsShipping;

        if (!$sameAsShipping) {
            return $proceed($context, $cart, $billingAddressInput);
        }

        $this->setBillingAddressFromShipping($cart->getId());
    }

    /**
     * @param int $cartId
     *
     * @throws InputException
     * @throws NoSuchEntityException
     */
    private function setBillingAddressFromShipping($cartId)
    {
        /** @var QuoteAddress $shippingAddress */
        $shippingAddress = $this->shippingAddressManagement->get($cartId);
        if ($shippingAddress instanceof QuoteAddress) {
            $billingAddress = $this->quoteAddressFactory->create();
            $addressData = [];
            foreach ($shippingAddress->getData() as $key => $value) {
                if (in_array($key, $this->_addressFields)) {
                    $addressData[$key] = $value;
                }
            }
            $billingAddress->setData($addressData)->setAddressType('billing');
            $this->billingAddressManagement->assign(
                $cartId,
                $billingAddress,
                false
            );
        }
    }
}
