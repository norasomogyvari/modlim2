<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\DirectoryGraphQl\Model\Resolver;

use Magento\DirectoryGraphQl\Model\Resolver\Countries as OriginalCountries;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class Countries extends OriginalCountries
{
    private $availableSortBy = [
        'name',
    ];

    /**
     * @param Field            $field
     * @param ContextInterface $context
     * @param ResolveInfo      $info
     * @param array|null       $value
     * @param array|null       $args
     *
     * @return array|Value|mixed
     * @throws \Exception
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $output = parent::resolve($field, $context, $info, $value, $args);

        $sortBy = false;
        if ($args &&
            array_key_exists('sort_by', $args) &&
            in_array($args['sort_by'], $this->availableSortBy)
        ) {
            $sortBy = $args['sort_by'];
        }

        switch ($sortBy) {
            case 'name':
                usort($output, [$this, 'sortByName']);
                break;
        }

        return $output;
    }

    public static function sortByName($a, $b)
    {
        $al = strtolower($a['full_name_locale']);
        $bl = strtolower($b['full_name_locale']);
        if ($al == $bl) {
            return 0;
        }
        return ($al > $bl) ? +1 : -1;
    }
}
