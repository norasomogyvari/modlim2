type CustomerOrder @doc(description: "Order mapping fields") {
    ship_to: String @doc(description: "The Shipping Address name") @resolver(class: "Itegration\\SalesGraphQl\\Model\\Resolver\\Orders\\ShipTo")
}

interface ProductInterface {
    price: ProductPrices @doc(description: "A ProductPrices object, indicating the price of an item.") @resolver(class: "Itegration\\SalesGraphQl\\Model\\Resolver\\Product\\Price")
    backorder: BackOrder @resolver(class: "Itegration\\SalesGraphQl\\Model\\Resolver\\Product\\BackOrder")
}

type ProductPrices @doc(description: "ProductPrices is deprecated, replaced by PriceRange. The ProductPrices object contains the regular price of an item, as well as its minimum and maximum prices. Only composite products, which include bundle, configurable, and grouped products, can contain a minimum and maximum price.") {
    basePrice: Price @doc(description: "The base price of a product.")
    finalPrice: Price @doc(description: "The final price of a product.")
}

type BackOrder {
    ship_message: String
    ship_date: String
    is_backorder: Boolean
    qty: Float
    salable_qty: Float
}

type Query {
    customerOrderDetails (
        order_id: Int! @doc(description: "Order ID")
    ): CustomerOrderDetails
    @resolver(class: "Itegration\\SalesGraphQl\\Model\\Resolver\\Orders\\Details") @doc(description: "Order Details")
}

type CustomerOrderDetails @doc(description: "Order Details") {
    id: Int @doc(description: "Order ID")
    shipping_address: OrderDetailsAddress @doc(description: "Shipping Address")
    shipping_method: String @doc(description: "Shipping method")
    billing_address: OrderDetailsAddress @doc(description: "Shipping Address")
    payment_method: String @doc(description: "Payment method")
    coupon_code: String
    base_discount_amount: Price @doc(description: "Base Discount Amount")
    base_discount_canceled: Price @doc(description: "Base Discount Canceled")
    base_discount_refunded: Price @doc(description: "Base Discount Refunded")
    base_grand_total: Price @doc(description: "Base Grand Total")
    base_shipping_amount: Price @doc(description: "Base Shipping Amount")
    base_shipping_canceled: Price @doc(description: "Base Shipping Canceled")
    base_shipping_invoiced: Price @doc(description: "Base Shipping Invoiced")
    base_shipping_refunded: Price @doc(description: "Base Shipping Refunded")
    base_shipping_tax_amount: Price @doc(description: "Base Shipping Tax Amount")
    base_shipping_tax_refunded: Price @doc(description: "Base Shipping Tax Refunded")
    base_subtotal: Price @doc(description: "Base Subtotal")
    base_subtotal_canceled: Price @doc(description: "Base Subtotal Canceled")
    base_subtotal_invoiced: Price @doc(description: "Base Subtotal Invoiced")
    base_subtotal_refunded: Price @doc(description: "Base Subtotal Refunded")
    base_tax_amount: Price @doc(description: "Base Tax Amount")
    base_tax_canceled: Price @doc(description: "Base Tax Canceled")
    base_tax_invoiced: Price @doc(description: "Base Tax Invoiced")
    base_tax_refunded: Price @doc(description: "Base Tax Refunded")
    base_total_canceled: Price @doc(description: "Base Total Canceled")
    base_total_invoiced: Price @doc(description: "Base Total Invoiced")
    discount_amount: Price @doc(description: "Discount Amount")
    discount_canceled: Price @doc(description: "Discount Canceled")
    discount_invoiced: Price @doc(description: "Discount Invoiced")
    discount_refunded: Price @doc(description: "Discount Refunded")
    grand_total: Price @doc(description: "Grand Total")
    shipping_discount: Price @doc(description: "Shipping Discount")
    shipping_amount: Price @doc(description: "Shipping Amount")
    shipping_canceled: Price @doc(description: "Shipping Canceled")
    shipping_invoiced: Price @doc(description: "Shipping Invoiced")
    shipping_refunded: Price @doc(description: "Shipping Refunded")
    shipping_tax_amount: Price @doc(description: "Shipping Tax Amount")
    shipping_tax_refunded: Price @doc(description: "Shipping Tax Refunded")
    subtotal: Price @doc(description: "Subtotal")
    subtotal_canceled: Price @doc(description: "Subtotal Canceled")
    subtotal_invoiced: Price @doc(description: "Subtotal Invoiced")
    subtotal_refunded: Price @doc(description: "Subtotal Refunded")
    tax_amount: Price @doc(description: "Tax amount")
    tax_canceled: Price @doc(description: "Tax Canceled")
    tax_invoiced: Price @doc(description: "Tax Invoiced")
    tax_refunded: Price @doc(description: "Tax Refunded")
    total_canceled: Price @doc(description: "Total Canceled")
    total_invoiced: Price @doc(description: "Total Invoiced")
    total_refunded: Price @doc(description: "Total Refunded")
    items: [OrderDetailsItems] @doc(description: "Order Items")
    shipments: [CustomerOrderDetailShipment] @doc(description: "OrderShipments")
    created_at: String
}

type CustomerOrderDetailShipment {
    id: Int @doc(description: "Order Shipment ID")
    increment_id: String
    items: [ShipmentItem]
}

type ShipmentItem {
    id: Int @doc(description: "Order Item ID")
    name: String @doc(description: "Name")
    sku: String @doc(description: "SKU")
    qty: Float @doc(description: "Row Total")
    options: [OrderDetailsItemOption]
}

type OrderDetailsItems @doc(description: "Order Items") {
    id: Int @doc(description: "Order Item ID")
    name: String @doc(description: "Name")
    sku: String @doc(description: "SKU")
    price: Price @doc(description: "Product Price")
    discount_amount: Price @doc(description: "Discount Amount")
    row_total: Price @doc(description: "Row Total")
    qty: Float @doc(description: "Row Total")
    options: [OrderDetailsItemOption]
    udropship_vendor_id: Int @doc(description: "Dropship Vendor Id")
    udropship_vendor_name: String @doc(description: "Dropship Vendor Name")
    product: ProductInterface
}

type OrderDetailsItemOption @doc(description: "Order Item Option") {
    label: String
    value: String
}

type OrderDetailsAddress  @doc(description: "Order Address") {
    id: Int @doc(description: "Order Address ID")
    firstname: String
    lastname: String
    company: String
    street: [String]
    city: String!
    region: OrderDetailsAddressRegion
    postcode: String
    country: CartAddressCountry
    telephone: String
    email: String @doc(description: "E-mail")
}

type OrderDetailsAddressRegion {
    region: String
    region_id: Int
    region_code: String
}
