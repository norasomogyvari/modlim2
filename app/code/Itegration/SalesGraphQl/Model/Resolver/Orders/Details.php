<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\SalesGraphQl\Model\Resolver\Orders;

use Itegration\SalesGraphQl\Model\Resolver\Orders\Details\Order as OrderDetails;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlAuthorizationException;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order as OrderModel;

class Details implements ResolverInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var OrderDetails
     */
    private $orderDetails;

    /**
     * @var bool
     */
    private $isDebugMode = true;

    /**
     * Details constructor.
     *
     * @param OrderRepositoryInterface $orderRepository
     * @param OrderDetails             $orderDetails
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        OrderDetails $orderDetails
    ) {
        $this->orderRepository = $orderRepository;
        $this->orderDetails = $orderDetails;
    }

    /**
     * @param Field            $field
     * @param ContextInterface $context
     * @param ResolveInfo      $info
     * @param array|null       $value
     * @param array|null       $args
     *
     * @return null|array
     * @throws GraphQlAuthorizationException
     * @throws LocalizedException
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        /** @var ContextInterface $context */
        if (!$this->isDebugMode && false === $context->getExtensionAttributes()->getIsCustomer()) {
            throw new GraphQlAuthorizationException(
                __('The current customer isn\'t authorized.')
            );
        }

        if (!array_key_exists('order_id', $args)) {
            throw new LocalizedException(__('order_id not defined'));
        }
        /** @var OrderModel $order */
        $order = $this->orderRepository->get($args['order_id']);
        if (!$this->isDebugMode && $order->getCustomerId() != $context->getUserId()) {
            throw new LocalizedException(
                __('this order not available in this query')
            );
        }

        return $this->orderDetails->getData($order);
    }
}
