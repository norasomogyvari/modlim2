<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\SalesGraphQl\Model\Resolver\Orders\Details\Address;

class Region
{
    /**
     * @param string $id
     * @param string $code
     * @param string $label
     *
     * @return array
     */
    public function getRegion($id, $code, $label)
    {
        return [
            'region_id'   => $id,
            'region_code' => $code,
            'region'      => $label,
        ];
    }
}
