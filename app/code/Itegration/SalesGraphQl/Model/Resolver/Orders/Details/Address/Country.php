<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\SalesGraphQl\Model\Resolver\Orders\Details\Address;

class Country
{
    /**
     * @param string $code
     * @param string $label
     *
     * @return array
     */
    public function getCountry($code, $label)
    {
        return [
            'code' => $code,
            'label' => $label,
        ];
    }
}
