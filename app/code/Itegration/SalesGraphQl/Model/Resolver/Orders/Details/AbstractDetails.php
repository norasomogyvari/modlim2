<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\SalesGraphQl\Model\Resolver\Orders\Details;

use Magento\Sales\Model\AbstractModel;
use Magento\Sales\Model\Order\Item as OrderItemModel;

abstract class AbstractDetails
{
    protected $mapper;

    /**
     * @param AbstractModel $item
     *
     * @return array
     */
    public function getData($item)
    {
        $returnArray = [];
        foreach ($this->mapper as $origFieldName => $returnKey) {
            $methodName = 'get' . ucfirst($origFieldName);
            if (method_exists($this, $methodName)) {
                $returnArray[$returnKey] = call_user_func_array(
                    [$this, $methodName],
                    [$item]
                );
            } elseif (null !== $item->getData($origFieldName)) {
                $returnArray[$returnKey] = $item->getData($origFieldName);
            }
        }

        return $returnArray;
    }
}
