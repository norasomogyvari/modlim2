<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\SalesGraphQl\Model\Resolver\Orders\Details\OrderItem;

class Option
{
    /**
     * @param string $label
     * @param string $value
     *
     * @return array
     */
    public function getOption($label, $value)
    {
        return [
            'label' => $label,
            'value' => $value,
        ];
    }
}
