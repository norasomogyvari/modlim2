<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\SalesGraphQl\Model\Resolver\Orders\Details;

use Itegration\SalesGraphQl\Model\Resolver\Orders\Details\Shipment\ShipmentItem;
use Magento\Sales\Model\Order\Shipment as OrderShipmentModel;
use Magento\Sales\Model\Order\Shipment\Item as OrderShipmentItemModel;

class Shipment extends AbstractDetails
{
    protected $mapper = [
        'id'          => 'id',
        'incrementId' => 'increment_id',
        'items'       => 'items',
    ];

    /**
     * @var ShipmentItem
     */
    private $shipmentItem;

    /**
     * Shipment constructor.
     *
     * @param ShipmentItem $shipmentItem
     */
    public function __construct(
        ShipmentItem $shipmentItem
    ) {
        $this->shipmentItem = $shipmentItem;
    }

    /**
     * @param OrderShipmentModel $shipment
     *
     * @return int
     */
    protected function getId($shipment)
    {
        return $shipment->getId();
    }

    /**
     * @param OrderShipmentModel $shipment
     *
     * @return string
     */
    protected function getIncrementId($shipment)
    {
        return $shipment->getIncrementId();
    }

    /**
     * @param OrderShipmentModel $shipment
     *
     * @return array
     */
    protected function getItems($shipment)
    {
        $returnArray = [];

        /** @var OrderShipmentItemModel $shipmentItem */
        foreach ($shipment->getItems() as $shipmentItem) {
            if (!$shipmentItem->getOrderItem()->isDeleted() &&
                !$shipmentItem->getOrderItem()->getParentItemId()
            ) {
                $returnArray[] = $this->shipmentItem->getData($shipmentItem);
            }
        }

        return $returnArray;
    }
}
