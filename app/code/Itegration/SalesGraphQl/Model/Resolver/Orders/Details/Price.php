<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\SalesGraphQl\Model\Resolver\Orders\Details;

use Magento\Sales\Model\Order as OrderModel;
use Magento\Sales\Model\Order\Item as OrderItemModel;

class Price
{
    /**
     * @var string[]
     */
    private $currencies = [];
    /**
     * @var string[]
     */
    private $baseCurrencies = [];

    /**
     * @param float  $amount
     * @param string $currency
     *
     * @return array
     */
    public function parsePrice($amount, $currency)
    {
        return [
            'amount'      => [
                'value'    => $amount,
                'currency' => $currency,
            ],
            'adjustments' => [],
        ];
    }

    /**
     * @param OrderModel|OrderItemModel $order
     *
     * @return string
     */
    public function getCurrency($order)
    {
        try {
            $store = $order->getStore();
            if (!array_key_exists($store->getId(), $this->currencies)) {
                $this->currencies[$store->getId()] = $store->getCurrentCurrencyCode();
            }
            return $this->currencies[$store->getId()];
        } catch (\Exception $e) {
            return 'EUR';
        }
    }

    /**
     * @param OrderModel|OrderItemModel $order
     *
     * @return string
     */
    public function getBaseCurrency($order)
    {
        try {
            $store = $order->getStore();
            if (!array_key_exists($store->getId(), $this->baseCurrencies)) {
                $this->baseCurrencies[$store->getId()] = $store->getBaseCurrencyCode();
            }
            return $this->currencies[$store->getId()];
        } catch (\Exception $e) {
            return 'EUR';
        }
    }
}
