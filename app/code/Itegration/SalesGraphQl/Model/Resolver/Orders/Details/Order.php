<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\SalesGraphQl\Model\Resolver\Orders\Details;

use Itegration\SalesGraphQl\Model\Resolver\Orders\Details\Address as AddressDetails;
use Itegration\SalesGraphQl\Model\Resolver\Orders\Details\OrderItem as OrderItemDetails;
use Itegration\SalesGraphQl\Model\Resolver\Orders\Details\Shipment as OrderShipment;
use Magento\Sales\Model\Order as OrderModel;
use Magento\Sales\Model\Order\Address as OrderAddressModel;
use Magento\Sales\Model\Order\Item as OrderItemModel;
use Magento\Sales\Model\Order\Shipment as OrderShipmentModel;

class Order extends AbstractDetails
{
    protected $mapper = [
        'id'                      => 'id',
        'shippingAddress'         => 'shipping_address',
        'shippingMethod'          => 'shipping_method',
        'billingAddress'          => 'billing_address',
        'paymentMethod'           => 'payment_method',
        'items'                   => 'items',
        'shipments'               => 'shipments',
        'couponCode'              => 'coupon_code',
        'baseDiscountAmount'      => 'base_discount_amount',
        'baseDiscountCanceled'    => 'base_discount_canceled',
        'baseDiscountRefunded'    => 'base_discount_refunded',
        'baseGrandTotal'          => 'base_grand_total',
        'baseShippingAmount'      => 'base_shipping_amount',
        'baseShippingCanceled'    => 'base_shipping_canceled',
        'baseShippingInvoiced'    => 'base_shipping_invoiced',
        'baseShippingRefunded'    => 'base_shipping_refunded',
        'baseShippingTaxAmount'   => 'base_shipping_tax_amount',
        'baseShippingTaxRefunded' => 'base_shipping_tax_refunded',
        'baseSubtotal'            => 'base_subtotal',
        'baseSubtotalCanceled'    => 'base_subtotal_canceled',
        'baseSubtotalInvoiced'    => 'base_subtotal_invoiced',
        'baseSubtotalRefunded'    => 'base_subtotal_refunded',
        'baseTaxAmount'           => 'base_tax_amount',
        'baseTaxCanceled'         => 'base_tax_canceled',
        'baseTaxInvoiced'         => 'base_tax_invoiced',
        'baseTaxRefunded'         => 'base_tax_refunded',
        'baseTotalCanceled'       => 'base_total_canceled',
        'baseTotalInvoiced'       => 'base_total_invoiced',
        'discountAmount'          => 'discount_amount',
        'discountCanceled'        => 'discount_canceled',
        'discountInvoiced'        => 'discount_invoiced',
        'discountRefunded'        => 'discount_refunded',
        'grandTotal'              => 'grand_total',
        'shippingDiscount'        => 'shipping_discount',
        'shippingAmount'          => 'shipping_amount',
        'shippingCanceled'        => 'shipping_canceled',
        'shippingInvoiced'        => 'shipping_invoiced',
        'shippingRefunded'        => 'shipping_refunded',
        'shippingTaxAmount'       => 'shipping_tax_amount',
        'shippingTaxRefunded'     => 'shipping_tax_refunded',
        'subtotal'                => 'subtotal',
        'subtotalCanceled'        => 'subtotal_canceled',
        'subtotalInvoiced'        => 'subtotal_invoiced',
        'subtotalRefunded'        => 'subtotal_refunded',
        'taxAmount'               => 'tax_amount',
        'taxCanceled'             => 'tax_canceled',
        'taxInvoiced'             => 'tax_invoiced',
        'taxRefunded'             => 'tax_refunded',
        'totalCanceled'           => 'total_canceled',
        'totalInvoiced'           => 'total_invoiced',
        'totalRefunded'           => 'total_refunded',
        'createdAt'               => 'created_at',
    ];

    /**
     * @var AddressDetails
     */
    private $addressDetails;

    /**
     * @var OrderItemDetails
     */
    private $orderItemDetails;

    /**
     * @var OrderShipment
     */
    private $orderShipment;

    /**
     * @var Price
     */
    private $price;

    /**
     * Order constructor.
     *
     * @param Address       $addressDetails
     * @param OrderItem     $orderItemDetails
     * @param OrderShipment $orderShipment
     * @param Price         $price
     */
    public function __construct(
        AddressDetails $addressDetails,
        OrderItemDetails $orderItemDetails,
        OrderShipment $orderShipment,
        Price $price
    ) {
        $this->addressDetails = $addressDetails;
        $this->orderItemDetails = $orderItemDetails;
        $this->orderShipment = $orderShipment;
        $this->price = $price;
    }

    /**
     * @param OrderModel $order
     *
     * @return int
     */
    protected function getId($order)
    {
        return $order->getId();
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getShippingAddress($order)
    {
        /** @var OrderAddressModel $address */
        $address = $order->getShippingAddress();
        return $this->parseAddress($address);
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getBillingAddress($order)
    {
        /** @var OrderAddressModel $address */
        $address = $order->getBillingAddress();
        return $this->parseAddress($address);
    }

    /**
     * @param OrderModel $order
     *
     * @return string
     */
    protected function getShippingMethod($order)
    {
        return $order->getShippingDescription();
    }

    /**
     * @param OrderModel $order
     *
     * @return string
     */
    protected function getPaymentMethod($order)
    {
        $method = $order->getPayment()->getMethodInstance();
        return $method->getTitle();
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getItems($order)
    {
        $returnArray = [];

        /** @var OrderItemModel $orderItem */
        foreach ($order->getAllVisibleItems() as $orderItem) {
            $returnArray[] = $this->orderItemDetails->getData($orderItem);
        }

        return $returnArray;
    }

    /**
     * @param OrderAddressModel $address
     *
     * @return array
     */
    private function parseAddress($address)
    {
        return $this->addressDetails->getData($address);
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getShipments($order)
    {
        $returnArray = [];

        /** @var OrderShipmentModel $shipment */
        foreach ($order->getShipmentsCollection() as $shipment) {
            $returnArray[] = $this->orderShipment->getData($shipment);
        }

        return $returnArray;
    }

    /**
     * @param OrderModel $order
     *
     * @return string
     */
    protected function getCouponCode($order)
    {
        return $order->getCouponCode();
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getBaseDiscountAmount($order)
    {
        return $this->price->parsePrice(
            $order->getBaseDiscountAmount(),
            $this->price->getBaseCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getBaseDiscountCanceled($order)
    {
        return $this->price->parsePrice(
            $order->getBaseDiscountCanceled(),
            $this->price->getBaseCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getBaseDiscountRefunded($order)
    {
        return $this->price->parsePrice(
            $order->getBaseDiscountRefunded(),
            $this->price->getBaseCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getBaseGrandTotal($order)
    {
        return $this->price->parsePrice(
            $order->getBaseGrandTotal(),
            $this->price->getBaseCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getBaseShippingAmount($order)
    {
        return $this->price->parsePrice(
            $order->getBaseShippingAmount(),
            $this->price->getBaseCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getBaseShippingCanceled($order)
    {
        return $this->price->parsePrice(
            $order->getBaseShippingCanceled(),
            $this->price->getBaseCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getBaseShippingInvoiced($order)
    {
        return $this->price->parsePrice(
            $order->getBaseShippingInvoiced(),
            $this->price->getBaseCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getBaseShippingRefunded($order)
    {
        return $this->price->parsePrice(
            $order->getBaseShippingRefunded(),
            $this->price->getBaseCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getBaseShippingTaxAmount($order)
    {
        return $this->price->parsePrice(
            $order->getBaseShippingTaxAmount(),
            $this->price->getBaseCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getBaseShippingTaxRefunded($order)
    {
        return $this->price->parsePrice(
            $order->getBaseShippingTaxRefunded(),
            $this->price->getBaseCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getBaseSubtotal($order)
    {
        return $this->price->parsePrice(
            $order->getBaseSubtotal(),
            $this->price->getBaseCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getBaseSubtotalCanceled($order)
    {
        return $this->price->parsePrice(
            $order->getBaseSubtotalCanceled(),
            $this->price->getBaseCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getBaseSubtotalInvoiced($order)
    {
        return $this->price->parsePrice(
            $order->getBaseSubtotalInvoiced(),
            $this->price->getBaseCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getBaseSubtotalRefunded($order)
    {
        return $this->price->parsePrice(
            $order->getBaseSubtotalRefunded(),
            $this->price->getBaseCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getBaseTaxAmount($order)
    {
        return $this->price->parsePrice(
            $order->getBaseTaxAmount(),
            $this->price->getBaseCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getBaseTaxCanceled($order)
    {
        return $this->price->parsePrice(
            $order->getBaseTaxCanceled(),
            $this->price->getBaseCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getBaseTaxInvoiced($order)
    {
        return $this->price->parsePrice(
            $order->getBaseTaxInvoiced(),
            $this->price->getBaseCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getBaseTaxRefunded($order)
    {
        return $this->price->parsePrice(
            $order->getBaseTaxRefunded(),
            $this->price->getBaseCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getBaseTotalCanceled($order)
    {
        return $this->price->parsePrice(
            $order->getBaseTotalCanceled(),
            $this->price->getBaseCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getBaseTotalInvoiced($order)
    {
        return $this->price->parsePrice(
            $order->getBaseTotalInvoiced(),
            $this->price->getBaseCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getDiscountAmount($order)
    {
        return $this->price->parsePrice(
            $order->getDiscountAmount(),
            $this->price->getCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getDiscountCanceled($order)
    {
        return $this->price->parsePrice(
            $order->getDiscountCanceled(),
            $this->price->getCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getDiscountInvoiced($order)
    {
        return $this->price->parsePrice(
            $order->getDiscountInvoiced(),
            $this->price->getCurrency($order)
        );
    }


    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getDiscountRefunded($order)
    {
        return $this->price->parsePrice(
            $order->getDiscountRefunded(),
            $this->price->getCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getGrandTotal($order)
    {
        return $this->price->parsePrice(
            $order->getGrandTotal(),
            $this->price->getCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getShippingDiscount($order)
    {
        return $this->price->parsePrice(
            $order->getShippingDiscountAmount(),
            $this->price->getCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getShippingAmount($order)
    {
        return $this->price->parsePrice(
            $order->getShippingAmount(),
            $this->price->getCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getShippingCanceled($order)
    {
        return $this->price->parsePrice(
            $order->getShippingCanceled(),
            $this->price->getCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getShippingInvoiced($order)
    {
        return $this->price->parsePrice(
            $order->getShippingInvoiced(),
            $this->price->getCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getShippingRefunded($order)
    {
        return $this->price->parsePrice(
            $order->getShippingRefunded(),
            $this->price->getCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getShippingTaxAmount($order)
    {
        return $this->price->parsePrice(
            $order->getShippingTaxAmount(),
            $this->price->getCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getShippingTaxRefunded($order)
    {
        return $this->price->parsePrice(
            $order->getShippingTaxRefunded(),
            $this->price->getCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getSubtotal($order)
    {
        return $this->price->parsePrice(
            $order->getSubtotal(),
            $this->price->getCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getSubtotalCanceled($order)
    {
        return $this->price->parsePrice(
            $order->getSubtotalCanceled(),
            $this->price->getCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getSubtotalInvoiced($order)
    {
        return $this->price->parsePrice(
            $order->getSubtotalInvoiced(),
            $this->price->getCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getSubtotalRefunded($order)
    {
        return $this->price->parsePrice(
            $order->getSubtotalRefunded(),
            $this->price->getCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getTaxAmount($order)
    {
        return $this->price->parsePrice(
            $order->getTaxAmount(),
            $this->price->getCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getTaxCanceled($order)
    {
        return $this->price->parsePrice(
            $order->getTaxCanceled(),
            $this->price->getCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getTaxInvoiced($order)
    {
        return $this->price->parsePrice(
            $order->getTaxInvoiced(),
            $this->price->getCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getTaxRefunded($order)
    {
        return $this->price->parsePrice(
            $order->getTaxRefunded(),
            $this->price->getCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getTotalCanceled($order)
    {
        return $this->price->parsePrice(
            $order->getTotalCanceled(),
            $this->price->getCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getTotalInvoiced($order)
    {
        return $this->price->parsePrice(
            $order->getTotalInvoiced(),
            $this->price->getCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return array
     */
    protected function getTotalRefunded($order)
    {
        return $this->price->parsePrice(
            $order->getTotalRefunded(),
            $this->price->getCurrency($order)
        );
    }

    /**
     * @param OrderModel $order
     *
     * @return string
     */
    protected function getCreatedAt($order)
    {
        return $order->getCreatedAt();
    }
}
