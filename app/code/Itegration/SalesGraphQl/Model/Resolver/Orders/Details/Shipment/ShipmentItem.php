<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\SalesGraphQl\Model\Resolver\Orders\Details\Shipment;

use Itegration\SalesGraphQl\Model\Resolver\Orders\Details\AbstractDetails;
use Magento\Sales\Model\Order\Shipment\Item as OrderShipmentItemModel;
use Itegration\SalesGraphQl\Model\Resolver\Orders\Details\OrderItem;

class ShipmentItem extends OrderItem
{

    protected $mapper = [
        'id'      => 'id',
        'name'    => 'name',
        'sku'     => 'sku',
        'qty'     => 'qty',
        'options' => 'options',
    ];

    /**
     * @param OrderShipmentItemModel $shipmentItem
     *
     * @return int|void
     */
    protected function getQty($shipmentItem)
    {
        return $shipmentItem->getQty();
    }

    /**
     * @param OrderShipmentItemModel $shipmentItem
     *
     * @return array
     */
    protected function getOptions($shipmentItem)
    {
        $orderItem = $shipmentItem->getOrderItem();
        return parent::getOptions($orderItem);
    }


}
