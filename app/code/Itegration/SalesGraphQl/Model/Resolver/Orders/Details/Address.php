<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\SalesGraphQl\Model\Resolver\Orders\Details;

use Itegration\SalesGraphQl\Model\Resolver\Orders\Details\Address\Country;
use Itegration\SalesGraphQl\Model\Resolver\Orders\Details\Address\Region;
use Magento\Sales\Model\Order\Address as OrderAddressModel;
use Magento\Directory\Model\CountryFactory;

class Address extends AbstractDetails
{
    /**
     * @var Region
     */
    private $region;

    /**
     * @var Country
     */
    private $country;

    /**
     * @var CountryFactory
     */
    private $countryFactory;

    /**
     * @var array
     */
    protected $mapper = [
        'id'        => 'id',
        'firstname' => 'firstname',
        'lastname'  => 'lastname',
        'region'    => 'region',
        'postcode'  => 'postcode',
        'name'      => 'name',
        'street'    => 'street',
        'city'      => 'city',
        'email'     => 'email',
        'telephone' => 'telephone',
        'country'   => 'country',
        'company'   => 'company',
    ];

    /**
     * Address constructor.
     *
     * @param Region         $region
     * @param Country        $country
     * @param CountryFactory $countryFactory
     */
    public function __construct(
        Region $region,
        Country $country,
        CountryFactory $countryFactory
    ) {
        $this->region = $region;
        $this->country = $country;
        $this->countryFactory = $countryFactory;
    }

    /**
     * @param OrderAddressModel $address
     *
     * @return int
     */
    protected function getId($address)
    {
        return $address->getId();
    }

    /**
     * @param OrderAddressModel $address
     *
     * @return string[]
     */
    protected function getStreet($address)
    {
        return $address->getStreet();
    }

    /**
     * @param OrderAddressModel $address
     *
     * @return array
     */
    protected function getRegion($address)
    {
        return $this->region->getRegion(
            $address->getRegionId(),
            $address->getRegionCode(),
            $address->getRegion()
        );
    }

    /**
     * @param OrderAddressModel $address
     *
     * @return array
     */
    protected function getCountry($address)
    {
        $country = $this->countryFactory->create()->loadByCode($address->getCountryId());
        return $this->country->getCountry(
            $country->getCountryId(),
            $country->getName()
        );
    }
}
