<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\SalesGraphQl\Model\Resolver\Orders\Details;

use Itegration\Dropship\Helper\Vendor as VendorHelper;
use Itegration\SalesGraphQl\Model\Resolver\Orders\Details\OrderItem\Option as OrderItemOption;
use Magento\Sales\Model\Order\Item as OrderItemModel;
use Magento\Catalog\Api\Data\ProductInterface;

class OrderItem extends AbstractDetails
{
    /**
     * @var Price
     */
    private $price;

    /**
     * @var OrderItemOption
     */
    private $option;

    /**
     * @var VendorHelper
     */
    private $vendorHelper;

    /**
     * @var array
     */
    private $vendors = [];

    /**
     * OrderItem constructor.
     *
     * @param Price           $price
     * @param OrderItemOption $option
     * @param VendorHelper    $vendorHelper
     */
    public function __construct(
        Price $price,
        OrderItemOption $option,
        VendorHelper $vendorHelper
    ) {
        $this->price = $price;
        $this->option = $option;
        $this->vendorHelper = $vendorHelper;
    }

    protected $mapper = [
        'id'                  => 'id',
        'name'                => 'name',
        'sku'                 => 'sku',
        'price'               => 'price',
        'discountAmount'      => 'discount_amount',
        'rowTotal'            => 'row_total',
        'qty'                 => 'qty',
        'options'             => 'options',
        'udropshipVendorId'   => 'udropship_vendor_id',
        'udropshipVendorName' => 'udropship_vendor_name',
        'product'             => 'product',
    ];

    /**
     * @param OrderItemModel $orderItem
     *
     * @return int
     */
    protected function getId($orderItem)
    {
        return $orderItem->getId();
    }

    /**
     * @param OrderItemModel $orderItem
     *
     * @return array
     */
    protected function getPrice($orderItem)
    {
        return $this->price->parsePrice(
            $orderItem->getPrice(),
            $this->price->getCurrency($orderItem)
        );
    }

    /**
     * @param OrderItemModel $orderItem
     *
     * @return array
     */
    protected function getDiscountAmount($orderItem)
    {
        return $this->price->parsePrice(
            $orderItem->getDiscountAmount(),
            $this->price->getCurrency($orderItem)
        );
    }

    /**
     * @param OrderItemModel $orderItem
     *
     * @return array
     */
    protected function getRowTotal($orderItem)
    {
        return $this->price->parsePrice(
            $orderItem->getRowTotal(),
            $this->price->getCurrency($orderItem)
        );
    }

    /**
     * @param OrderItemModel $orderItem
     *
     * @return array
     */
    protected function getOptions($orderItem)
    {
        $returnArray = [];

        $options = $orderItem->getProductOptions();
        if (array_key_exists('bundle_options', $options)) {
            foreach ($options['bundle_options'] as $bundleOption) {
                $values = [];
                foreach ($bundleOption['value'] as $value) {
                    $values[] = $value['title'];
                }
                $returnArray[] = $this->option->getOption(
                    $bundleOption['label'],
                    implode(', ', $values)
                );
            }
        } elseif (array_key_exists('attributes_info', $options)) {
            foreach ($options['attributes_info'] as $configurableOption) {
                $returnArray[] = $this->option->getOption(
                    $configurableOption['label'],
                    $configurableOption['value']
                );
            }
        }

        return $returnArray;
    }

    /**
     * @param OrderItemModel $orderItem
     *
     * @return float
     */
    protected function getQty($orderItem)
    {
        return $orderItem->getQtyOrdered();
    }

    /**
     * @param OrderItemModel $orderItem
     *
     * @return int
     */
    protected function getUdropshipVendorId($orderItem)
    {
        return $orderItem->getData('udropship_vendor');
    }

    /**
     * @param OrderItemModel $orderItem
     *
     * @return string
     */
    protected function getUdropshipVendorName($orderItem)
    {
        $vendorId = $this->getUdropshipVendorId($orderItem);
        if (!array_key_exists($vendorId, $this->vendors)) {
            $this->vendors[$vendorId] = $this->vendorHelper->getDropShipNameFromId(
                $vendorId
            );
        }
        return $this->vendors[$vendorId];
    }

    /**
     * @param OrderItemModel $orderItem
     *
     * @return array
     */
    protected function getProduct($orderItem) {
        $product = $orderItem->getProduct();
        if ($product instanceof ProductInterface) {
            $productData = $product->getData();
            $productData['model'] = $product;
            return $productData;
        }
    }
}
