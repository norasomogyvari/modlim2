<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\SalesGraphQl\Model\Resolver\Product;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Pricing\Price\FinalPrice;
use Magento\Catalog\Pricing\Price\RegularPrice;
use Magento\Directory\Model\CurrencyFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\Pricing\Adjustment\AdjustmentInterface;
use Magento\Framework\Pricing\Adjustment\CalculatorInterface;
use Magento\Framework\Pricing\Amount\AmountInterface;
use Magento\Framework\Pricing\PriceInfo\Factory as PriceInfoFactory;
use Magento\Framework\Pricing\PriceInfoInterface;
use Magento\Quote\Model\Quote\Item as QuoteItem;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\Store as StoreModel;

class Price implements ResolverInterface
{
    /**
     * @var PriceInfoFactory
     */
    private $priceInfoFactory;

    /**
     * @var CurrencyFactory
     */
    private $currencyFactory;

    /**
     * @var CalculatorInterface
     */
    private $calculator;

    /**
     * Price constructor.
     *
     * @param PriceInfoFactory    $priceInfoFactory
     * @param CurrencyFactory     $currencyFactory
     * @param CalculatorInterface $calculator
     */
    public function __construct(
        PriceInfoFactory $priceInfoFactory,
        CurrencyFactory $currencyFactory,
        CalculatorInterface $calculator
    ) {

        $this->priceInfoFactory = $priceInfoFactory;
        $this->currencyFactory = $currencyFactory;
        $this->calculator = $calculator;
    }

    /**
     * @param Field            $field
     * @param ContextInterface $context
     * @param ResolveInfo      $info
     * @param array|null       $value
     * @param array|null       $args
     *
     * @return Value|mixed|void
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {

        if (!isset($value['model'])) {
            throw new LocalizedException(
                __('"model" value should be specified')
            );
        }

        /** @var Product $product */
        $product = array_key_exists('child_model', $value) ?
            $value['child_model'] : $value['model'];

        /** @var PriceInfoInterface $priceInfo */
        $priceInfo = $this->priceInfoFactory->create($product);
        /** @var StoreModel $store */
        $store = $context->getExtensionAttributes()->getStore();

        if (array_key_exists('cart_item', $value)) {
            list($minimalPriceAmount, $regularPriceAmount, $maximalPriceAmount, $basePriceAmount, $finalPriceAmount) = $this->getPricesFromCartItem(
                $value['cart_item'],
                $product
            );
        } else {
            list($minimalPriceAmount, $regularPriceAmount, $maximalPriceAmount, $basePriceAmount, $finalPriceAmount) = $this->getPricesFromProduct(
                $product,
                $priceInfo,
                $store
            );
        }

        return [
            'minimalPrice' => $this->createAdjustmentsArray(
                $priceInfo->getAdjustments(),
                $minimalPriceAmount,
                $store
            ),
            'regularPrice' => $this->createAdjustmentsArray(
                $priceInfo->getAdjustments(),
                $regularPriceAmount,
                $store
            ),
            'maximalPrice' => $this->createAdjustmentsArray(
                $priceInfo->getAdjustments(),
                $maximalPriceAmount,
                $store
            ),
            'basePrice'    => [
                'amount'      => [
                    'value'    => $basePriceAmount,
                    'currency' => $store->getBaseCurrencyCode(),
                ],
                'adjustments' => [],
            ],
            'finalPrice'   => [
                'amount'      => [
                    'value'    => $finalPriceAmount,
                    'currency' => $store->getCurrentCurrencyCode(),
                ],
                'adjustments' => [],
            ],
        ];
    }

    /**
     * @param QuoteItem $cartItem
     * @param Product   $product
     *
     * @return array
     */
    private function getPricesFromCartItem($cartItem, $product)
    {
        $minimalPriceAmount = $this->calculator->getAmount(
            $cartItem->getConvertedPrice(),
            $product
        );
        $maximalPriceAmount = $regularPriceAmount = $minimalPriceAmount;
        $basePriceAmount = round((float)$cartItem->getPrice(), 2);
        $finalPriceAmount = round((float)$cartItem->getConvertedPrice(), 2);

        return [
            $minimalPriceAmount,
            $regularPriceAmount,
            $maximalPriceAmount,
            $basePriceAmount,
            $finalPriceAmount,
        ];
    }

    /**
     * @param Product            $product
     * @param PriceInfoInterface $priceInfo
     * @param StoreModel         $store
     *
     * @return array;
     * @throws LocalizedException
     *
     */
    private function getPricesFromProduct($product, $priceInfo, $store)
    {
        $product->unsetData('minimal_price');
        /** @var \Magento\Catalog\Pricing\Price\FinalPriceInterface $finalPrice */
        $finalPrice = $priceInfo->getPrice(FinalPrice::PRICE_CODE);
        $minimalPriceAmount = $finalPrice->getMinimalPrice();
        $maximalPriceAmount = $finalPrice->getMaximalPrice();
        $regularPriceAmount = $priceInfo->getPrice(RegularPrice::PRICE_CODE)
            ->getAmount();


        $rate = $this->currencyFactory->create()->load(
            $store->getCurrentCurrency()->getCode()
        )->getAnyRate('USD');
        $basePriceAmount = $finalPrice->getValue() * $rate;

        $finalPriceAmount = $product->getFinalPrice();

        return [
            $minimalPriceAmount,
            $regularPriceAmount,
            $maximalPriceAmount,
            $basePriceAmount,
            $finalPriceAmount,
        ];
    }

    /**
     * Fill a price with an adjustment array structure with amounts from an amount type
     *
     * @param AdjustmentInterface[] $adjustments
     * @param AmountInterface       $amount
     * @param StoreInterface        $store
     *
     * @return array
     */
    private function createAdjustmentsArray(
        array $adjustments,
        AmountInterface $amount,
        StoreInterface $store
    ): array {
        $priceArray = [
            'amount'      => [
                'value'    => $amount->getValue(),
                'currency' => $store->getCurrentCurrencyCode(),
            ],
            'adjustments' => [],
        ];
        $priceAdjustmentsArray = [];
        foreach ($adjustments as $adjustmentCode => $adjustment) {
            if ($amount->hasAdjustment(
                    $adjustmentCode
                ) && $amount->getAdjustmentAmount($adjustmentCode)) {
                $priceAdjustmentsArray[] = [
                    'code'        => strtoupper($adjustmentCode),
                    'amount'      => [
                        'value'    => $amount->getAdjustmentAmount(
                            $adjustmentCode
                        ),
                        'currency' => $store->getCurrentCurrencyCode(),
                    ],
                    'description' => $adjustment->isIncludedInDisplayPrice() ?
                        'INCLUDED' : 'EXCLUDED',
                ];
            }
        }
        $priceArray['adjustments'] = $priceAdjustmentsArray;
        return $priceArray;
    }
}
