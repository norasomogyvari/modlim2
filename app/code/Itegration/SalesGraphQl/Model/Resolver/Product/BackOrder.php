<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\SalesGraphQl\Model\Resolver\Product;

use Magento\Catalog\Model\Product;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\CatalogInventory\Model\Stock\Item as StockItem;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\InventorySalesApi\Api\StockResolverInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Stdlib\DateTime\DateTimeFormatterInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class BackOrder implements ResolverInterface
{
    const DEFAULT_BACKORDER_MESSAGE_PATH = 'cataloginventory/item_options/backorder_message';
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var StockResolverInterface
     */
    private $stockResolver;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var DateTimeFormatterInterface
     */
    private $dateTimeFormatter;

    /**
     * @var TimezoneInterface
     */
    private $timezone;

    /**
     * BackOrder constructor.
     *
     * @param StoreManagerInterface      $storeManager
     * @param StockRegistryInterface     $stockResolver
     * @param ScopeConfigInterface       $scopeConfig
     * @param DateTimeFormatterInterface $dateTimeFormatter
     * @param TimezoneInterface          $timezone
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        StockRegistryInterface $stockResolver,
        ScopeConfigInterface $scopeConfig,
        DateTimeFormatterInterface $dateTimeFormatter,
        TimezoneInterface $timezone
    ) {
        $this->storeManager = $storeManager;
        $this->stockResolver = $stockResolver;
        $this->scopeConfig = $scopeConfig;
        $this->dateTimeFormatter = $dateTimeFormatter;
        $this->timezone = $timezone;
    }

    /**
     * @param Field            $field
     * @param ContextInterface $context
     * @param ResolveInfo      $info
     * @param array|null       $value
     * @param array|null       $args
     *
     * @return Value|mixed|void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $returnArray = [
            'ship_message' => '',
            'ship_date'    => '',
            'is_backorder' => false,
        ];

        if (array_key_exists('model', $value)) {
            /** @var Product $product */
            $product = $value['model'];

            /** @var StockItem $stockItem */
            $stockItem = $this->stockResolver->getStockItem(
                $product->getId(),
                $this->storeManager->getStore()->getId()
            );

            if ($stockItem->getUseConfigBackorderMessage()) {
                $backorderMessage = $this->scopeConfig->getValue(
                    self::DEFAULT_BACKORDER_MESSAGE_PATH
                );
            } else {
                $backorderMessage = $stockItem->getBackorderMessage();
            }
            $backorderShippingDate = $stockItem->getBackorderShipDate();
            if ($backorderShippingDate) {
                $backorderShippingDate = new \DateTime($backorderShippingDate);
                $backorderShippingDate = $this->timezone->date($backorderShippingDate);
                $backorderShippingDate = $this->dateTimeFormatter->formatObject($backorderShippingDate);
            }
            $backorderMessage = str_replace(
                '{{date}}',
                $backorderShippingDate,
                $backorderMessage
            );

            $salableQty = (float)$stockItem->getQty() - (float)$stockItem->getMinQty();

            $returnArray['ship_message'] = $backorderMessage;
            $returnArray['ship_date'] = $backorderShippingDate;
            $returnArray['is_backorder'] = $stockItem->getQty() == 0 && $salableQty > 0;
            $returnArray['qty'] = $stockItem->getQty();
            $returnArray['salable_qty'] = $salableQty;
        }

        return $returnArray;
    }
}
