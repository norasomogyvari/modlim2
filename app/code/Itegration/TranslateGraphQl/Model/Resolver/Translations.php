<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\TranslateGraphQl\Model\Resolver;

use Magento\Framework\App\Area;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\TranslateInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\Store;
use Magento\Framework\Locale\Resolver as LocaleResolver;
use Magento\Store\Model\App\Emulation;

class Translations implements ResolverInterface
{
    /**
     * @var TranslateInterface
     */
    private $translate;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var LocaleResolver
     */
    private $localeResolver;

    /**
     * @var Emulation
     */
    private $emulation;

    /**
     * Translations constructor.
     *
     * @param TranslateInterface    $translate
     * @param StoreManagerInterface $storeManager
     * @param LocaleResolver        $localeResolver
     * @param Emulation             $emulation
     */
    public function __construct(
        TranslateInterface $translate,
        StoreManagerInterface $storeManager,
        LocaleResolver $localeResolver,
        Emulation $emulation
    ) {
        $this->translate = $translate;
        $this->storeManager = $storeManager;
        $this->localeResolver = $localeResolver;
        $this->emulation = $emulation;
    }

    /**
     * @param Field            $field
     * @param ContextInterface $context
     * @param ResolveInfo      $info
     * @param array|null       $value
     * @param array|null       $args
     *
     * @return Value|mixed|void
     * @throws GraphQlInputException
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!array_key_exists('expressions', $args) || !is_array(
                $args['expressions']
            )) {
            throw new GraphQlInputException(
                __('Required parameter "expressions" is missing')
            );
        }
        try {
            $storeId = $this->storeManager->getStore()->getId();
        } catch (\Exception $e) {
            throw new GraphQlInputException(
                __('Required parameter "expressions" is missing')
            );
        }
        $this->emulation->startEnvironmentEmulation($storeId, Area::AREA_FRONTEND, true);

        $translateData = $this->translate->loadData(Area::AREA_FRONTEND, true)->getData();
        $returnArray = [];

        foreach ($args['expressions'] as $expression) {
            $expression = str_replace('\"', '"', $expression);
            $expression = str_replace("\\'", "'", $expression);

            $value = null;
            if (array_key_exists($expression, $translateData)) {
                $value = $translateData[$expression];
            }
            $returnArray[] = [
                'key'   => $expression,
                'value' => $value,
            ];
        }

        $this->emulation->stopEnvironmentEmulation();

        return ['expressions' => $returnArray];
    }

}
