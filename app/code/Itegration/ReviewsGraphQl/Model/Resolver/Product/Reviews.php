<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ReviewsGraphQl\Model\Resolver\Product;

use Itegration\ReviewsGraphQl\Helper\Review as ReviewHelper;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class Reviews implements ResolverInterface
{
    /**
     * @var ReviewHelper
     */
    private $reviewHelper;

    /**
     * Reviews constructor.
     *
     * @param ReviewHelper $reviewHelper
     */
    public function __construct(
        ReviewHelper $reviewHelper
    ) {
        $this->reviewHelper = $reviewHelper;
    }

    /**
     * @param Field            $field
     * @param ContextInterface $context
     * @param ResolveInfo      $info
     * @param array|null       $value
     * @param array|null       $args
     *
     * @return array
     * @throws NoSuchEntityException
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $data = [];
        if (array_key_exists('entity_id', $value)) {
            $data = $this->reviewHelper->getReviews($value['entity_id'], 20, 1);
            $data = $data['items'];
        }

        return $data;
    }
}
