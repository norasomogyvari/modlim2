<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ReviewsGraphQl\Model\Resolver;

use Itegration\ReviewsGraphQl\Helper\Review as ReviewHelper;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class Reviews implements ResolverInterface
{
    /**
     * @var ReviewHelper
     */
    private $reviewHelper;

    /**
     * Reviews constructor.
     *
     * @param ReviewHelper $reviewHelper
     */
    public function __construct(
        ReviewHelper $reviewHelper
    ) {
        $this->reviewHelper = $reviewHelper;
    }

    /**
     * @param Field            $field
     * @param ContextInterface $context
     * @param ResolveInfo      $info
     * @param array|null       $value
     * @param array|null       $args
     *
     * @return Value|mixed|void
     * @throws LocalizedException
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $productId = 0;
        if (array_key_exists('product_id', $args)) {
            $productId = (int)$args['product_id'];
        }

        if (!$productId) {
            throw new LocalizedException(
                __('"product_id" value should be specified')
            );
        }

        $pageSize = 0;
        if (array_key_exists('page_size', $args)) {
            $pageSize = (int)$args['page_size'];
        }

        if (!$pageSize) {
            throw new LocalizedException(
                __('"page_size" value should be specified')
            );
        }

        $currentPage = 0;
        if (array_key_exists('current_page', $args)) {
            $currentPage = (int)$args['current_page'];
        }

        if (!$currentPage) {
            throw new LocalizedException(
                __('"current_page" value should be specified')
            );
        }

        return $this->reviewHelper->getReviews(
            $productId,
            $pageSize,
            $currentPage
        );
    }
}
