<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ReviewsGraphQl\Model\Resolver\Reviews;

use Magento\Framework\GraphQl\Query\Resolver\IdentityInterface;

class ReviewsIdentity implements IdentityInterface
{
    private $cacheTag = 'reviews';

    public function getIdentities(array $resolvedData): array
    {
        return empty($resolvedData['productId']) ?
            [] :
            [
                $this->cacheTag,
                sprintf('%s_%s', $this->cacheTag, $resolvedData['productId']),
            ];
    }

}
