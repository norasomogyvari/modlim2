<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ReviewsGraphQl\Helper;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Directory\Model\CountryFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Review\Model\ResourceModel\Review\Collection as ReviewCollection;
use Magento\Review\Model\ResourceModel\Review\CollectionFactory as ReviewCollectionFactory;
use Magento\Review\Model\Review as ReviewModel;
use Magento\Store\Model\StoreManagerInterface;
use Modli\ReviewsGraphQl\Model\DataProvider\Order as OrderProvider;
use Modli\ReviewsGraphQl\Model\DataProvider\Product as ProductProvider;

class Review
{
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var ReviewCollectionFactory
     */
    private $reviewCollectionFactory;

    /**
     * @var ProductProvider
     */
    private $productProvider;

    /**
     * @var OrderProvider
     */
    private $orderProvider;

    /**
     * @var CountryFactory
     */
    private $countryFactory;

    /**
     * @var array
     */
    protected $countryCodeNameMap = [];

    /**
     * Review constructor.
     *
     * @param ProductRepositoryInterface $productRepository
     * @param StoreManagerInterface      $storeManager
     * @param ReviewCollectionFactory    $reviewCollectionFactory
     * @param ProductProvider            $productProvider
     * @param OrderProvider              $orderProvider
     * @param CountryFactory             $countryFactory
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        StoreManagerInterface $storeManager,
        ReviewCollectionFactory $reviewCollectionFactory,
        ProductProvider $productProvider,
        OrderProvider $orderProvider,
        CountryFactory $countryFactory
    ) {
        $this->productRepository = $productRepository;
        $this->storeManager = $storeManager;
        $this->reviewCollectionFactory = $reviewCollectionFactory;
        $this->productProvider = $productProvider;
        $this->orderProvider = $orderProvider;
        $this->countryFactory = $countryFactory;
    }

    /**
     * @param     $productId
     * @param int $pageSize
     * @param int $currentPage
     *
     * @return array
     * @throws NoSuchEntityException
     */
    public function getReviews($productId, $pageSize = 10, $currentPage = 1)
    {
        $product = $this->getProduct($productId);

        $storeId = $this->storeManager->getStore()->getId();

        $reviewData = [];
        $reviewCollection = $this->reviewCollectionFactory->create()
            ->addStoreFilter($storeId)
            ->addStatusFilter(ReviewModel::STATUS_APPROVED)
            ->setDateOrder();

        $this->setProductFilter($product, $reviewCollection);
        $reviewCollection
            ->setPageSize($pageSize)
            ->setCurPage($currentPage);

        $count = (int)$reviewCollection->getConnection()->fetchOne(
            $reviewCollection->getSelectCountSql()
        );

        /**
         * @var ReviewCollection $reviews
         */
        $reviews = $reviewCollection->load()->addRateVotes();

        $customerAddressMap = $this->orderProvider->getCustomerEmailAddressMap(
            $reviews
        );

        /**
         * @var ReviewModel $review
         */
        foreach ($reviews as $review) {
            $ratingVotes = $review->getRatingVotes()->getData();
            $reviewData[] = [
                'review_id'       => $review->getReviewId(),
                'entity_id'       => $review->getEntityId(),
                'entity_code'     => $review->getEntityCode(),
                'entity_pk_value' => $review->getEntityPkValue(),
                'status_id'       => $review->getStatusId(),
                'customer_id'     => $review->getCustomerId(),
                'nickname'        => $review->getNickname(),
                'title'           => $review->getTitle(),
                'detail_id'       => $review->getDetailId(),
                'detail'          => $review->getDetail(),
                'created_at'      => $review->getCreatedAt(),
                'rating_votes'    => $ratingVotes,
                'country_code'    => @$customerAddressMap[$review->getEmail(
                )]['country_code'],
                'country'         => $this->getCountryNameByCode(
                    @$customerAddressMap[$review->getEmail()]['country_code']
                ),
                'region'          => @$customerAddressMap[$review->getEmail(
                )]['region'],
            ];
        }

        $pageNumber = 0;
        if ($pageSize > 0) {
            $pageNumber = ceil($count / $pageSize);
        }

        return [
            'total_count'  => $count,
            'page_size'    => $pageSize,
            'current_page' => $currentPage,
            'page_number' => $pageNumber,
            'items'        => $reviewData,
        ];
    }

    /**
     * @param int $productId
     *
     * @return ProductInterface
     * @throws NoSuchEntityException
     */
    private function getProduct($productId)
    {
        return $this->productRepository->getById($productId);
    }

    protected function setProductFilter(
        Product $product,
        ReviewCollection $reviewCollection
    ) {
        $productsToFilter = $this->productProvider->getFilterProductIds(
            $product
        );

        $reviewCollection->addEntityFilter(
            'product',
            $productsToFilter
        );
    }

    protected function getCountryNameByCode($countryCode)
    {
        if (empty($countryCode)) {
            return '';
        }

        if (!isset($this->countryCodeNameMap[$countryCode])) {
            $country = $this->countryFactory->create()->loadByCode(
                $countryCode
            );
            $this->countryCodeNameMap[$countryCode] = $country->getName();
        }

        return $this->countryCodeNameMap[$countryCode];
    }
}
