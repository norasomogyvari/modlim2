<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\BundleGraphQl\Model\Resolver\BundleItem;

use Itegration\BundleGraphQl\Helper\BundleOption as BundleOptionHelper;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class ConfigurableItemId implements ResolverInterface
{
    /**
     * @var BundleOptionHelper
     */
    private $bundleOptionHelper;

    /**
     * ConfigurableItemId constructor.
     *
     * @param BundleOptionHelper $bundleOptionHelper
     */
    public function __construct(
        BundleOptionHelper $bundleOptionHelper
    ){
        $this->bundleOptionHelper = $bundleOptionHelper;
    }

    /**
     * @param Field            $field
     * @param ContextInterface $context
     * @param ResolveInfo      $info
     * @param array|null       $value
     * @param array|null       $args
     *
     * @return Value|mixed|void
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $optionId = $value['option_id'];
        $parentId = $value['parent_id'];

        /** @var array $configurableItem */
        $configurableItem = $this->bundleOptionHelper->getConfigurableProductFromOption($optionId, $parentId);

        if (null !== $configurableItem){
            return $configurableItem['entity_id'];
        }

        return null;
    }
}
