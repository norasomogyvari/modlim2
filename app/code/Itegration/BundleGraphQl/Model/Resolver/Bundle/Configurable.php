<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\BundleGraphQl\Model\Resolver\Bundle;

use Itegration\BundleGraphQl\Helper\BundleOption as BundleOptionHelper;
use Magento\Bundle\Model\Product\Type;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class Configurable implements ResolverInterface
{
    /**
     * @var MetadataPool
     */
    private $metadataPool;

    /**
     * @var BundleOptionHelper
     */
    private $bundleOptionHelper;

    /**
     * Configurable constructor.
     *
     * @param MetadataPool       $metadataPool
     * @param BundleOptionHelper $bundleOptionHelper
     */
    public function __construct(
        MetadataPool $metadataPool,
        BundleOptionHelper $bundleOptionHelper
    ) {
        $this->metadataPool = $metadataPool;
        $this->bundleOptionHelper = $bundleOptionHelper;
    }

    /**
     * @param Field            $field
     * @param ContextInterface $context
     * @param ResolveInfo      $info
     * @param array|null       $value
     * @param array|null       $args
     *
     * @return array|null
     * @throws \Exception
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $linkField = $this->metadataPool->getMetadata(ProductInterface::class)
            ->getLinkField();
        if ($value['type_id'] !== Type::TYPE_CODE
            || !isset($value[$linkField])
            || !isset($value[ProductInterface::SKU])
        ) {
            return null;
        }

        return $this->getItems((int)$value[$linkField]);
    }

    /**
     * @param int $parentId
     *
     * @return array
     */
    protected function getItems($parentId)
    {
        return $this->bundleOptionHelper->getItems($parentId);
    }
}
