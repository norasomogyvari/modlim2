<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\BundleGraphQl\Model\Resolver\BundleItemOption;

use Itegration\BundleGraphQl\Helper\BundleOption as BundleOptionHelper;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class Product implements ResolverInterface
{
    /**
     * @var BundleOptionHelper
     */
    private $bundleOptionHelper;

    /**
     * Product constructor.
     *
     * @param BundleOptionHelper $bundleOptionHelper
     */
    public function __construct(
        BundleOptionHelper $bundleOptionHelper
    ){
        $this->bundleOptionHelper = $bundleOptionHelper;
    }

    /**
     * @param Field            $field
     * @param ContextInterface $context
     * @param ResolveInfo      $info
     * @param array|null       $value
     * @param array|null       $args
     *
     * @return Value|mixed|void
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $product = $this->bundleOptionHelper->getProduct($value['product_id']);
        if ($product instanceof ProductInterface) {
            $productData = $product->getData();
            $productData['model'] = $product;
            return $productData;
        }
        return null;
    }
}

