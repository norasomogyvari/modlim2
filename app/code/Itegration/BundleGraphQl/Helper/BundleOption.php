<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\BundleGraphQl\Helper;

use Magento\Bundle\Model\ResourceModel\Option\Collection as BundleOptionCollection;
use Magento\Bundle\Model\ResourceModel\Option\CollectionFactory as BundleOptionCollectionFactory;
use Magento\Bundle\Model\ResourceModel\Selection\Collection as BundleSelectionCollection;
use Magento\Bundle\Model\ResourceModel\Selection\CollectionFactory as BundleSelectionCollectionFactory;
use Magento\Catalog\Model\Product;
use Magento\Store\Model\StoreManagerInterface;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable as ConfigurableProductResourceModel;
use Magento\Bundle\Model\Option as BundleOptionModel;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Exception;

class BundleOption
{
    /**
     * @var BundleOptionCollectionFactory
     */
    private $bundleOptionCollectionFactory;

    /**
     * @var BundleSelectionCollectionFactory
     */
    private $bundleSelectionCollectionFactory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var ConfigurableProductResourceModel
     */
    private $configurableProductResourceModel;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var array
     */
    private $existsItems = [];

    /**
     * @var ProductInterface[]
     */
    private $products = [];

    /**
     * BundleOption constructor.
     *
     * @param BundleOptionCollectionFactory    $bundleOptionCollectionFactory
     * @param BundleSelectionCollectionFactory $bundleSelectionCollectionFactory
     * @param ConfigurableProductResourceModel $configurableProductResourceModel
     * @param ProductRepositoryInterface       $productRepository
     * @param StoreManagerInterface            $storeManager
     */
    public function __construct(
        BundleOptionCollectionFactory $bundleOptionCollectionFactory,
        BundleSelectionCollectionFactory $bundleSelectionCollectionFactory,
        ConfigurableProductResourceModel $configurableProductResourceModel,
        ProductRepositoryInterface $productRepository,
        StoreManagerInterface $storeManager
    ){
        $this->bundleOptionCollectionFactory = $bundleOptionCollectionFactory;
        $this->bundleSelectionCollectionFactory = $bundleSelectionCollectionFactory;
        $this->configurableProductResourceModel = $configurableProductResourceModel;
        $this->productRepository = $productRepository;
        $this->storeManager = $storeManager;
    }

    /**
     * @param int $parentId
     *
     * @return array
     */
    public function getItems($parentId) {
        if (!array_key_exists($parentId, $this->existsItems)) {
            $this->existsItems[$parentId] = $this->_getItems($parentId);
        }

        return $this->existsItems[$parentId];
    }

    private function _getItems($parentId)
    {
        /** @var BundleOptionCollection $optionsCollection */
        $optionsCollection = $this->bundleOptionCollectionFactory->create();

        try {
            $storeId = $this->storeManager->getStore()->getId();
        } catch (\Exception $e) {
            $storeId = 0;
        }

        $optionsCollection->joinValues($storeId);

        $productTable = $optionsCollection->getTable('catalog_product_entity');
        $linkField = $optionsCollection->getConnection()->getAutoIncrementField(
            $productTable
        );
        $optionsCollection->getSelect()->join(
            ['cpe' => $productTable],
            'cpe.' . $linkField . ' = main_table.parent_id',
            []
        )->where(
            "cpe.entity_id IN (?)",
            [$parentId]
        );
        $optionsCollection->setPositionOrder();

        $returnArray = [];

        /** @var BundleOptionModel $option */
        foreach ($optionsCollection as $option) {
            $configurableProduct = $this
                ->getConfigurableProductFromOption($option->getId(), $parentId);
            if ($configurableProduct) {
                $returnArray[] = $configurableProduct;
            }
        }

        return $returnArray;
    }

    /**
     * @param int $optionId
     * @param int $parentId
     *
     * @return array|null
     */
    public function getConfigurableProductFromOption($optionId, $parentId)
    {
        /** @var BundleSelectionCollection $selectionCollection */
        $selectionCollection = $this->bundleSelectionCollectionFactory->create();
        $selectionCollection->setOptionIdsFilter([$optionId]);

        $selectionCollection->getSelect()
            ->where('parent_product_id IN (?)', [$parentId]);

        $ids = [];
        /** @var Product $selection */
        foreach ($selectionCollection as $selection) {
            $ids[] = $selection->getId();
        }

        return $this->findConfigurableProduct($ids);
    }

    /**
     * @param int[] $ids
     *
     * @return array|null
     */
    protected function findConfigurableProduct($ids)
    {
        $parentProductIds = $this->configurableProductResourceModel
            ->getParentIdsByChild($ids);

        if (count($parentProductIds)) {
            $parentProductIds = array_shift($parentProductIds);
            try {
                $configurableProduct = $this->getProduct($parentProductIds);
                if (null !== $configurableProduct){
                    $configurableProductData = $configurableProduct->getData();
                    $configurableProductData['model'] = $configurableProduct;
                    return $configurableProductData;
                }
            } catch (\Exception $e) {
                return null;
            }
        }

        return null;
    }

    /**
     * @param $productId
     *
     * @return ProductInterface|null
     */
    public function getProduct($productId)
    {
        if (!array_key_exists($productId, $this->products)) {
            try {
                $this->products[$productId] = $this->productRepository
                    ->getById($productId);
            } catch (Exception $e) {
                $this->products[$productId] = null;
            }
        }

        return $this->products[$productId];
    }
}
