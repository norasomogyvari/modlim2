<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class Config
{
    const XML_PATH_PRODUCT_DETAIL_GRAPHQL_QUERY_PREFIX    = 'wyomind_elasticsearchcore/configuration/product_detail_graphql_query_prefix';
    const XML_PATH_PRODUCT_DETAIL_GRAPHQL_QUERY_ITEMS     = 'wyomind_elasticsearchcore/configuration/product_detail_graphql_query_items';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * Config constructor.
     *
     * @param ScopeConfigInterface  $scopeConfig
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    /**
     * @param string $key
     * @param null   $scopeId
     *
     * @return mixed
     */
    public function getStoreConfig($key, $scopeId = null)
    {
        $scope = ScopeInterface::SCOPE_STORES;
        if (!$scopeId) {
            $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT;
        }

        return $this->scopeConfig->getValue($key, $scope, $scopeId);
    }

    /**
     * @param null|int $scopeId
     *
     * @return string
     */
    public function getProductDetailGraphQLQuery($scopeId = null)
    {
        $prefix = $this->getStoreConfig(
            self::XML_PATH_PRODUCT_DETAIL_GRAPHQL_QUERY_PREFIX,
            $scopeId
        );

        $items = $this->getStoreConfig(
            self::XML_PATH_PRODUCT_DETAIL_GRAPHQL_QUERY_ITEMS,
            $scopeId
        );

        return str_replace(
            ['$ITEMS_HOLDER'],
            [$items],
            $prefix
        );
    }

    /**
     * @param int $storeId
     *
     * @return string
     */
    public function getStoreUrl($storeId)
    {
        try {
            return $this->storeManager->getStore($storeId)->getBaseUrl(UrlInterface::URL_TYPE_LINK, true);
        } catch (\Exception $e) {
            return '';
        }
    }

    /**
     * @param int $storeId
     *
     * @return string
     */
    public function getStoreCode($storeId)
    {
        try {
            return $this->storeManager->getStore($storeId)->getCode();
        } catch (\Exception $e) {
            return '';
        }
    }
}
