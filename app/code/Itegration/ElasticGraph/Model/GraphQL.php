<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Model;

class GraphQL
{
    /**
     * @param string $graphQlQuery
     * @param string $endPoint
     * @param string $endPointResolve
     * @param string $operationName
     * @param array  $optionalHeaders
     *
     * @return array
     */
    public function execute(
        $graphQlQuery,
        $endPoint,
        $endPointResolve = '',
        $operationName = 'productDetail',
        $optionalHeaders = []
    ) {
        $headers = array_merge(
            ['Content-Type: application/json'],
            $optionalHeaders
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endPoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $graphQlQuery);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RESOLVE, [$endPointResolve]);
        curl_setopt($ch, CURLOPT_DNS_USE_GLOBAL_CACHE, false);

        $result = curl_exec($ch);
        curl_close($ch);

        try {
            $data = json_decode($result, true);
            if (
                array_key_exists('data', $data) &&
                array_key_exists($operationName, $data['data']) &&
                array_key_exists('items', $data['data'][$operationName])
            ) {
                return $data['data'][$operationName]['items'];
            }
            return [];
        } catch (\Exception $e) {
            return [];
        }
    }

    /**
     * @param string       $field
     * @param array|string $values
     *
     * @return string
     */
    public function parseFilter($field, $values)
    {
        if (!is_array($values)) {
            $values = [$values];
        }

        return '{"' . $field . '":{"in":["' . implode($values, '","') . '"]}}';
    }
}
