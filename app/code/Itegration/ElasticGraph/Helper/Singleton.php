<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper;


class Singleton
{
    /**
     * @return $this
     */
    public static function create()
    {
        if (!array_key_exists(static::class, $GLOBALS)) {
            $GLOBALS[static::class] = new static();
        }

        return $GLOBALS[static::class];
    }
}
