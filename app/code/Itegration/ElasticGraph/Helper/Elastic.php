<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper;

use Itegration\ElasticGraph\Helper\Requester as ItegrationRequester;
use Itegration\ElasticGraph\Helper\Wyomind as WyomindHelper;
use Wyomind\ElasticsearchCore\Helper\Cache\FileSystem as WyomindFileSystem;
use Wyomind\ElasticsearchCore\Helper\Requester as WyomindRequester;
use Wyomind\ElasticsearchCore\Helper\Synonyms as WyomindSynonyms;
use Wyomind\ElasticsearchCore\Model\Client as WyomindClient;

class Elastic
{
    /** @var WyomindRequester */
    private $_requester;

    /** @var WyomindClient */
    private $_client;

    /** @var string */
    private $_store;

    public function __construct($store)
    {
        $this->_store = $store;

        $wyomindHelper = new WyomindHelper();
        $config = $wyomindHelper->getConfig($this->_store);
        $this->_client = $wyomindHelper->getClient($this->_store);

        $cache = new WyomindFileSystem();
        $synonymsHelper = new WyomindSynonyms();
        $this->_requester = new ItegrationRequester(
            $this->_client,
            $config,
            $cache,
            $synonymsHelper
        );
    }

    /**
     * @return ItegrationRequester
     */
    public function getRequester()
    {
        return $this->_requester;
    }

    /**
     * @return WyomindClient
     */
    public function getClient()
    {
        return $this->_client;
    }
}
