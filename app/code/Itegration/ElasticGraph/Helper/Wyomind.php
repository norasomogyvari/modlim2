<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper;

use Wyomind\ElasticsearchCore\Helper\Autocomplete\Config as WyomindConfig;
use Wyomind\ElasticsearchCore\Model\Client as WyomindClient;

class Wyomind
{
    /**
     * @var array
     */
    private $_clients = [];

    /**
     * @var array
     */
    private $_configs = [];

    /**
     * @param $store
     *
     * @return WyomindConfig
     * @throws \Exception
     */
    public function getConfig($store)
    {
        if (!array_key_exists($store, $this->_configs)) {
            $config = new WyomindConfig($store);
            $configData = $config->getData();
            $this->_configs[$store] = $config;
        }

        return $this->_configs[$store];
    }

    /**
     * @param $store
     *
     * @return WyomindClient
     * @throws \Exception
     */
    public function getClient($store)
    {
        if (!array_key_exists($store, $this->_clients)) {
            $config = $this->getConfig($store);
            $client = new WyomindClient($config);
            $client->init($store);

            $this->_clients[$store] = $client;
        }

        return $this->_clients[$store];
    }
}
