<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper;

class Data
{
    const CATEGORY_OPERATION_TYPE       = 'category';
    const PRODUCT_SEARCH_OPERATION_TYPE = 'productSearch';
}
