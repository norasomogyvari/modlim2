<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper;

use Wyomind\ElasticsearchCore\Helper\Requester as WyomindRequester;

class Requester extends WyomindRequester
{
    /**
     * @param string $storeCode
     * @param int    $customerGroupId
     * @param string $field
     * @param string $value
     * @param bool   $onlyVisible
     *
     * @return array
     */
    public function getProduct(
        $storeCode,
        $customerGroupId,
        $field,
        $value,
        $onlyVisible = true
    ) {
        $start = microtime(true);

        $md5func = "md5";
        $md5 = $md5func(
            $storeCode .
            $customerGroupId . '_' .
            $field . '_' .
            $value .
            '_product'
        );
        $data = $this->_cache->get($md5);
        if ($data) {
            $data['time'] = round((microtime(true) - $start) * 1000);
            $data['cache'] = true;
            return $data;
        }

        $matches[] = ['match' => [$field => $value]];

        $filter = [];
        if ($onlyVisible) {
            $filter = [
                'terms' => [
                    'visibility' => [3, 4],
                ],
            ];
        }

        $params = [
            'body' => [
                'sort'  => [],
                'query' => [
                    'bool' => [
                        'filter' => $filter,
                        'must'   => $matches,
                    ],
                ],
            ],
        ];

        $results = $this->_client->query(
            $this->_client->getIndexName($storeCode, false, 'product'),
            'product',
            $params
        );

        $this->_attributeOptions = $this->_config->getValue('swatches');

        $filteredProducts = $results['hits']['hits'];

        $newProducts = [];
        $this->postProcessProducts($newProducts, $filteredProducts);

        $data = [
            'count'   => $results['hits']['total'],
            'product' => $newProducts,
            'time'    => round((microtime(true) - $start) * 1000),
        ];

        $this->_cache->put($md5, $data);
        return $data;
    }

    /**
     * @param string $storeCode
     * @param int    $customerGroupId
     * @param string $field
     * @param array  $values
     * @param bool   $onlyVisible
     *
     * @return array
     */
    public function getElasticProducts(
        $storeCode,
        $customerGroupId,
        $field,
        $values,
        $onlyVisible = true
    ) {
        $start = microtime(true);

        if (!is_array($values)) {
            $values = [$values];
        }

        $md5func = "md5";
        $md5 = $md5func(
            $storeCode .
            $customerGroupId . '_' .
            $field . '_' .
            implode('-', $values) .
            '_product'
        );
        $data = $this->_cache->get($md5);
        if ($data) {
            $data['time'] = round((microtime(true) - $start) * 1000);
            $data['cache'] = true;
            return $data;
        }

        $matches = [];
        foreach ($values as $value) {
            $matches[] = ['match' => [$field => $value]];
        }

        $filter = [];
        if ($onlyVisible) {
            $filter = [
                'terms' => [
                    'visibility' => [3, 4],
                ],
            ];
        }

        $params = [
            'body' => [
                'sort'  => [],
                'query' => [
                    'bool' => [
                        'filter' => $filter,
                        'must'   => [
                            'bool' => [
                                'should' => $matches,
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $results = $this->_client->query(
            $this->_client->getIndexName($storeCode, false, 'product'),
            'product',
            $params
        );

        $this->_attributeOptions = $this->_config->getValue('swatches');

        $filteredProducts = $results['hits']['hits'];

        $newProducts = [];
        $this->postProcessProducts($newProducts, $filteredProducts);

        $data = [
            'count'    => $results['hits']['total'],
            'products' => $newProducts,
            'time'     => round((microtime(true) - $start) * 1000),
        ];

        $this->_cache->put($md5, $data);
        return $data;
    }

    /**
     * @param string $storeCode
     * @param string $field
     * @param mixed  $value
     *
     * @return array
     */
    public function getCategory($storeCode, $field, $value)
    {
        $start = microtime(true);

        $md5func = "md5";
        $md5 = $md5func(
            $storeCode .
            $field . '_' .
            $value .
            '_category'
        );
        $data = $this->_cache->get($md5);
        if ($data) {
            $data['time'] = round((microtime(true) - $start) * 1000);
            $data['cache'] = true;
            return $data;
        }

        $matches[] = ['match' => [$field => $value]];

        $filter = [];

        $params = [
            'body' => [
                'sort'  => [],
                'query' => [
                    'bool' => [
                        'filter' => $filter,
                        'must'   => $matches,
                    ],
                ],
            ],
        ];

        $results = $this->_client->query(
            $this->_client->getIndexName($storeCode, false, 'category'),
            'category',
            $params
        );


        $categories = $results['hits']['hits'];

        $newCategories = [];
        foreach ($categories as $category) {
            $newCategories[] = $category['_source'];
        }

        $data = [
            'count'    => $results['hits']['total'],
            'category' => $newCategories,
            'time'     => round((microtime(true) - $start) * 1000),
        ];

        $this->_cache->put($md5, $data);
        return $data;
    }

    /**
     * @param string $storeCode
     * @param string $field
     * @param mixed  $value
     *
     * @return array
     */
    public function getCmsPage($storeCode, $field, $value)
    {
        $start = microtime(true);

        $md5func = "md5";
        $md5 = $md5func(
            $storeCode .
            $field . '_' .
            $value .
            '_cms'
        );
        $data = $this->_cache->get($md5);
        if ($data) {
            $data['time'] = round((microtime(true) - $start) * 1000);
            $data['cache'] = true;
            return $data;
        }

        $matches[] = ['match' => [$field => $value]];

        $params = [
            'body' => [
                'sort'  => [],
                'query' => [
                    'bool' => [
                        'must' => $matches,
                    ],
                ],
            ],
        ];

        $results = $this->_client->query(
            $this->_client->getIndexName($storeCode, false, 'cms'),
            'cms',
            $params
        );


        $cmsPages = $results['hits']['hits'];
        $newCmsPages = [];
        foreach ($cmsPages as $cmsPage) {
            $newCmsPages[] = $cmsPage['_source'];
        }

        $data = [
            'count'    => $results['hits']['total'],
            'cmsPages' => $newCmsPages,
            'time'     => round((microtime(true) - $start) * 1000),
        ];

        $this->_cache->put($md5, $data);
        return $data;
    }

    /**
     * @param string $storeCode
     * @param string $field
     * @param mixed  $value
     *
     * @return array
     */
    public function getCmsBlock($storeCode, $field, $value)
    {
        $start = microtime(true);

        $md5func = "md5";
        $md5 = $md5func(
            $storeCode .
            $field . '_' .
            $value .
            '_cms_block'
        );
        $data = $this->_cache->get($md5);
        if ($data) {
            $data['time'] = round((microtime(true) - $start) * 1000);
            $data['cache'] = true;
            return $data;
        }

        $matches[] = ['match' => [$field => $value]];

        $params = [
            'body' => [
                'sort'  => [],
                'query' => [
                    'bool' => [
                        'must' => $matches,
                    ],
                ],
            ],
        ];

        $results = $this->_client->query(
            $this->_client->getIndexName($storeCode, false, 'cms_block'),
            'cms_block',
            $params
        );

        $cmsBlocks = $results['hits']['hits'];
        $newCmsBlocks = [];
        foreach ($cmsBlocks as $cmsBlock) {
            $newCmsBlocks[] = $cmsBlock['_source'];
        }

        $data = [
            'count'     => $results['hits']['total'],
            'cmsBlocks' => $newCmsBlocks,
            'time'      => round((microtime(true) - $start) * 1000),
        ];

        $this->_cache->put($md5, $data);
        return $data;
    }
}
