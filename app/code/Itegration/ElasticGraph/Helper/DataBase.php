<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper;

use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\ObjectManagerInterface;

class DataBase extends Singleton
{
    /**
     * @var null|AdapterInterface
     */
    private $_connection = null;

    /**
     * @var int[]
     */
    private $_storeId = [];

    /**
     * @return AdapterInterface
     */
    public function getConnection()
    {
        if (null == $this->_connection) {
            $objectManagerFactory = Bootstrap::createObjectManagerFactory(
                BP,
                []
            );
            /** @var ObjectManagerInterface $objectManager */
            $objectManager = $objectManagerFactory->create([]);
            /** @var ResourceConnection $resource */
            $resource = $objectManager->create(ResourceConnection::class);
            /** @var AdapterInterface _connection */
            $this->_connection = $resource->getConnection();
        }

        return $this->_connection;
    }

    /**
     * @param string $storeCode
     *
     * @return int
     */
    public function getStoreId($storeCode)
    {
        if (!array_key_exists($storeCode, $this->_storeId)) {
            $sql = "SELECT store_id FROM " . $this->getConnection()
                    ->getTableName(
                        'store'
                    ) . " WHERE code LIKE '" . $storeCode . "'";
            $storeId = (int)$this->getConnection()->fetchOne($sql);
            $this->_storeId[$storeCode] = $storeId;
        }

        return $this->_storeId[$storeCode];
    }
}
