<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper;


class Timer
{
    const GLOBAL_KEY = 'global';

    /**
     * @var array
     */
    private $_times = [];

    /**
     * @param string $label
     *
     * @return $this
     */
    public function startTimer($label = '')
    {
        if (!$label) {
            $label = self::GLOBAL_KEY;
        }

        if (!array_key_exists($label, $this->_times)) {
            $this->_times[$label] = [
                'start' => $this->_now(),
            ];
        } elseif (!array_key_exists('start', $this->_times[$label])) {
            $this->_times[$label]['start'] = $this->_now();
        }

        return $this;
    }

    /**
     * @param string $label
     *
     * @return $this
     */
    public function endTimer($label = '')
    {
        if (!$label) {
            $label = self::GLOBAL_KEY;
        }

        if (!array_key_exists($label, $this->_times)) {
            $this->_times[$label] = [
                'start' => $this->_now(),
                'end'   => $this->_now(),
            ];
        } elseif (!array_key_exists('end', $this->_times[$label])) {
            $this->_times[$label]['end'] = $this->_now();
            $this->_times[$label]['delta'] = round(
                $this->_times[$label]['end'] - $this->_times[$label]['start'],
                2
            );
            $this->_times[$label]['time_length'] = round(
                $this->_times[$label]['end'] - $this->_times[self::GLOBAL_KEY]['start'],
                2
            );
        }


        return $this;
    }

    public function flush()
    {
        foreach ($this->_times as $timeKey => $time) {
            $this->endTimer($timeKey);
        }
        $this->endTimer();

        return $this->_times;
    }

    /**
     * @return float
     */
    private function _now()
    {
        return microtime(true);
    }
}
