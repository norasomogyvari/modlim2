<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Request;

class CmsBlocks extends SearchByType
{
    /**
     * @param string $field
     * @param string $value
     *
     * @return array
     */
    public function getCmsBlock($field, $value)
    {
        $data = $this->_requester->getCmsBlock($this->_store, $field, $value);
        if (array_key_exists('cmsBlocks', $data) && count($data['cmsBlocks'])) {
            return $data['cmsBlocks'][0];
        }

        return [];
    }
}
