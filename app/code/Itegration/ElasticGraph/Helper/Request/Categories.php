<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Request;

class Categories extends SearchByType
{
    const TYPE = 'category';

    /**
     * @return array
     */
    public function getCategories()
    {
        return $this->_getStoredData(self::TYPE);
    }
}

