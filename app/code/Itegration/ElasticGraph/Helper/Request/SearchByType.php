<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Request;

use Itegration\ElasticGraph\Helper\Elastic;
use Itegration\ElasticGraph\Helper\Requester;

class SearchByType
{
    protected $_autoCompleteLimit  = 5;
    protected $_isHighLightEnabled = true;

    const DEFAULT_SEARCH_TERM = '';

    /** @var string[] */
    protected $_searchKeys = [
        'inputText',
        'search',
    ];

    /** @var array */
    protected $_variables;

    /** @var array */
    protected $_filters;

    /** @var string */
    protected $_store;

    /** @var Requester */
    protected $_requester;

    public function __construct($store, $variables)
    {
        $elastic = new Elastic($store);
        $this->_store = $store;
        $this->_requester = $elastic->getRequester();
        $this->_variables = $variables;
    }

    /**
     * @param $type
     *
     * @return array
     */
    protected function _getStoredData($type)
    {
        return $this->_requester->searchByType(
            $this->_store,
            $type,
            $this->_getSearchTerm(),
            $this->getAutoCompleteLimit(),
            $this->isHighLightEnabled()
        );
    }

    /**
     * @return int
     */
    protected function getAutoCompleteLimit()
    {
        return $this->_autoCompleteLimit;
    }

    /**
     * @return bool
     */
    protected function isHighLightEnabled()
    {
        return $this->_isHighLightEnabled;
    }

    /**
     * @return string
     */
    private function _getSearchTerm()
    {
        foreach ($this->_searchKeys as $searchKey) {
            if (array_key_exists($searchKey, $this->_variables)) {
                return $this->_variables[$searchKey];
            }
        }

        return self::DEFAULT_SEARCH_TERM;
    }
}