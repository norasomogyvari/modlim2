<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Request;

use Itegration\ElasticGraph\Helper\Elastic;
use Itegration\ElasticGraph\Helper\Requester;
use Magento\Catalog\Model\Category as CategoryModel;
use Magento\Framework\ObjectManagerInterface;
use Wyomind\ElasticsearchCore\Model\Client;

class Category
{
    const MAIN_CATEGORY_ID_KEY = 'id';
    const DEFAULT_CATEGORY_ID  = -1;

    /** @var array */
    private $_variables;

    /** @var string */
    private $_store;

    /** @var Client */
    private $_client;

    /** @var Requester */
    protected $_requester;

    /** @var array */
    private $_categories = [];

    public function __construct($store, $variables)
    {
        $elastic = new Elastic($store);
        $this->_store = $store;
        $this->_client = $elastic->getClient();
        $this->_requester = $elastic->getRequester();
        $this->_variables = $variables;
    }

    /**
     * @return array
     */
    public function getCategory()
    {
        $categoryId = self::DEFAULT_CATEGORY_ID;

        if (array_key_exists(self::MAIN_CATEGORY_ID_KEY, $this->_variables)) {
            $categoryId = $this->_variables[self::MAIN_CATEGORY_ID_KEY];
        }

        if (!array_key_exists($categoryId, $this->_categories)) {
            $this->_categories[$categoryId] = $this->getCategoryFromElastic(
                'id',
                $categoryId
            );
        }

        return $this->_categories[$categoryId];
    }

    /**
     * @param string $field
     * @param string $value
     *
     * @return array
     */
    public function getCategoryFromElastic($field, $value)
    {
        $category = $this->_requester->getCategory(
            $this->_store,
            $field,
            $value
        );

        if (array_key_exists('category', $category)) {
            return array_shift($category['category']);
        }

        return [];
    }
}
