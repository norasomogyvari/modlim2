<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Request;

use Itegration\ElasticGraph\Helper\Currency;
use Itegration\ElasticGraph\Helper\Elastic;
use Itegration\ElasticGraph\Helper\Requester;

class Product
{
    const DEFAULT_CUSTOMER_GROUP_ID = 0;

    /** @var array */
    protected $_variables;

    /** @var string */
    protected $_store;

    /** @var Requester */
    protected $_requester;

    /**
     * @var Currency
     */
    protected $_currencyHelper;

    public function __construct($store, $variables)
    {
        $elastic = new Elastic($store);
        $this->_store = $store;
        $this->_requester = $elastic->getRequester();
        $this->_variables = $variables;
        $this->_currencyHelper = Currency::create($store);
    }

    /**
     * @param string $field
     * @param string $value
     * @param bool   $onlyVisible
     *
     * @return array
     */
    public function getProduct($field, $value, $onlyVisible = true)
    {
        $product = $this->_requester->getProduct(
            $this->_store,
            self::DEFAULT_CUSTOMER_GROUP_ID,
            $field,
            $value,
            $onlyVisible
        );

        if (array_key_exists('product', $product)) {
            $product['currency'] = $this->_currencyHelper->getCurrency();
            return $product;
        }

        return [];
    }

    /**
     * @param string $field
     * @param array  $value
     * @param bool   $onlyVisible
     *
     * @return array
     */
    public function getProducts($field, $value, $onlyVisible = true)
    {
        $products = $this->_requester->getElasticProducts(
            $this->_store,
            self::DEFAULT_CUSTOMER_GROUP_ID,
            $field,
            $value,
            $onlyVisible
        );

        if (array_key_exists('products', $products)) {
            return $products['products'];
        }

        return [];
    }
}
