<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Request;

use Itegration\ElasticGraph\Helper\Currency;
use Itegration\ElasticGraph\Helper\Elastic;
use Itegration\ElasticGraph\Helper\ProductUrlSuffix;
use Itegration\ElasticGraph\Helper\LayerNavigation;
use Wyomind\ElasticsearchCore\Helper\Requester;

class Products
{
    const SORTER_KEY       = 'sorter';
    const FILTER_KEY       = 'filter';
    const CATEGORY_ID_KEY  = 'category_id';
    const SEARCH_KEY       = 'inputText';
    const PAGE_SIZE_KEY    = 'pageSize';
    const CURRENT_PAGE_KEY = 'currentPage';

    const FINAL_PRICE_FILTER = 'final_price';

    const DEFAULT_PAGER_SITE        = 10;
    const DEFAULT_SORT_ORDER        = 'position';
    const DEFAULT_SORT_DIRECTION    = 'asc';
    const DEFAULT_CUSTOMER_GROUP_ID = 0;
    const DEFAULT_SEARCH_TERM       = '';

    const MIN_PRICE_FILTER = -1;
    const MAX_PRICE_FILTER = 999999999999;

    /** @var string[] */
    protected $_searchKeys = [
        'inputText',
        'search',
    ];

    /** @var string[] */
    protected $_keepOriginalFilterKey = [
        'category_id',
        'price',
        'final_price',
        'rating',
        'have_graphql',
    ];

    /** @var array */
    protected $_variables;

    /** @var array */
    protected $_filters;

    /** @var string */
    protected $_store;

    /** @var Requester */
    protected $_requester;

    /** @var ProductUrlSuffix */
    protected $_productUrlSuffixHelper;

    /** @var LayerNavigation */
    protected $_layerNavigationHelper;

    /** @var Currency */
    protected $_currencyHelper;

    /**
     * Products constructor.
     *
     * @param string $store
     * @param array  $variables
     */
    public function __construct($store, $variables)
    {
        $elastic = new Elastic($store);
        $this->_store = $store;
        $this->_requester = $elastic->getRequester();
        $this->_variables = $variables;
        $this->_productUrlSuffixHelper = ProductUrlSuffix::create();
        $this->_layerNavigationHelper = LayerNavigation::create();
        $this->_currencyHelper = Currency::create($this->_store);
    }

    /**
     * @return \Itegration\ElasticGraph\Helper\Requester|Requester
     */
    public function getRequester()
    {
        return $this->_requester;
    }

    /**
     * @return string
     */
    public function getStore()
    {
        return $this->_store;
    }

    /**
     * @param array $sortData
     * @param bool  $loadSelectedFilters
     * @param bool  $loadBuckets
     * @param bool  $highlightEnabled
     * @param int   $layerUpdateSorting
     *
     * @return array
     */
    public function getProducts(
        $sortData = [],
        $loadSelectedFilters = false,
        $loadBuckets = false,
        $highlightEnabled = false,
        $layerUpdateSorting = 1
    ) {
        list($sortOrder, $sortDirection) = $this->_getSortOrder();

        $pageSize = $this->_getPageSize();
        $searchTerm = $this->_getSearchTerm();

        $products = $this->_requester->getProducts(
            $this->_store,
            self::DEFAULT_CUSTOMER_GROUP_ID,
            $this->_getCategoryId(),
            $searchTerm,
            $this->_calculateFrom(),
            $pageSize,
            $sortOrder,
            $sortDirection,
            $this->_getParsedFilters(),
            $loadSelectedFilters,
            $loadBuckets,
            $highlightEnabled,
            $layerUpdateSorting
        );

        if (array_key_exists('sortFields', $sortData)) {
            $products['sort_fields'] = $sortData['sortFields'];
        }
        if (array_key_exists('defaultSortBy', $sortData)) {
            $products['default_sort_by'] = $sortData['defaultSortBy'];
        }
        $products['total_count'] = $products['amount']['total'];
        $products['page_size'] = $pageSize;
        $products['currency'] = $this->_currencyHelper->getCurrency();
        $products['product_url_suffix'] = $this->_productUrlSuffixHelper->getProductUrlSuffix(
            $this->_store
        );
        $products['layer_navigation_options'] = $this->_layerNavigationHelper->getSettings(
            $this->_store,
            $searchTerm ? 'search' : 'category'
        );

        $products['search_term'] = $searchTerm;
        $products['store_id'] = $this->_store;

        return $products;
    }

    /**
     * @return array
     */
    private function _getSortOrder()
    {
        if (array_key_exists(self::SORTER_KEY, $this->_variables)) {
            $value = reset($this->_variables[self::SORTER_KEY]);
            $key = key($this->_variables[self::SORTER_KEY]);

            return [$key, $value];
        }

        return [self::DEFAULT_SORT_ORDER, self::DEFAULT_SORT_DIRECTION];
    }

    /**
     * @return array
     */
    private function _getFilters()
    {
        if (null == $this->_filters) {
            $this->_filters = [];
            if (array_key_exists(self::FILTER_KEY, $this->_variables)) {
                $this->_filters = $this->_variables[self::FILTER_KEY];
            }
        }

        $this->_filters['product_detail_graphql'] = ['neq' => ''];

        return $this->_filters;
    }


    /**
     * @return int
     */
    private function _getCategoryId()
    {
        $filters = $this->_getFilters();
        if (array_key_exists(self::CATEGORY_ID_KEY, $filters)) {
            $ids = $filters[self::CATEGORY_ID_KEY];
            return array_shift($ids);
        }

        return Category::DEFAULT_CATEGORY_ID;
    }

    /**
     * @return string
     */
    protected function _getSearchTerm()
    {
        foreach ($this->_searchKeys as $searchKey) {
            if (array_key_exists($searchKey, $this->_variables)) {
                $searchTerm = $this->_variables[$searchKey];
                $searchTerm = str_replace('"', '\"', $searchTerm);
                return $searchTerm;
            }
        }

        return self::DEFAULT_SEARCH_TERM;
    }

    /**
     * @return int
     */
    private function _calculateFrom()
    {
        $pageSize = $this->_getPageSize();
        $currentPage = 1;
        if (array_key_exists(self::CURRENT_PAGE_KEY, $this->_variables)) {
            $currentPage = $this->_variables[self::CURRENT_PAGE_KEY];
        }

        return (int)($pageSize * ($currentPage - 1));
    }

    /**
     * @return int
     */
    private function _getPageSize()
    {
        if (array_key_exists(self::PAGE_SIZE_KEY, $this->_variables)) {
            return $this->_variables[self::PAGE_SIZE_KEY];
        }

        return self::DEFAULT_PAGER_SITE;
    }

    /**
     * @return array
     */
    private function _getParsedFilters()
    {
        $parsedFilter = [];

        $filters = $this->_getFilters();

        $filters['quantity_and_stock_status'] = [
            'eq' => 1
        ];

        if (!array_key_exists(self::FINAL_PRICE_FILTER, $filters)) {
            $filters[self::FINAL_PRICE_FILTER] = [
                'gteq' => self::MIN_PRICE_FILTER,
            ];
        }
        foreach ($filters as $filterKey => $filterValue) {
            $filterKey = $this->_parseFilterKey($filterKey);
            if ($filterKey === self::FINAL_PRICE_FILTER) {
                $parsedFilter[$filterKey] = [
                    'min' => array_key_exists('gteq', $filterValue) ?
                        $filterValue['gteq'] : self::MIN_PRICE_FILTER,
                    'max' => array_key_exists('lteq', $filterValue) ?
                        $filterValue['lteq'] : self::MAX_PRICE_FILTER,
                ];
                $parsedFilter[$filterKey]['min'] = round(
                    $parsedFilter[$filterKey]['min'] / $this->_currencyHelper->getCurrencyRate(
                    ),
                    2
                );
                $parsedFilter[$filterKey]['max'] = round(
                    $parsedFilter[$filterKey]['max'] / $this->_currencyHelper->getCurrencyRate(
                    ),
                    2
                );
            } elseif ($filterKey !== self::CATEGORY_ID_KEY) {
                $ids = reset($filterValue);
                if (!is_array($ids)) {
                    $ids = [$ids];
                }
                $parsedFilter[$filterKey] = $ids;
            }
        }

        return $parsedFilter;
    }

    /**
     * @param string $originalFilterKey
     *
     * @return string
     */
    private function _parseFilterKey($originalFilterKey)
    {
        if (in_array($originalFilterKey, $this->_keepOriginalFilterKey)) {
            return $originalFilterKey;
        }

        switch ($originalFilterKey) {
            case 'product_category':
                return 'categories_ids';
                break;
            case self::FINAL_PRICE_FILTER:
                return self::FINAL_PRICE_FILTER;
                break;
            default:
                return $originalFilterKey . '_ids';
        }
    }
}
