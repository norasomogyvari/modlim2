<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Request;

class Cms extends SearchByType
{
    const TYPE = 'cms';

    /**
     * @return array
     */
    public function getCmsPages()
    {
        return $this->_getStoredData(self::TYPE);
    }

    /**
     * @param string $field
     * @param string $value
     *
     * @return array
     */
    public function getCmsPage($field, $value)
    {
        $data = $this->_requester->getCmsPage($this->_store, $field, $value);
        if (array_key_exists('cmsPages', $data) && count($data['cmsPages'])) {
            return $data['cmsPages'][0];
        }

        return [];
    }
}
