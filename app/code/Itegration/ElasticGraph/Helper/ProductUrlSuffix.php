<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper;

class ProductUrlSuffix extends Singleton
{
    /** @var array */
    private $_productUrlPrefix = [];

    /**
     * @param string $storeCode
     *
     * @return string
     */
    public function getProductUrlSuffix($storeCode)
    {
        if (!array_key_exists($storeCode, $this->_productUrlPrefix)) {
            $dataBase = DataBase::create();

            $sql = "SELECT value FROM core_config_data WHERE path LIKE 'catalog/seo/product_url_suffix' AND scope_id = " . $dataBase->getStoreId(
                    $storeCode
                ) . " AND scope = 'stores'";
            $this->_productUrlPrefix[$storeCode] = $dataBase->getConnection()
                ->fetchOne($sql);

            if (!$this->_productUrlPrefix[$storeCode]) {
                $sql = "SELECT value FROM core_config_data WHERE path LIKE 'catalog/seo/product_url_suffix' AND scope_id = 0 AND scope = 'default'";
                $this->_productUrlPrefix[$storeCode] = $dataBase->getConnection(
                )->fetchOne($sql);
            }
        }

        return $this->_productUrlPrefix[$storeCode];
    }
}
