<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper;

use Wyomind\ElasticsearchLayeredNavigation\Helper\Config as WyomindLayeredNavigationHelper;

class LayerNavigation extends Singleton
{
    /** @var array */
    private $_layerFilters = [];

    const POSITION_LEFT   = 0;
    const POSITION_TOP    = 1;
    const POSITION_RIGHT  = 2;
    const POSITION_MOBILE = 3;

    /**
     * @param string $storeCode
     * @param string $type
     * @param int $position
     *
     * @return array
     */
    public function getSettings(
        $storeCode,
        $type = 'category',
        $position = self::POSITION_LEFT
    ) {
        if (!array_key_exists($storeCode, $this->_layerFilters)) {
            $dataBase = DataBase::create();

            $path = WyomindLayeredNavigationHelper::XML_PATH_WELN_SETTINGS_DISPLAY . $type;
            switch ($position) {
                case self::POSITION_LEFT:
                default:
                    $path .= WyomindLayeredNavigationHelper::ATTRIBUTES_LEFT_LAYER;
                    break;
                case self::POSITION_TOP:
                    $path .= WyomindLayeredNavigationHelper::ATTRIBUTES_TOP_LAYER;
                    break;
                case self::POSITION_RIGHT:
                    $path .= WyomindLayeredNavigationHelper::ATTRIBUTES_RIGHT_LAYER;
                    break;
                case self::POSITION_MOBILE:
                    $path .= WyomindLayeredNavigationHelper::ATTRIBUTES_MOBILE_LAYER;
                    break;
            }

            $sql = "SELECT value FROM core_config_data WHERE path LIKE '$path' AND scope_id = " . $dataBase->getStoreId($storeCode) . " AND scope = 'stores'";
            $serializedData = $dataBase->getConnection()->fetchOne($sql);
            if (!$serializedData) {
                $sql = "SELECT value FROM core_config_data WHERE path LIKE '$path' AND scope_id = 0 AND scope = 'default'";
                $serializedData = $dataBase->getConnection()->fetchOne($sql);
            }

            if ($serializedData) {
                $data = json_decode($serializedData, true);
            } else {
                $data = [];
            }

            $returnArray = [];
            foreach ($data as $uid => $filter) {
                $returnArray[] = [
                    $filter['attribute_code'],
                    (int)($filter['show_results_count']??0),
                    (int)($filter['show_more_less']??0),
                    (int)($filter['show_more_less_nb_values']??5),
                    (int)($filter['show_layer_filter']??0),
                    (int)($filter['closed']??0),
                    (int)($filter['generate_url']??0),
                    (int)($filter['no_follow']??0),
                    (int)$filter['position']
                ];
            }

            usort($returnArray, function($a, $b) {
                $aPosition = $a[8];
                $bPosition = $b[8];

                if ($aPosition == $bPosition) {
                    return 0;
                }
                return ($aPosition < $bPosition) ? -1 : 1;
            });

            $this->_layerFilters[$storeCode] = $returnArray;
        }

        return $this->_layerFilters[$storeCode];
    }
}
