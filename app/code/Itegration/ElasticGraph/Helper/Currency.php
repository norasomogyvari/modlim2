<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper;

class Currency
{
    const DEFAULT_CURRENCY = 'EUR';

    private $_dataBase;

    /** @var string */
    private $_store;

    /** @var int */
    private $_storeId = null;

    /** @var null|string */
    private $_actualCurrency = null;

    /** @var null|string */
    private $_defaultCurrency = null;

    /** @var null|float */
    private $_actualRate = null;

    /**
     * @param string $store
     *
     * @return $this
     */
    public static function create($store)
    {
        if (!array_key_exists(self::class, $GLOBALS)) {
            $GLOBALS[self::class] = new self($store);
        }

        return $GLOBALS[self::class];
    }

    public function __construct($store)
    {
        $this->_store = $store;
        $this->_dataBase = DataBase::create();
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        if (null === $this->_actualCurrency) {
            $this->_actualCurrency = self::DEFAULT_CURRENCY;
        }

        return $this->_actualCurrency;
    }

    /**
     * @param string $currency
     *
     * @return float
     */
    public function setCurrency($currency)
    {
        try {
            $this->_actualCurrency = $currency;
            return $this->getCurrencyRate();
        } catch (\Exception $e) {
            return 1;
        }
    }

    /**
     * @return string
     */
    private function getStoreDefaultCurrency()
    {
        if (null == $this->_defaultCurrency) {
            $sql = "SELECT value FROM " . $this->_dataBase->getConnection()
                    ->getTableName(
                        'core_config_data'
                    ) . " WHERE path LIKE 'currency/options/base' AND scope_id = " . $this->_dataBase->getStoreId($this->_store) . " AND scope = 'stores'";
            $this->_defaultCurrency = $this->_dataBase->getConnection()->fetchOne($sql);
            if (!$this->_defaultCurrency) {
                $sql = "SELECT value FROM " . $this->_dataBase->getConnection()
                        ->getTableName(
                            'core_config_data'
                        ) . " WHERE path LIKE 'currency/options/base' AND scope_id = 0 AND scope = 'default'";
                $this->_defaultCurrency = $this->_dataBase->getConnection()->fetchOne($sql);
            }
            if (!$this->_defaultCurrency) {
                $this->_defaultCurrency = self::DEFAULT_CURRENCY;
            }
        }

        return $this->_defaultCurrency;
    }

    /**
     * @param string $defaultCode
     * @param string $actualCode
     *
     * @return float
     */
    private function _getCurrencyRate($defaultCode, $actualCode)
    {
        $sql = "SELECT rate FROM directory_currency_rate WHERE currency_from LIKE '" . $defaultCode . "' AND currency_to LIKE '" . $actualCode . "'";
        $rate = (float)$this->_dataBase->getConnection()->fetchOne($sql);

        if ($rate <= 0) {
            $rate = 1;
        }

        return $rate;
    }

    /**
     * @return float
     */
    public function getCurrencyRate()
    {
        if (null === $this->_actualRate) {
            $defaultCurrency = $this->getStoreDefaultCurrency();
            if ($this->getCurrency() === $defaultCurrency) {
                $this->_actualRate = 1;
            } else {
                $this->_actualRate = $this->_getCurrencyRate(
                    $defaultCurrency,
                    $this->getCurrency()
                );
            }
        }
        return $this->_actualRate;
    }
}
