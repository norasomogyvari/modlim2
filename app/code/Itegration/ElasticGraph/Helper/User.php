<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper;

class User extends Singleton
{
    /**
     * @var null|string
     */
    protected $_hash = null;

    /**
     * @var null|array
     */
    protected $_customerData = null;

    /**
     * @var DataBase
     */
    protected $_dataBase = null;

    public function __construct()
    {
        $this->_dataBase = DataBase::create();
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->_hash = $hash;
    }

    /**
     * @return array
     */
    public function getUserData()
    {
        if (null === $this->_customerData) {
            $this->_customerData = $this->getCustomerData();
        }

        return $this->_customerData;
    }

    /**
     * @return array
     */
    private function getCustomerData()
    {
        if ($this->_hash) {
            $sql = "SELECT CE.entity_id, CE.group_id FROM " .
                $this->_dataBase->getConnection()->getTableName(
                    'customer_entity'
                ) .
                " AS CE LEFT JOIN " .
                $this->_dataBase->getConnection()->getTableName('oauth_token') .
                " AS OT ON OT.customer_id = CE.entity_id WHERE OT.token LIKE ?";
            $user = $this->_dataBase->getConnection()->fetchRow(
                $sql,
                $this->_hash
            );

            if (array_key_exists('entity_id', $user)) {
                return [
                    'customer_id'       => $user['entity_id'],
                    'customer_group_id' => $user['group_id'],
                ];
            }
        }

        return [
            'customer_id'       => 0,
            'customer_group_id' => 0,
        ];
    }
}
