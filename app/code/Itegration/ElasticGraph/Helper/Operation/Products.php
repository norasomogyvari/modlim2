<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Operation;

use Itegration\ElasticGraph\Helper\Request\Category as CategoryRequest;
use Itegration\ElasticGraph\Helper\Request\Products as ProductsRequest;
use Itegration\ElasticGraph\Helper\Response\Products as ProductsResponse;
use Itegration\ElasticGraph\Helper\SortOrder;

class Products extends AbstractOperation
{
    const OPERATION_NAME = 'products';

    /**
     * Category constructor.
     *
     * @param array $params
     * @param array $definition
     */
    public function __construct($params, $definition)
    {
        parent::__construct($params, $definition);

        $this->_productRequest = new ProductsRequest(
            $this->_store,
            $this->_variables
        );
        $this->_categoryRequest = new CategoryRequest(
            $this->_store,
            $this->_variables
        );

        $this->_sortOrder = new SortOrder();
    }

    /**
     * @return array
     */
    public function processBody()
    {
        $category = $this->_categoryRequest->getCategory();
        $loadSelectedFilters = true;
        $loadBuckets = true;
        $highlightEnabled = false;
        $layerUpdateSorting = 1;
        if (array_key_exists('inputText', $this->_variables) &&
            array_key_exists('pageSize', $this->_variables) &&
            $this->_variables['pageSize'] <= 10
        ) {
            $loadSelectedFilters = false;
            $loadBuckets = false;
            $highlightEnabled = true;
            $layerUpdateSorting = 0;
        }
        $products = $this->_productRequest->getProducts(
            $this->_sortOrder->getSortOrder($category),
            $loadSelectedFilters,
            $loadBuckets,
            $highlightEnabled,
            $layerUpdateSorting
        );

        $productsData = [];
        if ($products) {
            $products['sorter'] = true;
            $products['userData'] = $this->_userData;
            $productsData = new ProductsResponse(
                $products,
                $this->_productRequest
            );
            $productsData = $productsData->getData();
            $this->handlePrices($productsData);
        }

        return $productsData;
    }
}
