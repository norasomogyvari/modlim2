<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Operation;

use Itegration\ElasticGraph\Helper\Request\Category as CategoryRequest;
use Itegration\ElasticGraph\Helper\Request\Products as ProductsRequest;
use Itegration\ElasticGraph\Helper\SortOrder;

abstract class AbstractOperation
{
    const STORE_KEY         = 'store';
    const VARIABLES_KEY     = 'variables';
    const QUERY_KEY         = 'query';
    const OPERATION_KEY     = 'operationName';
    const CURRENCY_RATE_KEY = 'currency_rate';
    const CURRENCY_CODE_KEY = 'currency';
    const USER_KEY          = 'user';

    /** @var null|string */
    protected $_requestType = null;

    /** @var string */
    protected $_store = 'default';

    /** @var array */
    protected $_variables = [];

    /** @var string */
    protected $_query = '';

    /** @var array */
    protected $_definition = [];

    /** @var SortOrder */
    protected $_sortOrder;

    /** @var ProductsRequest */
    protected $_productRequest;

    /** @var CategoryRequest */
    protected $_categoryRequest;

    /** @var float */
    protected $_currencyRate = 1;

    /** @var String */
    protected $_currencyCode = 'USD';

    /** @var array */
    protected $_userData = [];

    /**
     * AbstractOperation constructor.
     *
     * @param array $params
     * @param array $definition
     */
    public function __construct($params, $definition)
    {
        if (array_key_exists(self::STORE_KEY, $params)) {
            $this->_store = $params[self::STORE_KEY];
        }

        if (array_key_exists(self::VARIABLES_KEY, $params)) {
            $this->_variables = $params[self::VARIABLES_KEY];
        }

        if (array_key_exists(self::OPERATION_KEY, $params)) {
            $this->_requestType = $params[self::OPERATION_KEY];
        }

        if (array_key_exists(self::QUERY_KEY, $params)) {
            $this->_query = $params[self::QUERY_KEY];
        }

        if (array_key_exists(self::CURRENCY_CODE_KEY, $params)) {
            $this->_currencyCode = $params[self::CURRENCY_CODE_KEY];
        }

        if (array_key_exists(self::CURRENCY_RATE_KEY, $params)) {
            $this->_currencyRate = $params[self::CURRENCY_RATE_KEY];
        }

        if (array_key_exists(self::USER_KEY, $params)) {
            $this->_userData = $params[self::USER_KEY];
        }

        $this->_definition = $definition;
    }

    /**
     * @return array
     */
    abstract public function processBody();

    /**
     * @param array       $data
     * @param null|string $parentKey
     */
    protected function handlePrices(&$data, $parentKey = null)
    {
        if (is_array($data)) {
            foreach ($data as $key => &$value) {
                if (
                    $key === '__typename' &&
                    $value === 'Price' &&
                    $parentKey !== 'basePrice' &&
                    $parentKey !== 'minBasePrice' &&
                    $parentKey !== 'maxBasePrice'
                ) {
                    $this->parsePriceType($data);
                } elseif ($key === '__typename' && $value === 'PriceLayerFilterItem') {
                    $this->parsePriceLayerFilterItemType($data);
                } elseif (is_array($value)) {
                    $this->handlePrices($value, $key);
                }
            }
        }
    }

    /**
     * @param array $data
     */
    private function parsePriceLayerFilterItemType(&$data)
    {
        if (array_key_exists('price_value', $data)) {
            $data['price_value'] = $this->calculatePrice($data['price_value']);
        }
    }

    /**
     * @param array $data
     */
    private function parsePriceType(&$data)
    {
        if (array_key_exists('amount', $data)) {
            if (array_key_exists('currency', $data['amount'])) {
                $data['amount']['currency'] = $this->_currencyCode;
            }
            if (array_key_exists('value', $data['amount'])) {
                $data['amount']['value'] = $this->calculatePrice(
                    $data['amount']['value']
                );
            }
        }
    }

    /**
     * @param $originalPrice
     *
     * @return float
     */
    private function calculatePrice($originalPrice)
    {
        return round($this->_currencyRate * $originalPrice, 2);
    }
}
