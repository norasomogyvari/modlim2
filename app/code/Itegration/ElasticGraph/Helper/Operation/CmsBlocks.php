<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Operation;

use Itegration\ElasticGraph\Helper\Request\CmsBlocks as CmsBlocksRequest;
use Itegration\ElasticGraph\Helper\Response\CmsBlocks as CmsBlocksResponse;

class CmsBlocks extends AbstractOperation
{
    protected $_cmsBlocksRequest;

    /**
     * ShippingPage constructor.
     *
     * @param array $params
     * @param array $definition
     */
    public function __construct($params, $definition)
    {
        parent::__construct($params, $definition);

        $this->_cmsBlocksRequest = new CmsBlocksRequest($this->_store, $this->_variables);
    }

    /**
     * @return array|bool
     */
    public function processBody()
    {
        $argument = $this->_definition['arguments'];
        $field = array_keys($argument);
        $field = current($field);

        $value = $argument[$field];
        if (array_key_exists($value, $this->_variables)) {
            $value = $this->_variables[$value];
        }

        if (!is_array($value)) {
            $value = [$value];
        }

        if (substr($field, -1,1) === 's') {
            $field = substr($field, 0, -1);
        }

        $data = [];

        foreach ($value as $identifier) {
            $cmBlock = $this->_cmsBlocksRequest->getCmsBlock($field, $identifier);
            if (empty($cmBlock)) {
                return false;
            }
            $data[] = $cmBlock;
        }

        $data = new CmsBlocksResponse([
            'docs' => $data,
            'total_count' => count($data)
        ]);

        return $data->getData();
    }
}
