<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Operation;

use GraphQL\Language\AST\ArgumentNode;
use GraphQL\Language\AST\DocumentNode;
use GraphQL\Language\AST\FieldNode;
use GraphQL\Language\AST\OperationDefinitionNode;
use GraphQL\Language\Parser;
use Itegration\ElasticGraph\Helper\Currency as CurrencyHelper;
use Itegration\ElasticGraph\Helper\Timer;
use Itegration\ElasticGraph\Helper\Timer as TimerHelper;
use Itegration\ElasticGraph\Helper\User as UserHelper;

class Base
{
    const OPERATION_NAME_KEY = 'operationName';
    const QUERY_KEY          = 'query';
    const DEFAULT_STORE      = 'default';
    const DEFAULT_CURRENCY   = 'USD';

    /**
     * @var null|array
     */
    private $_bodyData = null;

    /**
     * @var null|array|bool
     */
    private $_data = null;

    /**
     * @var null|string
     */
    private $_query = null;

    /**
     * @var null|string
     */
    private $_operationName = null;

    /**
     * @var null|string
     */
    private $_store = null;

    /**
     * @var null|string
     */
    private $_currency = null;

    /**
     * @var null|string
     */
    private $_userHash = null;

    /**
     * @var null|TimerHelper
     */
    private $_timer = null;

    /**
     * @return bool
     */
    public function execute()
    {
        $this->_timer = new TimerHelper();

        $this->_timer->startTimer();

        if (!array_key_exists(self::OPERATION_NAME_KEY, $this->getBodyData())) {
            return false;
        }

        $this->_timer->startTimer('definitions');

        $definitions = $this->getDefinitions();
        if (!$definitions) {
            return false;
        }

        $this->_timer->endTimer('definitions');

        foreach ($definitions as $definition) {
            if ($this->getOperationName() === ProductDetail::OPERATION_NAME &&
                $definition['type'] === Products::OPERATION_NAME
            ) {
                $definition['alias'] = ProductDetail::OPERATION_NAME;
            }

            $aliasClassName = $this->getClassName($definition['alias']);
            $typeClassName = $this->getClassName($definition['type']);
            $timerClass = $aliasClassName ? $definition['alias'] : $definition['type'];

            $this->_timer->startTimer($timerClass . '_init');
            $className = $aliasClassName ? $aliasClassName : $typeClassName;

            if (false === $className) {
                return false;
            }
            /** @var AbstractOperation $helper */
            $helper = new $className($this->getBodyData(), $definition);
            $this->_timer->endTimer($timerClass . '_init');
            $this->_timer->startTimer($timerClass . '_process');
            $responseData = $helper->processBody();
            $this->_timer->endTimer($timerClass . '_process');
            $this->_timer->startTimer($timerClass . '_put');

            if (false === $responseData) {
                return false;
            }
            $alias = $definition['alias'];
            if (count(array_keys($responseData)) === 1) {
                $arrayKeys = array_keys($responseData);
                $alias = array_shift($arrayKeys);
                $responseData = $responseData[$alias];
            }

            $this->_data[$alias] = $responseData;

            $this->_timer->endTimer($timerClass . '_put');
        }
    }

    /**
     * @return string
     */
    private function getOperationName()
    {
        if (null === $this->_operationName) {
            $body = $this->getBodyData();
            $this->_operationName = $body[self::OPERATION_NAME_KEY];
        }

        return $this->_operationName;
    }

    /**
     * @return array|bool
     */
    private function getDefinitions()
    {
        $definitions = [];

        /** @var DocumentNode $tmp */
        try {
            $parsedQuery = Parser::parse($this->getQuery(), []);

            /** @var OperationDefinitionNode $definition */
            foreach ($parsedQuery->definitions as $definition) {
                if ($definition->operation === self::QUERY_KEY) {
                    /** @var FieldNode $selection */
                    foreach ($definition->selectionSet->selections as $index => $selection) {
                        $arguments = [];
                        /** @var ArgumentNode $argument */
                        foreach ($selection->arguments as $argument) {
                            $argumentName = $argument->name->value;
                            $argumentValue = null;
                            if (property_exists($argument, 'value') &&
                                property_exists($argument->value, 'value')
                            ) {
                                $argumentValue = $argument->value->value;
                            } elseif (property_exists($argument, 'value') &&
                                property_exists($argument->value, 'name') &&
                                property_exists($argument->value->name, 'value')
                            ) {
                                $argumentValue = $argument->value->name->value . '';
                            }

                            if (null !== $argumentValue) {
                                $arguments[$argumentName] = $argumentValue;
                            }
                        }

                        $type = $selection->name->value;
                        $alias = $type;
                        if (property_exists($selection, 'alias') &&
                            $selection->alias !== null &&
                            property_exists($selection->alias, 'value')
                        ) {
                            $alias = $selection->alias->value;
                        }

                        $definitions[] = [
                            'type'      => $type,
                            'alias'     => $alias,
                            'arguments' => $arguments,
                        ];
                    }
                }
            }

        } catch (\Exception $e) {
            return false;
        }

        return $definitions;
    }

    public function sendResponse()
    {
        header('Content-Type: application/json; charset=UTF-8');
        header(
            'Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0'
        );
        header('Pragma: no-cache');
        printf("%s", $this->getResponseData());
    }

    /**
     * @return string
     */
    protected function getResponseData()
    {
        $data = '{}';
        if (is_array($this->_data)) {
            $this->_data['timer'] = $this->_timer->flush();
            $data = json_encode(['data' => $this->_data]);
        }

        return $data;
    }

    /**
     * @return array
     */
    protected function getBodyData()
    {
        $this->_timer->startTimer('parse_request');
        if (null === $this->_bodyData) {
            try {
                $this->_bodyData = json_decode(
                    file_get_contents('php://input'),
                    true
                );
            } catch (\Exception $e) {
                $this->_bodyData = [];
            }
        }

        if (!array_key_exists('store', $this->_bodyData)) {
            $this->_bodyData['store'] = $this->getStore();
        }

        $userHelper = UserHelper::create();
        $userHelper->setHash($this->getUserHash());

        if (!array_key_exists('user', $this->_bodyData)) {
            $this->_bodyData['user'] = $userHelper->getUserData();
        }

        $currencyHelper = CurrencyHelper::create($this->getStore());
        $currencyHelper->setCurrency($this->getCurrency());

        if (!array_key_exists('currency', $this->_bodyData)) {
            $this->_bodyData['currency'] = $currencyHelper->getCurrency();
        }
        if (!array_key_exists('currency_rate', $this->_bodyData)) {
            $this->_bodyData['currency_rate'] = $currencyHelper->getCurrencyRate();
        }

        $this->_timer->endTimer('parse_request');

        return $this->_bodyData;
    }

    /**
     * @return string
     */
    private function getStore()
    {
        if (null === $this->_store) {
            $headers = getallheaders();
            if (array_key_exists('Store', $headers)) {
                $this->_store = $headers['Store'];
            } else {
                $this->_store = self::DEFAULT_STORE;
            }
        }

        return $this->_store;
    }

    /**
     * @return string
     */
    private function getCurrency()
    {
        if (null === $this->_currency) {
            $headers = getallheaders();
            if (
                array_key_exists('Content-Currency', $headers) &&
                $headers['Content-Currency']
            ) {
                $this->_currency = $headers['Content-Currency'];
            } else {
                $this->_currency = self::DEFAULT_CURRENCY;
            }
        }

        return $this->_currency;
    }

    /**
     * @return string
     */
    private function getUserHash()
    {
        if (null === $this->_userHash) {
            $headers = getallheaders();
            if (array_key_exists('Authorization', $headers)) {
                $this->_userHash = str_replace("Bearer ", "", $headers['Authorization']);
            } else {
                $this->_userHash = '';
            }
        }

        return $this->_userHash;
    }

    /**
     * @param string $className
     *
     * @return bool|string
     */
    protected function getClassName($className)
    {
        $className = ucfirst($className);
        $className = '\Itegration\ElasticGraph\Helper\Operation\\' . $className;
        if (!class_exists($className)) {
            return false;
        }
        return $className;
    }

    /**
     * @return string
     */
    protected function getQuery()
    {
        if (null === $this->_query) {
            $this->_query = '';
            $body = $this->getBodyData();
            if (array_key_exists(self::QUERY_KEY, $body)) {
                $this->_query = $body[self::QUERY_KEY];
            }
        }

        return $this->_query;
    }
}
