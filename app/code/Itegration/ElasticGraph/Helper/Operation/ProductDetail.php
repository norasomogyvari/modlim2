<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Operation;

use Itegration\ElasticGraph\Helper\Request\Product as ProductRequest;
use Itegration\ElasticGraph\Helper\Utils\ColorHelper;

class ProductDetail extends AbstractOperation
{
    const PD_GQL_KEY       = 'product_detail_graphql';
    const PD_GQL_PRICE_KEY = 'product_detail_price_graphql';
    const OPERATION_NAME   = 'productDetail';

    /**
     * Category constructor.
     *
     * @param array $params
     * @param array $definition
     */
    public function __construct($params, $definition)
    {
        parent::__construct($params, $definition);

        $this->_productRequest = new ProductRequest(
            $this->_store,
            $this->_variables
        );
    }

    /**
     * @return array|bool
     */
    public function processBody()
    {
        $product = $this->_productRequest->getProduct(
            'url_key',
            $this->_variables['urlKey']
        );

        if (array_key_exists('product', $product)) {
            $product = $product['product'];
            if (count($product)) {
                $product = current($product);

                if (array_key_exists(self::PD_GQL_KEY, $product) &&
                    !empty($product[self::PD_GQL_KEY])
                ) {
                    $productData = json_decode(
                        $product[self::PD_GQL_KEY],
                        true
                    );
                    $this->handlePrices($productData);
                    $productData = ColorHelper::create()->handleColorOrders(
                        $productData
                    );
                    return [
                        'items'      => [$productData],
                        '__typename' => 'Products',
                    ];
                }
            }
        }

        return false;
    }
}
