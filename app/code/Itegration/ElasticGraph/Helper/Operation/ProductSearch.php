<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Operation;

use Itegration\ElasticGraph\Helper\Request\Categories as CategoriesRequest;
use Itegration\ElasticGraph\Helper\Request\Category as CategoryRequest;
use Itegration\ElasticGraph\Helper\Request\Cms as CmsRequest;
use Itegration\ElasticGraph\Helper\Request\Products as ProductsRequest;
use Itegration\ElasticGraph\Helper\Response\Categories as CategoriesResponse;
use Itegration\ElasticGraph\Helper\Response\Category as CategoryResponse;
use Itegration\ElasticGraph\Helper\Response\CmsPages as CmsPagesResponse;
use Itegration\ElasticGraph\Helper\Response\ProductsSearch as ProductsSearchResponse;
use Itegration\ElasticGraph\Helper\SortOrder;
use Magento\Catalog\Model\Category as CategoryModel;

class ProductSearch extends AbstractOperation
{
    /** @var CmsRequest */
    protected $_cmsRequest;

    /** @var CategoriesRequest */
    protected $_categoriesRequest;

    /**
     * ProductSearch constructor.
     *
     * @param array $params
     * @param array $definition
     */
    public function __construct($params, $definition)
    {
        parent::__construct($params, $definition);

        $this->_productRequest = new ProductsRequest(
            $this->_store,
            $this->_variables
        );
        $this->_categoryRequest = new CategoryRequest(
            $this->_store,
            $this->_variables
        );

        $this->_sortOrder = new SortOrder();

        /*$this->_cmsRequest = new CmsRequest(
            $this->_store,
            $this->_variables
        );
        $this->_categoriesRequest = new CategoriesRequest(
            $this->_store,
            $this->_variables
        );*/
    }

    /**
     * @return array
     */
    public function processBody()
    {
        $returnData = [];

        $category = $this->_categoryRequest->getCategory();

        $products = $this->_productRequest->getProducts(
            $this->_sortOrder->getSortOrder($category),
            false,
             false,
            true,
            0
        );
        $productsData = [];
        if ($products) {
            $productsData = new ProductsSearchResponse(
                $products,
                $this->_productRequest
            );
            $productsData = $productsData->getData();
            $this->handlePrices($productsData);
            $returnData['products'] = $productsData;
        }

        return $returnData;
    }
}
