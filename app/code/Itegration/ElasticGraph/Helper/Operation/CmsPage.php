<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Operation;

use Itegration\ElasticGraph\Helper\Request\Cms as CmsRequest;
use Itegration\ElasticGraph\Helper\Response\CmsPages\CmsPage as CmsPageResponse;

class CmsPage extends AbstractOperation
{
    protected $_cmsRequest;

    /**
     * ShippingPage constructor.
     *
     * @param array $params
     * @param array $definition
     */
    public function __construct($params, $definition)
    {
        parent::__construct($params, $definition);

        $this->_cmsRequest = new CmsRequest($this->_store, $this->_variables);
    }

    /**
     * @return array|bool
     */
    public function processBody()
    {
        $argument = $this->_definition['arguments'];
        $field = array_keys($argument);
        $field = current($field);

        $value = $argument[$field];
        if (array_key_exists($value, $this->_variables)) {
            $value = $this->_variables[$value];
        }

        $cmsPage = $this->_cmsRequest->getCmsPage($field, $value);

        if (empty($cmsPage)) {
            return false;
        }

        $data = new CmsPageResponse($cmsPage);

        return $data->getData();
    }
}
