<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Operation;

use Itegration\ElasticGraph\Helper\Request\Category as CategoryRequest;
use Itegration\ElasticGraph\Helper\Response\Category as CategoryResponse;
use Itegration\ElasticGraph\Helper\SortOrder;
use Magento\Catalog\Model\Category as CategoryModel;

class Category extends AbstractOperation
{
    /**
     * Category constructor.
     *
     * @param array $params
     * @param array $definition
     */
    public function __construct($params, $definition)
    {
        parent::__construct($params, $definition);

        $this->_categoryRequest = new CategoryRequest(
            $this->_store,
            $this->_variables
        );

        $this->_sortOrder = new SortOrder();
    }

    /**
     * @return array
     */
    public function processBody()
    {
        $category = $this->_categoryRequest->getCategory();
        $categoryData = [];
        if (is_array($category) && array_key_exists('id', $category)) {
            $categoryData = new CategoryResponse($category);
            $categoryData = $categoryData->getData();
        }

        return $categoryData;
    }
}
