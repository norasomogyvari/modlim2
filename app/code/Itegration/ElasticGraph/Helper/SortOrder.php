<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper;

class SortOrder
{
    /**
     * @param array $category
     *
     * @return array
     */
    public function getSortOrder($category)
    {

        $sortFields = [];
        $defaultSortBy = '';
        if (is_array($category) && array_key_exists('sort_fields', $category)) {
            $sortFields = json_decode($category['sort_fields'], true);
        } else {
            $sortFields = [
                'price' => 'Price',
            ];
        }
        if (is_array($category) && array_key_exists('default_sort_by', $category)) {
            $defaultSortBy = $category['default_sort_by'];
        } else {
            $defaultSortBy = 'price';
        }

        if (!is_array($category) || !array_key_exists('id', $category)) {
            $newSortFields = [
                'score' => 'Relevance',
            ];
            $sortFields = array_merge($newSortFields, $sortFields);
            $defaultSortBy = 'score';
        }

        if (array_key_exists('position', $sortFields)) {
            $sortFields['position'] = 'New and Popular';
        }
        if (array_key_exists('name', $sortFields)) {
            unset($sortFields['name']);
        }

        return [
            'sortFields'    => $sortFields,
            'defaultSortBy' => $defaultSortBy,
        ];
    }
}
