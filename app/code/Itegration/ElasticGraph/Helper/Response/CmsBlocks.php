<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response;

use Itegration\ElasticGraph\Helper\Response\CmsBlocks\CmsBlock;

class CmsBlocks extends AbstractResponse
{
    /** @var array */
    protected $_fields = [
        'items'      => 'items',
        'totalCount' => 'total_count',
    ];

    /** @var string */
    protected $_typeKey = 'CmsBlocks';

    /**
     * @return int
     */
    protected function getTotalCount()
    {
        $returnData = 0;

        if (array_key_exists('count', $this->_data)) {
            $returnData = $this->_data['count'];
        }

        return $returnData;
    }

    /**
     * @return array
     */
    protected function getItems()
    {
        $returnArray = [];

        if (array_key_exists('docs', $this->_data) &&
            is_array($this->_data['docs'])
        ) {
            foreach ($this->_data['docs'] as $doc) {
                $cmsData = new CmsBlock($doc);
                $returnArray[] = $cmsData->getData();
            }
        }

        return $returnArray;
    }
}
