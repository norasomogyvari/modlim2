<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\CmsBlocks;

use Itegration\ElasticGraph\Helper\Response\AbstractResponse;

class CmsBlock extends AbstractResponse
{
    /** @var array */
    protected $_fields = [
        'id'              => 'id',
        'content'         => 'content',
        'identifier'      => 'identifier',
        'title'           => 'title',
        'title_suggester' => 'title_suggester',
    ];

    /**
     * @return string
     */
    protected function _getTypeKey()
    {
        return 'CmsBlock';
    }

    /**
     * @return string
     */
    protected function getContent()
    {
        if (array_key_exists('graphql_content', $this->_data)) {
            return $this->_data['graphql_content'];
        } else {
            if (array_key_exists('content', $this->_data)) {
                return $this->_data['content'];
            }
        }

        return '';
    }
}
