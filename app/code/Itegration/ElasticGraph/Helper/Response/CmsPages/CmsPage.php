<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\CmsPages;

use Itegration\ElasticGraph\Helper\Response\AbstractResponse;

class CmsPage extends AbstractResponse
{
    /** @var array */
    protected $_fields = [
        'id'               => 'id',
        'content'          => 'content',
        'identifier'       => 'identifier',
        'urlKey'           => 'url_key',
        'title'            => 'title',
        'title_suggester'  => 'title_suggester',
        'content_heading'  => 'content_heading',
        'page_layout'      => 'page_layout',
        'meta_title'       => 'meta_title',
        'meta_description' => 'meta_description',
        'meta_keywords'    => 'meta_keywords',
    ];

    /**
     * @return string
     */
    protected function _getTypeKey()
    {
        return 'CmsPage';
    }

    /**
     * @return string
     */
    protected function getContent()
    {
        if (array_key_exists('graphql_content', $this->_data)) {
            return $this->_data['graphql_content'];
        } else {
            if (array_key_exists('content', $this->_data)) {
                return $this->_data['content'];
            }
        }

        return '';
    }

    /**
     * @return string
     */
    protected function getUrlKey()
    {
        if (array_key_exists('identifier', $this->_data)) {
            return $this->_data['identifier'];
        }

        return '';
    }
}
