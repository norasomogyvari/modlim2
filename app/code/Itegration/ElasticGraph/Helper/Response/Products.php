<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response;

use Itegration\ElasticGraph\Helper\Response\Products\BundleProduct;
use Itegration\ElasticGraph\Helper\Response\Products\ConfigurableProduct;
use Itegration\ElasticGraph\Helper\Response\Products\LayerFilter;
use Itegration\ElasticGraph\Helper\Response\Products\PageInfo;
use Itegration\ElasticGraph\Helper\Response\Products\Product;
use Itegration\ElasticGraph\Helper\Response\Products\SortFields;

class Products extends AbstractResponse
{
    /** @var array */
    protected $_fields = [
        'sortFields'  => 'sort_fields',
        'filters'     => 'filters',
        'items'       => 'items',
        'pageInfo'    => 'page_info',
        'total_count' => 'total_count',
        'currency'    => 'currency',
    ];

    /** @var string */
    protected $_typeKey = 'Products';

    /**
     * @return array
     */
    protected function getSortFields()
    {
        $sortFieldData = [];
        if (
            array_key_exists('sort_fields', $this->_data) &&
            array_key_exists('default_sort_by', $this->_data)
        ) {
            $sortFieldData = [
                'default' => $this->_data['default_sort_by'],
                'options' => $this->_data['sort_fields'],
            ];
        }

        $sortFields = new SortFields($sortFieldData);

        return $sortFields->getData();
    }

    /**
     * @return array
     */
    protected function getFilters()
    {
        $filters = [];

        $this->_handleLayeredNavigationItems();

        if (array_key_exists('filters', $this->_data)) {
            foreach ($this->_data['filters'] as $filter) {
                $filterType = $filter['attributeCode'];
                unset($filter['attributeCode']);
                if (in_array($filterType, LayerFilter::getSkippedFilters())) {
                    continue;
                }

                if (substr($filterType, -4, 4) === '_ids') {
                    $filterType = substr($filterType, 0, -4);
                }

                $filter['type'] = $filterType;
                $filter['label'] = LayerFilter::getLabelByType($filter);
                $filterData = new LayerFilter($filter);
                $filterData = $filterData->getData();

                if (array_key_exists('filter_items', $filterData) &&
                    count($filterData['filter_items']) > 0
                ) {
                    $filters[] = $filterData;
                }
            }
        }

        return $filters;
    }

    /**
     * @return $this
     */
    private function _handleLayeredNavigationItems()
    {
        //if (!array_key_exists('layer_navigation_options', $this->_data)) {
        $returnData = [];
        //}

        foreach ($this->_data['layer_navigation_options'] as $layerNavigationOption) {
            $attributeCode = $layerNavigationOption[0];
            if (array_key_exists('aggregations', $this->_data)) {
                if (array_key_exists(
                    $attributeCode,
                    $this->_data['aggregations']
                )) {
                    $tmpData = $this->_data['aggregations'][$attributeCode];
                    $tmpData['attributeCode'] = $attributeCode;
                    $returnData[] = $tmpData;
                }
                if (array_key_exists(
                    $attributeCode . '_ids',
                    $this->_data['aggregations']
                )) {
                    $tmpData = $this->_data['aggregations'][$attributeCode . '_ids'];
                    $tmpData['attributeCode'] = $attributeCode . '_ids';
                    $returnData[] = $tmpData;
                }
            }
        }

        $this->_data['filters'] = $returnData;

        return $this;
    }

    /**
     * @return array
     */
    protected function getPageInfo()
    {
        $pageInfoData = [];

        if (array_key_exists('total_count', $this->_data) &&
            array_key_exists('page_size', $this->_data)) {
            $pageInfoData = [
                'total_pages' => ceil(
                    $this->_data['total_count'] / $this->_data['page_size']
                ),
            ];
        }

        $pageInfo = new PageInfo($pageInfoData);

        return $pageInfo->getData();
    }

    /**
     * @return array
     */
    protected function getItems()
    {
        $itemsData = [];

        if (array_key_exists('products', $this->_data)) {
            foreach ($this->_data['products'] as $product) {
//                if (array_key_exists(ProductDetail::PD_GQL_KEY, $product)) {
//                    $itemsData[] = json_decode($product[ProductDetail::PD_GQL_KEY], true);
//                } else {
                if (array_key_exists('currency', $this->_data)) {
                    $product['currency'] = $this->_data['currency'];
                }
                if (array_key_exists('product_url_suffix', $this->_data)) {
                    $product['product_url_suffix'] = $this->_data['product_url_suffix'];
                }
                if (array_key_exists('store_id', $this->_data)) {
                    $product['store_id'] = $this->_data['store_id'];
                }
                if (array_key_exists('sorter', $this->_data)) {
                    $product['sorter'] = $this->_data['sorter'];
                }
                if (array_key_exists('userData', $this->_data)) {
                    $product['userData'] = $this->_data['userData'];
                }

                switch ($product['type_id']) {
                    case 'bundle':
                        $productResponse = new BundleProduct(
                            $product,
                            $this->_requester
                        );
                        break;
                    case 'configurable':
                        $productResponse = new ConfigurableProduct(
                            $product,
                            $this->_requester
                        );
                        break;
                    case 'simple':
                    default:
                        $productResponse = new Product(
                            $product,
                            $this->_requester
                        );
                }

                $itemsData[] = $productResponse->getData();
                //}
            }
        }

        return $itemsData;
    }
}
