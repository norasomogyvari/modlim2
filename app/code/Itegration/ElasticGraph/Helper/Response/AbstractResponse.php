<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response;

use Itegration\ElasticGraph\Api\Data\ResponseItemInterface;
use Itegration\ElasticGraph\Helper\Request\Products as ProductsRequest;

abstract class AbstractResponse implements ResponseItemInterface
{
    /** @var array */
    protected $_data;

    /** @var string */
    protected $_typeKey = self::DEFAULT_TYPE;

    /** @var array */
    protected $_fields = [];

    /** @var ProductsRequest|null */
    protected $_requester = null;

    /**
     * AbstractResponse constructor.
     *
     * @param array $data
     * @param miced $requester
     */
    public function __construct($data, $requester = null)
    {
        $this->_data = $data;
        $this->_requester = $requester;
    }

    /**
     * @return array
     */
    public function getData()
    {
        $returnData = [];

        foreach ($this->getFields() as $origFieldName => $newFieldName) {
            $methodName = 'get' . ucfirst($origFieldName);
            if (method_exists($this, $methodName)) {
                $returnData[$newFieldName] = call_user_func_array(
                    [$this, $methodName],
                    [$this->_data]
                );
            } elseif (is_array($this->_data) &&
                array_key_exists($origFieldName, $this->_data)) {
                $returnData[$newFieldName] = $this->_data[$origFieldName];
            }
        }

        $returnData[self::TYPE_KEY] = $this->_getTypeKey();

        return $returnData;
    }


    /**
     * @return string
     */
    protected function _getTypeKey()
    {
        return $this->_typeKey;
    }

    /**
     * @return array
     */
    protected function getFields()
    {
        return $this->_fields;
    }
}
