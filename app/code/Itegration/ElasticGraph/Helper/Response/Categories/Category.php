<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\Categories;

use Itegration\ElasticGraph\Helper\Response\AbstractResponse;

class Category extends AbstractResponse
{
    /** @var array */
    protected $_fields = [
        'id'         => 'id',
        'url_key'    => 'url_key',
        'name'       => 'name',
        'meta_title' => 'meta_title',
    ];

    /**
     * @return string
     */
    protected function _getTypeKey()
    {
        return 'Category';
    }
}
