<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response;

class ProductsSearch extends Products
{
    protected $_extraFields = [
        'search_term' => 'search_term',
    ];

    /** @var string */
    protected $_typeKey = 'ProductsSearch';

    /**
     * @return array
     */
    protected function getFields()
    {
        return array_merge(parent::getFields(), $this->_extraFields);
    }
}
