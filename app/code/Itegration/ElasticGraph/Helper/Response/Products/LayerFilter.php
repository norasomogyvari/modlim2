<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\Products;

use Itegration\ElasticGraph\Helper\Response\AbstractResponse;
use Itegration\ElasticGraph\Helper\Response\Products\LayerFilter\Item;
use Itegration\ElasticGraph\Helper\Response\Products\LayerFilter\Price;
use Itegration\ElasticGraph\Helper\Response\Products\LayerFilter\Rating;
use Itegration\ElasticGraph\Helper\Response\Products\LayerFilter\SwatchText;
use Itegration\ElasticGraph\Helper\Response\Products\LayerFilter\SwatchVisual;

class LayerFilter extends AbstractResponse
{
    protected $fixKeys = [
        'label',
        'type',
    ];

    /** @var array */
    protected $_fields = [
        'label'            => 'name',
        'filterItemsCount' => 'filter_items_count',
        'requestVar'       => 'request_var',
        'filterItems'      => 'filter_items',
    ];

    /** @var string */
    protected $_typeKey = 'LayerFilter';

    /**
     * @return array
     */
    public static function getSkippedFilters()
    {
        return [
            'categories_ordered',
            'quantity_and_stock_status_ids',
        ];
    }

    /**
     * @param array $filter
     *
     * @return string
     */
    public static function getLabelByType($filter)
    {
        if (array_key_exists('label', $filter)) {
            return $filter['label'];
        }

        switch ($filter['type']) {
            case 'final_price':
                return 'Price';
                break;
            default:
            case 'rating':
            case 'categories':
                return ucfirst($filter['type']);
        }
    }

    /**
     * @return int
     */
    protected function getFilterItemsCount()
    {
        $count = 0;
        $values = $this->_data;
        if (array_key_exists('values', $this->_data)) {
            $values = $this->_data['values'];
        }

        foreach ($values as $dataKey => $value) {
            if (!in_array($dataKey, $this->fixKeys)) {
                $count++;
            }
        }

        return $count;
    }

    /**
     * @return string
     */
    protected function getRequestVar()
    {
        return $this->_data['type'];
    }

    /**
     * @return array
     */
    protected function getFilterItems()
    {
        $filterItems = [];

        $values = $this->_data;
        if (array_key_exists('values', $this->_data)) {
            $values = $this->_data['values'];
        }

        if ($this->_data['type'] === 'final_price') {
            $minPrice = new Price(
                [
                    'label'        => 'Min',
                    'value_string' => '1',
                    'price_value'  => $this->_data['min'],
                    'price_type'   => 'min',
                ]
            );

            $filterItems[] = $minPrice->getData();

            $maxPrice = new Price(
                [
                    'label'        => 'Max',
                    'value_string' => '2',
                    'price_value'  => $this->_data['max'],
                    'price_type'   => 'max',
                ]
            );

            $filterItems[] = $maxPrice->getData();
        } else {
            if ($this->_data['type'] === 'rating') {
                foreach ($values as $dataKey => $value) {
                    if (!is_array($value) ||
                        !array_key_exists('rating', $value)
                    ) {
                        continue;
                    }

                    $value['id'] = $value['rating'];
                    $value['label'] = 'No Rating';
                    if ($value['rating'] > -1) {
                        $value['label'] = (string)($value['rating'] / 20);
                    }
                    $layerItem = new Rating($value);

                    $filters = $layerItem->getData();
                    if ($filters['items_count'] > 0) {
                        $filterItems[] = $layerItem->getData();
                    }
                }
            } else {
                foreach ($values as $dataKey => $value) {
                    if (in_array($dataKey, $this->fixKeys)) {
                        continue;
                    }

                    if (array_key_exists('visualSwatch', $this->_data) &&
                        $this->_data['visualSwatch'] === true
                    ) {
                        $layerItem = new SwatchVisual($value);
                    } elseif (array_key_exists('textSwatch', $this->_data) &&
                        $this->_data['textSwatch'] === true
                    ) {
                        $layerItem = new SwatchText($value);
                    } else {
                        $layerItem = new Item($value);
                    }

                    $filters = $layerItem->getData();
                    if ($filters['items_count'] > 0) {
                        $filterItems[] = $layerItem->getData();
                    }
                }
            }
        }

        return $filterItems;
    }
}
