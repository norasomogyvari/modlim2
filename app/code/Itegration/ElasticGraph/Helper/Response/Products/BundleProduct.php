<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\Products;

use Itegration\ElasticGraph\Helper\Operation\ProductDetail;
use Itegration\ElasticGraph\Helper\Response\Products\Product\Prices;

class BundleProduct extends Product
{
    /** @var array */
    protected $_extraFields = [
        'configurableItems' => 'configurable_items',
        'items'             => 'items',
    ];

    /**
     * @return string
     */
    protected function _getTypeKey()
    {
        return 'BundleProduct';
    }

    /**
     * @return array
     */
    protected function getFields()
    {
        return array_merge($this->_fields, $this->_extraFields);
    }

    /**
     * @return mixed
     */
    protected function getConfigurableItems()
    {
        return [];
        if (array_key_exists('configurable_items', $this->_graphQlData)) {
            return $this->_graphQlData['configurable_items'];
        }
    }

    /**
     * @return mixed
     */
    protected function getItems()
    {
        return [];
        if (array_key_exists('items', $this->_graphQlData)) {
            return $this->_graphQlData['items'];
        }
    }

    protected function getPrice()
    {
        if (is_array($this->_graphQlData) &&
            array_key_exists('configurable_items', $this->_graphQlData)
        ) {
            $currency = '';
            if (array_key_exists('currency', $this->_data)) {
                $currency = $this->_data['currency'];
            }

            $minimalPrice = 0;
            $maximalPrice = 0;
            $tierPrice = -1;

            foreach ($this->_graphQlData['configurable_items'] as $configurableItem) {
                if (array_key_exists('variants', $configurableItem)) {
                    $configMinPrice = null;
                    $configMaxPrice = null;
                    foreach ($configurableItem['variants'] as $variant) {
                        if (array_key_exists(
                                'stock_status',
                                $variant['product']
                            ) &&
                            $variant['product']['stock_status'] == 'OUT_OF_STOCK') {
                            continue;
                        }
                        $price = $this->_getProductPrice($variant['product']);

                        if (null === $configMinPrice || $configMinPrice > $price) {
                            $configMinPrice = $price;
                        }
                        if (null === $configMaxPrice || $configMaxPrice < $price) {
                            $configMaxPrice = $price;
                        }
                    }
                    $minimalPrice += $configMinPrice;
                    $maximalPrice += $configMaxPrice;
                } else {
                    $price = $this->_getProductPrice($configurableItem);
                    $minimalPrice += $price;
                    $maximalPrice += $price;
                }
            }
            $finalPrice = $minimalPrice;

            $priceData = new Prices(
                [
                    'currency' => $currency,
                    'regularPrice' => $finalPrice,
                    'minimalPrice' => $minimalPrice,
                    'maximalPrice' => $maximalPrice,
                    'finalPrice' => $finalPrice,
                    'tierPrice' => $tierPrice,
                ]
            );

            return $priceData->getData();
        } else {
            return parent::getPrice();
        }
    }
}
