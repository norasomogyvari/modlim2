<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\Products;

use Itegration\ElasticGraph\Helper\Response\Products\Product\ConfigurableOption;
use Itegration\ElasticGraph\Helper\Response\Products\Product\Prices;
use Itegration\ElasticGraph\Helper\Response\Products\Product\Variant;

class ConfigurableProduct extends Product
{
    /**
     * @var null|array
     */
    private $_indexes = null;

    /**
     * @var null|array
     */
    private $_images = null;

    /** @var array */
    protected $_extraFields = [
        'configurableOptions' => 'configurable_options',
        'variants'            => 'variants',
    ];

    /**
     * @return string
     */
    protected function _getTypeKey()
    {
        return 'ConfigurableProduct';
    }

    /**
     * @return array
     */
    protected function getFields()
    {
        return array_merge($this->_fields, $this->_extraFields);
    }

    protected function getPrice()
    {
        if (is_array($this->_graphQlData) &&
            array_key_exists('variants', $this->_graphQlData)
        ) {
            $minimalPrice = null;
            $maximalPrice = null;
            $tierPrice = -1;

            $currency = '';
            if (array_key_exists('currency', $this->_data)) {
                $currency = $this->_data['currency'];
            }

            foreach ($this->_graphQlData['variants'] as $variant) {
                if (array_key_exists(
                        'stock_status',
                        $variant['product']
                    ) && $variant['product']['stock_status'] == 'OUT_OF_STOCK') {
                    continue;
                }

                $price = $this->_getProductPrice($variant['product']);

                if (null === $minimalPrice || $minimalPrice > $price) {
                    $minimalPrice = $price;
                }
                if (null === $maximalPrice || $maximalPrice < $price) {
                    $maximalPrice = $price;
                }
            }

            $finalPrice = $minimalPrice;

            $priceData = new Prices(
                [
                    'currency'     => $currency,
                    'regularPrice' => $finalPrice,
                    'minimalPrice' => $minimalPrice,
                    'maximalPrice' => $maximalPrice,
                    'finalPrice'   => $finalPrice,
                    'tierPrice'    => $tierPrice,
                ]
            );

            return $priceData->getData();
        } else {
            return parent::getPrice();
        }
    }

    /**
     * @return array
     */
    protected
    function getConfigurableOptions()
    {
        $returnData = [];
        if (array_key_exists('configurable_options', $this->_data)) {
            krsort($this->_data['configurable_options']);
            foreach ($this->_data['configurable_options'] as $attributeCode => $configurableOption) {
                $configurableOption['attribute_code'] = $attributeCode;
                $configurableOption['product_id'] = $this->_data['id'];
                $configurableOption['configurable_color_order'] = [];
                if (array_key_exists(
                    'configurable_color_order',
                    $this->_data
                )) {
                    $configurableOption['configurable_color_order'] = json_decode(
                        $this->_data['configurable_color_order'],
                        true
                    );
                }
                if (array_key_exists(
                    'configurable_color_order',
                    $this->_graphQlData
                )) {
                    $configurableOption['configurable_color_order'] = $this->_graphQlData['configurable_color_order'];
                }

                foreach ($configurableOption['values'] as &$configurableOptionValue) {
                    $configurableOptionValue['product_image'] = $this->getProductImage(
                        $configurableOption['id'],
                        $configurableOptionValue['id']
                    );
                }

                $optionData = new ConfigurableOption($configurableOption);
                $returnData[] = $optionData->getData();
            }
        }

        return $returnData;
    }

    /**
     * @param int $attributeId
     * @param int $attributeValueId
     *
     * @return string
     */
    private
    function getProductImage(
        $attributeId,
        $attributeValueId
    ) {
        $indexes = $this->getIndexes();

        $foundedImageId = false;
        foreach ($indexes as $imageId => $imageAttributes) {
            if (array_key_exists(
                    $attributeId,
                    $imageAttributes
                ) && $imageAttributes[$attributeId] == $attributeValueId) {
                $foundedImageId = $imageId;
                break;
            }
        }

        if ($foundedImageId) {
            $images = $this->getImages();
            if (array_key_exists($foundedImageId, $images) &&
                array_key_exists('img', $images[$foundedImageId])
            ) {
                return str_replace(
                    '/media/catalog/product',
                    '',
                    $images[$foundedImageId]['img']
                );
            }
        }

        return '';
    }

    /**
     * @return array
     */
    private
    function getIndexes()
    {
        if (null === $this->_indexes) {
            try {
                $this->_indexes = json_decode(
                    $this->_data['configurable_index'],
                    true
                );
            } catch (\Exception $e) {
                $this->_indexes = [];
            }
        }

        return $this->_indexes;
    }

    private
    function getImages()
    {
        if (null === $this->_images) {
            try {
                $this->_images = json_decode(
                    $this->_data['configurable_images'],
                    true
                );
            } catch (\Exception $e) {
                $this->_images = [];
            }
        }

        return $this->_images;
    }

    /**
     * @return array
     */
    protected
    function getVariants()
    {
        $returnData = [];
        return [];

        if (array_key_exists(
                'is_variant',
                $this->_data
            ) && $this->_data['is_variant'] === true) {
            return [];
        }

        if (array_key_exists('store_id', $this->_data)) {
            if (array_key_exists('configurable_index', $this->_data) &&
                array_key_exists('configurable_images', $this->_data)
            ) {
                $indexes = json_decode(
                    $this->_data['configurable_index'],
                    true
                );
                $images = json_decode(
                    $this->_data['configurable_images'],
                    true
                );

                $variants = $this->_requester->getRequester()
                    ->getElasticProducts(
                        $this->_requester->getStore(),
                        0,
                        'id',
                        array_keys($indexes),
                        false
                    );

                if (count($variants['products'])) {
                    foreach ($variants['products'] as $variantProduct) {
                        $id = $variantProduct['id'];
                        if ($variantProduct['quantity_and_stock_status_ids'] == 1 &&
                            array_key_exists($id, $images) &&
                            array_key_exists('img', $images[$id])
                        ) {
                            if (array_key_exists(
                                'product_url_suffix',
                                $this->_data
                            )) {
                                $variantProduct['product_url_suffix'] = $this->_data['product_url_suffix'];
                            }

                            $variant = new Variant(
                                [
                                    'product'    => $variantProduct,
                                    'image'      => $images[$id]['img'],
                                    'attributes' => $indexes[$id],
                                ]
                            );
                            $returnData[] = $variant->getData();
                        }
                    }
                }

                /*foreach ($indexes as $entityId => $index) {
                    if (array_key_exists($entityId, $images) &&
                        array_key_exists('img', $images[$entityId])
                    ) {
                        $variant = new Variant(
                            [
                                'id'         => $entityId,
                                'product_id' => $this->_data['id'],
                                'image'      => $images[$entityId]['img'],
                                'attributes' => $index,
                            ]
                        );
                        $returnData[] = $variant->getData();
                    }
                }*/
            }
        }

        return $returnData;
    }
}
