<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\Products\LayerFilter;

use Itegration\ElasticGraph\Helper\Response\AbstractResponse;
use Itegration\ElasticGraph\Helper\Response\Products\LayerFilter\Swatch\Data;

class SwatchVisual extends AbstractResponse
{
    /** @var array */
    protected $_fields = [
        'label'      => 'label',
        'id'         => 'value_string',
        'count'      => 'items_count',
        'swatchData' => 'swatch_data',
    ];

    /** @var string */
    protected $_typeKey = 'SwatchLayerFilterItem';

    /**
     * @return string
     */
    protected function getId()
    {
        return (string)$this->_data['id'];
    }

    /**
     * @return string
     */
    protected function getLabel()
    {
        if (is_array($this->_data) &&
            array_key_exists('label', $this->_data)) {
            return $this->_data['label'];
        }

        return '';
    }

    /**
     * @return array
     */
    protected function getSwatchData()
    {
        $swatchItemData = [];

        if (array_key_exists('data', $this->_data)) {
            $type = 2;
            if (substr($this->_data['data'], 0, 1) === '#') {
                $type = 1;
            }

            $swatchData = new Data([
                'type'  => $type,
                'value' => $this->_data['data'],
            ]);

            $swatchItemData = $swatchData->getData();
        }

        return $swatchItemData;
    }
}
