<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\Products\LayerFilter;

use Itegration\ElasticGraph\Helper\Response\AbstractResponse;

class Price extends AbstractResponse
{
    /** @var array */
    protected $_fields = [
        'label'        => 'label',
        'value_string' => 'value_string',
        'count'        => 'items_count',
        'price_value'  => 'price_value',
        'price_type'   => 'price_type',
    ];

    /** @var string */
    protected $_typeKey = 'PriceLayerFilterItem';
}
