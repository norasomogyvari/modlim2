<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\Products\LayerFilter;

use Itegration\ElasticGraph\Helper\Response\AbstractResponse;

class Item extends AbstractResponse
{
    /** @var array */
    protected $_fields = [
        'label' => 'label',
        'id'    => 'value_string',
        'count' => 'items_count',
    ];

    /** @var string */
    protected $_typeKey = 'LayerFilterItem';

    /**
     * @return string
     */
    protected function getLabel()
    {
        if (is_array($this->_data) &&
            array_key_exists('label', $this->_data)) {
            return $this->_data['label'];
        }

        return '';
    }

    /**
     * @return string
     */
    protected function getId()
    {
        return (string)$this->_data['id'];
    }
}
