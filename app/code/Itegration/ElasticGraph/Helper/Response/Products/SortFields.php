<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\Products;

use Itegration\ElasticGraph\Helper\Response\AbstractResponse;
use Itegration\ElasticGraph\Helper\Response\Products\SortFields\Option;

class SortFields extends AbstractResponse
{
    /** @var array */
    protected $_fields = [
        'default' => 'default',
        'options' => 'options',
    ];

    /** @var string */
    protected $_typeKey = 'SortFields';

    /**
     * @return array
     */
    protected function getOptions()
    {
        $optionsData = [];

        if (array_key_exists('options', $this->_data)) {
            foreach ($this->_data['options'] as $optionKey => $optionName) {
                $option = new Option([
                    'label' => (string)$optionName,
                    'value' => $optionKey,
                ]);
                $optionsData[] = $option->getData();
            }
        }

        return $optionsData;
    }
}
