<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\Products;

use Itegration\ElasticGraph\Helper\Response\AbstractResponse;

class PageInfo extends AbstractResponse
{
    /** @var array */
    protected $_fields = [
        'total_pages' => 'total_pages',
    ];

    /** @var string */
    protected $_typeKey = 'SearchResultPageInfo';
}
