<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\Products\SortFields;

use Itegration\ElasticGraph\Helper\Response\AbstractResponse;

class Option extends AbstractResponse
{
    /** @var array */
    protected $_fields = [
        'label' => 'label',
        'value' => 'value',
    ];

    /** @var string */
    protected $_typeKey = 'SortField';
}
