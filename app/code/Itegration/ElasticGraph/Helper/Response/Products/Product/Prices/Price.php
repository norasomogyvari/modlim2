<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\Products\Product\Prices;

use Itegration\ElasticGraph\Helper\Response\AbstractResponse;
use Itegration\ElasticGraph\Helper\Response\Products\Product\Prices\Price\Amount;

class Price extends AbstractResponse
{
    /** @var array */
    protected $_fields = [
        'amount' => 'amount',
    ];

    /** @var string */
    protected $_typeKey = 'Price';

    /**
     * @return array
     */
    protected function getAmount()
    {
        $amount = new Amount($this->_data);
        return $amount->getData();
    }
}
