<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\Products\Product;

use Itegration\ElasticGraph\Helper\Response\AbstractResponse;

class AverageRating extends AbstractResponse
{
    /** @var array */
    protected $_fields = [
        'rating_code' => 'rating_code',
        'value'       => 'value',
    ];

    /** @var string */
    protected $_typeKey = 'RatingAverageItem';
}
