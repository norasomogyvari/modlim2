<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\Products\Product;

use Itegration\ElasticGraph\Helper\Response\AbstractResponse;
use Itegration\ElasticGraph\Helper\Response\Products\Product\ConfigurableOption\Value;
use Itegration\ElasticGraph\Helper\Response\Products\Product\ConfigurableOption\SwatchVisualValue;
use Itegration\ElasticGraph\Helper\Response\Products\Product\ConfigurableOption\SwatchTextValue;
use Itegration\ElasticGraph\Helper\Utils\SizeHelper;
use Itegration\ElasticGraph\Helper\Utils\ColorHelper;

class ConfigurableOption extends AbstractResponse
{
    /** @var array */
    protected $_fields = [
        'attribute_code' => 'attribute_code',
        'attributeId'    => 'attribute_id',
        'id'             => 'id',
        'label'          => 'label',
        'values'         => 'values',
    ];

    /** @var string */
    protected $_typeKey = 'ConfigurableProductOptions';

    /**
     * @return int
     */
    protected function getAttributeId()
    {
        return $this->_data['id'];
    }

    /**
     * @return int
     */
    protected function getId()
    {
        return (int)($this->_data['product_id'] * 10000 + $this->_data['id']);
    }

    /**
     * @return array
     */
    protected function getValues()
    {
        $valuesData = [];

        if (array_key_exists('values', $this->_data)) {
            foreach ($this->_data['values'] as $index => $value) {
                $value['swatchType'] = false;
                if (array_key_exists('visualSwatch', $this->_data) &&
                    $this->_data['visualSwatch'] === true
                ) {
                    $valueData = new SwatchVisualValue($value);
                } elseif (array_key_exists('textSwatch', $this->_data) &&
                    $this->_data['textSwatch'] === true
                ) {
                    $valueData = new SwatchTextValue($value);
                } else {
                    $valueData = new Value($value);
                }
                $valuesData[] = $valueData->getData();
            }
        }

        switch ($this->_data['label']) {
            case 'Size':
                $valuesData = SizeHelper::create()->sortSizes($valuesData);
                break;
            case 'Color':
                $valuesData = ColorHelper::create()->sortColors(
                    $valuesData,
                    $this->_data['configurable_color_order']
                );
                break;
        }

        return $valuesData;
    }
}
