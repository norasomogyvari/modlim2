<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\Products\Product\Prices\Price;

use Itegration\ElasticGraph\Helper\Response\AbstractResponse;

class Amount extends AbstractResponse
{
    /** @var array */
    protected $_fields = [
        'currency' => 'currency',
        'value'    => 'value',
    ];

    /** @var string */
    protected $_typeKey = 'Money';

    /**
     * @return string
     */
    protected function getValue()
    {
        $returnValue = $this->_data['value'];
        if (!$returnValue) {
            $returnValue = -1;
        }

        return $returnValue;
    }
}
