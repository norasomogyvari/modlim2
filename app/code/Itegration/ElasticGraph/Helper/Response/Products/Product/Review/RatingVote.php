<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\Products\Product\Review;

use Itegration\ElasticGraph\Helper\Response\AbstractResponse;

class RatingVote extends AbstractResponse
{
    /** @var array */
    protected $_fields = [
        'customer_id'     => 'customer_id',
        'entity_pk_value' => 'entity_pk_value',
        'option_id'       => 'option_id',
        'percent'         => 'percent',
        'rating_code'     => 'rating_code',
        'rating_id'       => 'rating_id',
        'remote_ip'       => 'remote_ip',
        'remote_ip_long'  => 'remote_ip_long',
        'review_id'       => 'review_id',
        'store_id'        => 'store_id',
        'value'           => 'value',
        'vote_id'         => 'vote_id',
    ];

    /** @var string */
    protected $_typeKey = 'RatingVoteItem';
}
