<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\Products\Product;

use Itegration\ElasticGraph\Helper\Response\AbstractResponse;
use Itegration\ElasticGraph\Helper\Response\Products\Product\Prices\Price;

class Prices extends AbstractResponse
{
    /** @var array */
    protected $_fields = [
        'regularPrice' => 'regularPrice',
        'minimalPrice' => 'minimalPrice',
        'maximalPrice' => 'maximalPrice',
        'finalPrice'   => 'finalPrice',
        'tierPrice'    => 'tierPrice',
        'basePrice'    => 'basePrice',
        'minBasePrice' => 'minBasePrice',
        'maxBasePrice' => 'maxBasePrice',
    ];

    /** @var string */
    protected $_typeKey = 'ProductPrices';

    /**
     * @return array
     */
    protected function getRegularPrice()
    {
        return $this->_getPrice('regularPrice');
    }

    /**
     * @return array
     */
    protected function getMinimalPrice()
    {
        return $this->_getPrice('minimalPrice');
    }

    /**
     * @return array
     */
    protected function getMaximalPrice()
    {
        return $this->_getPrice('maximalPrice');
    }

    /**
     * @return array
     */
    protected function getFinalPrice()
    {
        return $this->_getPrice('finalPrice');
    }

    /**
     * @return array
     */
    protected function getTierPrice()
    {
        return $this->_getPrice('tierPrice');
    }

    /**
     * @param string $priceKey
     *
     * @return array
     */
    protected function _getPrice($priceKey)
    {
        $returnData = [];
        if (array_key_exists($priceKey, $this->_data)) {
            $regularPrice = new Price([
                'currency' => $this->_data['currency'],
                'value'    => $this->_data[$priceKey],
            ]);
            $returnData = $regularPrice->getData();
        }

        return $returnData;
    }
}
