<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\Products\Product;

use Itegration\ElasticGraph\Helper\Response\AbstractResponse;
use Itegration\ElasticGraph\Helper\Response\Products\Product\Variant\Attributes;
use Itegration\ElasticGraph\Helper\Response\Products\Product;

class Variant extends AbstractResponse
{
    /** @var array */
    protected $_fields = [
        'product'    => 'product',
        'attributes' => 'attributes',
    ];

    protected function getAttributes()
    {
        $returnArray = [];

        if (array_key_exists('attributes', $this->_data) &&
            is_array($this->_data['attributes'])
        ) {
            foreach ($this->_data['attributes'] as $attributeId => $attributeValue) {
                $attributeData = new Attributes(
                    [
                        'attribute_id' => (string)$attributeId,
                        'code'         => (string)$attributeId,
                        'label'        => '',
                        'value_index'  => (int)$attributeValue,
                    ]
                );

                $returnArray[] = $attributeData->getData();
            }
        }

        return $returnArray;
    }

    /**
     * @return int
     */
    protected function getUniqueId()
    {
        $id = $this->_data['product_id'];
        foreach ($this->_data['attributes'] as $attributeId => $attributeValue) {
            $id *= ($attributeId * $attributeValue);
        }
        return $id;
    }

    protected function getProduct()
    {
        $productData = [];

        if (array_key_exists('image', $this->_data)) {
            $product = $this->_data['product'];
            $product['base_image'] = $this->_data['image'];
            $product['is_variant'] = true;
            $product = new Product($product);
            $productData = $product->getData();
        }

        return $productData;
    }

    /** @var string */
    protected $_typeKey = 'ConfigurableVariant';
}
