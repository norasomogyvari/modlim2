<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\Products\Product\Variant;

use Itegration\ElasticGraph\Helper\Response\AbstractResponse;

class Attributes extends AbstractResponse
{
    /** @var array */
    protected $_fields = [
        'code'         => 'code',
        'label'        => 'label',
        'value_index'  => 'value_index',
        'attribute_id' => 'attribute_id',
    ];

    /** @var string */
    protected $_typeKey = 'ConfigurableAttributeOption';
}