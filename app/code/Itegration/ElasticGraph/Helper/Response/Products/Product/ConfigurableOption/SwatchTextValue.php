<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\Products\Product\ConfigurableOption;

use Itegration\ElasticGraph\Helper\Response\Products\Product\ConfigurableOption\SwatchData\Text;

class SwatchTextValue extends Value
{
    /** @var array */
    protected $_fields = [
        'defaultLabel'        => 'default_label',
        'label'               => 'label',
        'storeLabel'          => 'store_label',
        'useDefaultValue'     => 'use_default_value',
        'id'                  => 'value_index',
        'swatchData'          => 'swatch_data',
        'mediaGalleryEntries' => 'media_gallery_entries',
    ];

    /**
     * @return array
     */
    protected function getSwatchData()
    {
        $swatchData = [];
        if (array_key_exists('swatch', $this->_data)) {
            $swatch = new Text(
                [
                    'type'  => 0,
                    'value' => $this->_data['swatch'],
                ]
            );
            $swatchData = $swatch->getData();
        }

        return $swatchData;
    }
}
