<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\Products\Product\ConfigurableOption\SwatchData;

use Itegration\ElasticGraph\Helper\Response\AbstractResponse;

class Visual extends AbstractResponse
{
    /** @var array */
    protected $_fields = [
        'type'  => 'type',
        'value' => 'value',
    ];

    /** @var string */
    protected $_typeKey = 'VisualSwatchData';

    /**
     * @return string
     */
    protected function getValue()
    {
        $returnValue = $this->_data['value'];
        if (!$returnValue) {
            $returnValue = '';
        }
        return $returnValue;
    }
}
