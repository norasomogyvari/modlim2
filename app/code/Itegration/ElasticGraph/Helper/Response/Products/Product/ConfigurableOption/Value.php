<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\Products\Product\ConfigurableOption;

use Itegration\ElasticGraph\Helper\Response\AbstractResponse;
use Itegration\ElasticGraph\Helper\Response\Products\Product\MediaGalleryEntry;

class Value extends AbstractResponse
{
    /** @var array */
    protected $_fields = [
        'defaultLabel'        => 'default_label',
        'label'               => 'label',
        'storeLabel'          => 'store_label',
        'useDefaultValue'     => 'use_default_value',
        'id'                  => 'value_index',
        'mediaGalleryEntries' => 'media_gallery_entries',
    ];

    /** @var string */
    protected $_typeKey = 'ConfigurableProductOptionsValues';

    /**
     * @return string
     */
    protected function getDefaultLabel()
    {
        return $this->_data['label'];
    }

    /**
     * @return string
     */
    protected function getStoreLabel()
    {
        return $this->_data['label'];
    }

    /**
     * @return bool
     */
    protected function getUseDefaultValue()
    {
        return true;
    }

    /**
     * @return array
     */
    protected function getSwatchData()
    {
        $swatchData = [];

        if (array_key_exists('swatch', $this->_data)) {
            $swatch = new SwatchData(
                [
                    'value' => $this->_data['swatch'],
                ]
            );
            $swatchData = $swatch->getData();
        }

        return $swatchData;
    }

    protected function getMediaGalleryEntries()
    {
        $returnArray = [];
        if (array_key_exists('product_image', $this->_data)) {
            $mediaGalleryEntry = new MediaGalleryEntry(
                [
                    'label'    => $this->_data['label'],
                    'position' => 0,
                    'disabled' => false,
                    'file'     => $this->_data['product_image'],
                ]
            );
            $returnArray[] = $mediaGalleryEntry->getData();
        }
        return $returnArray;
    }
}
