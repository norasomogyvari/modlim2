<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\Products\Product;

use Itegration\ElasticGraph\Helper\Response\AbstractResponse;
use Itegration\ElasticGraph\Helper\Response\Products\Product\CategoryTree\Breadcrumb;

class CategoryTree extends AbstractResponse
{
    /** @var array */
    protected $_fields = [
        'id' => 'id',
        'breadcrumbs' => 'breadcrumbs',
    ];

    /** @var string */
    protected $_typeKey = 'CategoryTree';

    protected function getBreadcrumbs()
    {
        $returnValue = null;

        if (array_key_exists(
                'parent_id',
                $this->_data
            ) && $this->_data['parent_id']) {
            $breadcrumbData = new Breadcrumb(
                ['parent_id' => $this->_data['parent_id']]
            );
            $returnValue = [$breadcrumbData->getData()];
        }

        return $returnValue;
    }
}
