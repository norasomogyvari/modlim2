<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\Products\Product;

use Itegration\ElasticGraph\Helper\Response\AbstractResponse;
use Itegration\ElasticGraph\Helper\Response\Products\Product\Review\RatingVote;

class Review extends AbstractResponse
{
    /** @var array */
    protected $_fields = [
        'review_id'       => 'review_id',
        'country'         => 'country',
        'country_code'    => 'country_code',
        'created_at'      => 'created_at',
        'customer_id'     => 'customer_id',
        'detail'          => 'detail',
        'detail_id'       => 'detail_id',
        'nickname'        => 'nickname',
        'entity_code'     => 'entity_code',
        'entity_id'       => 'entity_id',
        'entity_pk_value' => 'entity_pk_value',
        'region'          => 'region',
        'status_id'       => 'status_id',
        'store_id'        => 'store_id',
        'title'           => 'title',
        'ratingVotes'     => 'rating_votes',
    ];

    /** @var string */
    protected $_typeKey = 'ReviewItem';

    /**
     * @return array
     */
    protected function getRatingVotes()
    {
        $returnArray = [];

        if (array_key_exists('rating_votes', $this->_data)) {
            foreach ($this->_data['rating_votes'] as $ratingVote) {
                $ratingVoteData = new RatingVote($ratingVote);
                $returnArray[] = $ratingVoteData->getData();
            }
        }

        return $returnArray;
    }
}
