<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response\Products;

use Itegration\ElasticGraph\Helper\Operation\ProductDetail;
use Itegration\ElasticGraph\Helper\Request\Products as ProductsRequestHelper;
use Itegration\ElasticGraph\Helper\Response\AbstractResponse;
use Itegration\ElasticGraph\Helper\Response\Products\Product\Description;
use Itegration\ElasticGraph\Helper\Response\Products\Product\Image;
use Itegration\ElasticGraph\Helper\Response\Products\Product\MediaGalleryEntry;
use Itegration\ElasticGraph\Helper\Response\Products\Product\Prices;
use Itegration\ElasticGraph\Helper\Response\Products\Product\Rating;

class Product extends AbstractResponse
{
    protected $_graphQlData = [];
    protected $_graphQlPriceData = [];

    /** @var array */
    protected $_fields = [
        'description'          => 'description',
        'id'                   => 'id',
        'mediaGalleryEntities' => 'media_gallery_entries',
        'metaDescription'      => 'meta_description',
        'name'                 => 'name',
        'price'                => 'price',
        'sku'                  => 'sku',
        'smallImage'           => 'small_image',
        'urlKey'               => 'url_key',
        'url'                  => 'url',
        'rating'               => 'rating',
        'vendorId'             => 'udropship_vendor',
        'vendorName'           => 'udropship_vendor_name',
        'productLabelListing'  => 'product_label_listing',
    ];

    public function __construct($data, $requester = null)
    {
        parent::__construct($data, $requester);

        if (array_key_exists(ProductDetail::PD_GQL_KEY, $this->_data)) {
            $this->_graphQlData = json_decode($this->_data[ProductDetail::PD_GQL_KEY], true);
        }
        if (array_key_exists(ProductDetail::PD_GQL_PRICE_KEY, $this->_data)) {
            $this->_graphQlPriceData = json_decode($this->_data[ProductDetail::PD_GQL_PRICE_KEY], true);
        }
    }


    /**
     * @return string
     */
    protected function _getTypeKey()
    {
        return ucfirst($this->_data['type_id']) . 'Product';
    }

    /**
     * @return array
     */
    protected function getDescription()
    {
        $descriptionData = [];

        if (array_key_exists('description', $this->_data)) {
            $description = new Description(
                ['html' => $this->_data['description']]
            );
            $descriptionData = $description->getData();
        }

        return $descriptionData;
    }

    /**
     * @return string
     */
    protected function getMetaDescription()
    {
        $descriptionData = '';

        if (array_key_exists('description', $this->_data)) {
            $descriptionData = $this->_data['description'];
        }

        return $descriptionData;
    }

    /**
     * @return array
     */
    protected function getMediaGalleryEntities()
    {
        $mediaGalleryEntries = [];
        if (array_key_exists(
                'sorter',
                $this->_data
            ) && $this->_data['sorter'] === true) {
            return [];
        }

        if (array_key_exists('media_gallery_entries', $this->_data)) {
            foreach ($this->_data['media_gallery_entries'] as $mediaGalleryEntry) {
                $mediaGalleryEntryData = new MediaGalleryEntry(
                    $mediaGalleryEntry
                );
                $mediaGalleryEntries[] = $mediaGalleryEntryData->getData();
            }
        }
        return $mediaGalleryEntries;
    }

    /**
     * @return array
     */
    protected function getPrice()
    {
        $priceKey = 'prices_' . ProductsRequestHelper::DEFAULT_CUSTOMER_GROUP_ID;

        $currency = '';
        $regularPrice = -1;
        $minimalPrice = -1;
        $maximalPrice = -1;
        $finalPrice = -1;
        $tierPrice = -1;
        if (array_key_exists('currency', $this->_data)) {
            $currency = $this->_data['currency'];
        }

        if (array_key_exists($priceKey, $this->_data)) {
            if (array_key_exists('price', $this->_data[$priceKey])) {
                $regularPrice = $this->_data[$priceKey]['price'];
            }
            if (array_key_exists('minimal_price', $this->_data[$priceKey])) {
                $minimalPrice = $this->_data[$priceKey]['minimal_price'];
            }
            if (array_key_exists('max_price', $this->_data[$priceKey])) {
                $maximalPrice = $this->_data[$priceKey]['max_price'];
            }
            if (array_key_exists('final_price', $this->_data[$priceKey])) {
                $finalPrice = $this->_data[$priceKey]['final_price'];
            }
            if (array_key_exists('tier_price', $this->_data[$priceKey])) {
                $tierPrice = $this->_data[$priceKey]['tier_price'];
            }
        }

        $priceData = new Prices(
            [
                'currency'     => $currency,
                'regularPrice' => $regularPrice,
                'minimalPrice' => $minimalPrice,
                'maximalPrice' => $maximalPrice,
                'finalPrice'   => $finalPrice,
                'tierPrice'    => $tierPrice,
                'basePrice'    => $finalPrice,
                'minBasePrice' => $minimalPrice,
                'maxBasePrice' => $maximalPrice,
            ]
        );

        return $priceData->getData();
    }

    /**
     * @return array
     */
    protected function getSmallImage()
    {
        $imageData = [];
        if (array_key_exists('base_image', $this->_data)) {
            $image = new Image(['url' => $this->_data['base_image']]);
            $imageData = $image->getData();
        }

        return $imageData;
    }

    /**
     * @return string
     */
    protected function getUrlKey()
    {
        if (array_key_exists('url_key', $this->_data)) {
            return $this->_data['url_key'];
        }

        if (array_key_exists('url_key', $this->_graphQlData)) {
            return $this->_graphQlData['url_key'];
        }

        $urlKey = '';
        if (array_key_exists('url', $this->_data)) {
            $url = $this->_data['url'];
            $urlKey = explode('/', $url);
            $urlKey = array_pop($urlKey);
        }
        $productUrlSuffix = '';
        if (array_key_exists('product_url_suffix', $this->_data)) {
            $productUrlSuffix = $this->_data['product_url_suffix'];
        }

        if ($productUrlSuffix &&
            $urlKey &&
            false !== strpos(
                $urlKey,
                $productUrlSuffix,
                -1 * (strlen($productUrlSuffix))
            )
        ) {
            $urlKey = substr($urlKey, 0, -1 * (strlen($productUrlSuffix)));
        }

        return $urlKey;
    }

    /**
     * @return array
     */
    protected function getRating()
    {
        $returnData = [];

        if (array_key_exists('rating', $this->_data) &&
            array_key_exists('review_count', $this->_data)
        ) {
            $rating = new Rating(
                [
                    'rating' => $this->_data['rating'],
                    'count'  => $this->_data['review_count'],
                ]
            );
            $returnData = $rating->getData();
        }

        return $returnData;
    }

    /**
     * @return string|null
     */
    protected function getVendorId()
    {
        if (array_key_exists('udropship_vendor', $this->_data)) {
            return $this->_data['udropship_vendor'];
        }
        return null;
    }

    /**
     * @return string|null
     */
    protected function getVendorName()
    {
        if (array_key_exists('udropship_vendor_name', $this->_data)) {
            return $this->_data['udropship_vendor_name'];
        }
        return null;
    }

    /**
     * @return string
     */
    protected function getProductLabelListing()
    {
        if (array_key_exists('product_label_listing', $this->_data)) {
            return $this->_data['product_label_listing'];
        }

        if (array_key_exists('product_label_listing', $this->_graphQlData)) {
            return $this->_graphQlData['product_label_listing'];
        }

        return '';
    }

    /**
     * @param array  $product
     * @param string $priceKey
     *
     * @return int
     */
    protected function _getProductPrice($product, $priceKey = 'finalPrice')
    {
        $returnValue = 0;
        if (array_key_exists('price', $product) &&
            array_key_exists($priceKey, $product['price']) &&
            array_key_exists('amount', $product['price'][$priceKey]) &&
            array_key_exists('value', $product['price'][$priceKey]['amount'])
        ) {
            $returnValue = $product['price'][$priceKey]['amount']['value'];
        }

        return $returnValue;
    }
}
