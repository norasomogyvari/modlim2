<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Response;

class Category extends AbstractResponse
{
    /** @var array */
    protected $_fields = [
        'id'                         => 'id',
        'description'                => 'description',
        'name'                       => 'name',
        'product_count'              => 'product_count',
        'meta_description'           => 'meta_description',
        'meta_title'                 => 'meta_title',
        'bottom_description_desktop' => 'bottom_description_desktop',
        'bottom_description_mobile'  => 'bottom_description_mobile',
    ];

    /** @var string */
    protected $_typeKey = 'CategoryTree';
}
