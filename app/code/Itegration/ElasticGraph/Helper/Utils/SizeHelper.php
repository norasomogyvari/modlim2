<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Utils;

use Itegration\ElasticGraph\Helper\Singleton;

class SizeHelper extends Singleton
{
    private $sizes = [
        "XXS"  => 0,
        "XS"   => 1,
        "S"    => 2,
        "M"    => 3,
        "L"    => 4,
        "XL"   => 5,
        "1X"   => 5,
        "XXL"  => 6,
        "2X"   => 6,
        "XXXL" => 7,
        "3X"   => 7,
    ];

    /**
     * @param array $valuesData
     *
     * @return array
     */
    public function sortSizes($valuesData)
    {
        usort(
            $valuesData,
            function ($a, $b) {
                $sizeA = array_key_exists($a['store_label'], $this->sizes) ?
                    $this->sizes[$a['store_label']] : $a['store_label'];
                $sizeB = array_key_exists($b['store_label'], $this->sizes) ?
                    $this->sizes[$b['store_label']] : $b['store_label'];

                if ($sizeA == $sizeB) {
                    return 0;
                }

                return $sizeA > $sizeB ? 1 : -1;
            }
        );

        return $valuesData;
    }
}
