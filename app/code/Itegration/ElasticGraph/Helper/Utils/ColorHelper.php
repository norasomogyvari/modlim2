<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Helper\Utils;
use Itegration\ElasticGraph\Helper\Singleton;

class ColorHelper extends Singleton
{
    /**
     * @param array $valuesData
     * @param array $sortOrder
     *
     * @return array
     */
    public function sortColors($valuesData, $sortOrder)
    {
        if (null === $sortOrder) {
            $sortOrder = [];
        }

        $sortOrder = array_flip($sortOrder);

        usort(
            $valuesData,
            function ($a, $b) use ($sortOrder) {
                $sizeA = array_key_exists($a['value_index'], $sortOrder) ?
                    $sortOrder[$a['value_index']] : $a['value_index'];
                $sizeB = array_key_exists($b['value_index'], $sortOrder) ?
                    $sortOrder[$b['value_index']] : $b['value_index'];

                if ($sizeA == $sizeB) {
                    return 0;
                }

                return $sizeA > $sizeB ? 1 : -1;
            }
        );

        return $valuesData;
    }

    /**
     * @param array $productData
     * @param array|null $colorOrder
     *
     * @return array
     */
    public function handleColorOrders($productData, $colorOrder = null)
    {
        if (is_array($productData) &&
            array_key_exists('configurable_color_order', $productData)
        ) {
            $colorOrder = $productData['configurable_color_order'];
        }
        if (is_array($productData)) {
            foreach ($productData as $key => &$data) {
                if ($key === 'configurable_options') {
                    $productData[$key] = $this->setColorOrders($data, $colorOrder);
                } else {
                    $productData[$key] = $this->handleColorOrders($data, $colorOrder);
                }
            }
        }

        return $productData;
    }

    /**
     * @param array $data
     * @param array $colorOrder
     *
     * @return array
     */
    private function setColorOrders($data, $colorOrder)
    {
        if (is_array($data)) {
            foreach ($data as $key => &$item) {
                if (is_array($item) &&
                    array_key_exists('attribute_code', $item) &&
                    $item['label'] == 'Color' &&
                    array_key_exists('values', $item)
                ) {
                    $data[$key]['values'] = $this->sortColors($data[$key]['values'], $colorOrder);
                }
            }
        }

        return $data;
    }
}
