<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Observer;

use Itegration\ElasticGraph\Helper\Operation\ProductDetail;
use Itegration\ElasticGraph\Model\Config as ElasticConfig;
use Itegration\ElasticGraph\Model\GraphQL as GraphQL;
use Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Wyomind\ElasticsearchCore\Model\Indexer\Product as ProductIndexer;
use Wyomind\ElasticsearchCore\Model\ResourceModel\ToReindex as ToReindexResourceModel;

class ProductExport implements ObserverInterface
{
    const PD_GQL_IDS_VARIABLE_KEY = '$IDS_VARIABLE';

    const SIMPLE_PRODUCT_TYPE       = 'SimpleProduct';
    const CONFIGURABLE_PRODUCT_TYPE = 'ConfigurableProduct';
    const BUNDLE_PRODUCT_TYPE       = 'BundleProduct';

    /**
     * @var ElasticConfig
     */
    private $elasticConfig;

    /**
     * @var GraphQL
     */
    private $graphQL;

    /**
     * @var ProductCollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var ToReindexResourceModel
     */
    private $toReindexResource;

    /**
     * ProductExport constructor.
     *
     * @param ElasticConfig            $elasticConfig
     * @param GraphQL                  $graphQL
     * @param ProductCollectionFactory $productCollectionFactory
     * @param ToReindexResourceModel   $toReindexResource
     */
    public function __construct(
        ElasticConfig $elasticConfig,
        GraphQL $graphQL,
        ProductCollectionFactory $productCollectionFactory,
        ToReindexResourceModel $toReindexResource
    ) {
        $this->elasticConfig = $elasticConfig;
        $this->graphQL = $graphQL;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->toReindexResource = $toReindexResource;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $now = new \DateTime();

        /** @var ProductIndexer $indexer */
        $indexer = $observer->getEvent()->getData('indexer');
        /** @var string $storeId */
        $storeId = $observer->getEvent()->getData('store_id');

        /** @var array $products */
        $products = $indexer->getProducts();

        $skus = [];
        $ids = [];
        foreach ($products as $product) {
            $skus[] = $product['sku'];
            $ids[] = $product['id'];
        }

        $data = $this->getGraphQLResponse(
            $ids,
            $storeId,
            $indexer->getProductChunkSize()
        );

        foreach ($data as $item) {
            if (array_key_exists('id', $item) &&
                array_key_exists($item['id'], $products)
            ) {
                $prices = [];
                $this->cutPrices($item, $prices);

                $products[$item['id']][ProductDetail::PD_GQL_KEY] = json_encode(
                    $item
                );
                $products[$item['id']][ProductDetail::PD_GQL_PRICE_KEY] = json_encode(
                    $prices
                );
                $ratingItems = $item['average_rating'];
                foreach ($ratingItems as $ratingItem) {
                    if ($ratingItem['rating_code'] == 'Stars' && $ratingItem['count']) {
                        $products[$item['id']]['rating'] = $ratingItem['value'] * 20;
                        $products[$item['id']]['review_count'] = $ratingItem['count'];
                    }
                }
                if (array_key_exists('udropship_vendor', $item)) {
                    $products[$item['id']]['udropship_vendor'] = $item['udropship_vendor'];
                }
                if (array_key_exists('udropship_vendor_name', $item)) {
                    $products[$item['id']]['udropship_vendor_name'] = $item['udropship_vendor_name'];
                }
                if ($products[$item['id']]['quantity_and_stock_status_ids'] != 0) {
                    $products[$item['id']]['quantity_and_stock_status_ids'] = $this->getQuantityAndStockStatus(
                        $item,
                        $products[$item['id']]['quantity_and_stock_status_ids']
                    );
                }
            }
        }

        $data = $this->getUrlKeys($skus, $storeId);
        foreach ($data as $productId => $productUrlKey) {
            if (array_key_exists($productId, $products)) {
                $products[$productId]['url_key'] = $productUrlKey;
            }
        }

        $indexer->setProducts($products);

        $this->toReindexResource->deleteMultiple('product', $ids, $now);
    }

    /**
     * @param array $item
     * @param int   $oldStatus
     *
     * @return int
     */
    protected function getQuantityAndStockStatus($item, $oldStatus)
    {
        $productType = $item['__typename'];

        switch ($productType) {
            case self::BUNDLE_PRODUCT_TYPE:
                if ($oldStatus == 0) {
                    $status = $oldStatus;
                } else {
                    $status = $this->handleBundleProductStatus(
                        $item,
                        $oldStatus
                    );
                }
                break;
            case self::CONFIGURABLE_PRODUCT_TYPE:
                $status = $this->handleConfigurableProductStatus(
                    $item
                );
                break;
            default:
                $status = $oldStatus;
        }

        return $status;
    }

    /**
     * @param array $item
     * @param int   $oldStatus
     *
     * @return int
     */
    private function handleConfigurableProductStatus($item)
    {
        if (array_key_exists('variants', $item)) {
            foreach ($item['variants'] as $variant) {
                if (array_key_exists('product', $variant) &&
                    array_key_exists('stock_status', $variant['product']) &&
                    $variant['product']['stock_status'] !== 'OUT_OF_STOCK'
                ) {
                    return 1;
                }
            }
        }

        return 0;
    }

    /**
     * @param array $item
     * @param int   $oldStatus
     *
     * @return int
     */
    private function handleBundleProductStatus($item, $oldStatus)
    {
        $status = $oldStatus;
        if (array_key_exists('items', $item)) {
            foreach ($item['items'] as $section) {
                if (true == $section['required']) {
                    $status = 0;
                    foreach ($section['options'] as $option) {
                        if (array_key_exists('product', $option) &&
                            array_key_exists(
                                'stock_status',
                                $option['product']
                            ) &&
                            $option['product']['stock_status'] !== 'OUT_OF_STOCK'
                        ) {
                            $status = 1;
                            break;
                        }
                    }
                    if ($status == 0) {
                        return 0;
                    }
                }
            }
        }
        return $status;
    }

    /**
     * @param array $skus
     * @param int   $storeId
     *
     * @return array
     */
    protected function getUrlKeys($skus, $storeId)
    {
        $returnArray = [];

        /** @var ProductCollection $productCollection */
        $productCollection = $this->productCollectionFactory->create();
        $productCollection
            ->setStoreId($storeId)
            ->addFieldToFilter('sku', ['in' => $skus])
            ->addAttributeToSelect('url_key');

        foreach ($productCollection as $product) {
            $productData = $product->getData();
            $id = $productData['entity_id'];
            if (array_key_exists('row_id', $productData)) {
                $id = $productData['row_id'];
            }
            $returnArray[$id] = $product->getData('url_key');
        }

        return $returnArray;
    }

    /**
     * @param int[] $ids
     * @param int   $storeId
     *
     * @return array
     */
    protected function getGraphQLResponse($ids, $storeId)
    {
        $graphQlQuery = $this->elasticConfig->getProductDetailGraphQLQuery();
        $storeUrl = $this->elasticConfig->getStoreUrl($storeId);

        if (!$graphQlQuery || !$storeUrl) {
            return [];
        }

        $graphQlQuery = str_replace(
            self::PD_GQL_IDS_VARIABLE_KEY,
            '[' . join(',', $ids) . ']',
            $graphQlQuery
        );

        $graphQlQuery = str_replace("\n", '\n', $graphQlQuery);
        $graphQlQuery = trim($graphQlQuery, "\n");

        $endPoint = $storeUrl;
        $endPointResolve = $this->getEndPointResolve($endPoint);
        $endPoint .= 'graphql';

        return $this->graphQL->execute(
            $graphQlQuery,
            $endPoint,
            $endPointResolve,
            'productDetail',
            ['STORE: ' . $this->elasticConfig->getStoreCode($storeId)]
        );
    }

    /**
     * @param string $endPoint
     *
     * @return string
     */
    private function getEndPointResolve($endPoint)
    {
        $urlData = parse_url($endPoint);

        $returnData = $urlData['host'];
        if ($urlData['scheme'] === 'https') {
            $returnData .= ':443';
        } else {
            $returnData .= ':80';
        }
        $returnData .= ':127.0.0.1';

        return $returnData;
    }

    /**
     * @param array $item
     * @param array $prices
     */
    private function cutPrices(&$item, &$prices)
    {
        if (is_array($item)) {
            if (array_key_exists('prices', $item) && array_key_exists(
                    'id',
                    $item
                )) {
                $prices[$item['id']] = $item['prices'];
                unset($item['prices']);
            }
            foreach ($item as $key => $value) {
                $this->cutPrices($item[$key], $prices);
            }
        }
    }
}
