<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Widget\Model\Template\FilterEmulate;
use Wyomind\ElasticsearchCore\Model\Indexer\Category as CategoryIndexer;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Catalog\Model\Category as CategoryModel;

class CategoryExport implements ObserverInterface
{
    /**
     * @var FilterEmulate
     */
    private $filterEmulate;

    /**
     * @var CategoryCollectionFactory
     */
    private $categoryCollectionFactory;

    /**
     * CategoryExport constructor.
     *
     * @param FilterEmulate             $filterEmulate
     * @param CategoryCollectionFactory $categoryCollectionFactory
     */
    public function __construct(
        FilterEmulate $filterEmulate,
        CategoryCollectionFactory $categoryCollectionFactory
    ) {
        $this->filterEmulate = $filterEmulate;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
    }


    public function execute(Observer $observer)
    {
        /** @var CategoryIndexer $indexer */
        $indexer = $observer->getEvent()->getData('indexer');
        /** @var string $storeId */
        $storeId = $observer->getEvent()->getData('store_id');

        $categories = $indexer->getCategories();

        $categoryModels = $this->getCategoryModels(array_keys($categories));

        foreach ($categories as $categoryIndex => $category) {
            $categories[$categoryIndex]['description'] = $this->filterEmulate->filter($categories[$categoryIndex]['description']);
            if (array_key_exists($categoryIndex, $categoryModels)) {
                $categories[$categoryIndex]['sort_fields'] = json_encode($categoryModels[$categoryIndex]->getAvailableSortByOptions());
                $categories[$categoryIndex]['default_sort_by'] = $categoryModels[$categoryIndex]->getDefaultSortBy();
                $categories[$categoryIndex]['product_count'] = $categoryModels[$categoryIndex]->getProductCount();
                $categories[$categoryIndex]['meta_description'] = $categoryModels[$categoryIndex]->getData('meta_description');
            }
        }


        $indexer->setCategories($categories);
    }

    /**
     * @param int[] $ids
     *
     * @return CategoryModel[]
     */
    private function getCategoryModels($ids)
    {
        $data = [];
        $categoryCollection = $this->categoryCollectionFactory->create()
            ->addIdFilter($ids)
            ->addAttributeToSelect('*');

        /** @var CategoryModel $category */
        foreach ($categoryCollection as $category) {
            $data[$category->getId()] = $category;
        }

        return $data;
    }
}
