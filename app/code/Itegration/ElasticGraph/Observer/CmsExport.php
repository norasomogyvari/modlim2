<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\App\Emulation;
use Magento\Widget\Model\Template\FilterEmulate;
use Wyomind\ElasticsearchCore\Model\Indexer\Cms as CmsIndexer;

class CmsExport implements ObserverInterface
{
    /**
     * @var FilterEmulate
     */
    private $filterEmulate;

    /**
     * @var Emulation
     */
    private $emulation;

    /**
     * CmsExport constructor.
     *
     * @param FilterEmulate $filterEmulate
     * @param Emulation     $emulation
     */
    public function __construct(
        FilterEmulate $filterEmulate,
        Emulation $emulation
    ) {
        $this->filterEmulate = $filterEmulate;
        $this->emulation = $emulation;
    }

    /**
     * @param Observer $observer
     *
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        /** @var CmsIndexer $indexer */
        $indexer = $observer->getEvent()->getData('indexer');
        /** @var string $storeId */
        $storeId = $observer->getEvent()->getData('store_id');

        $pages = $indexer->getPages();

        $this->emulation->startEnvironmentEmulation($storeId, 'frontend', true);

        foreach ($pages as $pageIndex => $page) {
            $pages[$pageIndex]['graphql_content'] = $this->filterEmulate->filter($pages[$pageIndex]['content']);
        }

        $this->emulation->stopEnvironmentEmulation();

        $indexer->setPages($pages);
    }
}
