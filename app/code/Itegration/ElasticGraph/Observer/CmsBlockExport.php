<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\App\Emulation;
use Magento\Widget\Model\Template\FilterEmulate;
use Wyomind\ElasticsearchCore\Model\Indexer\CmsBlock as CmsBlockIndexer;

class CmsBlockExport implements ObserverInterface
{
    /**
     * @var FilterEmulate
     */
    private $filterEmulate;

    /**
     * @var Emulation
     */
    private $emulation;

    /**
     * CmsBlockExport constructor.
     *
     * @param FilterEmulate $filterEmulate
     * @param Emulation     $emulation
     */
    public function __construct(
        FilterEmulate $filterEmulate,
        Emulation $emulation
    ) {
        $this->filterEmulate = $filterEmulate;
        $this->emulation = $emulation;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        /** @var CmsBlockIndexer $indexer */
        $indexer = $observer->getEvent()->getData('indexer');
        /** @var string $storeId */
        $storeId = $observer->getEvent()->getData('store_id');

        $blocks = $indexer->getBlocks();

        $this->emulation->startEnvironmentEmulation($storeId, 'frontend', true);

        foreach ($blocks as $blockIndex => $block) {
            $blocks[$blockIndex]['graphql_content'] = $this->filterEmulate->filter($blocks[$blockIndex]['content']);
        }

        $this->emulation->stopEnvironmentEmulation();

        $indexer->setBlocks($blocks);
    }
}
