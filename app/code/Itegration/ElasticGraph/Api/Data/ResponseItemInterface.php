<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\ElasticGraph\Api\Data;


interface ResponseItemInterface
{
    const TYPE_KEY = '__typename';
    const DEFAULT_TYPE = 'Default';

    public function getData();
}
