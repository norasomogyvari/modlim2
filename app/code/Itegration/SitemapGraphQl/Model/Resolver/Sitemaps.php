<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\SitemapGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Sitemap\Model\ResourceModel\Sitemap\Collection as SitemapCollection;
use Magento\Sitemap\Model\ResourceModel\Sitemap\CollectionFactory as SitemapCollectionFactory;
use Magento\Sitemap\Model\Sitemap as SitemapModel;
use Magento\Sitemap\Model\SitemapFactory as SitemapModelFactory;

class Sitemaps implements ResolverInterface
{
    /**
     * @var SitemapCollectionFactory
     */
    private $sitemapCollectionFactory;

    /**
     * @var SitemapModelFactory
     */
    private $sitemapModelFactory;

    /**
     * Sitemaps constructor.
     *
     * @param SitemapCollectionFactory $sitemapCollectionFactory
     * @param SitemapModelFactory      $sitemapModelFactory
     */
    public function __construct(
        SitemapCollectionFactory $sitemapCollectionFactory,
        SitemapModelFactory $sitemapModelFactory
    ) {
        $this->sitemapCollectionFactory = $sitemapCollectionFactory;
        $this->sitemapModelFactory = $sitemapModelFactory;
    }

    /**
     * @param Field $field
     * @param ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     *
     * @return Value|mixed|void
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        /** @var SitemapCollection $sitemapCollection */
        $sitemapCollection = $this->sitemapCollectionFactory->create();
        $sitemapCollection->join(
            ['st' => $sitemapCollection->getTable('store')],
            'st.store_id = main_table.store_id',
            'st.code as store_code'
        );

        /** @var SitemapModel $sitemapModel */
        $sitemapModel = $this->sitemapModelFactory->create();

        $returnArray = ['items' => []];

        /** @var SitemapModel $sitemapItem */
        foreach ($sitemapCollection->getConnection()->fetchAll(
            $sitemapCollection->getSelect()
        ) as $sitemapItem) {
            $returnArray['items'][] = [
                'store_code' => $sitemapItem['store_code'],
                'url'        => $sitemapModel->getSitemapUrl(
                    $sitemapItem['sitemap_path'],
                    $sitemapItem['sitemap_filename']
                ),
            ];
        }

        return $returnArray;
    }

}
