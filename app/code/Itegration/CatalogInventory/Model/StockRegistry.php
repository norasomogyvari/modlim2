<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\CatalogInventory\Model;

use Magento\CatalogInventory\Model\StockRegistry as OriginalStockRegistry;

class StockRegistry extends OriginalStockRegistry
{
    /**
     * @inheritdoc
     */
    public function updateStockItemBySku($productSku, \Magento\CatalogInventory\Api\Data\StockItemInterface $stockItem)
    {
        /** @var \Magento\CatalogInventory\Model\Stock\Item $stockItem */

        $productId = $this->resolveProductId($productSku);
        $websiteId = $stockItem->getWebsiteId() ?: null;
        $origStockItem = $this->getStockItem($productId, $websiteId);

        $mustSave = false;
        if (!$mustSave) {
            $mustSave = $origStockItem->getQty() != $stockItem->getQty();
        }
        if (!$mustSave && null != $stockItem->getData($stockItem::MIN_QTY)) {
            $mustSave = $origStockItem->getMinQty() != $stockItem->getData($stockItem::MIN_QTY);
        }
        if (!$mustSave && null != $stockItem->getData($stockItem::MIN_SALE_QTY)) {
            $mustSave = $origStockItem->getMinSaleQty() != $stockItem->getData($stockItem::MIN_SALE_QTY);
        }
        if (!$mustSave && null != $stockItem->getData($stockItem::MAX_SALE_QTY)) {
            $mustSave = $origStockItem->getMaxSaleQty() != $stockItem->getData($stockItem::MAX_SALE_QTY);
        }
        if (!$mustSave && null != $stockItem->getData($stockItem::IS_IN_STOCK)) {
            $mustSave = $origStockItem->getMaxSaleQty() != $stockItem->getData($stockItem::IS_IN_STOCK);
        }
        if (!$mustSave && null != $stockItem->getData($stockItem::IS_IN_STOCK)) {
            $mustSave = $origStockItem->getMaxSaleQty() != $stockItem->getData($stockItem::IS_IN_STOCK);
        }
        if (!$mustSave && null != $stockItem->getData($stockItem::NOTIFY_STOCK_QTY)) {
            $mustSave = $origStockItem->getNotifyStockQty() != $stockItem->getData($stockItem::NOTIFY_STOCK_QTY);
        }
        if (!$mustSave && null != $stockItem->getData($stockItem::MANAGE_STOCK)) {
            $mustSave = $origStockItem->getManageStock() != $stockItem->getData($stockItem::MANAGE_STOCK);
        }
        if (!$mustSave && null != $stockItem->getData($stockItem::BACKORDERS)) {
            $mustSave = $origStockItem->getBackorders() != $stockItem->getData($stockItem::BACKORDERS);
        }

        if ($mustSave) {
            return parent::updateStockItemBySku($productSku, $stockItem);
        }

        return $origStockItem->getItemId();
    }
}
