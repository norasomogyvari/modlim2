<?php
/**
 * Copyright ©2020 Itegration Ltd., Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author: Perencz Tamás <tamas.perencz@itegraion.com>
 */

namespace Itegration\CatalogInventory\Plugin\Model\System\Config\Backend\Minqty;

use Magento\CatalogInventory\Model\System\Config\Backend\Minqty;

class AllowNegativeMinQtyInConfigPlugin
{
    /**
     * Allow min_qty to be assigned a value below 0.
     * @param Minqty $subject
     * @param callable $proceed
     * @return mixed
     */
    public function aroundBeforeSave(
        Minqty $subject,
        callable $proceed
    ) {
        $originalMinQty = $proceed();
        $originalMinQty->setValue($subject->getFieldsetDataValue('min_qty'));
        return $originalMinQty;
    }
}
