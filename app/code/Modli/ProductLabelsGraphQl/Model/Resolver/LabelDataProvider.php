<?php
namespace Modli\ProductLabelsGraphQl\Model\Resolver;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Mageplaza\ProductLabelsGraphQl\Model\Resolver\LabelDataProvider as MageplazaLabelDataProvider;

use Mageplaza\ProductLabels\Block\Label;
use Mageplaza\ProductLabels\Helper\Data;
use Mageplaza\ProductLabels\Model\LabelRepository;
use Magento\Catalog\Api\ProductRepositoryInterface;

class LabelDataProvider extends MageplazaLabelDataProvider
{
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * LabelDataProvider constructor.
     *
     * @param LabelRepository $labelRepository
     * @param Label $label
     * @param Data $helperData
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        LabelRepository $labelRepository,
        Label $label,
        Data $helperData,
        ProductRepositoryInterface $productRepository
    ) {
        $this->productRepository = $productRepository;
        return parent::__construct($labelRepository, $label, $helperData);
    }

    /**
     * @inheritdoc
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        if (!$this->helperData->isEnabled()) {
            return [];
        }

        if (!array_key_exists('model', $value) || !$value['model'] instanceof ProductInterface) {
            throw new LocalizedException(__('"model" value should be specified'));
        }

        /* @var $product ProductInterface */
        $product   = $value['model'];
        $labelData = [];

        $productModel = $this->productRepository->getById($product->getId());
        /** @var Mageplaza\ProductLabels\Model\Rule $rule */
        foreach ($this->label->getRulesApplyProduct($productModel) as $rule) {
            if ($this->label->validateProductInRule($rule, $productModel->getId())) {
                $labelData[] = $this->labelRepository->getById($rule->getId());
            }
        }

        return $labelData;
    }
}
