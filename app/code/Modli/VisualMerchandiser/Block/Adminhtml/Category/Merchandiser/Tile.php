<?php
namespace Modli\VisualMerchandiser\Block\Adminhtml\Category\Merchandiser;

use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\CatalogInventory\Model\ResourceModel\Stock\StatusFactory as StockStatusFactory;

class Tile extends \Magento\VisualMerchandiser\Block\Adminhtml\Category\Merchandiser\Tile
{
    #protected $_template = 'Modli_VisualMerchandiser::category/merchandiser/tile.phtml';
    /**
     * @var Visibility
     */
    protected $visibility;

    /**
     * @var Status
     */
    protected $productStatus;

    /**
     * @var StockStatusFactory
     */
    protected $stockStatusFactory;

    /**
     * @var int
     */
    protected $_defaultLimit = 100;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Catalog\Helper\Image $catalogImage,
        Visibility $visibility,
        Status $productStatus,
        StockStatusFactory $stockStatusFactory,
        \Magento\VisualMerchandiser\Model\Category\Products $products,
        \Magento\VisualMerchandiser\Block\Adminhtml\Widget\Tile\Attribute\Factory $attributeFactory,
        array $data = [],
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency = null)
    {
        $this->visibility = $visibility;
        $this->productStatus = $productStatus;
        $this->stockStatusFactory = $stockStatusFactory;
        parent::__construct($context, $backendHelper, $coreRegistry, $catalogImage, $products, $attributeFactory, $data, $priceCurrency);
    }

    /**
     * @inheritDoc
     */
    protected function _prepareCollection()
    {
        $this->_products->setCacheKey($this->_getPositionCacheKey());
        $productPositions = $this->_backendSession->getCategoryProductPositions(true);
        $collection = $this->_products->getCollectionForGrid(
            (int) $this->getRequest()->getParam('id', 0),
            (int) $this->getRequest()->getParam('store'),
            $productPositions
        );
        $collection->setVisibility($this->visibility->getVisibleInCatalogIds());
        #$collection->addAttributeToFilter('status', ['in' => $this->productStatus->getVisibleStatusIds()]);
        $this->stockStatusFactory->create()->addStockDataToCollection($collection, false);

        #$this->stockFilter->addInStockFilterToCollection($collection);
        $collection->clear();
        $this->setCollection($collection);

        $this->_preparePage();

        $idx = ($collection->getCurPage() * $collection->getPageSize()) - $collection->getPageSize();

        foreach ($collection as $item) {
            $item->setPosition($idx);
            $idx++;
        }

        $this->_products->setCacheKey($this->_getPositionCacheKey());
        $this->_products->savePositions($collection);

        return $this;
    }
}
