<?php
namespace Modli\ReviewsFeefo\Console\Command\Orders;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Modli\ReviewsFeefo\Model\Order\Export as ExportModel;

class Export extends Command
{
    /**
     * @var ExportModel
     */
    protected $export;

    public function __construct(
        ExportModel $export,
        string $name = null
    ) {
        $this->export = $export;
        parent::__construct($name);
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return null|int
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->export->export();
    }
}
