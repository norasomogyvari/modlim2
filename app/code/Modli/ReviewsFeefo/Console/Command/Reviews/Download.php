<?php
namespace Modli\ReviewsFeefo\Console\Command\Reviews;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Modli\Reviews\Model\Loader\Page;

/**
 * Class SomeCommand
 */
class Download extends Command
{
    /**
     * @var Page
     */
    protected $pageLoader;

    public function __construct(
        Page $pageLoader,
        string $name = null
    )
    {
        $this->pageLoader = $pageLoader;
        parent::__construct($name);
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return null|int
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->pageLoader->load();
    }
}
