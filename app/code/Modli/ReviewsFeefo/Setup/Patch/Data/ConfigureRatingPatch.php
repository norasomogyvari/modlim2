<?php
namespace Modli\ReviewsFeefo\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Review\Model\RatingFactory;
use Magento\Store\Model\StoreManagerInterface;

class ConfigureRatingPatch
    implements DataPatchInterface,
    PatchRevertableInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var RatingFactory
     */
    protected $ratingFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param RatingFactory $ratingFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        RatingFactory $ratingFactory,
        StoreManagerInterface $storeManager
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->ratingFactory = $ratingFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        $storeIds = $this->getAllStoreIds();
        $ratingsToUpdate = [
            1 => \Modli\ReviewsFeefo\Model\Rating::FIT_RATING_CODE,
            2 => \Modli\ReviewsFeefo\Model\Rating::STARS_RATING_CODE
        ];
        foreach ($ratingsToUpdate as $ratingId => $code) {
            /**
             * @var $rating \Magento\Review\Model\Rating;
             */
            $rating = $this->ratingFactory->create();
            $rating->load($ratingId)
                ->setIsActive(true)
                ->setRatingCode($code)
                ->setStores($storeIds)
                ->save();
        }


        $this->moduleDataSetup->getConnection()->endSetup();
    }

    protected function getAllStoreIds()
    {
        $storeIds = [];
        foreach ($this->storeManager->getStores() as $store) {
            $storeIds[] = $store->getId();
        }

        return $storeIds;
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [

        ];
    }

    public function revert()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
