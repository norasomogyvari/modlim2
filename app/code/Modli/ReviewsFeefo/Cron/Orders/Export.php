<?php
namespace Modli\ReviewsFeefo\Cron\Orders;

use Modli\ReviewsFeefo\Model\Order\Export as ExportModel;

class Export
{
    /**
     * @var ExportModel
     */
    protected $export;

    public function __construct(ExportModel $export)
    {
        $this->export = $export;
    }

    public function execute()
    {
        $this->export->export();
    }
}
