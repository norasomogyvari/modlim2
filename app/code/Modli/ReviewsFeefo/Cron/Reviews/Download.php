<?php

namespace Modli\ReviewsFeefo\Cron\Reviews;

use Modli\Reviews\Model\Loader\Page;

class Download
{
    protected $reviews;

    public function __construct(Page $reviews)
    {
        $this->reviews = $reviews;
    }

    public function execute()
    {
        $this->reviews->load();
    }
}
