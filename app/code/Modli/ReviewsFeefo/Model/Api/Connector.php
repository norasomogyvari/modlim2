<?php
namespace Modli\ReviewsFeefo\Model\Api;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Encryption\EncryptorInterface;
use Modli\ReviewsFeefo\Helper\Config as ConfigHelper;
/**
 * Class Connector
 *
 * Feefo API adapter
 *
 * @package Modli\ReviewsFeefo\Model\Api
 */
class Connector
{
    const API_VERSION = 10;
    const API_BASE_URL = 'https://api.feefo.com/api/' . self::API_VERSION;
    const DEFAULT_PAGE_SIZE = 100;

    /**
     * Store configuration retrieving
     *
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Curl Adapter for single cURL requests
     *
     * @var Curl
     */
    protected $curl;

    /**
     * Json Serializer
     *
     * @var Json
     */
    protected $json;

    /**
     * @var string $token
     */
    protected $token;

    /**
     * @var ConfigHelper
     */
    protected $configHelper;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Curl $curl,
        Json $json,
        ConfigHelper $configHelper
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->curl = $curl;
        $this->json = $json;
        $this->configHelper = $configHelper;
    }

    /**
     * Get access token to server
     */
    protected function getToken()
    {
        if (is_null($this->token)) {

            $request = [
                "authenticationDTO" => [
                    "merchantIdentifier" => $this->configHelper->getMerchantId(),
                    "username" => $this->configHelper->getUser(),
                    "password" => $this->configHelper->getPassword(),
                    "apiKey" => $this->configHelper->getApiKey(),
                ]
            ];


            $request = $this->json->serialize($request);

            $this->curl->setHeaders([
                'Content-Type' => 'application/json'
            ]);
            $this->curl->post('https://api.feefo.com/api/apiauthenticate', $request);
            $response = $this->curl->getBody();
            $response = $this->unserializeResponse($response);
            if (!isset($response['apiToken'])) {
                throw new \InvalidArgumentException('Authorization failed');
            }
            $this->token = $response['apiToken'];
            $this->curl->removeHeader('Content-Type');
        }

        return $this->token;
    }

    /**
     * Return Feefo API request URL
     *
     * @param string $path
     * @param array $params
     *
     * @return string
     */
    protected function buildRequestUrl(string $path, array $params = []): string
    {
        $url = self::API_BASE_URL . '/' . $path . '?merchant_identifier=' . $this->configHelper->getMerchantId();
        if (!empty($params)) {
            $mergedParams = [];
            foreach ($params as $key => $value) {
                $mergedParams[] = "$key=$value";
            }
            $params = implode('&', $mergedParams);
            $url = $url . '&' . $params;
        }

        return $url;
    }

    public function debug()
    {
        return ($this->getPageReviews(1)['reviews'][0]);
    }

    /**
     * Return total reviews info
     *
     * @return array
     */
    public function getSummary(): array
    {
        $url = $this->buildRequestUrl('reviews/summary/product', ['since_period' => 'all']);
        return $this->requestApi($url);
    }

    /**
     * Return reviews page
     *
     * @param int $pageNumber Number of page.
     * @param int $pageSize Qty of reviews on page
     *
     * @return array
     */
    public function getPageReviews(int $pageNumber, $pageSize = self::DEFAULT_PAGE_SIZE): array
    {
        $params = [
            'since_period' => 'all',
            'page_size' => $pageSize,
            'page' => $pageNumber,
            'full_thread' => 'include',
        ];
        $url = $this->buildRequestUrl('reviews/product', $params);

        return $this->requestApi($url);
    }

    /**
     * Call Feefo API by URL and return response
     *
     * @param string $url URL to call
     * @return array
     */
    public function requestApi(string $url): array
    {
        $token = $this->getToken();
        if ($token) {
            $this->curl->setHeaders([
                'Authorization' => $token
            ]);
        }
        $this->curl->get($url);
        $this->curl->removeHeader('Authorization');
        $response = $this->curl->getBody();
        return $this->unserializeResponse($response);
    }

    /**
     * Return unserialized API response.
     *
     * @param string $response Serialized response
     * @return array
     */
    protected function unserializeResponse(string $response): array
    {
        try {
            return $this->json->unserialize($response);
        } catch (\InvalidArgumentException $e) {
            return [];
        }
    }

    /**
     * Return urls for paged reviews for multi curl
     *
     * @var int $totalPageCount
     *
     * @return array
     */
    protected function getPagedReviewsUrls(int $totalPageCount): array
    {

    }
}
