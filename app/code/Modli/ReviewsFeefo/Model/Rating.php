<?php
declare(strict_types=1);

namespace Modli\ReviewsFeefo\Model;

use Modli\Reviews\Model\RatingRepository;

class Rating
{
    const STARS_RATING_CODE = 'Stars';
    const FIT_RATING_CODE = 'Fit';

    /**
     * @var RatingRepository
     */
    protected $ratingRepository;

    /**
     * Rating constructor.
     *
     * @param RatingRepository $ratingRepository
     */
    public function __construct(RatingRepository $ratingRepository)
    {
        $this->ratingRepository = $ratingRepository;
    }


    public function getStarsRating()
    {
        return $this->ratingRepository->getRatingByCode(static::STARS_RATING_CODE);
    }

    public function getFitRating()
    {
        return $this->ratingRepository->getRatingByCode(static::FIT_RATING_CODE);
    }

    public function getFeefoRatings()
    {
        return [
            $this->getStarsRating()->getId() => $this->getStarsRating(),
            $this->getFitRating()->getId() => $this->getFitRating(),
        ];
    }

}
