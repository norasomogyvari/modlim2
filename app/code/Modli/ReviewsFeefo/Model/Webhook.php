<?php
namespace Modli\ReviewsFeefo\Model;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Modli\ReviewsFeefo\Model\Api\Connector;
use Modli\ReviewsFeefo\Model\Review\Data\Converter;
use Modli\Reviews\Model\Review\Processor;

class Webhook
{
    const XML_HMAC_PATH = 'catalog/feefo/hmac';
    const XML_ENABLED_PATH = 'catalog/feefo/webhook_enabled';

    /**
     * Store configuration retrieving
     *
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Connect to Feefo API
     *
     * @var Connector
     */
    protected $connector;

    /**
     * Convert reviews Feefo->Magento
     *
     * @var Converter
     */
    protected $converter;

    /**
     * Review processor
     *
     * @var Processor
     */
    protected $processor;

    /**
     * Webhook constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param Connector $connector
     * @param Converter $converter
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Connector $connector,
        Converter $converter,
        Processor $processor
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->connector = $connector;
        $this->converter = $converter;
        $this->processor = $processor;
    }

    /**
     * Validate request
     *
     * @param $hmac
     * @return bool
     */
    public function validate($hmac): bool
    {
        if (!$this->isEnabled()) {
            return false;
        }
        if ($this->getHmac() == $hmac) {
            return true;
        }

        return false;
    }

    protected function isEnabled(): bool
    {
        return (bool)$this->scopeConfig->getValue(self::XML_ENABLED_PATH);
    }

    protected function getHmac()
    {
        return (bool)$this->scopeConfig->getValue(self::XML_HMAC_PATH);
    }

    public function create($url): array
    {
        $reviews = $this->connector->requestApi($url);
        $reviews = $this->converter->convertBunch($reviews);
        return $this->processor->saveReviews($reviews);
    }

}
