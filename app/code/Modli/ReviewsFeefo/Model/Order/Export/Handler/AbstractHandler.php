<?php
namespace Modli\ReviewsFeefo\Model\Order\Export\Handler;

use Modli\ReviewsFeefo\Api\Order\Export\HandlerInterface;
use Modli\ReviewsFeefo\Logger\Export as ExportLogger;

abstract class AbstractHandler implements HandlerInterface
{
    const TYPE = 'DEFAULT';
    /**
     * @var ExportLogger
     */
    protected $logger;

    public function __construct(ExportLogger $logger)
    {
        $this->logger = $logger;
    }

    public function send(array $data)
    {
        $this->logger->debug("Order sending start. Hendler type: " . static::TYPE);
        $this->logger->debug('Row count: ' . count($data));
        try {
            $this->_send($data);
            $this->logger->debug("Order sending finish. Hendler type: " . static::TYPE);
        } catch (\Exception $e) {
            $this->logger->critical("Order sending error. " . $e->getMessage());
        }
    }

    abstract protected function _send(array $data);
}
