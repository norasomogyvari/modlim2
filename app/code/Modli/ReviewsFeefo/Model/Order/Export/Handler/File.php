<?php
namespace Modli\ReviewsFeefo\Model\Order\Export\Handler;

use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem\Io\File as FileIo;
use Modli\ReviewsFeefo\Logger\Export as ExportLogger;
use Magento\Framework\File\Csv;
use Modli\ReviewsFeefo\Helper\Config as ConfigHelper;
use Magento\Framework\Filesystem\Io\Ftp;

class File extends AbstractHandler
{
    const TYPE = 'FILE';

    const FILE_HEADER = ['merchant identifier', 'name', 'email', 'date', 'description', 'order ref',
        'product search code', 'mobile', 'parent product ref', 'amount', 'currency', 'product link', 'tags', 'locale',
        'product attrbiutes'];

    protected $filesystem;

    protected $fileIo;

    protected $csvProcessor;

    protected $configHelper;

    protected $ftp;

    protected $filePath;

    public function __construct(
        Filesystem $filesystem,
        FileIo $fileIo,
        ExportLogger $logger,
        CSV $csvProcessor,
        ConfigHelper $configHelper,
        Ftp $ftp
    )
    {
        $this->filesystem = $filesystem;
        $this->fileIo = $fileIo;
        $this->csvProcessor = $csvProcessor;
        $this->configHelper = $configHelper;
        $this->ftp = $ftp;
        $this->filePath = $this->getFilePath();

        parent::__construct($logger);
    }

    protected function _send(array $data)
    {
        $this->csvProcessor
            ->appendData($this->filePath, $data, 'a');
    }

    protected function getFilePath(): string
    {
        $mediaPath = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath();

        $dirPath = $mediaPath . 'feefo' . DIRECTORY_SEPARATOR . 'order' . DIRECTORY_SEPARATOR;
        if (!$this->fileIo->fileExists($dirPath)) {
            $this->fileIo->mkdir($dirPath, 777, true);
        }

        $todayPath = $dirPath . 'today' . DIRECTORY_SEPARATOR;
        if (!$this->fileIo->fileExists($todayPath)) {
            $this->fileIo->mkdir($todayPath, 777, true);
        }

        $fileName = $dirPath . 'order_date_' . date('Ymd') . '.csv';
        if (!file_exists($fileName)) {
            $this->csvProcessor
                ->saveData($fileName, [self::FILE_HEADER]);

            $todayOrderPath = $todayPath . 'orders.csv';
            if ($this->fileIo->fileExists($todayOrderPath)) {
                $this->fileIo->rm($todayOrderPath);
            }
            $this->fileIo->cp($fileName, $todayOrderPath);
        }

        return $fileName;
    }
}
