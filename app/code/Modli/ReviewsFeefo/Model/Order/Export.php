<?php
namespace Modli\ReviewsFeefo\Model\Order;

use Modli\ReviewsFeefo\Model\Order\Data\Converter;
use Modli\ReviewsFeefo\Model\Order\Export\Handler\Db;
use Modli\ReviewsFeefo\Model\Order\Export\Handler\File;
use Modli\ReviewsFeefo\Logger\Export as ExportLogger;
use Magento\Sales\Api\OrderRepositoryInterface; /*$orderRepository,*/
use Magento\Framework\Api\SearchCriteriaBuilder; /*$searchCriteriaBuilder*/


class Export
{
    const FROM_DATE = '2019-11-01 23:59:59';
    /**
     * @var Converter
     */
    protected $dataConverter;

    /**
     * @var Db
     */
    protected $handlerDb;

    /**
     * @var File
     */
    protected $handlerFile;

    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var ExportLogger
     */
    protected $logger;

    /**
     * Export constructor.
     *
     * @param Converter $dataConverter
     * @param Db $handlerDb
     * @param File $handlerFile
     * @param OrderRepositoryInterface $orderRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param ExportLogger $logger
     */
    public function __construct (
        Converter $dataConverter,
        Db $handlerDb,
        File $handlerFile,
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ExportLogger $logger
    ) {
        $this->dataConverter = $dataConverter;
        $this->handlerDb = $handlerDb;
        $this->handlerFile = $handlerFile;
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->logger = $logger;
    }

    public function export()
    {
        $this->logger->critical('Order export start.');
        $orders = $this->getOrders();
        foreach ($orders as $order) {
            $data = $this->dataConverter->prepareOrderData($order);
            $this->handlerFile->send($data);
            $this->handlerDb->send($data);
            $this->setOrderProcessed($order);
        }

        $this->logger->debug('Order export finish.');
    }

    protected function getOrders(): array
    {
        $date = new \DateTime(self::FROM_DATE);

        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(
                'status',
                \Magento\Sales\Model\Order::STATE_COMPLETE,
                'eq'
            )->addFilter(
                'created_at',
                $date->format('Y-m-d'),
                'gt'
            )->addFilter(
                'feefo_exported',
                0,
                'eq'
            )->create();

        return $this->orderRepository->getList($searchCriteria)->getItems();
    }

    protected function setOrderProcessed($order)
    {
        $order->setFeefoExported(1);
        $this->orderRepository->save($order);
    }
}
