<?php

namespace Modli\ReviewsFeefo\Model\Order\Data;


use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Item;
use Magento\Sales\Model\Order\Address;
use Magento\Catalog\Model\Product;
use Magento\Store\Model\StoreManagerInterface;
use Modli\ReviewsFeefo\Helper\Config as ConfigHelper;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\Product\Type;

class Converter
{
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var ConfigHelper
     */
    protected $configHelper;

    /**
     * @var string|null
     */
    protected $merchantId;

    /**
     * @var Configurable
     */
    protected $configurableResource;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    public function __construct(
        StoreManagerInterface $storeManager,
        ConfigHelper $configHelper,
        Configurable $configurableResource,
        ProductFactory $productFactory
    )
    {
        $this->storeManager = $storeManager;
        $this->configHelper = $configHelper;
        $this->merchantId = $configHelper->getMerchantId();
        $this->configurableResource = $configurableResource;
        $this->productFactory = $productFactory;
    }

    public function prepareOrderData(Order $order): array
    {
        $data = [];

        $items = $order->getItemsCollection();

        $storeId = $order->getStoreId();
        $store = $this->storeManager->getStore($storeId);
        $address = $this->getAddress($order);
        foreach ($items as $item) {
            if (in_array($item->getProductType(), [Type::TYPE_BUNDLE, 'configurable'])) {
                continue;
            }
            $product = $this->getProduct($item);

            $orderData = [
                'merchant identifier' => $this->configHelper->getMerchantId(),
                'name' => $this->getCustomerName($order),
                'email' => $order->getCustomerEmail(),
                'date' => (new \DateTime($order->getCreatedAt()))->format('d/m/Y'),
                'description' => $product->getName(),
                'order ref' => $order->getIncrementId(),
                'product search code' => $item->getSku(),
                'mobile' => $this->getTelephone($address),
                'parent product ref' => $product->getSku(),
                'amount' => $this->getPrice($item),
                'currency' => $order->getBaseCurrencyCode(),
                'product link' => $product->getProductUrl(),
                'tags' => $this->getAttributes($order, $store, $product),
                'locale' => $this->getLocale($store),
                'product attrbiutes' => ''
            ];
            $data[] = $orderData;
        }

        return $data;
    }

    public function getPrice(Item $item): float
    {
        $price = $item->getRowTotal();

        if ($price == 0 && $parentItemId = $item->getParentItemId()) {
            $parentItem = $item->getOrder()->getItemsCollection()->getItemById($parentItemId);
            $price = $parentItem->getPrice();
        }

        return number_format($price, 2);
    }

    protected function getAttributes($order, $store, $product): string
    {
        $attributes = [
            'store' => $store->getName(),
            'country' => $this->getAddress($order)->getCountryId(),
            'productbrand' => $product->getAttributeText('udropship_vendor'),
            'size' => $product->getData('size'),
            'campaign' => empty($this->getTelephone($this->getAddress($order))) ? 'EMAIL' : 'SMS',
        ];

        $result = [];
        foreach ($attributes as $code => $value) {
            $result[] = "$code=$value";
        }

        $result = implode(',', $result);

        return $result;
    }

    protected function getProduct(Item $item): ?Product
    {
        $product = $item->getProduct();
        $size = $product->getAttributeText('size');
        if ($item->getParentItemId()) {
            $productId = $product->getId();
            $configIds = $this->configurableResource->getParentIdsByChild($productId);
            if (isset($configIds[0])) {
                $product = $this->productFactory->create()->load($configIds[0]);
            }
        }
        $product->setData('size', $size);
        return $product;
    }

    protected function getTelephone(Address $address): string
    {
        if ($this->configHelper->isSendPhone()) {
            return $this->formatTelephone($address->getCountryId(), $address->getTelephone());
        } else {
            return '';
        }
    }

    /**
     * Foramat telephone as per US/UK format
     *
     * @param string $country
     * @param string $number
     * @return string $number|''
     */
    protected function formatTelephone(string $country,string $number)
    {
        $number = preg_replace("/[^0-9]/", "", $number);

        if ($country == 'US') {
            if (strlen($number) == 10) {
                $number = '+1' . $number;
                return $number;
            } elseif (strlen($number) == 11 && $number[0] == '1') {
                $number = '+' . $number;
                return $number;
            }
        } elseif ($country == 'GB') {
            if (strlen($number) == 9) {
                $number = '+447' . $number;
                return $number;
            } elseif (strlen($number) == 12 && substr($number, 0, 3) == '447') {
                $number = '+' . $number;
                return $number;
            }
        }
        return '';
    }

    protected function getCustomerName(Order $order): string
    {
        $customerName = $order->getCustomerFirstname() . ' ' . $order->getCustomerLastname();
        if (empty($customerName)) {
            $address = $this->getAddress($order);
            if ($address) {
                $customerName = $address->getFirstname() . ' ' . $address->getLastname();
            }
        }

        return $customerName;
    }

    protected function getAddress(Order $order): ?Address
    {
        $address = null;
        if ($order->getShippingAddress()) {
            $address = $order->getShippingAddress();
        } elseif ($order->getBillingAddress()) {
            $address = $order->getBillingAddress();
        }

        return $address;
    }

    protected function getLocale(\Magento\Store\Api\Data\StoreInterface $store)
    {
        $locale = $this->configHelper->getConfig('general/locale/code', false, $store->getCode());
        $locale = explode('_', $locale);
        return $locale[0];
    }
}
