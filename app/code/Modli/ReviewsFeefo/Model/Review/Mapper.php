<?php

namespace Modli\ReviewsFeefo\Model\Review;

use Modli\Reviews\Api\Review\MapperInterface;
use Modli\Reviews\Helper\Customer as CustomerHelper;
use Modli\Reviews\Helper\Product as ProductHelper;

class Mapper implements MapperInterface
{
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;

    /**
     * @var ProductHelper
     */
    protected $productHelper;

    public function __construct(
        CustomerHelper $customerHelper,
        ProductHelper $productHelper
    )
    {
        $this->customerHelper = $customerHelper;
        $this->productHelper = $productHelper;
    }

    public function map(array $data): array
    {
        $mappedData = [];
        if (isset($data['customer'])) {
            $customerData = [];
            if (isset($data['customer']['email'])){
                $customerData['customer_id'] = $this->customerHelper->getIdByEmail($data['customer']['email']);
                $customerData['nickname'] = $data['display_name']['name'] ?? '';
                $customerData['email'] = $data['customer']['email'];
            }
        }
        $mappedData = array_merge($mappedData, $customerData);
        if (isset($data['products'])) {
            //oreach ()
        }
        /*$mappedData = [
            'product_id' => $this->productHelper->getIdBySku(),
            'status_id' => '',
            'nickname' =>
            'customer_id' => $this->customerHelper->getIdByEmail($data['customer']['email']),
        ];*/


        return $data;
    }
}
