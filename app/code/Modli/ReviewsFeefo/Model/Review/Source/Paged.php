<?php

namespace Modli\ReviewsFeefo\Model\Review\Source;

use Modli\Reviews\Api\Review\Api\PagedInterface;
use Modli\ReviewsFeefo\Model\Api\Connector;
use Modli\ReviewsFeefo\Model\Review\Data\Converter;

class Paged implements PagedInterface
{
    const REVIEWS_PER_PAGE = 100;
    /**
     * @var Connector
     */
    protected $api;

    /**
     * @var Converter
     */
    protected $converter;

    /**
     * Feefo Paged constructor.
     *
     * @var Connector $api
     * @var Converter $converter
     */
    public function __construct(
        Connector $api,
        Converter $converter
    ) {
        $this->api = $api;
        $this->converter = $converter;
    }

    /**
     * @inheritDoc
     */
    public function getPageCount(): int
    {
        $summary = $this->api->getSummary();
        if (!isset($summary['meta']['count'])) {
            throw new \InvalidArgumentException('Error getting page count.');
        }

        return ceil($summary['meta']['count']/self::REVIEWS_PER_PAGE);
    }

    /**
     * @inheritDoc
     */
    public function getPageReviews(int $pageNum): array
    {
        $pageReviews = $this->api->getPageReviews($pageNum);
        return $this->converter->convertBunch($pageReviews);
    }
}
