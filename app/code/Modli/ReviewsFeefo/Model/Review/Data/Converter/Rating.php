<?php
namespace Modli\ReviewsFeefo\Model\Review\Data\Converter;

use Modli\ReviewsFeefo\Model\Rating as RatingSource;
use Magento\Review\Model\ResourceModel\Rating\Option\CollectionFactory;

class Rating
{
    /**
     * @var RatingSource
     */
    protected $ratingSource;

    /**
     * @var CollectionFactory
     */
    protected $ratingOptionCollectionFactory;

    protected $optionValueMap = [];

    /**
     * Rating constructor.
     *
     * @param RatingSource $ratingSource
     * @param CollectionFactory $ratingOptionCollectionFactory
     */
    public function __construct(
        RatingSource $ratingSource,
        CollectionFactory $ratingOptionCollectionFactory
    ) {
        $this->ratingSource = $ratingSource;
        $this->ratingOptionCollectionFactory = $ratingOptionCollectionFactory;
        $this->initOptionValueMap();
    }

    protected function initOptionValueMap()
    {
        $ratingOptionCollection = $this->ratingOptionCollectionFactory->create();
        foreach ($ratingOptionCollection as $ratingOption) {
            /**
             * @var $ratingOption \Magento\Review\Model\Rating\Option
             */
            $this->optionValueMap[$ratingOption->getRatingId()][$ratingOption->getValue()] = $ratingOption->getId();
        }
    }

    public function getRating(array $data): array
    {
        $rating = [];
        foreach ($this->getRatingsValue($data) as $ratingId => $value) {
            $optionId = $this->getOptionByValue($value, $ratingId);
            if ($optionId) {
                $rating[$ratingId] = $optionId;
            }
        }

        return $rating;
    }

    protected function getRatingsValue(array $data)
    {
        $starsRating = $this->ratingSource->getStarsRating();
        $fitRating = $this->ratingSource->getFitRating();

        $ratings = [];
        if (isset($data['rating']['rating'])) {
            $ratings[$starsRating->getId()] = (int)$data['rating']['rating'];
        }


        if (isset($data['custom'][0]['answer'])) {
            $ratings[$fitRating->getId()] = (int)$data['custom'][0]['answer'];
        }

        return $ratings;
    }


    protected function getOptionByValue(int $value, int $ratingId)
    {
        return (int)(@$this->optionValueMap[$ratingId][$value]);
    }
}
