<?php
namespace Modli\ReviewsFeefo\Model\Review\Data;

use Modli\Reviews\Helper\Customer as CustomerHelper;
use Modli\Reviews\Helper\Product as ProductHelper;
use Magento\Store\Model\Store;
use Magento\Review\Model\Review;
use Modli\ReviewsFeefo\Model\Review\Data\Converter\Rating as RatingConverter;

class Converter
{
    const SOURCE_CODE = 'feefo';
    /**
     * @var CustomerHelper
     */
    protected $customerHelper;

    /**
     * @var ProductHelper
     */
    protected $productHelper;

    /**
     * @var RatingConverter
     */
    protected $ratingConverter;

    public function __construct(
        CustomerHelper $customerHelper,
        ProductHelper $productHelper,
        RatingConverter $ratingConverter
    ) {
        $this->customerHelper = $customerHelper;
        $this->productHelper = $productHelper;
        $this->ratingConverter = $ratingConverter;
    }

    public function convertBunch(array $feefoReviews): array
    {
        $reviews = [];
        if (!isset($feefoReviews['reviews'])) {
            return [];
        }
        foreach ($feefoReviews['reviews'] as $feefoReview) {
            $customerData = $this->getCustomerData($feefoReview);
            foreach ($feefoReview['products'] as $productReview) {
                $review = $this->mapReview($productReview);
                $productData = $this->getProductData($productReview);
                $review = array_merge($review, $customerData);
                $review = array_merge($review, $productData);
                $reviews[] = $review;
            }
        }

        return $reviews;
    }

    protected function mapReview(array $data): array
    {
        $map = [
            'review' => 'detail',
            #'created_at' => 'created_at',
            'id' => 'source_id',
        ];
        $result = [];
        foreach ($map as $source => $dest) {
            $result[$dest] = $data[$source];
        }
        $result['created_at'] = date('Y-m-d H:i:s', strtotime($data['created_at']));
        $result['store_id'] = Store::DEFAULT_STORE_ID;
        $result['title'] = mb_substr($result['detail'], 0, 255);
        $result['status_id'] = Review::STATUS_APPROVED;
        $result['source_code'] = self::SOURCE_CODE;
        if (isset($data['rating']['rating'])) {
            $result['rating'] = $this->ratingConverter->getRating($data);
        }

        return $result;
    }

    protected function getCustomerData(array $feefoReview): array
    {
        $customerData = [];
        if (isset($feefoReview['customer'])) {
            $data = $feefoReview['customer'];
            $customerData['nickname'] = $data['display_name'] ?? $data['name'];
            if (isset($data['email'])) {
                $customerData['customer_id'] = $this->customerHelper->getIdByEmail($data['email']);
                $customerData['email'] = $data['email'];
            }
        }

        return  $customerData;
    }

    protected function getProductData(array $productReview): array
    {
        $productData = [];
        if (!isset($productReview['product']['sku'])) {
            throw new \InvalidArgumentException('Feefo review product update. No product SKU');
        }
        $productData['product_id'] = $this->productHelper->getIdBySku($productReview['product']['sku']);

        return $productData;
    }
}
