<?php

namespace Modli\ReviewsFeefo\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Store\Model\ScopeInterface;

class Config
{
    const XML_INDENTIFIER_PATH = 'catalog/feefo/merchant_id';
    const XML_USER_PATH = 'catalog/feefo/user';
    const XML_PASSWORD_PATH = 'catalog/feefo/password';
    const XML_API_KEY_PATH = 'catalog/feefo/api_key';
    const XML_SEND_PHONE_PATH = 'catalog/feefo/send_phone';

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var EncryptorInterface
     */
    protected $encryptor;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        EncryptorInterface $encryptor
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->encryptor = $encryptor;
    }

    public function getConfig(string $path, bool $decrypt = false, $scopeCode = null): string
    {
        $value = (string)$this->scopeConfig->getValue($path, ScopeInterface::SCOPE_STORE, $scopeCode);
        if ($decrypt) {
            $value = $this->encryptor->decrypt($value);
        }
        return $value;
    }

    /**
     * Return Feefo Merchant ID
     *
     * @return string|null
     */
    public function getMerchantId(): string
    {
        return $this->getConfig(self::XML_INDENTIFIER_PATH);
    }

    public function getUser()
    {
        return $this->getConfig(self::XML_USER_PATH);
    }

    public function getPassword()
    {
        return $this->getConfig(self::XML_PASSWORD_PATH, true);
    }

    public function getApiKey()
    {
        return $this->getConfig(self::XML_API_KEY_PATH, true);
    }

    public function isSendPhone(): bool
    {
        return (bool)$this->scopeConfig->getValue(self::XML_SEND_PHONE_PATH);
    }

}
