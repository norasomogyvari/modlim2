<?php
namespace Modli\ReviewsFeefo\Api\Order\Export;

interface HandlerInterface
{
    public function send(array $data);
}
