<?php
namespace Modli\ReviewsFeefo\Controller\Webhook;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Serialize\Serializer\Json;
use Modli\ReviewsFeefo\Logger\Webhook as Logger;
use Modli\ReviewsFeefo\Model\Webhook;


class Post  extends \Magento\Framework\App\Action\Action implements HttpPostActionInterface
{
    /**
     * @var JsonFactory
     */
    protected $jsonResultFactory;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var Json
     */
    protected $json;

    /**
     * @var Webhook
     */
    protected $webhook;

    public function __construct(
        JsonFactory $jsonResultFactory,
        Logger $logger,
        Json $json,
        Webhook $webhook,
        Context $context
    )
    {
        $this->logger = $logger;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->json = $json;
        $this->webhook = $webhook;

        parent::__construct($context);
    }

    public function execute()
    {
        $requestBody = file_get_contents('php://input');
        $this->logger->debug('Feefo webhook request. Body: ' . $requestBody);

        try {
            $request = $this->json->unserialize($requestBody);
            $valid = $this->webhook->validate($request['hmac']);
            if ($valid) {
                $reviews = $this->webhook->create($request['data']);
                $ids = [];
                foreach ($reviews as $review) {
                    $ids[] = $review->getId();
                }
                $result = [
                    'success' => true,
                    'message' => __('New review ids: ' . implode(',', $ids)),
                ];
            } else {
                $result = [
                    'success' => false,
                    'message' => __('Webhooks are disabled or wrong hmac'),
                ];
            }
        } catch (\InvalidArgumentException $e) {
            $this->logger->critical('Can\'t unserialize request: ' . $requestBody);
            $result = [
                'success' => false,
                'message' => $e->getMessage()
            ];
        } catch (\Exception $e) {
            $this->logger->critical("Error webhook handling: $requestBody. Error {$e->getMessage()}");
            $result = [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }
        $result = $this->json->serialize($result);
        return $this->jsonResultFactory
            ->create()
            ->setData($result);
    }
}
