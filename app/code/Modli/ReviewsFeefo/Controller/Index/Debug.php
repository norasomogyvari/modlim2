<?php
namespace Modli\ReviewsFeefo\Controller\Index;

use Modli\Reviews\Model\Loader\Page;

class Debug extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $configLoader = $objectManager->get(\Magento\Framework\ObjectManager\ConfigLoaderInterface::class);
        $objectManager->configure($configLoader->load('adminhtml'));
        $feefoApi = $objectManager->get(Page::class);

        var_dump($feefoApi->load());


        return $this->_pageFactory->create();
    }
}
