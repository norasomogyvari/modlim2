<?php

namespace Modli\QuoteGraphQl\Plugin;

use Magento\Framework\Pricing\PriceCurrencyInterface;

class AddCreditToDiscount
{
    /**
     * @var PriceCurrencyInterface
     */
    protected $priceHelper;

    public function __construct(PriceCurrencyInterface $priceHelper)
    {
        $this->priceHelper = $priceHelper;
    }

    public function aroundResolve(\Magento\QuoteGraphQl\Model\Resolver\Discounts $subject, callable $proceed, ...$args)
    {
        $discounts = $proceed(...$args);
        $value = $args[3];
        $context = $args[1];
        $quote = $value['model'];

        $balanceApplied = $quote->getCustomerBalanceAmountUsed();
        if ($balanceApplied > 0) {
            $store = $context->getExtensionAttributes()->getStore();
            $currentCurrency = $store->getCurrentCurrency();
            $discounts[] = [
                'label' => __('Credit'),
                'amount' => [
                    'value' => $this->priceHelper->round($balanceApplied),
                    'currency' => $currentCurrency->getCode(),
                ]
            ];
        }

        return $discounts;
    }
}
