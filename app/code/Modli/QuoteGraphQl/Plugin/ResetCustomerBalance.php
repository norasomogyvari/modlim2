<?php
namespace Modli\QuoteGraphQl\Plugin;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class ResetCustomerBalance
{
    public function beforeResolve(\Magento\QuoteGraphQl\Model\Resolver\CartPrices $subject, ...$args)
    {
        //Reset Used  Customer balance for correct total calculation
        if (isset($args[3]['model'])) {
            $quote = $args[3]['model'];
            $quote->setBaseCustomerBalAmountUsed(0);
            $quote->setCustomerBalanceAmountUsed(0);
        }

        return $args;
    }
}
