<?php

namespace Modli\Sales\Plugin\Order;

use Magento\Catalog\Helper\Image;
use Magento\Catalog\Api\ProductRepositoryInterface;

class Item
{
    /**
     * @var Image
     */
    protected $imageHelper;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    public function __construct(
        Image $imageHelper,
        ProductRepositoryInterface $productRepository
    )
    {
        $this->imageHelper = $imageHelper;
        $this->productRepository = $productRepository;
    }

    public function afterGetProduct(\Magento\Sales\Model\Order\Item $item, $result)
    {
        if (!$result instanceof \Magento\Catalog\Model\Product) {
            return $result;
        }


        if ($result->getImageUrl() === null) {
            $imageUrl = $this->imageHelper->init($result, 'product_small_image')->getUrl();
            $result->setImageUrl($imageUrl);
        }

        return $result;

    }
}
