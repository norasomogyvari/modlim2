<?php

namespace Modli\RetentionScience\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;

class SetCronSchedule implements DataPatchInterface, PatchRevertableInterface
{
    const XML_RESCI_WAVES_SYNC_AUTO_CONFIG_PATH = 'waves/sync/cron_schedule';

    /**
     * @var WriterInterface
     */
    protected $configWriter;

    /**
     * @param WriterInterface $configWriter
     */
    public function __construct(
        WriterInterface $configWriter
    ) {
        $this->configWriter = $configWriter;
    }

    /**
     * @inheritdoc
     */
    public function apply()
    {
        $this->configWriter->save(self::XML_RESCI_WAVES_SYNC_AUTO_CONFIG_PATH, '0 10 * * *');
    }

    /**
     * @inheritdoc
     */
    public function revert()
    {
        $this->configWriter->save(self::XML_RESCI_WAVES_SYNC_AUTO_CONFIG_PATH, '0 0 * * *');
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }
}
