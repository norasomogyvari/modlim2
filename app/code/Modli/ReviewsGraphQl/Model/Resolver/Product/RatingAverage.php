<?php

namespace Modli\ReviewsGraphQl\Model\Resolver\Product;

use Magento\Catalog\Model\Product;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\GraphQl\Query\ResolverInterface;

use Modli\ReviewsGraphQl\Model\DataProvider\Rating as RatingProvider;



class RatingAverage implements ResolverInterface
{

    /**
     * @var RatingProvider
     */
    protected $ratingProvider;

    /**
     * Reviews constructor.
     *
     * @param RatingProvider $ratingProvider
     *
     */
    public function __construct(
        RatingProvider $ratingProvider
    ) {
        $this->ratingProvider = $ratingProvider;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!isset($value['model'])) {
            throw new LocalizedException(__('"model" value should be specified'));
        }

        /** @var Product $product */
        $product = $value['model'];


        return $this->ratingProvider->getAverageRating($product);
    }


}
