<?php

namespace Modli\ReviewsGraphQl\Model\Resolver\Product;

use Magento\Catalog\Model\Product;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Review\Model\ResourceModel\Review\Collection as ReviewCollection;
use Magento\Review\Model\ResourceModel\Review\CollectionFactory;
use Magento\Review\Model\Review;
use Magento\Review\Model\ReviewFactory;
use Magento\Store\Model\StoreManagerInterface;
use ScandiPWA\ReviewsGraphQl\Model\Resolver\Product\Reviews as ScandiPWAReviews;
use Modli\ReviewsGraphQl\Model\DataProvider\Product as ProductProvider;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Directory\Model\CountryFactory;
use Modli\ReviewsGraphQl\Model\DataProvider\Order as OrderProvider;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;

class Reviews extends ScandiPWAReviews
{

    /**
     * @var ProductCollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var ProductProvider
     */
    protected $productProvider;

    /**
     * @var OrderProvider
     */
    protected $orderProvider;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var CountryFactory
     */
    protected $countryFactory;

    /**
     * @var array
     */
    protected $countryCodeNameMap = [];

    const EXTRA_PRODUCT_ATTRIBUTES = ['product_color', 'size'];

    /**
     * Reviews constructor.
     *
     * @param ReviewFactory $reviewFactory
     * @param StoreManagerInterface $storeManager
     * @param CollectionFactory $reviewCollectionFactory
     * @param ProductProvider $productProvider
     * @param OrderProvider $orderProvider
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param CountryFactory $countryFactory
     * @param ProductCollectionFactory $productCollectionFactory
     */
    public function __construct(
        ReviewFactory $reviewFactory,
        StoreManagerInterface $storeManager,
        CollectionFactory $reviewCollectionFactory,
        ProductProvider $productProvider,
        OrderProvider $orderProvider,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        CountryFactory $countryFactory,
        ProductCollectionFactory $productCollectionFactory
    ) {
        $this->orderProvider = $orderProvider;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->countryFactory = $countryFactory;
        $this->productProvider = $productProvider;
        $this->productCollectionFactory = $productCollectionFactory;

        return parent::__construct($reviewFactory, $storeManager, $reviewCollectionFactory);
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!isset($value['model'])) {
            throw new LocalizedException(__('"model" value should be specified'));
        }

        /** @var Product $product */
        $product = $value['model'];

        if (array_key_exists('store_id', $value)) {
            $storeId = $value['store_id'];
        } else {
            $storeId = $this->storeManager->getStore()->getId();
        }

        $reviewData = [];
        if ($this->reviewCollection === null) {
            $this->reviewCollection = $this->reviewCollectionFactory->create()->addStoreFilter(
                $storeId
            )->addStatusFilter(
                Review::STATUS_APPROVED
            )->setDateOrder();

            $this->setProductFilter($product, $this->reviewCollection);
        }

        /**
         * @var ReviewCollection $reviews
         */
        $reviews = $this->reviewCollection->load()->addRateVotes();

        $customerAddressMap = $this->orderProvider->getCustomerEmailAddressMap($reviews);

        /**
         * @var Review $review
         */
        foreach ($reviews as $review) {
            $ratingVotes = $review->getRatingVotes()->getData();
            $reviewData[] = [
                'review_id' => $review->getReviewId(),
                'entity_id' => $review->getEntityId(),
                'entity_code' => $review->getEntityCode(),
                'entity_pk_value' => $review->getEntityPkValue(),
                'status_id' => $review->getStatusId(),
                'customer_id' => $review->getCustomerId(),
                'nickname' => $review->getNickname(),
                'title' => $review->getTitle(),
                'detail_id' => $review->getDetailId(),
                'detail' => $review->getDetail(),
                'created_at' => $review->getCreatedAt(),
                'rating_votes' => $ratingVotes,
                'country_code' => @$customerAddressMap[$review->getEmail()]['country_code'],
                'country' => $this->getCountryNameByCode(@$customerAddressMap[$review->getEmail()]['country_code']),
                'region' => @$customerAddressMap[$review->getEmail()]['region'],
                'source_id' => $review->getSourceId(),
            ];
        }
        $extraProductAttributes = array_intersect(array_keys($info->getFieldSelection()), self::EXTRA_PRODUCT_ATTRIBUTES);
        if (!empty($extraProductAttributes)) {
            $this->attachExtraAttributes($reviewData, $product, $extraProductAttributes);
        }

        return $reviewData;
    }

    protected function setProductFilter(Product $product, ReviewCollection $reviewCollection)
    {
        $productsToFilter = $this->productProvider->getFilterProductIds($product);

        $reviewCollection->addEntityFilter(
            'product',
            $productsToFilter
        );
    }

    protected function getCountryNameByCode($countryCode)
    {
        if (empty($countryCode)) {
            return '';
        }

        if (!isset($this->countryCodeNameMap[$countryCode])) {
            $country = $this->countryFactory->create()->loadByCode($countryCode);
            $this->countryCodeNameMap[$countryCode] = $country->getName();
        }

        return $this->countryCodeNameMap[$countryCode];
    }

    protected function attachExtraAttributes(array &$reviewData, Product $product, array $extraProductAttributes)
    {
        $productCollection = $this->productCollectionFactory
            ->create()
            ->addAttributeToFilter(
                'entity_id',
                ['in' => $this->productProvider->getFilterProductIds($product)]
            )
            ->addAttributeToSelect($extraProductAttributes);

        foreach ($reviewData as &$review) {
            $product = $productCollection->getItemById($review['entity_pk_value']);
            if (!$product) {
                continue;
            }
            foreach ($extraProductAttributes as $attributeCode) {
                $review[$attributeCode] = $this->getProductAttribute($product, $attributeCode);
            }
        }
    }

    protected function getProductAttribute(Product $product, string $attributeCode)
    {
        return $product->getResource()->getAttribute($attributeCode)->getFrontend()->getValue($product);
    }
}
