<?php
namespace Modli\ReviewsGraphQl\Model\DataProvider;

use Magento\Framework\App\ResourceConnection;
use Magento\Review\Model\ResourceModel\Review\Collection as ReviewCollection;

class Order
{
    /**
     * @var ResourceConnection
     */
    protected $connection;

    public function __construct(ResourceConnection $connection)
    {
        $this->connection = $connection;
    }


    public function getCustomerEmailAddressMap(ReviewCollection $reviews)
    {
        $emails = [];
        foreach ($reviews as $review) {
            $emails[] = $review->getEmail();
        }
        $emails = array_unique($emails);
        if (empty($emails)) {
            return [];
        }
        $orderTable = $this->connection->getTableName('sales_order');
        $addressTable = $this->connection->getTableName('sales_order_address');

        $select = $this->connection->getConnection()
            ->select()
            ->from(['o' => $orderTable], ['email' => 'customer_email'])
            ->joinLeft(
                ['a' => $addressTable],
                "a.parent_id=o.entity_id AND a.address_type = 'billing'",
                ['country_id', 'region']
            )
            ->where('a.entity_id IS NOT NULL')
            ->where("o.customer_email IN ('?')", new \Zend_Db_Expr(implode("','", $emails)));

        $result = $this->connection->getConnection()->fetchAll($select);
        $customerEmailAddressMap = [];
        foreach ($result as $row) {
            $customerEmailAddressMap[$row['email']] = [
                'country_code' => $row['country_id'],
                'region' => $row['region'],
            ];
        }

        return $customerEmailAddressMap;
    }




}
