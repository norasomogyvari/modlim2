<?php

namespace Modli\ReviewsGraphQl\Model\DataProvider;

use Magento\Framework\App\ResourceConnection;
use Magento\Catalog\Model\Product as ProductModel;
use Modli\ReviewsGraphQl\Model\DataProvider\Product as ProductProvider;
use Magento\Review\Model\ResourceModel\Rating\CollectionFactory;
use Magento\Review\Model\Rating as RatingModel;
use Magento\Store\Model\StoreManagerInterface;

class Rating
{
    /**
     * @var ResourceConnection
     */
    protected $connection;

    /**
     * @var CollectionFactory
     */
    protected $ratingCollectionFactory;

    /**
     * @var ProductProvider
     */
    protected $productProvider;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    public function __construct(
        ResourceConnection $connection,
        ProductProvider $productProvider,
        CollectionFactory $ratingCollectionFactory,
        StoreManagerInterface $storeManager
    ) {
        $this->connection = $connection;
        $this->productProvider = $productProvider;
        $this->ratingCollectionFactory = $ratingCollectionFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * @param ProductModel $product
     *
     * @return array
     */
    public function getAverageRating(ProductModel $product)
    {
        $productsToFilter = $this->productProvider->getFilterProductIds(
            $product
        );
        $ratingCollection = $this->ratingCollectionFactory
            ->create()->addFieldToFilter('is_active', true);
        $averageRating = [];

        foreach ($ratingCollection as $rating) {
            $ratingCalculation = $this->getRatingCalculation(
                $rating,
                $productsToFilter
            );
            $averageRating[] = [
                'rating_code' => $rating->getRatingCode(),
                'value'       => $ratingCalculation['average'],
                'count'       => $ratingCalculation['count'],
            ];
        }
        return $averageRating;
    }

    /**
     * @param RatingModel $rating
     * @param array       $productIds
     *
     * @return array
     */
    protected function getRatingCalculation(
        RatingModel $rating,
        array $productIds
    ) {
        try {
            $storeId = $this->storeManager->getStore()->getId();
            $table = $this->connection->getTableName(
                'rating_option_vote_aggregated'
            );
            $productIds = implode(',', $productIds);
            $fields = new \Zend_Db_Expr(
                'ROUND(SUM(vote_value_sum)/SUM(vote_count), 2) as average, SUM(vote_count) as count'
            );
            $select = $this->connection->getConnection()
                ->select()
                ->from($table, $fields)
                ->where('rating_id = ?', $rating->getId())
                ->where('store_id = ?', $storeId)
                ->where("entity_pk_value IN($productIds)");

            $result = $this->connection->getConnection()->fetchRow($select);
        } catch (\Exception $e) {
            $result = [
                'average' => 0,
                'count'   => 0,
            ];
        }

        $result['average'] = $result['average'] ?? 0;
        $result['count'] = $result['count'] ?? 0;

        return $result;
    }
}
