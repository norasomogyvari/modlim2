<?php

namespace Modli\ReviewsGraphQl\Model\DataProvider;

use Magento\Catalog\Model\Product as ProductModel;

class Product
{

    public function getFilterProductIds(ProductModel $product)
    {
        $typeInstance = $product->getTypeInstance();
        $productIds = $typeInstance->getChildrenIds($product->getId(), true);
        $productIds = reset($productIds);
        $productIds[] = $product->getId();

        return $productIds;
    }


}
