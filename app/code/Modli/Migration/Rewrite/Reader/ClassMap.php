<?php
/**
 * Rewrite added in order to allow load custom class-map.xml file.
 *
 * @author    Pavlo Shunkov <pashu@smile.fr>
 * @copyright 2019 Smile
 */

namespace Modli\Migration\Rewrite\Reader;

use Migration\Exception;
use Migration\Reader\ClassMap as Base;

/**
 * Class ClassMap.
 */
class ClassMap extends Base
{
    /**
     * @inheritDoc
     */
    protected function validate()
    {
        $mapFile = $this->config->getOption(self::MAP_FILE_OPTION);
        $configFile = file_exists($mapFile) ? $mapFile : $this->getRootDir() . $mapFile;
        if (!is_file($configFile)) {
            throw new Exception('Invalid map filename: ' . $configFile);
        }
        $xml = file_get_contents($configFile);
        $document = new \Magento\Framework\Config\Dom($xml, $this->validationState);

        if (!$document->validate($this->getRootDir() . 'etc/' . self::CONFIGURATION_SCHEMA)) {
            throw new Exception('XML file is invalid.');
        }

        $this->xml = new \DOMXPath($document->getDom());
        return $this;
    }

    /**
     * Returns document root.
     *
     * @return string
     * @throws \ReflectionException
     */
    protected function getRootDir()
    {
        $parent = new \ReflectionClass(Base::class);
        return dirname(dirname(dirname(dirname($parent->getFileName())))) . DIRECTORY_SEPARATOR;
    }
}
