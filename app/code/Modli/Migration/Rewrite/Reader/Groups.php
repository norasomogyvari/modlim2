<?php
/**
 * Rewrite added in order to allow load custom class-map.xml file.
 *
 * @author    Pavlo Shunkov <pashu@smile.fr>
 * @copyright 2019 Smile
 */

namespace Modli\Migration\Rewrite\Reader;

use Migration\Exception;
use Migration\Reader\Groups as Base;

/**
 * Class Groups.
 */
class Groups extends Base
{
    /**
     * @inheritDoc
     */
    public function init($groupsFile)
    {
        $xmlFile = file_exists($groupsFile) ? $groupsFile : $this->getRootDir() . $groupsFile;
        if (!is_file($xmlFile)) {
            throw new Exception('Invalid groups filename: ' . $xmlFile);
        }

        $xml = file_get_contents($xmlFile);
        $document = new \Magento\Framework\Config\Dom($xml, $this->validationState);

        if (!$document->validate($this->getRootDir() . 'etc/' . self::CONFIGURATION_SCHEMA)) {
            throw new Exception('XML file is invalid.');
        }

        $this->xml = new \DOMXPath($document->getDom());
        return $this;
    }

    /**
     * Returns document root.
     *
     * @return string
     * @throws \ReflectionException
     */
    protected function getRootDir()
    {
        $parent = new \ReflectionClass(Base::class);
        return dirname(dirname(dirname(dirname($parent->getFileName())))) . DIRECTORY_SEPARATOR;
    }
}
