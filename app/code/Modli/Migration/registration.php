<?php
/**
 * Register Modli Migration module.
 *
 * @author    Pavlo Shunkov <pashu@smile.fr>
 * @copyright 2019 Smile
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Modli_Migration',
    __DIR__
);
