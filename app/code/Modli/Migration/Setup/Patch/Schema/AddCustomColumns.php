<?php
/**
 * Schema path. Add custom columns.
 *
 * @author    Pavlo Shunkov <pashu@smile.fr>
 * @copyright 2019 Smile
 */

namespace Modli\Migration\Setup\Patch\Schema;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class AddCustomColumns
 */
class AddCustomColumns implements DataPatchInterface
{
    /**
     * @var SchemaSetupInterface
     */
    protected $schemaSetup;

    /**
     * AddLegacyColumns constructor.
     * @param SchemaSetupInterface $schemaSetup
     **/
    public function __construct(
        SchemaSetupInterface $schemaSetup
    ) {
        $this->schemaSetup = $schemaSetup;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->schemaSetup->startSetup();

        $dataTypesConfigs = [
            'base_text' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'default' => null,
                'length' => 255
            ],
            'base_decimal' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                'nullable' => true,
                'unsigned' => true,
                'default' => null,
                'length' => '12,4'
            ]
        ];

        $newColumnsData = [
            'catalog_product_entity' => [
                'vendor_sku' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'default' => null,
                    'length' => 64
                ]
            ],
            'newsletter_subscriber' => [
                'legacy_subscriber_firstname' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'default' => null
                ],
                'legacy_subscriber_lastname' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'default' => null
                ]
            ],
            'sales_creditmemo' => [
                'legacy_giftcert_amount' => 'base_decimal',
                'legacy_base_giftcert_amount' => 'base_decimal',
                'legacy_base_credit_amount' => 'base_decimal',
                'legacy_credit_amount' => 'base_decimal',
                'legacy_base_credit_total_refunded' => 'base_decimal',
                'legacy_credit_total_refunded' => 'base_decimal',
            ],
            'sales_invoice' => [
                'legacy_giftcert_amount' => 'base_decimal',
                'legacy_base_giftcert_amount' => 'base_decimal',
                'legacy_base_credit_amount' => 'base_decimal',
                'legacy_credit_amount' => 'base_decimal'
            ],
            'sales_order' => [
                'legacy_giftcert_code' => 'base_text',
                'legacy_giftcert_amount' => 'base_decimal',
                'legacy_base_giftcert_amount' => 'base_decimal',
                'legacy_base_credit_amount' => 'base_decimal',
                'legacy_credit_amount' => 'base_decimal',
                'legacy_giftcert_amount_invoiced' => 'base_decimal',
                'legacy_base_giftcert_amount_invoiced' => 'base_decimal',
                'legacy_giftcert_amount_credited' => 'base_decimal',
                'legacy_base_giftcert_amount_credited' => 'base_decimal',
                'legacy_base_credit_invoiced' => 'base_decimal',
                'legacy_credit_invoiced' => 'base_decimal',
                'legacy_base_credit_refunded' => 'base_decimal',
                'legacy_credit_refunded' => 'base_decimal',
                'legacy_base_credit_total_refunded' => 'base_decimal',
                'legacy_credit_total_refunded' => 'base_decimal'
            ],
            'quote' => [
                'legacy_giftcert_code' => 'base_text',
                'legacy_base_credit_amount_used' => 'base_decimal',
                'legacy_credit_amount_used' => 'base_decimal'
            ],
            'quote_address' => [
                'legacy_giftcert_code' => 'base_text',
                'legacy_giftcert_amount' => 'base_decimal',
                'legacy_base_giftcert_amount' => 'base_decimal',
                'legacy_base_credit_amount' => 'base_decimal',
                'legacy_credit_amount' => 'base_decimal',
                'legacy_base_giftcert_balances' => 'base_decimal',
                'legacy_giftcert_balances' => 'base_decimal'
            ],
            'sales_shipment' => [
                'legacy_base_total_value' => 'base_decimal',
                'legacy_total_value' => 'base_decimal'
            ]
        ];

        foreach ($newColumnsData as $tableName => $columnsData) {
            foreach ($columnsData as $columnName => $columnData) {
                if (!is_array($columnData)) {
                    $columnData = $dataTypesConfigs[$columnData];
                }

                $this->schemaSetup->getConnection()->addColumn(
                    $this->schemaSetup->getTable($tableName),
                    $columnName,
                    array_merge($columnData, ['comment' => $this->getComment($columnName)])
                );
            }
        }

        $this->schemaSetup->endSetup();
    }

    /**
     * Returns comment based on column name.
     *
     * @param string $columnName
     *
     * @return string
     */
    protected function getComment($columnName)
    {
        $columnName = explode('_', $columnName);

        array_walk(
            $columnName,
            function (&$value, $key) {
                $value = ucfirst($value);
            }
        );

        return implode(' ', $columnName);
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
