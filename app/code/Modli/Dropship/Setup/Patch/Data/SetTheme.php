<?php

namespace Modli\Dropship\Setup\Patch\Data;


use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;

/**
 * Class AddCropPointsAttribute
 *
 * Install  crop points attribute
 *
 * @package Modli\CropImage\Setup\Patch\Data
 */
class SetTheme implements DataPatchInterface, PatchRevertableInterface
{
    /**
     * @var WriterInterface
     */
    protected $configWriter;

    /**
     * @param WriterInterface $configWriter
     */
    public function __construct(
        WriterInterface $configWriter
    ) {
        $this->configWriter = $configWriter;
    }

    /**
     * @inheritdoc
     */
    public function apply()
    {
        $this->configWriter->save('udropship/vendor/interface_theme', 'Modli/vendor');
    }

    /**
     * @inheritdoc
     */
    public function revert()
    {
        $this->configWriter->save('udropship/vendor/interface_theme', 'Opalo/kaizen');
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }
}
