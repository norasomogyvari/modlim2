<?php
namespace Modli\Dropship\Model\Pdf;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Psr\Log\LoggerInterface;
use Unirgy\Dropship\Helper\Data as HelperData;
use Unirgy\Dropship\Model\Source;
use Magento\Sales\Model\Order;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;

class Statement extends \Unirgy\Dropship\Model\Pdf\Statement
{
    /**
     * @var Order
     */
    protected $order;

    /**
     * @var ProductCollectionFactory
     */
    protected $productCollectionFactory;

    public function __construct(
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        HelperData $helper, LoggerInterface $logger,
        ScopeConfigInterface $configScopeConfigInterface,
        Source $source,
        Order $order,
        ProductCollectionFactory $productCollectionFactory,
        array $data = []
    )
    {
        $this->order = $order;
        $this->productCollectionFactory = $productCollectionFactory;

        parent::__construct($inlineTranslation, $helper, $logger, $configScopeConfigInterface, $source, $data);
    }

    public function insertGridHeader()
    {
        $this->rectangle(7.5, .4, .8, .8)
            ->moveRel(.1, .1)
            ->movePush()
            ->font('bold', 12)
            ->setAlign('left')
            ->text(__("Date"));
        if ($this->isInPayoutAmount('all', 'exclude_hide')) {
            $this->moveRel(1.2, 0)->text(__("Order#"))
                ->moveRel(1.6, 0)->text(__("Product"))
                ->moveRel(1.5, 0)->text(__("Comm (%)/Trans"))
                ->moveRel(1.6, 0)->text(__("Net Amount"))
                ->movePop(0, .4);
        } else {
            $this->moveRel(0.8, 0)->text(__("Order#"))
                ->moveRel(0.8, 0)->text(__("Customer Name"))
                ->moveRel(1.4, -0.1)->text(__("Vendor"))
                ->moveRel(0, 0.15)->text(__("SKU"))
                ->moveRel(0.8, -0.05)->text(__("Price"));

            $isInPayoutLabel = '';
            foreach (array(
                         'shipping' => __("Shipping"),
                         'tax' => __("Tax"),
                         'discount' => __("Discount")
                     ) as $iipKey=>$iipLabel
            ) {
                if (!$this->isInPayoutAmount($iipKey, 'exclude_hide')) {
                    $isInPayoutLabel .= $iipLabel.'/';
                }
            }

            $commissionPercent = $this->_hlp->getScopeConfig('udropship/tiercom/commission_percent');

            $isInPayoutLabel = substr($isInPayoutLabel, 0, -1);
            $this->moveRel(0.8, 0)->text($isInPayoutLabel);
            $this->moveRel(1.3, -0.1)->text(__("Comm"))
                ->moveRel(0, 0.15)->text(__("($commissionPercent%)"))
                ->moveRel(0.75, -0.15)->text(__("Net"))
                ->moveRel(0, 0.15)->text(__("Amount"))
                ->movePop(0, .4)
            ;
        }

        return $this;
    }

    public function insertOrder($order)
    {
        foreach (['trans_fee','com_percent','com_amount'] as $_k) {
            $order[$_k] = strpos($order[$_k], '-') === 0
                ? substr($order[$_k], 1)
                : '-'.$order[$_k];
        }

        $this->checkPageOverflow()
            ->setMaxHeight(0)
            ->font('normal', 10)
            ->movePush()
            ->setAlign('left')
            ->text($this->_hlp->formatDate($order['date'], \IntlDateFormatter::SHORT));
        if ($this->isInPayoutAmount('all', 'exclude_hide')) {
            $this->moveRel(1.2, 0)->text($order['id'])
                ->moveRel(1.6, 0)->text($order['subtotal'])
                ->moveRel(1.5, 0)->text("{$order['com_amount']} ({$order['com_percent']}%) / {$order['trans_fee']}")
                ->setAlign('right');
            if (!$this->_hlp->isStatementAsInvoice()) {
                $this->moveRel(3, 0)->text($order['total_payout']);
            } else {
                $this->moveRel(3, 0)->text($order['total_invoice']);
            }
        } else {
            $orderModel = $this->order->loadByIncrementId($order['id']);
            $customerName = $orderModel->getShippingAddress()->getName();
            $vendorSkus = $this->getOrderProductsVendorSku($orderModel, false);
            $this->moveRel(.8, 0)->text($order['id'])
                ->moveRel(0.8, 0)->text($customerName)
                ->moveRel(1.4, 0)->font('normal', 8)->text( isset($vendorSkus[$order['sku']]) ? $vendorSkus[$order['sku']] : '', null, 10)
                ->moveRel(0.8, 0)->font('normal', 10)->text($order['subtotal']);

            $isInPayoutLabel = '';
            foreach (array(
                         'shipping' => $order['shipping'],
                         'tax' => $order['tax'],
                         'discount' => '-'.@$order['discount']
                     ) as $iipKey=>$iipLabel
            ) {
                if (!$this->isInPayoutAmount($iipKey, 'exclude_hide')) {
                    $isInPayoutLabel .= $iipLabel.' / ';
                }
            }
            $isInPayoutLabel = substr($isInPayoutLabel, 0, -3);
            $this->moveRel(0.8, 0)->text($isInPayoutLabel);

            $this->moveRel(1.3, 0)->text("{$order['com_amount']}")
                ->setAlign('right')
                ->moveRel(1.2, 0)->text($order['total_payout']);
        }
        $this->movePop(0, $this->getMaxHeight()+5, 'point')
            ->moveRel(-.1, 0)
            ->line(7.5, 0, .7)
            ->moveRel(.1, .1);

        if (!empty($order['adjustments'])) {
            foreach ($order['adjustments'] as $adj) {
                $this->checkPageOverflow()
                    ->setMaxHeight(0)
                    ->font('normal', 10)
                    ->movePush()
                    ->setAlign('left')
                    ->text($this->_hlp->formatDate($order['date'], \IntlDateFormatter::SHORT))
                    ->moveRel(.8, 0)->text($order['id'])
                    ->moveRel(1, 0)->text($adj['comment'], null, 70)
                    ->setAlign('right')
                    ->moveRel(5.5, 0)->price($adj['amount'])
                    ->movePop(0, $this->getMaxHeight()+5, 'point')
                    ->moveRel(-.1, 0)
                    ->line(7.5, 0, .7)
                    ->moveRel(.1, .1);
            }
        }

        return $this;
    }

    protected function getOrderProductsVendorSku(Order $order, bool $implode = true)
    {
        $vendor = $this->getStatement()->getVendor();
        $items = $order->getAllItems();
        $productIds = array();
        foreach ($items as $item) {
            $productIds[] = $item->getProductId();
        }
        $productCollection =
            $this->productCollectionFactory->create()
                ->addFieldToFilter('entity_id', array('in' => $productIds))
                ->addFieldToFilter('type_id', 'simple')
                ->addAttributeToFilter('udropship_vendor', $vendor->getId())
                ->addAttributeToSelect('vendor_sku');

        $vendorSkus = array();

        foreach ($productCollection as $product) {
            if ($product->getVendorSku()) {
                $vendorSkus[$product->getSku()] = $product->getVendorSku();
            }
        }
        if ($implode) {
            return implode(' ', $vendorSkus);
        } else {
            return $vendorSkus;
        }
    }
}
