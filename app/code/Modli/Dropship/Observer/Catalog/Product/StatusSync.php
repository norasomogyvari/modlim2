<?php
namespace Modli\Dropship\Observer\Catalog\Product;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Catalog\Model\Product;

class StatusSync implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        /**
         * @var $product Product
         */
        $product = $observer->getEvent()->getProduct();
        if ($product->getTypeId() !== Configurable::TYPE_CODE) {
            return;
        }

        $status = $product->getStatus();
        /**
         * @var $typeInstance Configurable
         */
        $typeInstance = $product->getTypeInstance();
        $children = $typeInstance
            ->getUsedProductCollection($product)
            ->addAttributeToFilter('status', ['neq' => $status]);
        foreach ($children as $child) {
            $child->setStatus($status)->save();
        }
    }
}
