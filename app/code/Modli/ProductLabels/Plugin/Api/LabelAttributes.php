<?php
namespace Modli\ProductLabels\Plugin\Api;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\SearchResults;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Mageplaza\ProductLabels\Model\Rule;

class LabelAttributes extends \Mageplaza\ProductLabels\Plugin\Api\LabelAttributes
{
    /**
     * @param ProductRepositoryInterface $subject
     * @param ProductInterface $entity
     *
     * @return ProductInterface
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @SuppressWarnings(Unused)
     */
    public function afterGet(
        ProductRepositoryInterface $subject,
        ProductInterface $entity
    ) {
        return $entity;
    }

    /**
     * @param ProductRepositoryInterface $subject
     * @param SearchResults $searchCriteria
     *
     * @return SearchResults
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @SuppressWarnings(Unused)
     */
    public function afterGetList(
        ProductRepositoryInterface $subject,
        SearchResults $searchCriteria
    ) {
        return $searchCriteria;
    }
}
