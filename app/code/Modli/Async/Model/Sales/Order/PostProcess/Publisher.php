<?php
namespace Modli\Async\Model\Sales\Order\PostProcess;

use Magento\Framework\MessageQueue\PublisherInterface;
use Magento\Sales\Api\Data\OrderInterface;

class Publisher
{
    const TOPIC_NAME = 'order.postprocess';

    /**
     * @var PublisherInterface
     */
    protected $publisher;

    public function __construct(PublisherInterface $publisher)
    {
        $this->publisher = $publisher;
    }

    public function execute(OrderInterface $order)
    {
        return $this->publisher->publish(self::TOPIC_NAME, $order->getId());
    }
}
