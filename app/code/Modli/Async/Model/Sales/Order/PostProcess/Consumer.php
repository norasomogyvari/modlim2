<?php
namespace Modli\Async\Model\Sales\Order\PostProcess;

use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Event\ManagerInterface;
use Psr\Log\LoggerInterface;


class Consumer
{
    /**
     * @var ManagerInterface
     */
    protected $eventManager;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    public function __construct(
        ManagerInterface $eventManager,
        LoggerInterface $logger,
        OrderRepositoryInterface $orderRepository
    )
    {
        $this->eventManager = $eventManager;
        $this->logger = $logger;
        $this->orderRepository = $orderRepository;
    }

    public function processMessage(string $orderId)
    {

        $order = $this->orderRepository->get((int)$orderId);
        $this->logger->debug("Order: {$order->getIncrementId()} Async actions start");
        $this->eventManager->dispatch('sales_order_post_process', ['order' => $order]);
    }
}
