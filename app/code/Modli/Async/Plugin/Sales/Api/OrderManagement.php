<?php

namespace Modli\Async\Plugin\Sales\Api;

use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Modli\Async\Model\Sales\Order\PostProcess\Publisher;
use Psr\Log\LoggerInterface;

class OrderManagement
{
    /**
     * @var Publisher
     */
    protected $publisher;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(Publisher $publisher, LoggerInterface $logger)
    {
        $this->publisher = $publisher;
        $this->logger = $logger;
    }

    public function afterPlace(
        OrderManagementInterface $subject,
        OrderInterface $order
    ) {
        try {
            $this->publisher->execute($order);
        } catch (\Exception $e) {
            $this->logger->error($e);
        }

        return $order;
    }
}
