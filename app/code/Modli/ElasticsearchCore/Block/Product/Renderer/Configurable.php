<?php
namespace Modli\ElasticsearchCore\Block\Product\Renderer;

use Modli\ElasticsearchCore\Helper\Image as CropperHelper;

class Configurable extends \Wyomind\ElasticsearchCore\Block\Product\Renderer\Configurable
{
    /**
     * @var CropperHelper
     */
    protected $cropperHelper;

    /**
     * Configurable constructor
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\Stdlib\ArrayUtils $arrayUtils
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\ConfigurableProduct\Helper\Data $helper
     * @param \Magento\Catalog\Helper\Product $catalogProduct
     * @param \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \Magento\ConfigurableProduct\Model\ConfigurableAttributeData $configurableAttributeData
     * @param \Magento\Catalog\Helper\Image $imageHelper
     * @param \Wyomind\ElasticsearchCore\Helper\Config $config
     * @param CropperHelper $cropperHelper
     * @param array $data
     * @param \Magento\Framework\Locale\Format $localeFormat
     * @param \Magento\Customer\Model\Session $customerSession
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Stdlib\ArrayUtils $arrayUtils,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\ConfigurableProduct\Helper\Data $helper,
        \Magento\Catalog\Helper\Product $catalogProduct,
        \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\ConfigurableProduct\Model\ConfigurableAttributeData $configurableAttributeData,
        \Wyomind\ElasticsearchCore\Helper\Config $config,
        CropperHelper $cropperHelper,
        array $data = [],
        \Magento\Framework\Locale\Format $localeFormat = null,
        \Magento\Customer\Model\Session $customerSession = null
    )
    {
        parent::__construct(
            $context,
            $arrayUtils,
            $jsonEncoder,
            $helper,
            $catalogProduct,
            $currentCustomer,
            $priceCurrency,
            $configurableAttributeData,
            $config,
            $data,
            $localeFormat,
            $customerSession
        );
        $this->cropperHelper = $cropperHelper;

    }

    /**
     * @return array
     */
    protected function getOptionImages()
    {
        $images = [];
        foreach ($this->getAllowProducts() as $product) {
            $productImages = $this->helper->getGalleryImages($product) ?: [];
            foreach ($productImages as $image) {
                if ($image->getFile() == $product->getImage()) {

                    $images[$product->getId()] = [
                        'img' => $this->cropperHelper->getImageForIdx(
                            ['image' => $image->getFile(), 'crop_points' => $product->getCropPoints()],
                            $product->getStoreId()
                        ),
                        'caption' => $image->getLabel(),
                    ];
                }
            }
        }

        return $images;
    }
}
