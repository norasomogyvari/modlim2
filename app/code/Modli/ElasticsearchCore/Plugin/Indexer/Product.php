<?php
namespace Modli\ElasticsearchCore\Plugin\Indexer;

use Modli\ElasticsearchCore\Helper\Image;

class Product
{
    /**
     * @var Image
     */
    protected $imageHelper;

    /**
     * Plugin constructor.
     *
     * @param Image $imageHelper
     */
    public function __construct(
        Image $imageHelper
    ) {
        $this->imageHelper = $imageHelper;
    }

    /**
     * Plugin crops images before elastic indexing.
     *
     * @param \Wyomind\ElasticsearchCore\Model\Indexer\Product $subject
     * @param \Closure $proceed
     * @param int|string $storeId
     * @param array $ids
     *
     * @return \Generator
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function aroundExport(
        \Wyomind\ElasticsearchCore\Model\Indexer\Product $subject,
        \Closure $proceed,
        $storeId,
        $ids = []
    )
    {
        foreach ($proceed($storeId, $ids) as $products) {
            foreach ($products as &$product) {
                try {
                    $image = $this->imageHelper->getImageForIdx($product, (int)$storeId);
                    $product['image'] = $image;
                    $product['base_image'] = $image;
                } catch (\Exception $e) {

                }
            }
            yield $products;
        }
    }


}
