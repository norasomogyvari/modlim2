<?php
/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Modli\ElasticsearchCore\Console\Command;


class IndexerUpdate extends \Symfony\Component\Console\Command\Command
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_coreDate = null;

    /**
     * @var \Wyomind\ElasticsearchCore\Helper\IndexerFactory
     */
    protected $_indexerHelperFactory;

    /**
     * @var \Wyomind\ElasticsearchCore\Model\Index
     */
    protected $_indexModel = null;

    /**
     * @var \Wyomind\ElasticsearchCore\Model\ToReindexFactory
     */
    protected $_toReindexModelFactory = null;

    /**
     * Class constructor
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $coreDate
     * @param \Wyomind\ElasticsearchCore\Helper\IndexerFactory $indexerHelperFactory
     * @param \Wyomind\ElasticsearchCore\Model\Index $indexModel
     * @param \Wyomind\ElasticsearchCore\Model\ToReindexFactory $toReindexModelFactory
     */
    public function __construct(
        \Magento\Framework\Stdlib\DateTime\DateTime $coreDate,
        \Wyomind\ElasticsearchCore\Helper\IndexerFactory $indexerHelperFactory,
        \Wyomind\ElasticsearchCore\Model\Index $indexModel,
        \Wyomind\ElasticsearchCore\Model\ToReindexFactory $toReindexModelFactory
    )
    {
        $this->_coreDate = $coreDate;
        $this->_indexerHelperFactory = $indexerHelperFactory;
        $this->_indexModel = $indexModel;
        $this->_toReindexModelFactory = $toReindexModelFactory;

        parent::__construct();
    }

    /**
     * @param \Magento\Cron\Model\Schedule $schedule
     */
    public function execute(\Symfony\Component\Console\Input\InputInterface $input,
                            \Symfony\Component\Console\Output\OutputInterface $output)
    {
        $indexers = $this->_indexerHelperFactory->create()->getAllIndexers();

        /** @var \Wyomind\ElasticsearchCore\Model\Indexer\AbstractIndexer $indexer */
        foreach ($indexers as $indexer) {
            $type = $indexer->getType();

            /** @var \Wyomind\ElasticsearchCore\Model\ToReindex $toReindexModel */
            $toReindexModel = $this->_toReindexModelFactory->create();
            $indexerLastEntries = $toReindexModel->getIndexerLastEntries($type);

            $itemsToReindex = [];
            foreach ($indexerLastEntries as $toReindex) {
                //if ($toReindex['last_entry'] > $indexer->getLastIndexDate()) {
                $itemsToReindex[] = $toReindex['to_reindex'];
                //}
            }
            #var
            if (count($itemsToReindex)) {
                foreach ($itemsToReindex as $entityId) {
                    $toReindexModel->deleteEntityToReindex($type, $entityId);
                    $indexer->deleteRow($entityId);
                    $indexer->executeRow($entityId);
                }
            }

            $index = $this->_indexModel->loadByIndexerId($type);
            $index->setIndexerId($type);
            $index->setUpdateMode('schedule');
            $index->setReindexed(1);
            $datetime = $this->_coreDate->date('Y-m-d H:i:s', $this->_coreDate->gmtTimestamp());
            $index->setLastIndexDate($datetime);
            $index->save();
        }
    }

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setName('modli:elastic:indexer:update')
            ->setDescription(__('Updates Wyomind Elasticsearch Index'));
        parent::configure();
    }

}
