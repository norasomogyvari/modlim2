<?php
namespace Modli\ElasticsearchCore\Helper;

use Magento\Framework\Serialize\Serializer\Json;
use Magento\Store\Model\StoreManagerInterface;
use \Magento\Catalog\Helper\Image as CatalogImage;
use Modli\CropImage\Model\Image\Cropper;
use Wyomind\ElasticsearchCore\Helper\Config;
use Magento\Framework\View\Asset\Repository;
use Magento\Framework\View\Design\Theme\FlyweightFactory;

class Image
{
    /**
     * @var Json
     */
    protected $json;

    /**
     * @var Cropper
     */
    protected $cropper;

    /**
     * @var CatalogImage
     */
    protected $imageHelper;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Wyomind\ElasticsearchCore\Helper\Config
     */
    protected $config;

    /**
     * @var \Magento\Framework\View\Asset\Repository
     */
    protected $assetRepo;

    /**
     * @var \Magento\Framework\View\Design\Theme\FlyweightFactory
     */
    protected $themeFactory;

    /**
     * Helper constructor.
     *
     * @param Json $json
     * @param Cropper $cropper
     * @param CatalogImage $image
     * @param Config $config
     * @param StoreManagerInterface $storeManager
     * @param Repository $assetRepo,
     * @param FlyweightFactory $themeFactory
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function __construct(
        Json $json,
        Cropper $cropper,
        CatalogImage $image,
        Config $config,
        StoreManagerInterface $storeManager,
        Repository $assetRepo,
        FlyweightFactory $themeFactory
    ) {
        $this->json = $json;
        $this->cropper = $cropper;
        $this->imageHelper = $image;
        $this->config = $config;
        $this->storeManager = $storeManager;
        $this->assetRepo = $assetRepo;
        $this->themeFactory = $themeFactory;
    }

    /**
     * Processes image for elastic search index
     *
     * @param array $product
     * @param int $storeId
     *
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getImageForIdx(array $product, int $storeId): string
    {
        if (!isset($product['image'])) {
            return $this->getPlaceholder($storeId);
        }
        $image = $product['image'];

        if (!isset($product['crop_points'])) {
            return $this->getImageUrl($image, $storeId);
        }
        $cropPoints = $product['crop_points'];

        try {
            $cropPoints = $this->json->unserialize($cropPoints);

            if (isset($cropPoints[$image])) {
                $cropPoints = $cropPoints[$image];
                $image = $this->cropper->cropByPoints($cropPoints, $image);
            }
        } catch (\Exception $e) {
            return $this->getImageUrl($image, $storeId);
        }

        return $this->getImageUrl($image, $storeId);
    }

    protected function getImageUrl(string $imagePath, int $storeId): string
    {
        $path = ['media', 'catalog', 'product'];
        $path = implode(DIRECTORY_SEPARATOR, $path);
        return DIRECTORY_SEPARATOR . $path . $imagePath;
    }

    /**
     * @param int $storeId
     *
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getPlaceholder(int $storeId)
    {
        $store = $this->storeManager->getStore($storeId);
        $themeId = $this->config->getTheme($store);

        return $this->assetRepo
            ->createAsset('Magento_Catalog::images/product/placeholder/image.jpg', [
                'area' => 'frontend',
                'theme' => $this->themeFactory->create($themeId)->getThemePath(),
            ])
            ->getUrl();
    }
}
