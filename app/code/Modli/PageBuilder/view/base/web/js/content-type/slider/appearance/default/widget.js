/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'Magento_PageBuilder/js/events',
    'slick'
], function ($, events) {
    'use strict';

    return function (config, sliderElement) {
        var $element = $(sliderElement);

        /**
         * Prevent each slick slider from being initialized more than once which could throw an error.
         */
        if ($element.hasClass('slick-initialized')) {
            $element.slick('unslick');
        }

        let slickConfig = {
            autoplay: $element.data('autoplay'),
            autoplaySpeed: $element.data('autoplay-speed') || 0,
            fade: $element.data('fade'),
            infinite: $element.data('is-infinite'),
            arrows: $element.data('show-arrows'),
            dots: $element.data('show-dots'),
            slidesToShow: $element.data('slides-to-show'),
            responsive: getResponsiveConfig(),
        };

        console.log(slickConfig);


        $element.slick(slickConfig);

        // Redraw slide after content type gets redrawn
        events.on('contentType:redrawAfter', function (args) {
            if ($element.closest(args.element).length) {
                $element.slick('setPosition');
            }
        });

        function getResponsiveConfig()
        {
            let responsiveConfig = [];
            let breakpoints = [1, 2, 3];
            breakpoints.forEach(function (item) {
                let screenSize = parseInt($element.data('bp-' + item + '-screen'));
                if (screenSize <= 0 || isNaN(screenSize)) {
                    return;
                }
                let breakpointConfig = {
                    breakpoint: screenSize,
                    settings: {},
                };


                let slidesToShow = parseInt($element.data('bp-' + item + '-slides-to-show'));
                if (slidesToShow > 0) {
                    breakpointConfig['settings']['slidesToShow'] = slidesToShow;
                }
                let slidesToScroll = parseInt($element.data('bp-' + item + '-slides-to-scroll'));
                if (slidesToScroll > 0) {
                    breakpointConfig['settings']['slidesToScroll'] = slidesToScroll;
                }
                responsiveConfig.push(breakpointConfig);
            });

            return responsiveConfig;
        }
    };
});
