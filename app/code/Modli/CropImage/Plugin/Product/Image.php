<?php
namespace Modli\CropImage\Plugin\Product;

use \Modli\CropImage\Model\Image\Cropper;

/**
 * Plugin Image
 *
 * @package Modli\CropImage\Plugin\Product
 */
class Image
{
    /**
     * @var Cropper
     */
    protected $cropper;

    /**
     * Plugin constructor.
     * @param Cropper $cropper
     */
    public function __construct(Cropper $cropper)
    {
        $this->cropper = $cropper;
    }

    /**
     * Crop large product image.
     *
     * @param \Magento\Catalog\Model\Product\Image $subject
     * @param string $file
     * @return array
     */
    public function beforeSetBaseFile(\Magento\Catalog\Model\Product\Image $subject, $file)
    {
        if (empty($file)) {
            return  [$file];
        }
        $file = $this->cropper->setProduct($subject->getProduct())->cropImage($file);

        return [$file];
    }
}
