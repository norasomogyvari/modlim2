<?php
namespace Modli\CropImage\Plugin\Product;

use \Modli\CropImage\Model\Image\Cropper;
use \Magento\Catalog\Model\Product;

/**
 * Class Gallery
 *
 * @package Modli\CropImage\Plugin\Product
 */
class Gallery
{
    /**
     * @var Cropper
     */
    protected $cropper;

    /**
     * Gallery constructor.
     *
     * @param Cropper $cropper
     */
    public function __construct(Cropper $cropper)
    {
        $this->cropper = $cropper;
    }

    /**
     * Media gallery cropping
     *
     * @param Product $subject
     * @param array $result
     *
     * @return array $result
     */
    public function afterGetMediaGalleryImages(Product $subject, $result)
    {
        if (!$result instanceof \Magento\Framework\Data\Collection) {
            return $result;
        }

        $this->cropper->setProduct($subject);
        foreach ($result as &$image) {
            $this->cropper->cropGalleryItem($image);
        }

        return $result;
    }
}
