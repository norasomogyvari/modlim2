<?php
namespace Modli\CropImage\Plugin\Product;

use \Modli\CropImage\Model\Image\Cropper;

/**
 * Plugin ImageFactory
 *
 * @package Modli\CropImage\Plugin\Product
 */
class ImageFactory
{
    /**
     * @var Cropper
     */
    protected $cropper;

    /**
     * ImageFactory plugin constructor.
     *
     * @param Cropper $cropper
     */
    public function __construct(Cropper $cropper)
    {
        $this->cropper = $cropper;
    }

    /**
     * Replace product images with cropped
     *
     * @param \Magento\Catalog\Block\Product\ImageFactory $subject
     * @param \Magento\Catalog\Model\Product $product
     * @param string $imageId
     * @param array|null $attributes
     * @return array
     *
     * @SuppressWarnings("unused")
     */
    public function beforeCreate(
        \Magento\Catalog\Block\Product\ImageFactory $subject,
        \Magento\Catalog\Model\Product $product,
        string $imageId,
        array $attributes = null
    ) {
        $this->cropper->cropProductImages($product);
        return [$product, $imageId, $attributes];
    }
}
