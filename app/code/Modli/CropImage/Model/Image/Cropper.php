<?php
namespace Modli\CropImage\Model\Image;

use \Magento\Framework\Image\AdapterFactory;
use \Magento\Catalog\Api\Data\ProductInterface;
use \Magento\Framework\DataObject;
use \Magento\Framework\Serialize\Serializer\Json;
use \Magento\Framework\Filesystem;

/**
 * Class Cropper for cropping media image
 *
 * @package Modli\CropImage\Model\Image
 */
class Cropper
{
    const ATTRIBUTE_CODE = 'crop_points';

    const CROPPER_SUFFIX = '-modli-cropped-';
    /**
     * @var AdapterFactory
     */
    protected $imageAdapterFactory;

    /**
     * @var ProductInterface
     */
    protected $product;

    /**
     * @var Json
     */
    protected $jsonSerializer;

    /**
     * @var array
     */
    protected $cropPoints;

    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * Cropper constructor.
     *
     * @param AdapterFactory $imageAdapterFactory
     * @param Json $jsonSerializer
     * @param Filesystem $filesystem
     */
    public function __construct(
        AdapterFactory $imageAdapterFactory,
        Json $jsonSerializer,
        Filesystem $filesystem
    ) {
        $this->imageAdapterFactory = $imageAdapterFactory;
        $this->jsonSerializer = $jsonSerializer;
        $this->filesystem = $filesystem;
    }

    /**
     * Set product
     *
     * @param ProductInterface $product
     *
     * @return $this
     */
    public function setProduct(ProductInterface $product)
    {
        $this->product = $product;
        $this->_processCropPoints();

        return $this;
    }

    /**
     * Unserialize crop points to array
     */
    protected function _processCropPoints()
    {
        $cropPoints = $this->getProduct()->getCropPoints();
        try {
            $this->cropPoints = $this->jsonSerializer->unserialize($cropPoints);
        } catch (\Exception $e) {
            $this->cropPoints = [];
        }
    }

    /**
     * Get product
     *
     * @return ProductInterface
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Crop image if points exist
     *
     * @param string $image
     * @return string
     *
     * @phpcs:disable Magento2.Functions.DiscouragedFunction
     */
    public function getCroppedPath(array $croppedPoints, string $image)
    {
        if (empty($croppedPoints)) {
            return $image;
        }
        $croppedPoints = implode('-', $croppedPoints);
        $croppedImageSuffix = self::CROPPER_SUFFIX . ($croppedPoints);

        $extension = pathinfo($image, PATHINFO_EXTENSION);
        $withoutExtension = substr($image, 0, -1 * (strlen($extension) + 1));
        return  $withoutExtension . $croppedImageSuffix . '.' .$extension;
    }

    /**
     * Crop image if points exist
     *
     * @param string $image
     * @return string
     *
     * @phpcs:disable Magento2.Functions.DiscouragedFunction
     */
    public function cropImage(string $image)
    {
        if (!$this->isValid($image)) {
            return $image;
        }
        $points = $this->cropPoints[$image];

        return $this->cropByPoints($points, $image);
    }

    public function cropByPoints(array $points, string $image)
    {
        $croppedPath = $this->getCroppedPath($points, $image);
        $croppedAbsolutePath = $this->filesystem
                ->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)
                ->getAbsolutePath('catalog/product') . $croppedPath;

        if (!file_exists($croppedAbsolutePath)) {
            try {
                $originAbsolutePath = $this->filesystem
                        ->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)
                        ->getAbsolutePath('catalog/product') . $image;

                $imageResize = $this->imageAdapterFactory->create();
                $imageResize->open($originAbsolutePath);

                $originalWidth = $imageResize->getOriginalWidth();
                $originalHeight = $imageResize->getOriginalHeight();

                $imageResize->crop(
                    (int)$points[1],
                    (int)$points[0],
                    (int)($originalWidth - $points[2]),
                    (int)($originalHeight - $points[3])
                );
                $imageResize->save($croppedAbsolutePath);
            } catch (\Exception $e) {
                return $image;
            }
        }

        return $croppedPath;
    }

    /**
     * Validate is image cropable
     *
     * @param string $image
     * @return bool
     */
    protected function isValid($image)
    {
        if (empty($this->cropPoints)) {
            return false;
        }

        if (!isset($this->cropPoints[$image])) {
            return false;
        }

        if (strpos($image, self::CROPPER_SUFFIX) !== false) {
            return false;
        }

        return true;
    }

    /**
     * Crop gallery item image
     *
     * @param DataObject $image
     */
    public function cropGalleryItem(DataObject $image)
    {
        $sourceFile = $image['file'];
        $croppedFile = $this->cropImage($sourceFile);
        if (!$croppedFile || ($croppedFile == $sourceFile)) {
            return;
        }
        foreach ($image->getData() as $key => $value) {
            if (is_string($value)) {
                $image->setData($key, str_replace($sourceFile, $croppedFile, $value));
            }
        }
    }

    public function cropProductImages(ProductInterface $product)
    {
        $imageKeys = ['image', 'small_image', 'thumbnail'];
        $this->setProduct($product);

        foreach ($imageKeys as $imageKey) {
            $image = $product->getData($imageKey);
            if ($image) {
                $image = $this->cropImage($image);
            }
            $product->setData($imageKey, $image);
        }
    }

}
