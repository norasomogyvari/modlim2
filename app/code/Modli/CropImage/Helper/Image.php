<?php
namespace Modli\CropImage\Helper;

/**
 * Class rewrite in order to add product to image model
 *
 * @package Modli\CropImage\Helper
 */
class Image extends \Magento\Catalog\Helper\Image
{
    /**
     * @inheritDoc
     */
    public function init($product, $imageId, $attributes = [])
    {
        parent::init($product, $imageId, $attributes);
        $this->_getModel()->setProduct($product);

        return $this;
    }
}
