define([
    'jquery',
    'cropPointsModel',
    'productGallery',
    'croppie'
], function ($, cropPointsModel, productGallery, croppie) {
    $.widget('modli.cropImage', {
        _create: function() {
            this._initCropper();
        },

        _initCropper: function() {
            var $this = this;
            $(document).on('openDialog', '.gallery.ui-sortable', function (e) {
                var attrCode = '';
                try {
                    attrCode = $(e.target).closest('.gallery').data('attr-code')
                } catch (e) {

                }

                cropPointsModel.key = attrCode;

                var imageUrl = $('.image-panel-preview img').attr('src');
                imageUrl = imageUrl.replace('"', '');
                var imagePath = imageUrl.split('media/catalog/product');
                imagePath = imagePath[1];
                $this.file = imagePath;

                var img = new Image();
                img.src = imageUrl;

                img.onload = function() {
                    var points = $this.getImagePoints(imagePath);
                    var boundaryWidth = 300;

                    var baseWidth = boundaryWidth * 0.75;
                    var baseHeight = baseWidth / 0.715;

                    var realHeight = img.naturalHeight;
                    var realWidth = img.naturalWidth;

                    var boundaryHeight = (boundaryWidth * realHeight) / realWidth;
                    if (realWidth > realHeight) {
                        boundaryHeight = (boundaryWidth * realWidth) / realHeight;
                    }

                    var options = {
                        customClassstring: 'cropper',
                        enforceBoundary: true,
                        viewport: {width: baseWidth, height: baseHeight, type: 'square'},
                        boundary: {width: boundaryWidth, height: boundaryHeight, type: 'square'},
                        update: function (data) {
                            $this.points = data.points;
                            $this.save();
                            console.log($this.points);
                        },
                        points: points,
                        enforceBoundaryboolean: false,
                        maxZoom: 0.1338,
                    };

                    var crop = $('.image-panel-preview img').croppie(options);
                }
            });
        },

        save: function() {
            var value = this.getValue();
            var file = this.file;
            var points = this.points;
            if (this.points.length == 0) {
                return this;
            }

            value[file] = points;
            value = JSON.stringify(value);

            cropPointsModel.save(value);
        },

        getValue: function() {
            var value = cropPointsModel.get();

            try {
                value = JSON.parse(value);
            } catch (e) {
                value = {};
            }

            return value;
        },

        getImagePoints: function(file) {
            var value = this.getValue();
            console.log(value);
            if (value && value[file] !== undefined) {
                return value[file];
            } else {
                return false;
            }
        },
    });

    return $.modli.cropImage;
});
