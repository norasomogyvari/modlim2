define([
    'uiRegistry'
], function (registry) {
    return {
        source: "Registry",
        get: function() {
            return this.getField().value()
        },
        save: function(value) {
            this.getField().value(value)
        },
        getField: function () {
            return registry.get('inputName = product[crop_points]')
        },
        registry: registry
    };
});
