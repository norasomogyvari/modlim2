define([
    'jquery'
], function ($) {
    return {
        key: '',
        getField: function() {
            $(':focus').blur();
            if (this.key) {
                inputName = 'product[media_gallery_cfg_images][' + this.key + '][crop_points]';
            } else {
                inputName = 'product[crop_points]';
            }
            return $('textarea[name="' + inputName + '"]' ).focus();
        },
        source: "jQuery",
        get: function() {
            return this.getField().val()
        },
        save: function(value) {
            this.getField().val(value)
        }
    };
});
