<?php
namespace Modli\CropImageUdropship\Plugin\DropshipVendorProduct\Helper;

class Data
{
    public function beforeProcessQcMediaChange(\Unirgy\DropshipVendorProduct\Helper\Data $subject, $prod, $qcProd, $isNew)
    {
        $cfgFirstAttrs = $subject->getCfgFirstAttributes($prod, $isNew);
        $cfgFirstAttrKey = '';
        foreach ($cfgFirstAttrs as $__ca) {
            $__id = $__ca->getAttributeId();
            $__code = $__ca->getAttributeCode();
            $cfgFirstAttrKey .= $__id.'-'.$qcProd->getData($__code).'-';
        }
        $cfgFirstAttrKey = rtrim($cfgFirstAttrKey, '-');
        $mediaImgKey = sprintf('media_gallery_cfg_images/'.$cfgFirstAttrKey);
        $mediaGallery = $prod->getData($mediaImgKey);
        $qcProd->setData('crop_points', $mediaGallery['crop_points']);
    }
}
