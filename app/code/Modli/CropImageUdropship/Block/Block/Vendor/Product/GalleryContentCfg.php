<?php
namespace Modli\CropImageUdropship\Block\Block\Vendor\Product;

class GalleryContentCfg extends \Unirgy\DropshipVendorProduct\Block\Vendor\Product\GalleryContentCfg
{
    protected $_template = 'Modli_CropImageUdropship::unirgy/udprod/vendor/product/gallery.phtml';

    public function getCropPoints()
    {
        return $this->_product->getCropPoints();
    }


}
