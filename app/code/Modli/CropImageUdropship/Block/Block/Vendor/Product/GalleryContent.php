<?php

namespace Modli\CropImageUdropship\Block\Block\Vendor\Product;

use Magento\Backend\Block\Template\Context;
use Magento\Catalog\Model\Product\Media\Config;
use Magento\Framework\Json\EncoderInterface;

class GalleryContent extends \Unirgy\DropshipVendorProduct\Block\Vendor\Product\GalleryContent
{
    protected $_template = 'Modli_CropImageUdropship::unirgy/udprod/vendor/product/gallery.phtml';

    public function getCropPoints()
    {
        return $this->_prodHlp->getProductToEdit()->getCropPoints();
    }
}
