<?php
declare(strict_types=1);

namespace Modli\ConfigurableProduct\Api;

interface ChildrenAttributeRepositoryInterface
{
    /**
     * Return array of attribute options ids
     *
     * @param string $sku
     * @param string $attributeCode
     * @return array
     */
    public function getChildrenAttributeValues(string $sku, string $attributeCode): array;
}
