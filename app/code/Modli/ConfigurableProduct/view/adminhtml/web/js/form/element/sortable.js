define([
    'Magento_Ui/js/form/element/textarea',
    'jquery',
    'ko',
    'mage/url',
    'underscore',
    'text!Modli_ConfigurableProduct/template/form/element/sortable/list-item.html'
],function (Textarea, $, ko, url, _, listItemTemplate) {
    return Textarea.extend({
        initialize: function () {
            this._super();
            this.listHtml = ko.observable('');
            this.containerPrefix = 'sortable';

            fetch(
                url.build(`/rest/V1/configurable-products/${this.sku}/children/attribute-values/${this.attributeCode}`),
            ).then(
                response => response.json()
            ).then(data => {
                this.buildElement(data);
            })
            /*.catch(err => console.log(err))*/;

            return this;
        },
        buildElement: function (data) {
            data = this.sortData(data);
            this.buildSortedList(data);
        },

        sortData: function(data) {
            let values = this.getValue();
            values = values.reverse();
            //Sort values in option
            let sortedValues = [];
            return  _.sortBy(data, function (element) {
               return values.indexOf(parseInt(element.value))
            }).reverse();
        },

        buildSortedList: function (data) {
            let elementId = this.uid;
            let listHtml = '';
            data.forEach(function (option) {
                listHtml += _.template(listItemTemplate)({option: option});
            });
            this.listHtml(listHtml);
        },
        applySortableWidget: function() {
            let component = this;
            let sortableContainerId = this.containerPrefix + this.uid;
            let list = $('#' + sortableContainerId);

            $('#' + sortableContainerId).sortable({
                stop: serializeSorting
            });
            function serializeSorting() {
                let value = [];
                list.find('li').each(function () {
                    value.push($(this).data('value-id'));
                });
                component.value(JSON.stringify(value));
            }
            serializeSorting();
        },
        getValue: function () {
            try {
                return JSON.parse(this.value());
            } catch (e) {
                console.warn(e, this.value());
                return [];
            }
        },
    });
});
