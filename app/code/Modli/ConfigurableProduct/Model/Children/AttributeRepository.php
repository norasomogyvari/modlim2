<?php
declare(strict_types=1);

namespace Modli\ConfigurableProduct\Model\Children;

use Modli\ConfigurableProduct\Api\ChildrenAttributeRepositoryInterface;
use Magento\ConfigurableProduct\Api\LinkManagementInterface;
use Magento\Eav\Api\AttributeRepositoryInterface;

class AttributeRepository implements ChildrenAttributeRepositoryInterface
{
    /**
     * @var LinkManagementInterface
     */
    protected $linkManagement;

    /**
     * @var AttributeRepositoryInterface
     */
    protected $attributeRepository;

    /**
     * AttributeRepository constructor.
     * @param LinkManagementInterface $linkManagement
     * @param AttributeRepositoryInterface $attributeRepository
     */
    public function __construct(
        LinkManagementInterface $linkManagement,
        AttributeRepositoryInterface $attributeRepository
    )
    {
        $this->linkManagement = $linkManagement;
        $this->attributeRepository = $attributeRepository;
    }

    /**
     * @inheritDoc
     */
    public function getChildrenAttributeValues(string $sku, string $attributeCode): array
    {
        $children = $this->linkManagement->getChildren($sku);
        $attributeValues = [];
        foreach ($children as $child) {
            $attributeValues[] = $child->getData($attributeCode);
        }
        $attributeValues = array_unique($attributeValues);

        return $this->prepareResult($attributeValues, $attributeCode);
    }

    protected function prepareResult(array $values, string $attributeCode): array
    {
        /**
         * @var \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource $sourceModel
         */
        $options = $this->attributeRepository->get(
            \Magento\Catalog\Model\Product::ENTITY,
            $attributeCode
        )->getOptions();

        $result = [];
        foreach ($options as $option) {
            if (in_array($option->getValue(), $values)) {
                $result[] = $option->getData();
            }
        }

        return $result;
    }
}
