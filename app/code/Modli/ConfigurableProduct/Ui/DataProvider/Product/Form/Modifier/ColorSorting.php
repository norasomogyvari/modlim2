<?php
declare(strict_types=1);

namespace Modli\ConfigurableProduct\Ui\DataProvider\Product\Form\Modifier;

use Magento\Framework\App\RequestInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Catalog\Model\ProductRepository;

class ColorSorting extends AbstractModifier
{
    /**
     * @var ArrayManager
     */
    protected $arrayManager;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @param RequestInterface $request
     * @param ArrayManager $arrayManager
     * @param ProductRepository $productRepository
     */
    public function __construct(
        RequestInterface $request,
        ArrayManager $arrayManager,
        ProductRepository $productRepository
    ) {
        $this->arrayManager = $arrayManager;
        $this->request = $request;
        $this->productRepository = $productRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {
        $meta = $this->customizeCustomAttrField($meta);

        return $meta;
    }

    public function customizeCustomAttrField(array $meta)
    {
        $fieldCode = \Modli\ConfigurableProduct\Helper\Data::ATTRIBUTE_CODE;
        $elementPath = $this->arrayManager->findPath($fieldCode, $meta, null, 'children');
        $containerPath = $this->arrayManager->findPath(static::CONTAINER_PREFIX . $fieldCode, $meta, null, 'children');

        if (!$elementPath) {
            return $meta;
        }

        $meta = $this->arrayManager->merge(
            $containerPath,
            $meta,
            [
                'arguments' => [
                    'data' => [
                        'config' => [
                        ],
                    ],
                ],
                'children'  => [
                    $fieldCode => [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'component' => 'Modli_ConfigurableProduct/js/form/element/sortable',
                                    'elementTmpl'   => 'Modli_ConfigurableProduct/form/element/sortable',
                                    'attributeCode' => 'product_color',
                                    'sku' => $this->getSku((int)$this->request->getParam('id'))
                                ],
                            ],
                        ],
                    ]
                ]
            ]
        );

        return $meta;
    }

    protected function getSku(int $productId)
    {
        return $this->productRepository->getById($productId)->getSku();
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        return $data;
    }
}
