<?php
namespace Modli\CustomBundle\Model;

use Magento\Bundle\Model\ResourceModel\Option\CollectionFactory;
use Magento\Bundle\Model\ResourceModel\Selection\CollectionFactory as SelectionCollectionFactory;
use Magento\Catalog\Model\Product;
use Magento\Store\Model\StoreManagerInterface;

class Processor implements \Modli\CustomBundle\Api\ProcessorInterface
{
    /**
     * @var CollectionFactory
     */
    protected $optionCollectionFactory;

    /**
     * @var SelectionCollectionFactory
     */
    protected $selectionCollectionFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Processor constructor.
     *
     * @param CollectionFactory $optionCollectionFactory
     * @param SelectionCollectionFactory $selectionCollectionFactory
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        CollectionFactory $optionCollectionFactory,
        SelectionCollectionFactory $selectionCollectionFactory,
        StoreManagerInterface $storeManager
    )
    {
        $this->optionCollectionFactory = $optionCollectionFactory;
        $this->selectionCollectionFactory = $selectionCollectionFactory;
        $this->storeManager = $storeManager;

    }

    public function getOptionAttributeValues(int $productId, string $attributeCode): array
    {
        $storeId = $this->storeManager->getStore()->getId();
        $optionAttributeValues = [];
        $options = $this->optionCollectionFactory->create()
            ->joinValues($storeId)
            ->setPositionOrder()
            ->addFieldToFilter('parent_id', $productId);
        foreach ($options as $option) {
            $optionAttributeValues[$option->getId()] = [
                'title' => $option->getTitle(),
                'id' => $option->getId()
            ];
        }

        $selections = $this->selectionCollectionFactory
            ->create()
            ->addAttributeToSelect($attributeCode);
        $selections->getSelect()->where('selection.parent_product_id = ?', $productId);

        foreach ($selections as $selection) {
            $optionId = $selection->getOptionId();
            $value = $selection->getData($attributeCode);
            if (isset($optionAttributeValues[$optionId]['values'][$value])) {
                 continue;
            }
            $optionAttributeValues[$optionId]['values'][$value] = [
                'id' => $value,
                'title' => $this->getAttributeValueTitle($selection, $attributeCode),
            ];
        }

        foreach ($optionAttributeValues as $optionId => &$values) {
            $values['values'] = array_values($values['values']);
        }

        return $optionAttributeValues;
    }

    protected function getAttributeValueTitle(Product $product, string $attributeCode)
    {
        return $product->getResource()->getAttribute($attributeCode)->getFrontend()->getValue($product);
    }
}
