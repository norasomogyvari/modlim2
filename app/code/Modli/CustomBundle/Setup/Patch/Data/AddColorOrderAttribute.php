<?php

namespace Modli\CustomBundle\Setup\Patch\Data;

use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use \Magento\Catalog\Model\Config;
use \Magento\Eav\Api\AttributeManagementInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

/**
 * Class AddCropPointsAttribute
 *
 * Install  crop points attribute
 *
 * @package Modli\CropImage\Setup\Patch\Data
 */
class AddColorOrderAttribute implements DataPatchInterface
{
    const ATTRIBUTE_CODE = \Modli\CustomBundle\Helper\Data::ATTRIBUTE_CODE;

    /** @var ModuleDataSetupInterface */
    private $moduleDataSetup;

    /** @var EavSetupFactory */
    private $eavSetupFactory;

    /** @var Config */
    private $config;

    /** @var AttributeManagementInterface */
    private $attributeManagement;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory $eavSetupFactory
     * @param Config $config
     * @param AttributeManagementInterface $attributeManagement
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory,
        Config $config,
        AttributeManagementInterface $attributeManagement
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->config = $config;
        $this->attributeManagement = $attributeManagement;
    }

    /**
     * @inheritdoc
     */
    public function apply()
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $eavSetup->removeAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            self::ATTRIBUTE_CODE
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            self::ATTRIBUTE_CODE,
            [
                'type' => 'text',
                'backend' => '',
                'frontend' => '',
                'label' => 'Bundle Color Order',
                'input' => 'textarea',
                'input_renderer' => 'Modli\CustomBundle\Block\Adminhtml\Product\Helper\Form\SortableSet',
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false,
                'apply_to' => 'bundle'
            ]
        );
        $this->assignAttributeToGroup(
            self::ATTRIBUTE_CODE,
            'Migration_Design'
        );
    }


    protected function assignAttributeToGroup(string $attributeCode, string $attributeGroupName)
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $entityTypeId = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
        $attributeSetIds = $eavSetup->getAllAttributeSetIds($entityTypeId);
        foreach ($attributeSetIds as $attributeSetId) {
            if ($attributeSetId) {
                $groupId = $this->config->getAttributeGroupId($attributeSetId, $attributeGroupName);
                if (empty($groupId)) continue;
                $this->attributeManagement->assign(
                    \Magento\Catalog\Model\Product::ENTITY,
                    $attributeSetId,
                    $groupId,
                    $attributeCode,
                    999
                );
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function revert()
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $eavSetup->removeAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            self::ATTRIBUTE_CODE
        );
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }
}
