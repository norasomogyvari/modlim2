<?php
namespace Modli\CustomBundle\Api;

interface ProcessorInterface
{
    /**
     * @param int $productId
     * @param string $attributeCode
     * @return array
     */
    public function getOptionAttributeValues(int $productId, string $attributeCode): array;
}
