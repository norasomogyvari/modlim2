define([
    'Magento_Ui/js/form/element/textarea',
    'jquery',
    'ko',
    'mage/url',
    'uiRegistry',
    'text!Modli_CustomBundle/template/form/element/sortable-group/list.html'
],function (Textarea, $, ko, url, registry, listTemplate) {
    return Textarea.extend({
        initialize: function () {
            this._super();
            this.listGroupHtml = ko.observable('');
            this.containerPrefix = 'sortable';
            let productId = 90289;
            fetch(
                url.build(`/rest/V1/custom-bundle/option-attribute-values?productId=${productId}&attributeCode=${this.attributeCode}`),
            ).then(
                response => response.json()
            ).then(data => {
                this.buildElement(data);
            })
            /*.catch(err => console.log(err))*/;

            return this;
        },
        buildElement: function (data) {
            this.sortData(data);
            this.buildSortedList(data);
        },

        sortData: function(data) {
            let value = this.getValue();
            for (let optionId in value) {
                //Find option for this sorted values
                let optionBlock = data.find(el => {
                    return el.id == optionId;
                });

                if (typeof optionBlock === "undefined") {
                    continue;
                }

                //Sort values in option
                let sortedValues = value[optionId];

                for (let i = sortedValues.length - 1; i >= 0; i--) {
                    let values = optionBlock.values;
                    let valueIndex = values.findIndex(valueObject => {
                        return valueObject.id == sortedValues[i];
                    });
                    if (typeof valueIndex === "undefined") {
                        return;
                    }

                    let valueElement = values[valueIndex];
                    values.splice(valueIndex, 1);
                    values.unshift(valueElement);
                }
            }
        },

        buildSortedList: function (data) {
            let elementId = this.uid;
            let listGroupHtml = '';
            data.forEach(function (option) {
                let listHtml = _.template(listTemplate)({'option': option});
                listGroupHtml += listHtml;
            });
            this.listGroupHtml(listGroupHtml);
        },
        applySortableWidget: function() {
            let component = this;
            let sortableContainerId = this.containerPrefix + this.uid + ' ul';
            let sortableLists = $('#' + sortableContainerId);

            function serializeSorting() {
                let value = {};
                sortableLists.each(function () {
                    let optionId = $(this).data('option-id');
                    if ( typeof value[optionId] === "undefined") {
                        value[optionId] = [];
                    }
                    $(this).find('li').each(function () {
                        value[optionId].push($(this).data('value-id'));
                    })
                });
                component.value(JSON.stringify(value));
            }
            serializeSorting();
            sortableLists.each(function() {
                $(this).sortable({
                    stop: serializeSorting
                });
            });
        },
        getValue: function () {
            try {
                return JSON.parse(this.value());
            } catch (e) {
                console.warn(e, this.value());
                return {};
            }
        },
    });
});
