<?php
namespace Modli\Dev\Model\NewRelic;

class Transaction
{
    /**
     * @var array Array of started transactions flags
     */
    public $started = [];

    /**
     * @param string $name Name of transaction to start
     */
    public function start(string $name): void
    {
        if ($this->canStart($name)) {
            newrelic_name_transaction($name);
            $this->started[$name] = true;
        }
    }

    /**
     * Can if transaction can be started
     * @param string $name Name of transaction to check
     * @return bool
     */
    protected function canStart(string $name): bool
    {
        return extension_loaded('newrelic') && !isset($this->started[$name]);
    }
}
