<?php
namespace Modli\Dev\Plugin\Grapqhl;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Modli\Dev\Model\NewRelic\Transaction;

class NewRelic
{
    const PREFIX = 'graphql-';
    /**
     * @var Transaction
     */
    protected $transaction;

    /**
     * NewRelic constructor.
     *
     * @param Transaction $transaction
     */
    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * @param ResolverInterface $subject
     * @param Field $field
     * @param $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     */
    public function beforeResolve(ResolverInterface $subject, Field $field,
                                  $context,
                                  ResolveInfo $info,
                                  array $value = null,
                                  array $args = null)
    {
        $transactionName = $info->path[0] ?? 'graphql';
        $this->transaction->start(self::PREFIX . $transactionName);
    }
}
