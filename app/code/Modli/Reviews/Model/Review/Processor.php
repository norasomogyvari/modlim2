<?php
namespace Modli\Reviews\Model\Review;

use Modli\Reviews\Api\RatingRepositoryInterface;
use Modli\Reviews\Api\ReviewRepositoryInterface;

class Processor
{
    protected $reviewRepository;
    protected $ratingRepository;

    public function __construct(
        RatingRepositoryInterface $ratingRepository,
        ReviewRepositoryInterface $reviewRepository
    ) {
        $this->reviewRepository = $reviewRepository;
        $this->ratingRepository = $ratingRepository;
    }

    public function saveReviews(array $reviews): array
    {
        $result = [];
        foreach ($reviews as $review) {
            $reviewModel = $this->reviewRepository->save($review);
            $rating = $review['rating'];
            if (is_null($review['product_id'])) {
                continue;
            }
            $this->ratingRepository->cleanByReview($reviewModel->getId());
            foreach ($rating as $ratingId => $value) {
                $this->ratingRepository->save($value, $review['product_id'], $reviewModel->getId(), $ratingId);
            }

            $reviewModel->aggregate();
            $result[] = $reviewModel;
        }

        return $result;
    }
}
