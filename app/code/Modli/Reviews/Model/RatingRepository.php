<?php
namespace Modli\Reviews\Model;

use Modli\Reviews\Api\RatingRepositoryInterface;
use Magento\Review\Model\RatingFactory;
use Magento\Review\Model\Rating;
use Magento\Review\Model\ResourceModel\Rating\Option\Vote\CollectionFactory;

class RatingRepository implements RatingRepositoryInterface
{
    /**
     * Rating model factory
     *
     * @var \Magento\Review\Model\RatingFactory
     */
    protected $ratingFactory;

    /**
     * Rating Option Vote Collection Factory
     *
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var Rating[]
     */
    protected $cache = [];

    public function __construct(
        RatingFactory $ratingFactory,
        CollectionFactory $collectionFactory
    )
    {
        $this->ratingFactory = $ratingFactory;
        $this->collectionFactory = $collectionFactory;
    }

    public function save(int $optionId, int $productId, int $reviewId, int $ratingId): Rating
    {
        return $this->ratingFactory->create()
            ->setRatingId($ratingId)
            ->setReviewId($reviewId)
            ->addOptionVote($optionId, $productId);
    }

    /**
     * @param string $code
     * @return Rating
     */
    public function getRatingByCode(string $code): Rating
    {
        if (!isset($this->cache[$code])) {
            $this->cache[$code] = $this->ratingFactory->create()
                ->load($code, 'rating_code');
        }

        return $this->cache[$code];
    }

    public function cleanByReview(int $reviewId)
    {
        $ratingVotes = $this->collectionFactory->create()
            ->addFieldToFilter('review_id', $reviewId);

        foreach ($ratingVotes as $ratingVote) {
            /**
             * @var $ratingVote \Magento\Review\Model\Rating\Option\Vote
             */
            $ratingVote->delete();
        }
    }
}
