<?php
namespace Modli\Reviews\Model\Loader;

use Modli\Reviews\Api\LoaderInterface;
use Modli\Reviews\Api\Review\Api\PagedInterface;
use Modli\Reviews\Api\RatingRepositoryInterface;
use Modli\Reviews\Api\ReviewRepositoryInterface;
use Modli\Reviews\Api\Review\MapperInterface;
use \Modli\Reviews\Logger\Logger;
use \Modli\Reviews\Model\Review\Processor;

class Page implements LoaderInterface
{
    protected $pagedReviewApi;
    protected $logger;
    protected $processor;

    public function __construct(
        PagedInterface $pagedReviewApi,
        Logger $logger,
        Processor $processor
    ) {
        $this->pagedReviewApi = $pagedReviewApi;
        $this->logger = $logger;
        $this->processor = $processor;
    }

    public function load()
    {
        $this->logger->debug('Reviews page by page full update start');
        $this->logger->debug('Source: ' . get_class($this->pagedReviewApi));

        $pageCount = $this->getPageCount();
        $this->logger->debug("Total Page count: $pageCount");

        for ($i = 1; $i <= $pageCount; $i++) {
            $this->logger->debug("Page $i downloading...");
            $pageReviews = $this->getPageReviews($i);
            $this->logger->debug("Page $i reviews downloaded. Total count: " . count($pageReviews));
            $this->logger->debug("Saving page $i reviews start...");
            $this->savePageReviews($pageReviews);
            $this->logger->debug("Saving page $i reviews done.");
        }
    }

    /**
     * Reviews API pages count.
     *
     * @return int
     */
    protected function getPageCount(): int
    {
        return $this->pagedReviewApi->getPageCount();
    }

    /**
     * Return page reviews
     *
     * @param int $pageNum Number of page to retrieve reviews.
     *
     * @return array
     */
    protected function getPageReviews(int $pageNum): array
    {
        return $this->pagedReviewApi->getPageReviews($pageNum);
    }

    protected function savePageReviews(array $reviews)
    {
        $this->processor->saveReviews($reviews);
    }
}
