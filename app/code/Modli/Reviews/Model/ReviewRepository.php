<?php

namespace Modli\Reviews\Model;

use Magento\Review\Model\Review;
use Magento\Store\Model\Store;
use Modli\Reviews\Api\ReviewRepositoryInterface;
use Magento\Review\Model\ReviewFactory;
use Magento\Review\Model\ResourceModel\Review\CollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

class ReviewRepository implements ReviewRepositoryInterface
{
    /**
     * @var Magento\Review\Model\ReviewFactory;
     */
    protected $reviewFactory;

    /**
     * @var CollectionFactory
     */
    protected $reviewCollectionFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManagement;

    protected $storeIds = [];
    /**
     * ReviewRepository constructor.
     *
     * @param ReviewFactory $reviewFactory\
     */
    public function __construct(
        ReviewFactory $reviewFactory,
        CollectionFactory $reviewCollectionFactory,
        StoreManagerInterface $storeManagement
    )
    {
        $this->reviewFactory = $reviewFactory;
        $this->reviewCollectionFactory = $reviewCollectionFactory;
        $this->storeManagement = $storeManagement;
    }

    protected function getStoreAllIds()
    {
        if (empty($this->storeIds)) {
            $stores = $this->storeManagement->getStores();
            foreach ($stores as $store) {
                $this->storeIds[] = $store->getId();
            }
        }

        return $this->storeIds;
    }

    /**
     * @param array $reviewData Reviews data in array format to save in the DB.
     */
    public function save(array $reviewData)
    {
        $review = $this
            ->getReview($reviewData)
            ->addData($reviewData);

        $setForceCreatedAt = isset($reviewData['created_at']) && is_null($review->getId());

        $review->setEntityId($review->getEntityIdByCode(Review::ENTITY_PRODUCT_CODE))
            ->setEntityPkValue($reviewData['product_id'])
            ->setStoreId(Store::DEFAULT_STORE_ID)
            ->setStores($this->getStoreAllIds())
            ->save();

        if ($setForceCreatedAt) {
            $review->setCreatedAt($reviewData['created_at'])->save();
        }

        return $review;
    }

    protected function getReview($reviewData)
    {
        $reviewCollection =
            $this->reviewCollectionFactory
                ->create()
                ->addFieldToFilter('source_code', $reviewData['source_code'])
                ->addFieldToFilter('source_id', $reviewData['source_id']);

        if ($review = $reviewCollection->getFirstItem()) {
            return $review;
        } else {
            return $this->reviewFactory->create();
        }
    }
}
