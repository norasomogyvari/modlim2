<?php
namespace Modli\Reviews\Model\ResourceModel\Review;

use Magento\Review\Model\ResourceModel\Review\Collection as ReviewCollection;

class Collection extends ReviewCollection
{
    /**
     * Add entity filter
     *
     * @param int|string $entity
     * @param int|array $pkValue
     * @return \Magento\Review\Model\ResourceModel\Review\Collection
     */
    public function addEntityFilter($entity, $pkValue)
    {
        $reviewEntityTable = $this->getReviewEntityTable();
        if (is_numeric($entity)) {
            $this->addFilter('entity', $this->getConnection()->quoteInto('main_table.entity_id=?', $entity), 'string');
        } elseif (is_string($entity)) {
            $this->_select->join(
                $reviewEntityTable,
                'main_table.entity_id=' . $reviewEntityTable . '.entity_id',
                ['entity_code']
            );

            $this->addFilter(
                'entity',
                $this->getConnection()->quoteInto($reviewEntityTable . '.entity_code=?', $entity),
                'string'
            );
        }

        if (is_array($pkValue)) {
            $pkValue = implode(',', $pkValue);
            $this->addFilter(
                'entity_pk_value',
                "main_table.entity_pk_value IN ($pkValue)",
                'string'
            );
        } else {
            $this->addFilter(
                'entity_pk_value',
                $this->getConnection()->quoteInto('main_table.entity_pk_value=?', $pkValue),
                'string'
            );
        }

        return $this;
    }
}
