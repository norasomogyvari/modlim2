<?php
namespace Modli\Reviews\Helper;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\App\ResourceConnection;

class Product
{
    /**
     * @var ResourceConnection
     */
    protected $connection;

    /**
     * @var ProductRepositoryInterface
     */
    protected $repository;

    /**
     * Customer constructor.
     *
     * @param ProductRepositoryInterface $repository
     */
    public function __construct(
        ProductRepositoryInterface $repository,
        ResourceConnection $connection
    ) {
        $this->connection = $connection;
        $this->repository = $repository;
    }

    public function getIdBySku($sku)
    {
        try {
            $table = $this->connection->getTableName('catalog_product_entity');
            $sql = "SELECT entity_id FROM $table WHERE sku = '$sku' LIMIT 0, 1";
            $result = $this->connection->getConnection()->fetchCol($sql);
            return $result[0];
        } catch (\Exception $e) {
            return null;
        }
    }
}

