<?php
namespace Modli\Reviews\Helper;

use \Magento\Customer\Api\CustomerRepositoryInterface;
use \Magento\Framework\Exception\NoSuchEntityException;

class Customer
{
    /**
     * @var CustomerRepositoryInterface
     */
    protected $repository;

    /**
     * Customer constructor.
     *
     * @param CustomerRepositoryInterface $repository
     */
    public function __construct(
        CustomerRepositoryInterface $repository
    ) {
        $this->repository = $repository;
    }

    public function getIdByEmail($email)
    {
        try {
            return $this->repository->get($email)->getId();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }
}
