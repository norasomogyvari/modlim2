<?php
namespace Modli\Reviews\Api;
use Magento\Review\Model\Rating;

interface RatingRepositoryInterface
{
    public function save(int $optionId, int $productId, int $reviewId, int $ratingId): Rating;

    public function cleanByReview(int $reviewId);
}
