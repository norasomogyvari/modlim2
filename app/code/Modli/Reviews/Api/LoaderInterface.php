<?php
namespace Modli\Reviews\Api;

interface LoaderInterface
{
    public function load();
}
