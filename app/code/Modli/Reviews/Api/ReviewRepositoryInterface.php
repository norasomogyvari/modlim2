<?php

namespace Modli\Reviews\Api;

interface ReviewRepositoryInterface
{
    public function save(array $reviewData);
}
