<?php
namespace Modli\Reviews\Api\Review;

interface MapperInterface
{
    public function map(array $data): array;
}
