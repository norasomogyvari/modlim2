<?php

namespace Modli\Reviews\Api\Review\Api;

interface PagedInterface
{
    /**
     * Return review page Qty
     *
     * @return int
     */
    public function getPageCount(): int;

    /**
     * Return page reviews
     *
     * @param int $pageNum Number of page to retrieve reviews.
     *
     * @return array
     */
    public function getPageReviews(int $pageNum): array;
}
