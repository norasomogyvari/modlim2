<?php
namespace Modli\CatalogInventory\UI\DataProvider\Product;

use Magento\Framework\Data\Collection;
use Magento\Ui\DataProvider\AddFieldToCollectionInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class AddBackorderMessageFieldToCollection implements AddFieldToCollectionInterface
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * AddBackorderFieldToCollection constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * {@inheritdoc}
     */
    public function addField(Collection $collection, $field, $alias = null)
    {
        $defaultBackorderMessage = $this->scopeConfig->getValue('cataloginventory/item_options/backorder_message');
        $tableAlias = 'backorder_message';
        $collection->joinField(
            $tableAlias,
            'cataloginventory_stock_item',
            "IF(at_$tableAlias.use_config_backorder_message, '$defaultBackorderMessage', at_$tableAlias.backorder_message)",
            'product_id=entity_id',
            '{{table}}.stock_id=1',
            'left'
        );
    }
}
