<?php
namespace Modli\CatalogInventory\UI\DataProvider\Product;

use Magento\Eav\Model\Entity\Collection\AbstractCollection;
use Magento\Framework\Data\Collection;
use Magento\Ui\DataProvider\AddFilterToCollectionInterface;


class AddBackorderMessageFilterToCollection implements AddFilterToCollectionInterface
{
    /**
     * {@inheritdoc}
     */
    public function addFilter(Collection $collection, $field, $condition = null)
    {
        #var_dump($condition); die;
        if (isset($condition['like'])) {
            $collection->getSelect()->where(
                AbstractCollection::ATTRIBUTE_TABLE_ALIAS_PREFIX . 'backorder_message.backorder_message LIKE ?',
                $condition['like']
            );
        }
    }
}
