<?php
namespace Modli\CatalogInventory\UI\DataProvider\Product;

use Magento\Framework\Data\Collection;
use Magento\Ui\DataProvider\AddFieldToCollectionInterface;


class AddBackorderDateFieldToCollection implements AddFieldToCollectionInterface
{
    /**
     * {@inheritdoc}
     */
    public function addField(Collection $collection, $field, $alias = null)
    {
        $collection->joinField(
            'backorder_ship_date',
            'cataloginventory_stock_item',
            'backorder_ship_date',
            'product_id=entity_id',
            '{{table}}.stock_id=1',
            'left'
        );
    }
}
