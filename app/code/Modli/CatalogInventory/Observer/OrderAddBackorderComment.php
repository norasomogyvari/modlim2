<?php
namespace Modli\CatalogInventory\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Modli\CatalogInventory\Model\Order\BackorderHandler;

class OrderAddBackorderComment implements ObserverInterface
{
    /**
     * @var BackorderHandler
     */
    protected $backorderHandler;

    public function __construct(BackorderHandler $backorderHandler)
    {
        $this->backorderHandler = $backorderHandler;
    }

    public function execute(Observer $observer)
    {
        /**
         * @var $order \Magento\Sales\Model\Order
         */
        $order = $observer->getEvent()->getOrder();
        $this->backorderHandler->addBackorderComment($order);
    }
}
