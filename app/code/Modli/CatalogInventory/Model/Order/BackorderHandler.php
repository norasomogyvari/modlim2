<?php

namespace Modli\CatalogInventory\Model\Order;

use Magento\Sales\Model\Order;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\CatalogInventory\Api\StockItemRepositoryInterface;
use Magento\CatalogInventory\Api\StockItemCriteriaInterfaceFactory;

class BackorderHandler
{
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var StockItemRepositoryInterface
     */
    protected $stockItemRepository;

    /**
     * @var StockItemCriteriaInterfaceFactory
     */
    protected $stockItemCriteriaFactory;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        FilterBuilder $filterBuilder,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        StockItemRepositoryInterface $stockRepository,
        StockItemCriteriaInterfaceFactory $stockItemCriteriaFactory
    )
    {
        $this->productRepository = $productRepository;
        $this->filterBuilder = $filterBuilder;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->stockItemRepository = $stockRepository;
        $this->stockItemCriteriaFactory = $stockItemCriteriaFactory;
    }

    /**
     * Add comment to order with backordered products info
     *
     * @param Order $order
     *
     */
    public function addBackorderComment(Order $order)
    {
        $items = $order->getItems();
        $backOrderProducts = [];
        foreach ($items as $item) {
            if ($item->getQtyBackordered() > 0) {
                $backOrderProducts[] = $item->getProductId();
            }
        }
        $comment = $this->getBackorderComment($backOrderProducts);
        $order->addCommentToStatusHistory($comment)->save();
    }

    protected function getBackorderComment(array $productIds): string
    {
        $products = $this->getProducts($productIds);
        $comment = '';
        foreach ($products as $product) {
            $backorderShipDate = $this->getProductShipOrderDate($product);
            $comment .= "Pre Order (Magento SKU: {$product->getSku()}) (Vendor SKU:{$product->getVendorSku()}) 
                - ship by '$backorderShipDate' \n";
        }

        return $comment;
    }

    /**
     * Get product list.
     *
     * @param int[] $productIds
     *
     * @return \Magento\Catalog\Api\Data\ProductInterface[]
     */
    protected function getProducts(array $productIds): array
    {
        $filter = $this->filterBuilder
            ->setField('entity_id')
            ->setConditionType('in')
            ->setValue($productIds)
            ->create();

        $searchCriteria = $this->searchCriteriaBuilder->addFilters([$filter])->create();

        return $this->productRepository->getList($searchCriteria)->getItems();
    }

    protected function getProductShipOrderDate(ProductInterface $product): string
    {
        $searchCriteria = $this->stockItemCriteriaFactory->create();
        $searchCriteria->setProductsFilter([$product->getId()]);

        $items = $this->stockItemRepository->getList($searchCriteria)->getItems();
        if (count($items) > 0) {
            $item = reset($items);
            $shipDate = $item->getBackorderShipDate();
            if (!$shipDate) {
                return '';
            }
            $date = date_create($shipDate);
            return $date->format('Y-m-d');
        } else {
            return '';
        }
    }
}
