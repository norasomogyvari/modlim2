<?php
namespace Modli\Integration\Helper;

use Magento\Framework\App\ResourceConnection;

class Sales
{
    protected $resourceConnection;

    public function __construct(ResourceConnection $resourceConnection)
    {
        $this->resourceConnection = $resourceConnection;
    }

    public function getOrderShipmentIdVendorMap($orderId)
    {
        $shipmentIdTable = $this->resourceConnection->getTableName('sales_shipment');
        $select = $this->resourceConnection
            ->getConnection()
            ->select()
            ->from($shipmentIdTable, ['entity_id', 'udropship_vendor'])
            ->where("order_id = ?", $orderId);


        $shipments = $this->resourceConnection->getConnection()->fetchAll($select);
        $result = [];
        foreach ($shipments as $shipment) {
            $result[$shipment['udropship_vendor']] = $shipment['entity_id'];
        }

        return $result;
    }

    public function getOrderIdByShipmentId($shipmentId)
    {
        $shipmentIdTable = $this->resourceConnection->getTableName('sales_shipment');
        $select = $this->resourceConnection
            ->getConnection()
            ->select()
            ->from($shipmentIdTable, ['order_id', 'udropship_vendor'])
            ->where("entity_id = ?", $shipmentId);


        return $this->resourceConnection->getConnection()->fetchOne($select);
    }

    public function getOrderIncrIdByShipmentIncrId($shipmentIncrementId)
    {
        $shipmentIdTable = $this->resourceConnection->getTableName('sales_shipment');
        $orderTable = $this->resourceConnection->getTableName('sales_order');
        $select = $this->resourceConnection
            ->getConnection()
            ->select()
            ->from(['s' => $shipmentIdTable], [])
            ->joinInner(['o' => $orderTable], 'o.entity_id = s.order_id', ['o.increment_id'])
            ->where("s.increment_id = ?", $shipmentIncrementId);


        return $this->resourceConnection->getConnection()->fetchOne($select);
    }
}
