<?php
namespace Modli\Integration\Helper;

use Magento\Framework\App\ResourceConnection;

class Catalog
{
    protected $resourceConnection;

    public function __construct(ResourceConnection $resourceConnection)
    {
        $this->resourceConnection = $resourceConnection;
    }

    public function getSkuByVendorSku(string $vendorSku)
    {
        $productTable = $this->resourceConnection->getTableName('catalog_product_entity');
        $select = $this->resourceConnection
            ->getConnection()
            ->select()
            ->from($productTable, ['sku'])
            ->where("vendor_sku = ?", $vendorSku);

        $sku = $this->resourceConnection->getConnection()->fetchOne($select);

        if (!$sku) {
            #$sku = $vendorSku;
        }

        return $sku;
    }

    public function getVendorSkuBySku($sku)
    {
        $productTable = $this->resourceConnection->getTableName('catalog_product_entity');
        $select = $this->resourceConnection
            ->getConnection()
            ->select()
            ->from($productTable, ['vendor_sku'])
            ->where("sku = ?", $sku);

        return $this->resourceConnection->getConnection()->fetchOne($select);
    }


}
