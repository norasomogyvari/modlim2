<?php
namespace Modli\Integration\Model\Mapper;

use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\ShipmentInterface;
use Modli\Integration\Helper\Catalog as CatalogHelper;
use Modli\Integration\Model\Vendor\Acl\Filter\Sales as SalesFilter;

class OrderShipment
{
    /**
     * @var CatalogHelper
     */
    protected $catalogHelper;

    /**
     * @var SalesFilter
     */
    protected $orderFilter;

    public function __construct(CatalogHelper $catalogHelper, SalesFilter $salesFilter)
    {
        $this->catalogHelper = $catalogHelper;
        $this->orderFilter = $salesFilter;
    }

    public function map(OrderInterface $order, ShipmentInterface $shipment)
    {
        $this->mapItems($order, $shipment);
        $this->mapData($order, $shipment);
        if ($this->orderFilter->isUseShipmentIdInsteadOfOrderId()) {
            $order->setEntityId($shipment->getId());
        }
    }

    protected function mapItems(OrderInterface $order, ShipmentInterface $shipment)
    {

        $orderItems = $order->getItems();
        $shipmentItems = $shipment->getItems();
        $allowedItemIds = [];
        foreach ($shipmentItems as $shipmentItem) {
            $allowedItemIds[] = $shipmentItem->getOrderItemId();
        }
        $allowedItems = [];
        foreach ($orderItems as $orderItem) {
            if (in_array($orderItem->getId(), $allowedItemIds)) {
                if ($this->orderFilter->isUseVendorSku()) {
                    $vendorSku = $this->catalogHelper->getVendorSkuBySku($orderItem->getSku());
                }
                if ($vendorSku) {
                    $orderItem->setSku($vendorSku);
                }

                $allowedItems[] = $orderItem;
                if ($this->orderFilter->isUseShipmentIdInsteadOfOrderId()) {
                    $orderItem->setOrderId($shipment->getId());
                }
            }
        }
        $order->setItems($allowedItems);
    }

    protected function mapData(OrderInterface $order, ShipmentInterface $shipment)
    {
        $attributes = [];

        $apiKeyDataKeyMap = array(
            'tax_amount' => 'tax_amount',
            'discount_amount' => 'discount_amount',
            'subtotal' => 'row_total',
            'base_tax_amount' => 'base_tax_amount',
            'base_discount_amount' => 'base_discount_amount',
            'base_subtotal' => 'base_row_total',
        );

        foreach ($apiKeyDataKeyMap as $apiKey => $dataKey) {
            $attributes[$apiKey] = 0;
            foreach ($order->getItems() as $orderItem) {
                $attributes[$apiKey] += $orderItem->getData($dataKey);
            }
            if (in_array($apiKey, array('discount_amount', 'base_discount_amount'))) {
                $attributes[$apiKey] = -$attributes[$apiKey];
            }
            $attributes[$apiKey] = number_format($attributes[$apiKey], 4);
        }


        $attributes['shipping_amount'] = $shipment->getShippingAmount();
        $attributes['base_shipping_amount'] = $shipment->getBaseShippingAmount();
        $attributes['total_qty_ordered'] = $shipment->getTotalQty();
        $attributes['base_shipping_discount_amount'] = -$shipment->getBaseDiscountAmount();
        $attributes['base_shipping_discount_amount'] = $shipment->getBaseDiscountAmount();
        $attributes['total_item_count'] = $shipment->getTotalQty();
        $attributes['shipping_incl_tax'] = $attributes['shipping_amount'] + $shipment->getShippingTax();
        $attributes['base_shipping_incl_tax'] = $attributes['base_shipping_amount'] + $shipment->getBaseShippingTax();
        $attributes['grand_total'] = $attributes['subtotal'] + $attributes['discount_amount']
            + $attributes['tax_amount'] + $attributes['shipping_amount'];
        $attributes['base_grand_total'] = $attributes['base_subtotal'] + $attributes['base_discount_amount']
            + $attributes['base_tax_amount'] + $attributes['base_shipping_amount'];
        $attributes['base_subtotal_incl_tax'] = $attributes['base_subtotal'] + $attributes['base_tax_amount'];
        $attributes['subtotal_incl_tax'] = $attributes['subtotal'] + $attributes['tax_amount'];

        $zeroValuesAttributes = ['total_paid', 'total_refunded', 'total_invoiced', 'base_total_paid',
            'base_total_invoiced', 'base_discount_invoiced', 'base_shipping_invoiced', 'base_shipping_tax_amount',
            'base_subtotal_invoiced', 'base_tax_invoiced', 'base_tax_invoiced', 'base_total_invoiced_cost',
            'discount_invoiced', 'shipping_invoiced', 'shipping_tax_amount', 'subtotal_invoiced', 'tax_invoiced',
            'paypal_ipn_customer_notified', 'hidden_tax_amount', 'base_hidden_tax_amount',
            'base_shipping_hidden_tax_amount', 'base_shipping_hidden_tax_amount', 'hidden_tax_invoiced',
            'base_hidden_tax_invoiced', ];
        foreach ($zeroValuesAttributes as $attrCode) {
            $attributes[$attrCode] = 0;
        }
        $attributes['shipping_method'] = $shipment->getUdropshipMethod();
        $attributes['shipping_description'] = $shipment->getUdropshipMethodDescription();
        if ($this->orderFilter->isUseShipmentIdInsteadOfOrderId()) {
            $attributes['order_id'] = $shipment->getId();
            $attributes['increment_id'] = $shipment->getIncrementId();
        }

        if ($this->orderFilter->isOrderProcessingStatus($order->getStatus())) {
            $attributes['state'] = \Magento\Sales\Model\Order::STATE_PROCESSING;
            $attributes['status'] = \Magento\Sales\Model\Order::STATE_PROCESSING;
        }

        $order->addData($attributes);
    }
}
