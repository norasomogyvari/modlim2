<?php
namespace Modli\Integration\Model\Vendor\Acl\Filter;

use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroupBuilder;
use Magento\Framework\Authorization;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Model\Order as OrderModel;
use Modli\Integration\Helper\Sales as SalesHelper;
use Modli\Integration\Helper\Catalog as CatalogHelper;
use Magento\Sales\Api\Data\ShipmentInterface;
use Modli\Integration\Model\Vendor\Acl\Resource\Source;
use Unirgy\Dropship\Model\ResourceModel\Vendor\CollectionFactory as VendorCollectionFactory;

class Sales extends AbstractFilter
{
    /**
     * @var SalesHelper
     */
    protected $salesHelper;

    public function __construct(
        Authorization $authorization,
        VendorCollectionFactory $vendorCollectionFactory,
        FilterGroupBuilder $filterGroupBuilder,
        FilterBuilder $filterBuilder,
        SalesHelper $salesHelper,
        CatalogHelper $catalogHelper
    )
    {
        parent::__construct(
            $authorization,
            $vendorCollectionFactory,
            $filterGroupBuilder,
            $filterBuilder,
            $catalogHelper
        );
        $this->salesHelper = $salesHelper;
    }

    const SHIP_INST_OF_ODR = 'Modli_Integration::order_use_po';
    const SHIP_ID_INST_OF_ODR_ID = 'Modli_Integration::order_po_shipment_id';
    const CUSOTMER_EMAIL_ACL = 'Modli_Integration::customer_email';

    public function isShipmentsInsteadOfOrders()
    {
        return $this->isAllowed(self::SHIP_INST_OF_ODR);
    }

    public function isUseShipmentIdInsteadOfOrderId()
    {
        return $this->isAllowed(self::SHIP_ID_INST_OF_ODR_ID);
    }

    public function filterOrder(OrderModel $order)
    {
        $shipments = $order->getShipmentsCollection();
        foreach ($shipments as $shipment) {
            if (!$this->isShipmentAllowed($shipment)) {
                $shipments->removeItemByKey($shipment->getId());
            }
        }
        if (!$this->isAllowed(self::CUSOTMER_EMAIL_ACL)) {
            $order->setCustomerEmail(null);
        }
    }

    public function isShipmentAllowed(ShipmentInterface $shipment)
    {
        $vendorId = $shipment->getUdropshipVendor();
        return $this->isVendorAllowed($vendorId);
    }

    public function getOrderShipment(OrderModel $order, int $shipmentId = 0): ShipmentInterface
    {
        $shipmentCollection = $order->getShipmentsCollection();
        if ($shipment = $shipmentCollection->getItemById($shipmentId)) {
            return $shipment;
        }
        foreach ($shipmentCollection as $shipment) {
            if ($this->isShipmentAllowed($shipment)) {
                return $shipment;
            }
        }

        throw new NoSuchEntityException(
            __('Order not exists')
        );
    }

    public function getOrderIdByShipmentId($shipmentId)
    {
        return $this->salesHelper->getOrderIdByShipmentId($shipmentId);
    }

    public function getOrderIncrIdByShipmentIncrId($shipmentIncrementId)
    {
        return $this->salesHelper->getOrderIncrIdByShipmentIncrId($shipmentIncrementId);
    }

    public function filterSearchCriteria(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        if (!$this->isUseShipmentIdInsteadOfOrderId()) {
            return;
        }
        foreach($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() == 'order_id') {
                    $id = $this->getOrderIdByShipmentId($filter->getValue());
                    $filter->setValue($id);
                } elseif ($filter->getField() == 'increment_id') {
                    $id = $this->getOrderIncrIdByShipmentIncrId($filter->getValue());
                    $filter->setValue($id);
                }
            }
        }
    }

    public function isOrderProcessingStatus($status)
    {
        return $this->isAllowed(Source::ORDER_STATUS_RESOURCE_PREFIX . $status);
    }

    public function filterShipmentItems(\Magento\Sales\Api\Data\ShipmentInterface $shipment): void
    {
        if (!$this->isUseVendorSku()) {
            return;;
        }
        $items = $shipment->getItems();
        foreach ($items as $item) {
            $sku = $item->getSku();
            $vendorSku = $this->catalogHelper->getVendorSkuBySku($sku);
            if ($vendorSku) {
                $item->setSku($vendorSku);
                $item->setSystemSku($sku);
            }
        }
    }
}
