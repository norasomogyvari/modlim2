<?php
namespace Modli\Integration\Model\Vendor\Acl\Filter;

use Magento\Framework\Api\SearchCriteriaInterface;

class Catalog extends AbstractFilter
{
    public function isProductAllowed(\Magento\Catalog\Api\Data\ProductInterface $product)
    {
        return $this->isVendorAllowed($product->getUdropshipVendor());
    }

    public function filterSku(string $sku): string
    {
        if ($this->isUseVendorSku()) {
            $vendorSku = $this->catalogHelper->getSkuByVendorSku($sku);
            $sku = $vendorSku ? $vendorSku : $sku;
        }

        if (!$sku || !$this->isValidSku($sku)) {
            return false;
        }
        return $sku;
    }

    protected function isValidSku(string $sku): bool
    {
        $vendorId = $this->getVendorIdFromSku($sku);
        return $this->isVendorAllowed($vendorId);
    }

    /**
     * @todo quick implementaion. Fix in future to take vendor Id from DB.
     *
     * @param string $sku
     *
     * @return string
     */
    protected function getVendorIdFromSku(string $sku): int
    {
        $skuParts = explode('-', $sku);
        $skuParts = reset($skuParts);
        return (int)$skuParts;
    }



    public function replaceSkuWithVendorSku(\Magento\Catalog\Api\Data\ProductInterface $product)
    {
        if ($this->isUseVendorSku()) {
            $vendorSku = $product->getVendorSku();
            if (!is_null($vendorSku)) {
                $product->setData('system_sku', $product->getSku());
                $product->setData('sku', $vendorSku);
            }
        }
    }

    protected function filterSearchCriteria(SearchCriteriaInterface $searchCriteria)
    {
        if (!$this->isUseVendorSku()) {
            return;
        }
        foreach($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() == 'sku') {
                    $filter->setField('vendor_sku');
                }
            }
        }
    }

    public function prepareSearchCriteria(SearchCriteriaInterface $searchCriteria)
    {
        $this->filterSearchCriteria($searchCriteria);
        $this->addVendorFilterToSearchCriteria($searchCriteria);
    }
}
