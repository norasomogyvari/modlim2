<?php
namespace Modli\Integration\Model\Vendor\Acl\Filter;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Authorization;
use Unirgy\Dropship\Model\ResourceModel\Vendor\CollectionFactory as VendorCollectionFactory;
use Magento\Framework\Api\Search\FilterGroupBuilder;
use Magento\Framework\Api\FilterBuilder;
use Modli\Integration\Helper\Catalog as CatalogHelper;

abstract class AbstractFilter
{
    const ADMIN_ROLE = 'Magento_Backend::all';
    const LIMIT_ROLE = 'Modli_Integration::vendor_limit';
    const VENDOR_ROLE_PREFIX = 'Modli_Integration::vendor-';
    const USE_VSKU = 'Modli_Integration::use_vendor_sku';
    const VENDOR_ATTRIBUTE_CODE = 'udropship_vendor';

    /**
     * @var Authorization
     */
    protected $authorization;

    /**
     * @var VendorCollectionFactory
     */
    protected $vendorCollectionFactory;

    /**
     * @var FilterGroupBuilder
     */
    protected $filterGroupBuilder;

    /**
     * @var FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var array|null
     */
    protected $allowedVendorIds;

    /**
     * @var CatalogHelper
     */
    protected $catalogHelper;

    /**
     * AbstractFilter constructor.
     * @param Authorization $authorization
     * @param VendorCollectionFactory $vendorCollectionFactory
     * @param FilterGroupBuilder $filterGroupBuilder
     * @param FilterBuilder $filterBuilder
     * @param CatalogHelper $catalogHelper
     */
    public function __construct(
        Authorization $authorization,
        VendorCollectionFactory $vendorCollectionFactory,
        FilterGroupBuilder $filterGroupBuilder,
        FilterBuilder $filterBuilder,
        CatalogHelper $catalogHelper
    )
    {
        $this->authorization = $authorization;
        $this->vendorCollectionFactory = $vendorCollectionFactory;
        $this->filterGroupBuilder = $filterGroupBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->catalogHelper = $catalogHelper;
    }

    protected function isAllowed($resource)
    {
        return $this->authorization->isAllowed($resource);
    }

    protected function isAdmin(): bool
    {
        return $this->isAllowed(static::ADMIN_ROLE);
    }

    protected function isLimitedByVendor(): bool
    {
        return $this->isAllowed(static::LIMIT_ROLE);
    }

    protected function isVendorAllowed(int $vendorId): bool
    {
        return $this->isAdmin() || !$this->isLimitedByVendor() ||
            $this->isAllowed(static::VENDOR_ROLE_PREFIX . $vendorId);
    }

    public function isUseVendorSku()
    {
        return $this->authorization->isAllowed(static::USE_VSKU);
    }

    public function addVendorFilterToSearchCriteria(SearchCriteriaInterface $searchCriteria)
    {
        if (!$this->isLimitedByVendor()) {
            return;
        }
        $vendorIds = $this->getAllowedVendorIds();

        $vendorFilter = $this->filterBuilder
            ->create()
            ->setField(self::VENDOR_ATTRIBUTE_CODE)
            ->setConditionType('in')
            ->setValue($vendorIds);
        $vendorFilterGroup = $this->filterGroupBuilder
            ->setFilters([$vendorFilter])
            ->create();


        $filterGroups = $searchCriteria->getFilterGroups();
        $filterGroups[] = $vendorFilterGroup;
        $searchCriteria->setFilterGroups($filterGroups);
    }

    /**
     * Role allowed vendor ids
     *
     * @return array
     */
    protected function getAllowedVendorIds(): array
    {
        if (is_null($this->allowedVendorIds)) {
            $this->allowedVendorIds = [];
            $vendorIds = $this->vendorCollectionFactory->create()->getAllIds();
            foreach ($vendorIds as $vendorId) {
                if ($this->isVendorAllowed($vendorId)) {
                    $this->allowedVendorIds[] = $vendorId;
                }
            }

        }
        return $this->allowedVendorIds;
    }

}
