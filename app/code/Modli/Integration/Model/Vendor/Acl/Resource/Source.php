<?php

namespace Modli\Integration\Model\Vendor\Acl\Resource;

use Unirgy\Dropship\Model\ResourceModel\Vendor\CollectionFactory as VendorCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory as OrderStatusCollectionFactory;
use Magento\Framework\Escaper;

class Source
{
    const ORDER_STATUS_RESOURCE_PREFIX = 'Modli_Integration::status-';

    /**
     * @var VendorCollectionFactory
     */
    protected $vendorCollectionFactory;

    /**
     * @var OrderStatusCollectionFactory
     */
    protected $orderStatusCollectionFactory;

    /**
     * @var Escaper
     */
    protected $escaper;

    /**
     * @var array
     */
    protected $resourceList = [];


    public function __construct(
        VendorCollectionFactory $vendorCollectionFactory,
        OrderStatusCollectionFactory $orderStatusCollectionFactory,
        Escaper $escaper
    ) {
        $this->vendorCollectionFactory = $vendorCollectionFactory;
        $this->orderStatusCollectionFactory = $orderStatusCollectionFactory;
        $this->escaper = $escaper;
    }

    public function getVendorResourceList()
    {
        $result = [];
        $vendors = $this->vendorCollectionFactory->create()
            ->addStatusFilter(\Unirgy\Dropship\Model\Source::VENDOR_STATUS_ACTIVE)
            ->addOrder('vendor_name', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);
        $sortOrder = 1;
        foreach ($vendors as $vendor) {
            $result[] = [
                'id' => 'Modli_Integration::vendor-' . $vendor->getId(),
                'title' => $this->escaper->escapeQuote($vendor->getVendorName()),
                'sortOrder' => $sortOrder,
                'disabled' => false,
                'children' => [],
            ];
            $sortOrder++;
        }

        return $result;
    }

    public function getOrderStatusResourceList()
    {
        $result = [];
        $statuses = $this->orderStatusCollectionFactory->create()->toOptionHash();
        $sortOrder = 1;
        foreach ($statuses as $status => $label) {
            $result[] = [
                'id' => self::ORDER_STATUS_RESOURCE_PREFIX . $status,
                'title' => $this->escaper->escapeQuote($label),
                'sortOrder' => $sortOrder,
                'disabled' => false,
                'children' => [],
            ];
            $sortOrder++;
        }


        return $result;
    }
}
