<?php
namespace Modli\Integration\Plugin\Framework\Acl\AclResource;

use Magento\Framework\Stdlib\ArrayManager;
use Modli\Integration\Model\Vendor\Acl\Resource\Source as VendorResource;
class TreeBuilder
{
    const BASE_PATH = 'Magento_Backend::admin/Modli_Integration::admin/';
    const VENDOR_FILTER_PATH = self::BASE_PATH . 'Modli_Integration::vendor_limit';
    const ORDER_STATUS_PATH = self::BASE_PATH . 'Modli_Integration::processing_order_status';

    /**
     * @var ArrayManager
     */
    protected $arrayManager;

    /**
     * @var VendorResource
     */
    protected $aclResource;

    public function __construct(
        ArrayManager $arrayManager,
        VendorResource $aclResource
    ) {
        $this->arrayManager = $arrayManager;
        $this->aclResource = $aclResource;
    }

    public function beforeBuild(\Magento\Framework\Acl\AclResource\TreeBuilder $subject, array $resourceList)
    {
        //Fix for recursion call  of \Magento\Framework\Acl\AclResource\TreeBuilder::build
        if (!isset($resourceList[0]['id']) || $resourceList[0]['id'] !== 'Magento_Backend::admin') {
            return [$resourceList];
        }


        $vendors =  $this->aclResource->getVendorResourceList();
        $resourceList = $this->arrayManager->set(
            $this->getIdxPath(
                self::VENDOR_FILTER_PATH,
                $resourceList
            ),
            $resourceList,
            $vendors
        );

        $statuses = $this->aclResource->getOrderStatusResourceList();
        $resourceList = $this->arrayManager->set(
            $this->getIdxPath(
                self::ORDER_STATUS_PATH,
                $resourceList
            ),
            $resourceList,
            $statuses
        );

        return [$resourceList];
    }

    protected function getIdxPath(string $path, array $resourceList)
    {
        $idxPath = [];
        $arrayToSearch = $resourceList;

        $path = explode('/', $path);
        foreach ($path as $p) {
            foreach ($arrayToSearch as $key => $element) {
                if (!isset($element['id'])) {
                    break 2;
                }
                if ($element['id'] == $p) {
                    $idxPath[] = $key;
                    break;
                }
            }
            if (!isset($element['children'])) {
                break;
            }
            $arrayToSearch = $element['children'];
        }
        $idxPath = implode('/children/', $idxPath) . '/children';
        return $idxPath;
    }

}
