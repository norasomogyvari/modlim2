<?php
namespace Modli\Integration\Plugin\Api\Catalog;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Modli\Integration\Model\Vendor\Acl\Filter\Catalog;
use Magento\Framework\Api\SearchCriteriaInterface;

class ProductRepository
{
    /**
     * @var Catalog
     */
    protected $catalog;

    public function __construct(
        Catalog $catalog
    ) {
        $this->catalog = $catalog;
    }

    public function aroundGet(
        ProductRepositoryInterface $subject,
        \Closure $proceed,
        $sku
    ) {
        $sku = $this->catalog->filterSku($sku);
        $product = $proceed($sku);
        if (!$this->catalog->isProductAllowed($product)) {
            throw new NoSuchEntityException(
                __("The product that was requested doesn't exist. Verify the product and try again.")
            );
        }
        $this->catalog->replaceSkuWithVendorSku($product);

        return $product;
    }

    public function aroundGetList(
        ProductRepositoryInterface $subject,
        \Closure $proceed,
        SearchCriteriaInterface $searchCriteria
    ) {

        $this->catalog->prepareSearchCriteria($searchCriteria);

        $searchResult = $proceed($searchCriteria);
        $resultItems = [];
        foreach ($searchResult->getItems() as $product) {
            if ($this->catalog->isProductAllowed($product)) {
                $this->catalog->replaceSkuWithVendorSku($product);
                $resultItems[] = $product;
            }
        }
        $searchResult->setItems($resultItems);

        return $searchResult;
    }

}
