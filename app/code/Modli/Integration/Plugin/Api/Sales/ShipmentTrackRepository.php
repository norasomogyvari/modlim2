<?php
namespace Modli\Integration\Plugin\Api\Sales;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\ShipmentRepositoryInterface;
use Modli\Integration\Model\Vendor\Acl\Filter\Sales as OrderFilter;

class ShipmentTrackRepository
{
    /**
     * @var OrderFilter
     */
    protected $orderFilter;

    /**
     * @var ShipmentRepositoryInterface
     */
    protected $shipmentRepository;


    public function __construct(
        OrderFilter $orderFilter,
        ShipmentRepositoryInterface $shipmentRepository
    )
    {
        $this->orderFilter = $orderFilter;
        $this->shipmentRepository = $shipmentRepository;
    }

    public function beforeSave(
        \Magento\Sales\Api\ShipmentTrackRepositoryInterface $subject,
        \Magento\Sales\Api\Data\ShipmentTrackInterface $entity
    )
    {
        if (!$this->orderFilter->isShipmentsInsteadOfOrders()) {
            return [$entity];
        }

        if ($this->orderFilter->isUseShipmentIdInsteadOfOrderId()) {
            $shipment = $this->shipmentRepository->get($entity->getParentId());
            if (!$this->orderFilter->isShipmentAllowed($shipment)) {
                throw new NoSuchEntityException(
                    __("Shipment with id {$entity->getParentId()} is not exists")
                );
            }
            $entity->setOrderId($shipment->getOrderId());
        }

        return [$entity];
    }
}
