<?php
namespace Modli\Integration\Plugin\Api\Sales;

use Magento\Framework\Exception\NoSuchEntityException;
use Modli\Integration\Model\Vendor\Acl\Filter\Sales as OrderFilter;

class ShipmentRepository
{
    public function __construct(
        OrderFilter $orderFilter
    )
    {
        $this->orderFilter = $orderFilter;
    }

    public function aroundGet(
        \Magento\Sales\Api\ShipmentRepositoryInterface $subject,
        \Closure $proceed,
        $id
    )
    {
        $shipment = $proceed($id);
        if (!$this->orderFilter->isShipmentAllowed($shipment)) {
            throw new NoSuchEntityException(
                __("Shipment that was requested doesn't exist. Verify the entity and try again.")
            );
        }
        $this->orderFilter->filterShipmentItems($shipment);
        return $shipment;
    }

    public function aroundGetList(
        \Magento\Sales\Api\ShipmentRepositoryInterface $subject,
        \Closure $proceed,
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    )
    {
        $this->orderFilter->filterSearchCriteria($searchCriteria);
        $shipments = $proceed($searchCriteria);
        foreach ($shipments as $shipment) {
            $this->orderFilter->filterShipmentItems($shipment);
        }

        return $shipments;
    }
}
