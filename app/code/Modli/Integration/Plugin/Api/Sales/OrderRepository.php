<?php
namespace Modli\Integration\Plugin\Api\Sales;

use Magento\Framework\Exception\NoSuchEntityException;
use Modli\Integration\Model\Vendor\Acl\Filter\Sales as OrderFilter;
use Magento\Sales\Api\ShipmentRepositoryInterface;
use Modli\Integration\Model\Mapper\OrderShipment as Mapper;

class OrderRepository
{
    /**
     * @var OrderFilter
     */
    protected $orderFilter;

    /**
     * @var ShipmentRepositoryInterface
     */
    protected $shipmentRepository;

    /**
     * @var Mapper
     */
    protected $mapper;

    public function __construct(
        OrderFilter $orderFilter,
        ShipmentRepositoryInterface $shipmentRepository,
        Mapper $mapper
    )
    {
        $this->shipmentRepository = $shipmentRepository;
        $this->orderFilter = $orderFilter;
        $this->mapper = $mapper;
    }

    public function aroundGet(
        \Magento\Sales\Api\OrderRepositoryInterface $subject,
        \Closure $proceed,
        $id
    )
    {
        if (!$this->orderFilter->isShipmentsInsteadOfOrders()) {
            return $proceed($id);
        }

        $shipmentId = null;
        if ($this->orderFilter->isUseShipmentIdInsteadOfOrderId()) {
            $shipmentId = $id;
            $id = $this->orderFilter->getOrderIdByShipmentId($id);
        }
        $order = $proceed($id);
        $this->orderFilter->filterOrder($order);
        $shipment = $this->orderFilter->getOrderShipment($order, (int)$shipmentId);
        $this->mapper->map($order, $shipment);

        return $order;
    }

    public function aroundGetList(
        \Magento\Sales\Api\OrderRepositoryInterface $subject,
        \Closure $proceed,
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    )
    {
        if (!$this->orderFilter->isShipmentsInsteadOfOrders()) {
            return $proceed($searchCriteria);
        }

        $this->orderFilter->filterSearchCriteria($searchCriteria);

        /**
         * @var $orders \Magento\Sales\Model\ResourceModel\Order\Collection
         */
        $orders = $proceed($searchCriteria);

        foreach ($orders as $order) {
            try {
                $this->orderFilter->filterOrder($order);
                $shipment = $this->orderFilter->getOrderShipment($order);
                $this->mapper->map($order, $shipment);
            } catch (\Exception $e) {
                $orders->removeItemByKey($order->getId());
            }
        }

        return $orders;
    }
}
