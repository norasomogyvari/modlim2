<?php
namespace Modli\Integration\Plugin\Api\CatalogInventory;

use Magento\Framework\Exception\NoSuchEntityException;
use Modli\Integration\Model\Vendor\Acl\Filter\Catalog;

class StockRegistry
{
    /**
     * @var Catalog
     */
    protected $catalog;

    public function __construct(
        Catalog $catalog
    ) {
        $this->catalog = $catalog;
    }

    public function beforeGetStockItemBySku(
        \Magento\CatalogInventory\Api\StockRegistryInterface $subject,
        $productSku
    )
    {
        $sku = $this->catalog->filterSku($productSku);
        if (!$sku) {
            throw new NoSuchEntityException(
                __('The Product with the "%1" SKU doesn\'t exist.', $productSku)
            );
        }
        return [$sku];
    }

    public function beforeUpdateStockItemBySku(
        \Magento\CatalogInventory\Api\StockRegistryInterface $subject,
        $productSku,
        \Magento\CatalogInventory\Api\Data\StockItemInterface $stockItem
    )
    {
        $sku = $this->catalog->filterSku($productSku);
        if (!$sku) {
            throw new NoSuchEntityException(
                __('The Product with the "%1" SKU doesn\'t exist.', $productSku)
            );
        }
        return [$sku, $stockItem];
    }

    public function beforeGetStockStatusBySku(
        \Magento\CatalogInventory\Api\StockRegistryInterface $subject,
        $productSku,
        $scopeId = null
    )
    {
        $sku = $this->catalog->filterSku($productSku);
        if (!$sku) {
            throw new NoSuchEntityException(
                __('The Product with the "%1" SKU doesn\'t exist.', $productSku)
            );
        }
        return [$sku, $scopeId];
    }
}
