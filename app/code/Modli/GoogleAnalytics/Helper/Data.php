<?php

namespace Modli\GoogleAnalytics\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Sales\Model\Order;
use Magento\Catalog\Model\Session;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Registry;
use Magento\Catalog\Model\ResourceModel\Category;
use Magento\Catalog\Model\ResourceModel\Product;
use Magento\Catalog\Api\CategoryRepositoryInterface;

class Data extends AbstractHelper
{
    protected $_storeManager;
    protected $_catalogSession;
    protected $_coreRegistry;
    protected $_categoryName = [];
    protected $_resourceCategory;
    protected $_categoryRepository;
    protected $_resourceProduct;

    public function __construct(
        StoreManagerInterface $storeManager,
        Session $catalogSession,
        Category $resourceCategory,
        Product $resourceProduct,
        CategoryRepositoryInterface $categoryRepository,
        Registry $registry,
        Context $context
    ) {
        $this->_storeManager = $storeManager;
        $this->_catalogSession = $catalogSession;
        $this->_coreRegistry = $registry;
        $this->_resourceCategory = $resourceCategory;
        $this->_categoryRepository = $categoryRepository;
        $this->_resourceProduct = $resourceProduct;

        parent::__construct($context);
    }

    public function debug($message) {
        if (is_array($message)) {
            $message = print_r($message, true);
        }
        $this->_logger->info($message);
    }

    public function getOrderGuaClientId(Order $order)
    {
        return $order->getGaClientId();
    }

    public function getAccountId($store = null)
    {
        return $this->getStoreConfig('modli_googleanalytics/general/account', $store);
    }

    public function isActive($store = null)
    {
        return $this->getStoreConfig('modli_googleanalytics/general/active', $store);
    }

    public function isGoogleAnalyticsAvailable($store = null)
    {
        return $this->getAccountId($store) && $this->isActive($store);
    }

    public function getStoreConfig($path, $scopeValue = null,
                                   $scopeType = \Magento\Store\Model\ScopeInterface::SCOPE_STORE)
    {
        return $this->scopeConfig->getValue(
            $path, $scopeType, $scopeValue
        );
    }

    public function getStoreConfigFlag($path, $scopeValue = null,
                                       $scopeType = \Magento\Store\Model\ScopeInterface::SCOPE_STORE)
    {
        return (bool) $this->getStoreConfig($path, $scopeValue, $scopeType);
    }

    public function getItemPriceForCreditmemo($creditmemoItem)
    {
        $price = $creditmemoItem->getBaseRowTotal() - $creditmemoItem->getBaseDiscountAmount();
        $price = round($price / max(1,$creditmemoItem->getQty()), 4);

        return $price;
    }

    public function useStoreCurrency($storeId)
    {
        return false;
    }

    public function getTransactionRevenueForOrder($order)
    {
        $storeId = $order->getStoreId();
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $grandTotal = $useStoreCurrency ? $order->getGrandTotal() : $order->getBaseGrandTotal();

        return $grandTotal;
    }

    public function getTransactionShippingForOrder($order)
    {
        $storeId = $order->getStoreId();
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $shippingAmount = $useStoreCurrency ? $order->getShippingAmount() : $order->getBaseShippingAmount();

        return $shippingAmount;
    }

    public function getTransactionTaxForOrder($order)
    {
        $storeId = $order->getStoreId();
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $taxAmount = $useStoreCurrency ? $order->getTaxAmount() : $order->getBaseTaxAmount();
        return $taxAmount;
    }

    public function getTransactionAffiliation($order)
    {
        if ($order->getRemoteIp()) {
            return $order->getStore()->getName();
        }
        //admin order
        return __('Admin order (%1)', $order->getStore()->getName());
    }

    public function getStore($storeId = null)
    {
        return $this->_storeManager->getStore($storeId);
    }

    public function getProductBrandValue($product, $storeId = null)
    {
        $brandAttributeCode = $this->getBrandAttributeCode();
        $brandValue = $this->getProductAttributeValue($product, $brandAttributeCode, $storeId);

        return $brandValue;
    }

    protected function getBrandAttributeCode()
    {
        return 'udropship_vendor';
    }


    public function getProductVariantValue($product,  $orderItem = null) {
        if (!$product) {
            return '';
        }
        try {
            if ($orderItem && $orderItem instanceof \Magento\Sales\Model\Order\Item) {
                $options = [];
                if ($productOptions = $orderItem->getProductOptions()) {
                    if (isset($productOptions['options'])) {
                        $options = array_merge($options, $productOptions['options']);
                    }
                    if (isset($productOptions['additional_options'])) {
                        $options = array_merge($options, $productOptions['additional_options']);
                    }
                    if (!empty($productOptions['attributes_info'])) {
                        $options = array_merge($productOptions['attributes_info'],  $options);
                    }
                }
            } else {
                $options = [];
                if ($product->getTypeId() == 'configurable') {
                    $options = $product->getTypeInstance(true)->getSelectedAttributesInfo($product);
                }
                $productOptions = $product->getTypeInstance(true)->getOrderOptions($product);

                if (isset($productOptions['options'])) {
                    $options = array_merge($options, $productOptions['options']);
                }
            }
        } catch (\Exception $e) {
            return '';
        }
        $variant = [];
        foreach ($options as $option) {
            $variant[] = $option['value'];
        }
        return $this->formatData(implode(' - ', $variant));
    }

    public function getColorAttributeCode()
    {
        return 'product_color';
    }

    public function getProductAttributeValue($product, $attributeCode, $storeId = false)
    {
        if (!$product || !$product->getId()) {
            return '';
        }
        if (!$storeId) {
            $storeId = $this->getCurrentStore()->getId();
        }
        $attribute = $this->_resourceProduct->getAttribute($attributeCode);
        if (!$attribute->getId()) {
            return '';
        }
        if ($product->getData($attributeCode)) {
            $data = $product->getData($attributeCode);
        } else {
            $data = $this->_resourceProduct->getAttributeRawValue($product->getId(),
                $attributeCode, $storeId);
        }
        if (!$data) {
            return '';
        }
        if (preg_match('%[0-9,]+%i', $data) && $attribute->usesSource() && $attribute->getSource()->getOptionText($data)) {
            $data = $attribute->getSource()->getOptionText($data);
            if (is_array($data)) {
                $data = implode(', ', $data);
            }
        }
        return $this->formatData($data);
    }

    public function getItemPriceForOrder($orderItem)
    {
        $order = $orderItem->getOrder();
        $storeId = null;
        if ($order) {
            $storeId = $order->getStoreId();
        }
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $price = $useStoreCurrency ? $orderItem->getRowTotal() - $orderItem->getDiscountAmount() : $orderItem->getBaseRowTotal() - $orderItem->getBaseDiscountAmount();
        $priceInclTax = $price + ($useStoreCurrency ? $orderItem->getTaxAmount() + $orderItem->getHiddenTaxAmount() :  $orderItem->getBaseTaxAmount() + $orderItem->getBaseHiddenTaxAmount());
        $price = round($price / max(1,$orderItem->getQtyOrdered()), 4);

        return $price;
    }

    public function formatData($data)
    {
        $data = strip_tags($data);
        $data = str_replace('"', '', $data);
        $data = str_replace("'", ' ', $data);
        return @iconv('UTF-8', 'UTF-8//IGNORE', $data);
    }

    public function getCurrentStore() {
        return $this->getStore();
    }

    public function getProductCategoryValue($product, $useCurrentCategory = true, $storeId = null) {
        if (!$product || !$product->getId()) {
            return '';
        }
        if (!$storeId) {
            $storeId = $this->getCurrentStore()->getId();
        }
        $categoryId = false;
        $sessionCache = $this->_catalogSession->getGuaProductCategoryValue();
        if (!is_array($sessionCache)) {
            $sessionCache = [];
        }
        if (!isset($sessionCache[$product->getId() . '-' . $storeId])) {
            $sessionCache[$product->getId() . '-' . $storeId] = false;
            $lastCategoryViewed = $this->_catalogSession->getLastViewedCategoryId();
            if ($lastCategoryViewed && in_array($lastCategoryViewed,
                    $product->getCategoryIds())) {
                $sessionCache[$product->getId() . '-' . $storeId] = $lastCategoryViewed;
            }

            if (!$sessionCache[$product->getId() . '-' . $storeId]) {
                $category = $this->_coreRegistry->registry('current_category');
                if ($useCurrentCategory && $category && $category->getId()) {
                    $sessionCache[$product->getId() . '-' . $storeId] = $category->getId();
                } else {
                    $rootCategoryId = $this->getStore($storeId)->getRootCategoryId();
                    $sessionCache[$product->getId() . '-' . $storeId] = false;

                    $categoryCollection = $product->getCategoryCollection();
                    $categoryCollection->setStoreId($storeId);
                    $categoryCollection->addAttributeToSelect('is_active',
                        'left');
                    $categoryCollection->addAttributeToSelect('path', 'left');
                    $categoryCollection->setOrder('entity_id', 'ASC');
                    foreach ($categoryCollection as $category) {
                        $isActive = $this->_resourceCategory->getAttributeRawValue($category->getId(),
                            'is_active', $storeId);
                        if ($isActive && $category->getLevel() > 1 && preg_match('%^1/' . $rootCategoryId . '/%',
                                $category->getPath())) {
                            $sessionCache[$product->getId() . '-' . $storeId] = $category->getId();
                        }
                    }
                }
            }
        }
        $categoryId = $sessionCache[$product->getId() . '-' . $storeId];
        $this->_catalogSession->setGuaProductCategoryValue($sessionCache);

        if (!$categoryId) {
            return '';
        }

        if (!isset($this->_categoryName[$categoryId])) {
            $category = $this->_categoryRepository->get($categoryId);
            $this->_categoryName[$categoryId] = $category->getName();
            while ($category->getLevel() > 2) {
                $category = $category->getParentCategory();
                $this->_categoryName[$categoryId] = $category->getName() . '/' . $this->_categoryName[$categoryId];
            }
        }

        return $this->formatData($this->_categoryName[$categoryId]);
    }

    public function getCatalogSession()
    {
        return $this->_catalogSession;
    }

    public function generateUuid() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,
            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

}
