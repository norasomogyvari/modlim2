<?php

namespace Modli\GoogleAnalytics\Console\Command\Test;

use Modli\GoogleAnalytics\Model\Measurement\Protocol;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\App\State;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

class Order extends Command
{
    const ORDER_ID = 'order_id';

    /**
     * @var Protocol
     */
    protected $measurementProtocol;

    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var State
     */
    protected $state;

    /**
     * Class constructor
     */
    public function __construct(
        Protocol $measurementProtocol,
        OrderRepositoryInterface $orderRepository,
        State $state
    ) {
        $this->measurementProtocol = $measurementProtocol;
        $this->orderRepository = $orderRepository;
        $this->state = $state;

        parent::__construct();
    }

    /**
     * @param \Magento\Cron\Model\Schedule $schedule
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);

        $orderId = $input->getOption(self::ORDER_ID);
        $order = $this->orderRepository->get((int)$orderId);
        $this->measurementProtocol->sendTransactionRequestFromOrder($order);
    }

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $options = [
            new InputOption(
                self::ORDER_ID,
                null,
                InputOption::VALUE_REQUIRED,
                'Order ID'
            )
        ];

        $this->setName('modli:ga:test:order')
            ->setDescription('Test order for Google Analytics')
            ->setDefinition($options);

        parent::configure();
    }

}
