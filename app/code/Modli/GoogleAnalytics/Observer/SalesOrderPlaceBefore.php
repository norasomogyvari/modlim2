<?php
/**
 * Copyright (c) 2011-2017 Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */

namespace Modli\GoogleAnalytics\Observer;

use Modli\GoogleAnalytics\Helper\GaId;
use Magento\Framework\Event\ObserverInterface;

class SalesOrderPlaceBefore implements ObserverInterface
{
    protected $_helper;

    public function __construct(GaId $helper)
    {
        $this->_helper = $helper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $this->_saveGuaClientId($order);
    }

    protected function _saveGuaClientId($order, $createIfNotFound = true) {
        $clientId = $this->_helper->getGaClientId();
        if ($clientId && $order->getGuaClientId() != $clientId) {
            $order->setGaClientId($clientId);
        }
    }

}
