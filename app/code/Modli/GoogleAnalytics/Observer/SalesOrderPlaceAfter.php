<?php

namespace Modli\GoogleAnalytics\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Modli\GoogleAnalytics\Helper\Data;
use Modli\GoogleAnalytics\Model\Measurement\Protocol;

class SalesOrderPlaceAfter implements ObserverInterface
{
    protected $_helper;
    protected $_measurementProtocol;

    public function __construct(Data $helper, Protocol $measurementProtocol) {
        $this->_helper = $helper;
        $this->_measurementProtocol = $measurementProtocol;
    }

    /**
     * Send order info to Google Analytics
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        /**
         * @var $order \Magento\Sales\Model\Order
         */
        $order = $observer->getEvent()->getOrder();
        try {
            $this->_measurementProtocol->sendTransactionRequestFromOrder($order);
        } catch (\Exception $e) {
            $this->_helper->debug('Error :' . $e->getMessage());
        }
    }

    public function canExecRequest($storeId = null)
    {
        return $this->_helper->isGoogleAnalyticsAvailable($storeId);
    }
}
