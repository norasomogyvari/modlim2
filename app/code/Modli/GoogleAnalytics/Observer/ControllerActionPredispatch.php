<?php
/**
 * Copyright (c) 2011-2017 Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */

namespace Modli\GoogleAnalytics\Observer;

use Magento\Framework\Event\ObserverInterface;

use Magento\Customer\Model\Session;
use Magento\Framework\Stdlib\CookieManagerInterface;

class ControllerActionPredispatch implements ObserverInterface
{

    protected $_customerSession;
    protected $_cookieManager;
    protected $_measurementProtocol;
    protected $_cart;

    public function __construct(Session $customerSession, CookieManagerInterface $cookieManager)
    {
        $this->_customerSession = $customerSession;
        $this->_cookieManager = $cookieManager;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $clientId = $this->_customerSession->getGaClientId();
        $cookieName = '_ga';
        $cookieValue = $this->_cookieManager->getCookie($cookieName);
        if($cookieValue && $clientId != $cookieValue) {
            $guaClientId = preg_replace('%GA1\.[0-9]+\.%', '', $cookieValue);
            $this->_customerSession->setGaClientId($guaClientId);
        }
    }
}
