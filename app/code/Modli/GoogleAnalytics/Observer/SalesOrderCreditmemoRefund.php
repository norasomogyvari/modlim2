<?php

namespace Modli\GoogleAnalytics\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Modli\GoogleAnalytics\Helper\Data;
use Modli\GoogleAnalytics\Model\Measurement\Protocol;

class SalesOrderCreditmemoRefund implements ObserverInterface
{
    protected $_helper;
    protected $_measurementProtocol;

    public function __construct(Data $helper, Protocol $measurementProtocol) {
        $this->_helper = $helper;
        $this->_measurementProtocol = $measurementProtocol;
    }

    public function execute(Observer $observer) {
        $creditmemo = $observer->getEvent()->getCreditmemo();
        $order = $creditmemo->getOrder();
        try {
            $this->_sendRefundRequestFromCreditmemo($creditmemo, $order);
            $this->_measurementProtocol->sendEvent('sales',
                'order_refunded',
                'Order : ' . $order->getIncrementId() . ' has been refunded',
                1, $order->getGuaClientId(), $order->getStoreId());
        } catch (\Exception $e) {
            $this->_helper->debug('Error :' . $e->getMessage());
        }
    }

    protected function _sendRefundRequestFromCreditmemo($creditmemo, $order) {
        $storeId = $order->getStoreId();

        if ($this->_helper->getOrderGuaClientId($order)) {
            return;
        }
        if (!$this->_helper->isGoogleAnalyticsAvailable($storeId)) {
            return;
        }

        $params = [];
        $guaClientId = $order->getGuaClientId();
        $transactionId = $order->getIncrementId();
        $items = $creditmemo->getAllItems();
        $refundTotal = $creditmemo->getGrandTotal();

        $itemIncrement = 1;
        foreach($items as $item) {
            $hasParentItem = (bool) $item->getOrderItem()->getParentItemId();
            if(!$hasParentItem && $item->getQty() > 0) {
                $itemPrice = $this->_helper->getItemPriceForCreditmemo($item);
                $params[sprintf(Protocol::PRODUCT_ID, $itemIncrement)] = $item->getSku();
                $params[sprintf(Protocol::PRODUCT_QUANTITY, $itemIncrement)] = $item->getQty();
                $params[sprintf(Protocol::PRODUCT_PRICE, $itemIncrement)] = $itemPrice;
                $itemIncrement++;
            }
        }

        $params[Protocol::TRANSACTION_ID] = $transactionId;
        $params[Protocol::TRANSACTION_REVENUE] = $refundTotal;
        $params[Protocol::PRODUCT_ACTION] = 'refund';
        $params[Protocol::USER_ID] = $order->getCustomerId();
        $this->_measurementProtocol->sendEvent('transaction', 'creditmemo',
            $transactionId,  true, $guaClientId, $order->getStoreId(),
            $params);
    }
}
