<?php
namespace Modli\GoogleAnalytics\Model\Measurement;

use \Magento\Sales\Model\Order;

class Protocol
{
    const PROTOCOL_URL = "https://ssl.google-analytics.com/collect";
    const CLIENT_ID = "cid";
    const HIT_TYPE = "t";
    const EVENT_CATEGORY = "ec";
    const EVENT_ACTION = "ea";
    const EVENT_LABEL = "el";
    const EVENT_VALUE = "ev";
    const TRACKING_ID = "tid";
    const USER_ID = "uid";

    const PRODUCT_ID = "pr%did";
    const PRODUCT_PRICE = "pr%dpr";
    const PRODUCT_NAME = "pr%dnm";
    const PRODUCT_QUANTITY = "pr%dqt";
    const PRODUCT_CATEGORY = "pr%dca";
    const PRODUCT_BRAND = "pr%dbr";
    const PRODUCT_VARIANT = "pr%dva";
    const PRODUCT_SIZE = "pr%cd6";
    const PRODUCT_BUNDLE_INFO = "pr%cd7";
    const PRODUCT_TYPE = "pr%cd8";
    const PRODUCT_PREORDER = "pr%cd10";
    const PRODUCT_SIZE_AVAILABLE_COUNT = "pr%cd11";
    const PRODUCT_ATTRIBUTE_SET = "pr%cd12";
    const PRODUCT_COST = "pr%cd13";

    const TRANSACTION_ID = "ti";
    const TRANSACTION_REVENUE = "tr";
    const TRANSACTION_AFFILIATION = "ta";
    const TRANSACTION_SHIPPING = "ts";
    const TRANSACTION_TAX = "tt";
    const TRANSACTION_COUPON = "tcc";
    const PRODUCT_ACTION_LIST = "pal";

    const DOCUMENT_LOCATION_URL = "dl";
    const DOCUMENT_HOSTNAME = "dh";
    const DOCUMENT_PATH = "dp";
    const PRODUCT_ACTION = "pa";

    const USER_AGENT_OVERRIDE = "ua";
    const IP_OVERRIDE = "uip";

    const SESSION_CONTROL = 'sc';
    const CURRENCY = "cu";

    protected $_helper;
    protected $_dateFactory;
    protected $_urlDecoder;
    protected $_customerRepositoryInterface;
    protected $_httpClient;

    public function __construct(
        \Modli\GoogleAnalytics\Helper\Data $helper,
        \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Magento\Framework\Url\DecoderInterface $urlDecoder,
        \Magento\Framework\HTTP\ZendClient $httpClient
    ) {
        $this->_helper = $helper;
        $this->_dateFactory = $dateFactory;
        $this->_urlDecoder = $urlDecoder;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->_httpClient = $httpClient;
    }

    public function sendEvent($eventCategory,
                              $eventAction,
                              $eventLabel,
                              $eventValue,
                              $guaClientId = false,
                              $storeId = null,
                              $additionalParameters = [])
    {
        $params = [
            self::CLIENT_ID => $guaClientId,
            self::HIT_TYPE => 'event',
            self::EVENT_CATEGORY => $eventCategory,
            self::EVENT_ACTION => $eventAction,
            self::EVENT_LABEL => $eventLabel,
        ];

        if ($eventValue !== false) {
            $eventValue = round($eventValue);
            $params[self::EVENT_VALUE] = $eventValue;
        }

        $params = array_merge($params, $additionalParameters);

        return $this->execRequest($params, $storeId);
    }

    public function execRequest($params, $storeId = null)
    {
        if (!$this->canExecRequest($storeId)) {
            return false;
        }

        if (!$params) {
            $this->_helper->debug('Empty params :');
            $this->_helper->debug(debug_backtrace());
            return false;
        }
        $params[self::TRACKING_ID] = $this->_helper->getAccountId($storeId);

        $this->sendHit($params);

        return true;
    }

    public function sendHit($params) {
        try {
            $response = $this->_httpClient->setUri(self::PROTOCOL_URL)
                ->setParameterPost($params)
                ->setConfig(['timeout' => 5])
                ->request('POST');
            $this->_helper->debug('GUA Request :');
            $this->_helper->debug($params);
            $this->_helper->debug($response);
        } catch (\Exception $e) {
            $this->_helper->debug($e->getMessage());
            return false;
        }
    }

    public function canExecRequest($storeId = null)
    {
        return $this->_helper->isGoogleAnalyticsAvailable($storeId);
    }

    public function sendTransactionRequestFromOrder(Order $order)
    {
        $storeId = $order->getStoreId();
        $guaClientId = $this->_helper->getOrderGuaClientId($order);
        $transactionId = $order->getIncrementId();

        $revenue = $this->_helper->getTransactionRevenueForOrder($order);
        $shipping = $this->_helper->getTransactionShippingForOrder($order);
        $tax = $this->_helper->getTransactionTaxForOrder($order);
        $currency =  $order->getBaseCurrencyCode();

        $affiliation = $this->_helper->getTransactionAffiliation($order);
        $items = $order->getAllItems();
        $coupon = strtoupper($order->getCouponCode());
        $params = $this->_getAdditionalParamsForOrder($order);

        $itemIncrement = 1;
        foreach ($items as $item) {
            $hasParentItem = (bool) $item->getParentItemId();
            if (!$hasParentItem) {
                $itemParams = $this->getItemParams($item, $itemIncrement++);
                $params = array_merge($params, $itemParams);
            }
        }
        $transactionParams = $this->getTransactionParams($storeId,
            $transactionId, $affiliation, $revenue, $shipping, $tax,
            $currency, $coupon);
        $params = array_merge($params, $transactionParams);
        $this->sendEvent('transaction', 'order', $transactionId,
            true, $guaClientId, $order->getStoreId(), $params);
    }

    public function getTransactionParams($storeId,
                                         $id,
                                         $affiliation,
                                         $revenue,
                                         $shipping,
                                         $tax,
                                         $currency,
                                         $coupon = '') {
        $params = [
            self::TRANSACTION_ID => $id,
            self::TRANSACTION_AFFILIATION => $affiliation,
            self::TRANSACTION_REVENUE => $revenue,
            self::TRANSACTION_SHIPPING => $shipping,
            self::TRANSACTION_TAX => $tax,
            self::PRODUCT_ACTION => 'purchase',
            self::CURRENCY => $currency,
        ];
        if ($coupon) {
            $params[self::TRANSACTION_COUPON] = $coupon;
        }
        if ($this->_helper->getCatalogSession()->getLastListName()) {
            $params[self::PRODUCT_ACTION_LIST] = $this->_helper->getCatalogSession()->getLastListName();
        }
        return $params;
    }

    protected function _getAdditionalParamsForOrder($order) {
        $params = [];

        $params[self::USER_ID] = $order->getCustomerId();
        $params[self::DOCUMENT_LOCATION_URL] = $this->_helper->getStore($order->getStoreId())->getUrl('checkout/onepage/success');

        //ShoppingFlux Compatibility

        if ($order->getRemoteIp()) {
            $params[self::IP_OVERRIDE] = $order->getRemoteIp();
        }

        return $params;
    }

    public function getItemParams($item, $itemIncrement)
    {
        /**
         * @var $orderItem \Magento\Sales\Model\Order\Item
         */
        $orderItem = $item->getOrderItem() ? $item->getOrderItem() : $item;
        $order = $orderItem->getOrder();
        $product = $orderItem->getProduct();
        $storeId = $order ? $order->getStoreId() : $orderItem->getStoreId();
        $itemCategory = $this->_helper->getProductCategoryValue($product, true,
            $storeId);
        $itemName = $item->getName();
        $itemBrand = $this->_helper->getProductBrandValue($product, $storeId);
        $itemVariation = $this->_helper->getProductVariantValue($product, $orderItem);

        $itemPrice = $this->_helper->getItemPriceForOrder($orderItem);
        $itemSku = $item->getSku();
        $itemQty = round($item->getQty() ? $item->getQty() : $item->getQtyOrdered());

        $params = [];
        $params[sprintf(self::PRODUCT_ID, $itemIncrement)] = $itemSku;
        $params[sprintf(self::PRODUCT_NAME, $itemIncrement)] = $this->_helper->formatData($itemName);
        $params[sprintf(self::PRODUCT_CATEGORY, $itemIncrement)] = $this->_helper->formatData($itemCategory);
        $params[sprintf(self::PRODUCT_BRAND, $itemIncrement)] = $this->_helper->formatData($itemBrand);
        $params[sprintf(self::PRODUCT_VARIANT, $itemIncrement)] = $itemVariation;
        $params[sprintf(self::PRODUCT_PRICE, $itemIncrement)] = $itemPrice;
        $params[sprintf(self::PRODUCT_QUANTITY, $itemIncrement)] = $itemQty;
        $params[sprintf(self::PRODUCT_TYPE, $itemIncrement)] = $product->getTypeId();
        $params[sprintf(self::PRODUCT_ATTRIBUTE_SET, $itemIncrement)] = $product->getAttributeSetId();

        return $params;
    }
}
