<?php
namespace Modli\CropImageGraphQl\Plugin;

use \Magento\CatalogGraphQl\Model\Resolver\Products\Query\Filter as ParentFilter;

class Filter
{
    public function afterGetProductFields(ParentFilter $subject, $result)
    {
        $result[] = \Modli\CropImage\Model\Image\Cropper::ATTRIBUTE_CODE;

        return $result;
    }
}
