<?php
namespace Modli\CropImageGraphQl\Plugin\Resolver\Product;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Itegration\Dropship\Model\Resolver\Product\MediaGalleryEntries as MediaGalleryEntriesSubject;
use Magento\Catalog\Model\Product;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Modli\CropImage\Model\Image\Cropper;

class MediaGalleryEntries
{
    protected $cropper;

    public function __construct(Cropper $cropper)
    {
        $this->cropper = $cropper;
    }


    public function aroundResolve(
        MediaGalleryEntriesSubject $subject,
        \Closure $proceed,
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        /** @var Product $product */
        $product = $value['model'];
        $this->cropper->setProduct($product);

        $mediaGalleryEntries = $proceed($field, $context, $info, $value, $args);

        foreach ($mediaGalleryEntries as $key => $entry) {
            $mediaGalleryEntries[$key]['file'] = $this->cropper->cropImage($entry['file']);
            #var_dump($entry['file']); die;
        }

        return $mediaGalleryEntries;
    }
}
