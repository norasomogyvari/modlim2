<?php
namespace Modli\CropImageGraphQl\Plugin\DataProvider;

use Magento\CatalogGraphQl\Model\Resolver\Products\DataProvider\Product as DataProvider;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use \Modli\CropImage\Model\Image\Cropper;

class Product
{
    protected $cropper;

    public function __construct(Cropper $cropper)
    {
        $this->cropper = $cropper;
    }

    public function beforeGetList(
        DataProvider $subject,
        SearchCriteriaInterface $searchCriteria,
        array $attributes = [],
        bool $isSearch = false,
        bool $isChildSearch = false
    ) {
        $attributes[] = \Modli\CropImage\Model\Image\Cropper::ATTRIBUTE_CODE;

        return [$searchCriteria, $attributes, $isSearch, $isChildSearch];
    }

    public function afterGetList(DataProvider $subject, SearchResultsInterface $result)
    {
        $items = $result->getItems();
        foreach ($items as $item) {
            $this->cropper->cropProductImages($item);
            $mediaGallery = $item->getMediaGallery();

            foreach ((array)$mediaGallery['images'] as &$mediaItem) {
                $mediaId = $mediaItem['value_id'];
                $cropped = $this->cropper->cropImage($mediaItem['file']);
                $mediaGallery['images'][$mediaId]['file'] = $cropped;
            }

            $item->setMediaGallery($mediaGallery);
        }

        $result->setItems($items);
        return $result;
    }
}
