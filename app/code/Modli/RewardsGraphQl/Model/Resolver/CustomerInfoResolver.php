<?php

namespace Modli\RewardsGraphQl\Model\Resolver;

use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Mirasvit\Rewards\Api\Service\Customer\TierInterface;
use Mirasvit\Rewards\Api\Service\Customer\Management\SearchInterface as TransactionSearchInterface;
use Mirasvit\Rewards\Helper\Balance as BalanceHelper;

class CustomerInfoResolver implements ResolverInterface
{
    private $balanceHelper;
    private $customerFactory;
    private $tierService;
    private $transactionSearchInterface;

    public function __construct(
        BalanceHelper $balanceHelper,
        CustomerFactory $customerFactory,
        TierInterface $tierService,
        TransactionSearchInterface $transactionSearchInterface
    ) {
        $this->balanceHelper              = $balanceHelper;
        $this->customerFactory            = $customerFactory;
        $this->tierService                = $tierService;
        $this->transactionSearchInterface = $transactionSearchInterface;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        /** @var \Magento\Customer\Model\Data\Customer $customer */
        $customer   = $value['model'];
        $customerId = $customer->getId();

        $balance = $this->balanceHelper->getBalancePoints($customerId);

        $tier  = $this->tierService->getCustomerTier($customerId);
        $tierId = $tier ? $tier->getTierId() : false;

        $transactions = $this->transactionSearchInterface->getTransactions($customerId);

        return [
            'balance'      => $balance,
            'tier_id'      => $tierId,
            'transactions' => $transactions,
            'expires_at'   => $this->getExpiresDate($transactions)
        ];
    }

    protected function getExpiresDate($transactions)
    {
        if (empty($transactions)) {
            return null;
        }

        $expiresDates = [];
        foreach ($transactions as $transaction) {
            /**
             * @var $transaction \Mirasvit\Rewards\Model\Transaction
             */
            if (!$transaction->getIsExpired() && !empty($transaction->getExpiresAt())) {
                $expiresDates[] = date_create($transaction->getExpiresAt());
            }
        }

        $expiresAt = min($expiresDates);

        return $expiresAt->format('Y-m-d H:i:s');
    }
}

