<?php
namespace Modli\RewardsGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Mirasvit\Rewards\Repository\TierRepository;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Store\Model\StoreManagerInterface;

class GetRewardTiers implements ResolverInterface
{
    /**
     * @var TierRepository
     */
    protected $tierRepository;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * GetRewardTiers constructor.
     *
     * @param TierRepository $tierRepository
     * @param StoreManagerInterface $storeManager
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        TierRepository $tierRepository,
        StoreManagerInterface $storeManager,
        SearchCriteriaBuilder $searchCriteriaBuilder
    )
    {
        $this->tierRepository = $tierRepository;
        $this->storeManager = $storeManager;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $websiteId = $this->storeManager->getStore()->getWebsiteId();
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('is_active', 1)
            ->addFilter('website_id', [$websiteId], 'in')
            ->create();

        $tiers = $this->tierRepository->getTiers($searchCriteria);
        $result = [];
        foreach ($tiers->getItems() as $tier) {
            $result[] = [
                'tier_id' => $tier->getId(),
                'name' => $tier->getName(),
                'description' => $tier->getDescription(),
                'min_earn_points' => $tier->getMinEarnPoints(),
            ];
        }

        return $result;
    }
}
