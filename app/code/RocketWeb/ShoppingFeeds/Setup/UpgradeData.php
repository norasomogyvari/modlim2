<?php
/**
 * RocketWeb
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category  RocketWeb
 * @package   RocketWeb_ShoppingFeeds
 * @copyright Copyright (c) 2016 RocketWeb (http://rocketweb.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author    Rocket Web Inc.
 */


/**
 * @category   RocketWeb
 * @package    RocketWeb_ShoppingFeeds
 * @author     RocketWeb
 */
namespace RocketWeb\ShoppingFeeds\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Upgrade the module DB data
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '2.1.1') < 0) {
            $this->setDefualtIsSkip($setup);
        }
        if (version_compare($context->getVersion(), '2.1.9') < 0) {
            $this->cleanConfigDuplicates($setup);
        }
        if (version_compare($context->getVersion(), '2.2.0') < 0) {
            $this->fixSkipAttributeInput($setup);
            $this->cleanDelimiterValues($setup);
        }

        $setup->endSetup();
    }

    private function setDefualtIsSkip(ModuleDataSetupInterface $setup)
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->updateAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'rw_shoppingfeeds_skip_submit',
            'default_value',
            '0'
        );
    }

    private function cleanConfigDuplicates(ModuleDataSetupInterface $setup)
    {
        $setup->run("DELETE t1 FROM {$setup->getTable('rw_shoppingfeeds_feed_config')} as t1
            INNER JOIN {$setup->getTable('rw_shoppingfeeds_feed_config')} as t2
            ON t1.id > t2.id AND t1.feed_id = t2.feed_id AND t1.path = t2.path
            WHERE t1.value IS NULL OR t1.value = ''");
    }

    /**
     * Attribute was getting bad default value at input, and saving a product through admin
     * without editing the attribute caused it to save default ot Yes instead of No. Resulting
     * in skipping unintended products.
     *
     * @param ModuleDataSetupInterface $setup
     */
    private function fixSkipAttributeInput(ModuleDataSetupInterface $setup)
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->updateAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'rw_shoppingfeeds_skip_submit',
            'frontend_input',
            'boolean'
        );
    }

    private function cleanDelimiterValues(ModuleDataSetupInterface $setup)
    {
        $setup->run("DELETE FROM {$setup->getTable('rw_shoppingfeeds_feed_config')} WHERE path IN ('output_params_delimiter','output_params_delimiter_other')");
    }
}
