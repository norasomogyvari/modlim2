<?php
/**
 * RocketWeb
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category  RocketWeb
 * @package   RocketWeb_ShoppingFeeds
 * @copyright Copyright (c) 2016 RocketWeb (http://rocketweb.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author    Rocket Web Inc.
 */

namespace RocketWeb\ShoppingFeeds\Model\Product\Adapter\Type;

use \RocketWeb\ShoppingFeeds\Model\Product\Adapter\AdapterAbstract;
use \RocketWeb\ShoppingFeeds\Model\Feed\Source\Product\Inheritance;

/**
 * Composite Adapter, holds business logic between Product, Config and Mapper
 *
 * Class Composite
 * @package RocketWeb\ShoppingFeeds\Model\Product\Adapter\Type
 */
class Composite extends AdapterAbstract
{
    protected $allowed_parent = [
        \RocketWeb\ShoppingFeeds\Model\Feed\Source\Product\AssociatedMode::ONLY_PARENT,
        \RocketWeb\ShoppingFeeds\Model\Feed\Source\Product\AssociatedMode::BOTH_PARENT_ASSOCIATED
    ];

    protected $allowed_assoc = [
        \RocketWeb\ShoppingFeeds\Model\Feed\Source\Product\AssociatedMode::ONLY_ASSOCIATED,
        \RocketWeb\ShoppingFeeds\Model\Feed\Source\Product\AssociatedMode::BOTH_PARENT_ASSOCIATED
    ];

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $associatedProductCollection
     * @return array
     */
    protected function prepareAssociatedProductAdapters($associatedProductCollection)
    {
        $associatedProductAdapters = [];

        foreach ($associatedProductCollection as $associatedProduct) {
            /** @var \Magento\Catalog\Model\Product $associatedProduct */
            if ($associatedProduct->isDisabled()) {
                continue;
            }

            $associatedProductAdapter = $this->adapterFactory->create($associatedProduct, $this->getFeed(), false);
            if ($associatedProductAdapter !== false) {
                $associatedProductAdapter->setParentAdapter($this)
                    ->setData('generator', $this->getGenerator())
                    ->setData('associated_product_adapters', []);
                if ($this->isTestMode()) {
                    $associatedProductAdapter->setTestMode();
                }
                $associatedProductAdapters[] = $associatedProductAdapter;
            }
        }

        return $associatedProductAdapters;
    }

    /**
     * Internal method to pull feed config for specific product type
     * This needs to be overwritten in child class
     *
     * @return int
     */
    public function getAssociatedProductsMode()
    {
        return \RocketWeb\ShoppingFeeds\Model\Feed\Source\Product\AssociatedMode::BOTH_PARENT_ASSOCIATED;
    }

    /**
     * Internal method to pull feed config for specific product type
     * This needs to be overwritten in child class
     *
     * @return array
     */
    public function getAssociatedMapInheritance()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    protected function internalMap()
    {
        $associatedMode = $this->getAssociatedProductsMode();
        $rows = [];

        if (in_array($associatedMode, $this->allowed_parent)) {
            $this->setData('map_parent', true);
            // Map current product
            $fields = [];
            foreach ($this->feed->getColumnsMap() as $arr) {
                $row = false;

//                // backwards value inheritance attempt
//                $associatedProductAdapters = $this->getData('associated_product_adapters');
//                foreach ($associatedProductAdapters as $associatedProductAdapter) {
//                    $row = $this->mapByInheritance($associatedProductAdapter, $arr);
//                    if ($row !== false) {
//                        break;
//                    }
//                }

                $column = $arr['column'];
                if (!$row) {
                    $row = $this->getMapValue($arr);
                }

                if (isset($fields[$column])) {
                    if (is_array($fields[$column])) {
                        $fields[$column][] = $row;
                    } else {
                        $fields[$column] = [$fields[$column], $row];
                    }
                } else {
                    $fields[$column] = $row;
                }
            }
            $rows[] = $fields;
        }

        if (!$this->checkEmptyColumns($rows)) {
            $rows = [];
        }

        if (in_array($associatedMode, $this->allowed_assoc)) {
            $rows = array_merge($rows, $this->mapAssociatedProducts());
        }

        return $rows;
    }

    /**
     * Get rows for associated products
     *
     * @return array
     */
    protected function mapAssociatedProducts()
    {
        $rows = [];
        $associatedProductAdapters = $this->getData('associated_product_adapters');
        $feedType = $this->feed->getType();
        $generator = $this->getData('generator');

        /** @var \RocketWeb\ShoppingFeeds\Model\Product\Adapter\AdapterAbstract $associatedProductAdapter */
        foreach ($associatedProductAdapters as $associatedProductAdapter) {
            if ($associatedProductAdapter->isSkipped()) {
                if ($generator) {
                    $generator->getLogger()->info(sprintf('Product skipped: %s', $associatedProductAdapter->getSkipMessage()));
                    $generator->updateCountSkip(1);
                }
                continue;
            }

            if ($associatedProductAdapter->isDuplicate()) {
                $this->logger->info(sprintf(
                    'Associated product skipped: %s',
                    $associatedProductAdapter->getSkipMessage()
                ));
                continue;
            }

            $fields = [];
            foreach ($this->feed->getColumnsMap() as $arr) {
                $cell = $this->mapByInheritance($associatedProductAdapter, $arr);
                // Grab from associated by default if no inheritance rule defined
                if ($cell === false) {
                    $cell = $associatedProductAdapter->getMapValue($arr);
                }

                $column = $arr['column'];
                $directive = $this->feedTypesConfig->getDirective($feedType, $arr['attribute']);
                if (!empty($directive)) {
                    $mapperData = $this->mapperFactory->getMapperData($directive, $feedType);
                    $mapper = $this->mapperFactory->create($directive, $associatedProductAdapter);
                    if (isset($mapperData['filter']) && $mapperData['filter']) {
                        $skip = $mapper->filter($cell);
                        if ($skip) {
                            $this->logger->info(sprintf('Skipped product #%s filtered by column "%s"', $associatedProductAdapter->getProduct()->getSku(), $column));
                            if ($this->hasData('generator')) {
                                $this->getData('generator')->updateCountSkip(1);
                            }
                            continue 2;
                        }
                    }
                }

                if (isset($fields[$column])) {
                    if (is_array($fields[$column])) {
                        $fields[$column][] = $cell;
                    } else {
                        $fields[$column] = [$fields[$column], $cell];
                    }
                } else {
                    $fields[$column] = $cell;
                }
            }

            $associatedProductAdapter->checkEmptyColumns($fields);
            if (!$associatedProductAdapter->isSkipped()) {
                array_push($rows, $fields);
            } else {
                if ($generator) {
                    $generator->getLogger()->info(sprintf('Product skipped: %s', $associatedProductAdapter->getSkipMessage()));
                    $generator->updateCountSkip(1);
                }
            }
        }

        return $rows;
    }

    /**
     * @inheritdoc
     */
    public function getChildrenCount()
    {
        $associatedAdapters = $this->getData('associated_product_adapters');
        return is_array($associatedAdapters) ? self::DEFAULT_CHILDREN_COUNT + count($associatedAdapters) : self::DEFAULT_CHILDREN_COUNT;
    }


    /**
     * @inheritdoc
     */
    public function hasSpecialPrice($processRules = true, $product = null)
    {
        $associatedProductAdapters = $this->getData('associated_product_adapters');

        $has = false;
        /** @var \RocketWeb\ShoppingFeeds\Model\Product\Adapter\AdapterAbstract $associatedProductAdapter */
        foreach ($associatedProductAdapters as $associatedProductAdapter) {
            if ($associatedProductAdapter->hasSpecialPrice($processRules, $product)) {
                $has = true;
                break;
            }
        }

        return $has;
    }

    protected function mapByInheritance(\RocketWeb\ShoppingFeeds\Model\Product\Adapter\AdapterAbstract $adapter, $column = [])
    {
        if (array_key_exists('column', $column)) {
            $column_name = $column['column'];
            $map = $this->getAssociatedMapInheritance();

            foreach ($map as $row) {
                if ($row['column'] == $column_name) {
                    if ($column['attribute'] == 'directive_concatenate' && !empty($row['extra'])) {
                        $attributes = explode(',', $row['extra']);
                        // for concatenation, map each attribute individually as defined in inheritance rule
                        foreach ($attributes as $code) {
                            $code = trim($code);
                            if (strpos($column['param'], $code) !== false) {
                                $val = $this->mapByInheritanceFrom(
                                    $adapter,
                                    ['column' => 'concat_'.$code, 'attribute' => $code],
                                    $row['from']
                                );
                                $column['param'] = str_replace("{{{$code}}}", $val, $column['param']);
                            }
                        }
                        // map column definition on associated product only,
                        return $adapter->getMapValue($column);
                    } else {
                        // map column definition as defined in inheritance rule
                        return $this->mapByInheritanceFrom($adapter, $column, $row['from']);
                    }
                }
            }
        }

        return false;
    }

    protected function mapByInheritanceFrom(\RocketWeb\ShoppingFeeds\Model\Product\Adapter\AdapterAbstract $adapter, $column = [], $from = Inheritance::ASSOCIATED_FIRST)
    {
        switch ($from) {
            case Inheritance::PARENT_ONLY:
                $value = $this->getMapValue($column);
                break;
            case Inheritance::ASSOCIATED_ONLY:
                $value = $adapter->getMapValue($column);
                break;
            case Inheritance::PARENT_FIRST:
                $value = $this->getMapValue($column);
                if (empty($value)) {
                    $value = $adapter->getMapValue($column);
                }
                break;
            case Inheritance::ASSOCIATED_FIRST:
                $value = $adapter->getMapValue($column);
                if (empty($value)) {
                    $value = $this->getMapValue($column);
                }
                break;
        }
        return $value;
    }

    /**
     * @param $rows
     * @return $this
     */
    protected function checkEmptyColumns($row)
    {
        $skipEmptyColumn = $this->feed->getConfig('filters_skip_column_empty');

        if (is_array($skipEmptyColumn)) {
            foreach ($skipEmptyColumn as $column) {
                if (isset($row[$column]) && $row[$column] == "") {
                    $this->getLogger()->info(
                        sprintf(
                            "product id %d product sku %s, skipped - by product skip rule, has %s empty.",
                            $this->getProduct()->getId(),
                            $this->getProduct()->getSku(),
                            $column
                        )
                    );
                    $this->getGenerator()->updateCountSkip(1);
                    return false;
                }
            }
        }

        return true;
    }
}
