<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/8/16
 * Time: 3:34 PM
 */

namespace RetentionScience\Waves\Sync;


class Params
{

    /**
     * @var TypeInterface[]
     */
    private $availableTypes = [];

    /**
     * @var TypeInterface
     */
    private $type;

    /**
     * @var string[]
     */
    private $spec = [];

    /**
     * @var string
     */
    private $source;

    /**
     * @var string
     */
    private $message;

    /**
     * @var integer[]
     */
    private $storeIds;

    /**
     * Params constructor.
     * @param array $availableTypes
     */
    public function __construct(
        array $availableTypes
    )
    {
        foreach($availableTypes as $typeName => $typeModel) {
            $this->addType($typeModel);
        }
    }

    /**
     * @param TypeInterface $typeModel
     * @return $this
     */
    public function addType(TypeInterface $typeModel) {
        $this->availableTypes[] = $typeModel;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getAvailableTypeNames() {
        $typeNames = [];
        foreach($this->availableTypes as $typeModel) {
            $typeNames[] = $typeModel->getName();
        }
        return $typeNames;
    }

    /**
     * @return TypeInterface[]
     */
    public function getAvailableTypes() {
        return $this->availableTypes;
    }

    /**
     * @param string $typeName
     * @return $this
     * @throws \Exception
     */
    public function setType($typeName)
    {
        foreach($this->availableTypes as $typeModel) {
            if($typeModel->getName() === $typeName) {
                $this->type = $typeModel;
                return $this;
            }
        }
        throw new \Exception('Unknown type: ' . $typeName);
    }

    /**
     * @param string[] $spec
     * @return $this
     */
    public function setSpec($spec) {
        $this->spec = $spec;
        return $this;
    }

    /**
     * @param string $source
     * @return $this
     */
    public function setSource($source) {
        $this->source = $source;
        return $this;
    }

    /**
     * @return string
     */
    public function getSource() {
        return $this->source;
    }

    /**
     * @return TypeInterface
     */
    public function getType()
    {
        $typeSpec = $this->getTypeSpec();
        foreach($typeSpec as $name => $value) {
            $setMethod = 'set' . $name;
            if(method_exists($this->type, $setMethod)) {
                $this->type->{$setMethod}($value);
            }
        }
        return $this->type;
    }

    /**
     * @return string[]
     */
    public function getTypeSpec() {
        $typeSpec = [];
        foreach($this->type->getParams() as $typeParam) {
            $paramName = $typeParam['name'];
            foreach($this->spec as $param) {
                if(strpos($param, $paramName . ':') === 0) {
                    $typeSpec[$paramName] = substr($param, strlen($paramName) + 1);
                }
            }
        }
        return $typeSpec;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function validateType()
    {
        $typeSpec = $this->getTypeSpec();
        foreach($this->type->getParams() as $typeParam) {
            if(isset($typeParam['required']) && $typeParam['required'] && ! isset($typeSpec[$typeParam['name']])) {
                throw new \Exception('Expecting param "' . $typeParam['name'] . '" for Sync Type "' . $this->type->getName() . '"');
            } else {
                $validateMethod = 'validate' . $typeParam['name'];
                if(method_exists($this->type, $validateMethod)) {
                    $isValid = $this->type->{$validateMethod}(isset($typeSpec[$typeParam['name']]) ? $typeSpec[$typeParam['name']] : '');
                    if(! $isValid) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * @param string $message
     * @return $this
     */
    public function setMessage($message) {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * @param integer[] $storeIds
     * @return $this
     */
    public function setStoreIds($storeIds) {
        $this->storeIds = $storeIds;
        return $this;
    }

    /**
     * @return integer[]
     */
    public function getStoreIds() {
        return $this->storeIds;
    }
}