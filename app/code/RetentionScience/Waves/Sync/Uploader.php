<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/14/16
 * Time: 5:53 PM
 */

namespace RetentionScience\Waves\Sync;


use Exception;
use RetentionScience\Waves\Connection\RetentionScience\Exception as ExceptionAlias;
use RetentionScience\Waves\Helper\Data;
use Magento\Framework\Filesystem\Io\File as FileIo;
use Magento\Framework\Archive\Helper\File;
use Magento\Framework\Archive\Helper\File\Gz;
use Magento\Framework\Archive\Helper\FileFactory;
use Magento\Framework\Archive\Helper\File\GzFactory;

class Uploader
{

    /**
     * @var Data
     */
    private $helper;
    /**
     * @var Logger
     */
    private $logger;
    /**
     * @var Params
     */
    private $params;
    /**
     * @var FileIo
     */
    private $fileIo;
    /**
     * @var FileFactory
     */
    private $fileFactory;
    /**
     * @var GzFactory
     */
    private $gzFactory;

    /**
     * Uploader constructor.
     * @param Logger $logger
     * @param Data $helper
     * @param FileIo $fileIo
     * @param FileFactory $fileFactory
     * @param GzFactory $gzFactory
     */
    public function __construct(
        Logger $logger,
        Data $helper,
        FileIo $fileIo,
        FileFactory $fileFactory,
        GzFactory $gzFactory
    )
    {
        $this->helper = $helper;
        $this->logger = $logger;
        $this->fileIo = $fileIo;
        $this->fileFactory = $fileFactory;
        $this->gzFactory = $gzFactory;
    }

    /**
     * @param Params $params
     * @return $this
     */
    public function setParams(Params $params) {
        $this->params = $params;
        return $this;
    }

    /**
     * @param string[] $fileList
     * @param bool $remove
     * @return void
     * @throws ExceptionAlias
     */
    public function processFiles(array $fileList, $remove = false) {
        $api = $this->helper->getApi();
        $this->logger
            ->setExportName('upload')
            ->setExportSource($this->params->getSource())
            ->setExportProcessedRecord(0)
            ->setExportTotalRecords(count($fileList))
            ->logStart();
        $this->logger->setListenErrors(true);
        try {
            foreach($fileList as $group => $filename) {
                if (! $this->fileIo->fileExists($filename)) {
                    continue;
                }
                if(! $this->fileIo->isWriteable($filename)) {
                    throw new Exception('File "' . $filename . '" is not writable');
                }
                if($this->helper->isBulkCompressionEnabled()) {
                    $gzfilename = $filename . '.gz';
                    /** @var File $file */
                    $file = $this->fileFactory->create([
                        'filePath' => $filename,
                    ]);
                    $file->open('rb');
                    /** @var Gz $gzipFile */
                    $gzipFile = $this->gzFactory->create([
                        'filePath' => $gzfilename,
                    ]);
                    $gzipFile->open('wb');
                    while(! $file->eof()) {
                        $gzipFile->write($file->read(1024 * 512));
                    }
                    $gzipFile->close();
                    $file->close();
                    $fileList[$group] = $gzfilename;
                    $this->fileIo->rm($filename);
                }
            }
            $api->sync_data($fileList);
            if($remove) {
                foreach($fileList as $filename) {
                    $this->fileIo->rm($filename);
                }
            }
            $this->logger->logSuccess();
        } catch(Exception $e) {
            // Catch exception
            $errno = $e->getCode();
            $errstr = $e->getMessage();
            $errfile = $e->getFile();
            $errline = $e->getLine();
            // Do stuff
            $this->logger->logException($errno, $errstr, $errfile, $errline);
        }

        $this->logger->setListenErrors(false);
        $this->logger->sendLogs();
    }

}