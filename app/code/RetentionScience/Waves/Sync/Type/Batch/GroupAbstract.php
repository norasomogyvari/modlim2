<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/8/16
 * Time: 8:26 PM
 */

namespace RetentionScience\Waves\Sync\Type\Batch;


use Exception;
use Magento\Eav\Model\Config;
use Magento\Eav\Model\Entity\Attribute\AbstractAttribute;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Resource;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Module\Manager;
use RetentionScience\Waves\Connection\AWS\CloudWatch;
use RetentionScience\Waves\Connection\RetentionScience\Exception as ExceptionAlias;
use RetentionScience\Waves\Helper\Data;
use Magento\Framework\Filesystem\Io\File as FileIo;

abstract class GroupAbstract
{

    CONST RECONNECT_DELAY = 10;

    CONST RECONNECT_LIMIT = 3;

    CONST WRITE_LINES_BUFFER_MAX_SIZE = 1000;

    /**
     * Used for batch filename generation
     * @var int
     */
    private $timestamp;
    /**
     * @var Data
     */
    private $helper;
    /**
     * @var ResourceConnection
     */
    private $resource;
    /**
     * @var AdapterInterface
     */
    private $readConnection;
    /**
     * @var DirectoryList
     */
    private $directoryList;
    /**
     * Cache bulk file name
     * @var string
     */
    private $bulkFile;
    /**
     * Number of currently processed records
     * @var int
     */
    protected $processedRecords;
    /**
     * Number of total records
     * @var int
     */
    private $totalRecords;
    /**
     * @var Config
     */
    private $eavConfig;
    /**
     * @var AbstractAttribute[]
     */
    private $eavAttributes;
    /**
     * Offset in query
     * @var int
     */
    protected $offset;
    /**
     * @var CloudWatch
     */
    private $cloudWatch;
    /**
     * @var Manager
     */
    private $moduleManager;
    /**
     * Cached records
     * @var array
     */
    protected $data = [];
    /**
     * Cached records ids
     * @var int[]
     */
    protected $entityIds;
    /**
     * Number of lines in buffer
     * @var int
     */
    private $bufferSize = 0;
    /**
     * Buffer
     * @var string[]
     */
    private $buffer = [];
    /**
     * @var FileIo
     */
    private $fileIo;

    /**
     * GroupAbstract constructor.
     * @param Manager $moduleManager
     * @param Data $helper
     * @param ResourceConnection $resource
     * @param DirectoryList $directoryList
     * @param Config $eavConfig
     * @param CloudWatch $cloudWatch
     * @param FileIo $fileIo
     */
    public function __construct(
        Manager $moduleManager,
        Data $helper,
        ResourceConnection $resource,
        DirectoryList $directoryList,
        Config $eavConfig,
        CloudWatch $cloudWatch,
        FileIo $fileIo
    )
    {
        $this->helper = $helper;
        $this->resource = $resource;
        $this->readConnection = $resource->getConnection(ResourceConnection::DEFAULT_CONNECTION);
        $this->directoryList = $directoryList;
        $this->eavConfig = $eavConfig;
        $this->cloudWatch = $cloudWatch;
        $this->moduleManager = $moduleManager;
        $this->fileIo = $fileIo;
    }




    /**
     * @return void
     */
    abstract protected function loadEntityData();

    /**
     * @return void
     */
    abstract protected function loadAdditionalData();

    /**
     * @return int
     */
    abstract protected function getTotalRecords();

    /**
     * @param string[] $data
     * @return string
     */
    abstract protected function getPrimaryKey($data);

    /**
     * @return string
     */
    abstract protected function getBulkFileName();

    /**
     * Look at http://devdocs.magento.com/guides/v2.1/extension-dev-guide/staging/Staging.html
     * @return string|bool
     */
    abstract protected function getStageModule();

    /**
     * @return int
     */
    abstract protected function getLimit();

    /**
     * @return string[]
     */
    abstract protected function getExportFields();

    /**
     * @return string
     */
    abstract public function getExportName();


    /**
     * Runs export and saves all data into bulk file
     * @param int $timestamp
     * @return void
     * @throws ExceptionAlias
     */
    public function run($timestamp)
    {
        $this->timestamp = $timestamp;
        $this->bulkFile = null;
        $this->totalRecords = $this->getTotalRecords();
        $this->offset = 0;
        $this->processedRecords = 0;
        if ($this->helper->isEnabled()){
            $this->cloudWatch->logMessage("run called for file " . $this->getBulkFile() . " TotalRecords " . $this->totalRecords);
        }

        while($this->offset < $this->totalRecords) {

            $this->loadEntityData();
            $this->loadAdditionalData();
            $this->exportData();
            $this->cleanData();
            
            $this->offset += $this->getLimit();
        }
    }

    /**
     * @return Data
     */
    protected function getHelper() {
        return $this->helper;
    }

    /**
     * @return ResourceConnection
     */
    protected function getResource() {
        return $this->resource;
    }

    /**
     * @return AdapterInterface
     */
    protected function getReadConnection() {
        return $this->readConnection;
    }

    /**
     * @param $query
     * @return array
     * @throws Exception
     */
    protected function fetchAll($query, $bind = array()) {
        return $this->query('fetchAll', $query, 1, $bind);
    }

    /**
     * @param $query
     * @return string
     * @throws Exception
     */
    protected function fetchOne($query, $bind = array()) {
        return $this->query('fetchOne', $query, 1, $bind);
    }

    /**
     * @param $method
     * @param $query
     * @param int $reconnections
     * @return array|string
     * @throws Exception
     */
    protected function query($method, $query, $reconnections = 1, $bind = array()) {
        try {
            if($method == 'fetchAll') {
                return $this->getReadConnection()->fetchAll($query, $bind);
            } else {
                return $this->getReadConnection()->fetchOne($query, $bind);
            }
        } catch(Exception $e) {
            if($reconnections < self::RECONNECT_LIMIT) {
                trigger_error('Mysql Error: ' . $e->getMessage(), E_USER_NOTICE);
                sleep(self::RECONNECT_DELAY);
                $this->getReadConnection()->closeConnection();
                $this->getReadConnection()->getConnection();
                return $this->query($method, $query, ++ $reconnections);
            } else {
                throw $e;
            }
        }
    }

    /**
     * @param $entity
     * @return string
     */
    protected function getTableName($entity) {
        return $this->getResource()->getTableName($entity);
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getBulkFile() {
        if(is_null($this->bulkFile)) {
            $directory = $this->directoryList->getPath('tmp');
            if(! is_dir($directory)) {
                if(! $this->fileIo->mkdir($directory, 0777, true)) {
                    throw new Exception('Unable to create temporary directory');
                }
            }
            if(! $this->fileIo->isWriteable($directory)) {
                throw new Exception('Temporary directory is not writable');
            }
            $this->bulkFile = $directory . DIRECTORY_SEPARATOR . 'rs_bulk_' . $this->getBulkFileName() . '_' . $this->timestamp . '.bulk';
        }
        return $this->bulkFile;
    }

    /**
     * @return int
     */
    public function getProcessedRecords() {
        return $this->processedRecords;
    }

    /**
     * @return int
     */
    public function getTotalRecordsCalculated() {
        return $this->totalRecords;
    }

    /**
     * @return Config
     */
    protected function getEavConfig() {
        return $this->eavConfig;
    }

    /**
     * @param string $entity
     * @param string $attribute
     * @return AbstractAttribute
     * @throws LocalizedException
     */
    protected function getEavAttribute($entity, $attribute) {
        if(! isset($this->eavAttributes[$entity . '_' . $attribute])) {
            $this->eavAttributes[$entity . '_' . $attribute] = $this->getEavConfig()->getAttribute($entity, $attribute);
        }
        return $this->eavAttributes[$entity . '_' . $attribute];
    }

    /**
     * @param string $entity
     * @param string $attribute
     * @param string $entityIdField
     * @param bool $useStore
     * @return array
     * @throws LocalizedException
     * @throws ExceptionAlias
     */
    protected function getAttributeData($entity, $attribute, $entityIdField = NULL, $useStore = TRUE) {
        $attribute = $this->getEavAttribute($entity, $attribute);
        if($attribute->getAttributeId()) {
            $backendTable = $attribute->getBackendTable();
            if(! empty($entityIdField)) {
                $_entityIds = array();
                $_mapIds = array();
                if(! empty($this->data)) {
                    foreach($this->data AS $record) {
                        if(! empty($record[$entityIdField])) {
                            $_entityIds[] = $record[$entityIdField];
                            $_mapIds[$record[$entityIdField]] = $record['entity_id'];
                        }
                    }
                    if(! empty($_entityIds)) {
                        if($useStore) {
                            $attributeData = $this->fetchAll(
                                '   SELECT
                                    `default_value`.`' . $this->getKeyField() . '` AS `' . $entityIdField . '`,
                                    IF(`store_value`.`value` IS NULL, `default_value`.`value`, `store_value`.`value`) AS `value`
                                FROM
                                    `' . $backendTable . '` AS `default_value`
                                LEFT OUTER JOIN
                                    `' . $backendTable . '` AS `store_value`
                                    ON
                                        `store_value`.`' . $this->getKeyField() . '` = `default_value`.`' . $this->getKeyField() . '` AND
                                        `store_value`.`attribute_id` = `default_value`.`attribute_id` AND
                                        `store_value`.`store_id` = ' . $this->getStoreId() . '
                                WHERE
                                    `default_value`.`attribute_id` = ' . $attribute->getAttributeId() . ' AND
                                    `default_value`.`' . $this->getKeyField() . '` IN (' . implode(', ', $_entityIds) . ') AND
                                    `default_value`.`store_id` = 0
                            ');
                        } else {
                            $attributeData = $this->fetchAll(
                                '   SELECT
                                    `default_value`.`' . $this->getKeyField() . '` AS `' . $entityIdField . '`,
                                    `default_value`.`value`
                                FROM
                                    `' . $backendTable . '` AS `default_value`
                                WHERE
                                    `default_value`.`attribute_id` = ' . $attribute->getAttributeId() . ' AND
                                    `default_value`.`' . $this->getKeyField() . '` IN (' . implode(', ', $_entityIds) . ')
                            ');
                        }
                        if(! empty($attributeData)) {
                            foreach($attributeData AS & $data) {
                                $dataAttributeId = $data[$entityIdField];
                                $entityId = $_mapIds[$dataAttributeId];
                                $data['entity_id'] = $entityId;
                            }
                        }
                        return $attributeData;
                    }
                }
            } else {
                if($useStore) {
                    return $this->fetchAll(
                        '   SELECT
                                    `default_value`.`' . $this->getKeyField() . '` AS `entity_id`,
                                    IF(`store_value`.`value` IS NULL, `default_value`.`value`, `store_value`.`value`) AS `value`
                                FROM
                                    `' . $backendTable . '` AS `default_value`
                                LEFT OUTER JOIN
                                    `' . $backendTable . '` AS `store_value`
                                    ON
                                        `store_value`.`' . $this->getKeyField() . '` = `default_value`.`' . $this->getKeyField() . '` AND
                                        `store_value`.`attribute_id` = `default_value`.`attribute_id` AND
                                        `store_value`.`store_id` = ' . $this->getStoreId() . '
                                WHERE
                                    `default_value`.`attribute_id` = ' . $attribute->getAttributeId() . ' AND
                                    `default_value`.`' . $this->getKeyField() . '` IN (' . implode(', ', $this->entityIds) . ') AND
                                    `default_value`.`store_id` = 0
                            ');
                } else {
                    return $this->fetchAll('   SELECT
                                    `default_value`.`' . $this->getKeyField() . '` AS `entity_id`,
                                    `default_value`.`value`
                                FROM
                                    `' . $backendTable . '` AS `default_value`
                                WHERE
                                    `default_value`.`attribute_id` = ' . $attribute->getAttributeId() . ' AND
                                    `default_value`.`' . $this->getKeyField() . '` IN (' . implode(', ', $this->entityIds) . ')
                            ');
                }
            }
        }
        return [];
    }

    /**
     * @param string $entity
     * @param string $attribute
     * @param string $entityIdField
     * @param bool $getLabel
     * @param bool $useStore
     * @throws LocalizedException
     * @throws ExceptionAlias
     */
    protected function fillAttributeData($entity, $attribute, $entityIdField = NULL, $getLabel = TRUE, $useStore = TRUE) {
        $data = $this->getAttributeData($entity, $attribute, $entityIdField, $useStore);
        if(! empty($data)) {
            foreach($data AS $record) {
                if($this->getEavAttribute($entity, $attribute)->getFrontendInput() === 'select' AND $getLabel) {
                    $this->data[$record['entity_id']][$attribute] = $this
                        ->getEavAttribute($entity, $attribute)
                        ->getSource()
                        ->getOptionText($record['value']);
                } else {
                    $this->data[$record['entity_id']][$attribute] = $record['value'];
                }
            }
        }
    }

    /**
     * @param string $tableName
     * @param string $key
     * @param array $fields
     * @param string $where
     * @param string $keyField
     * @throws Exception
     */
    protected function fillTableData($tableName, $key, array $fields, $where = '', $keyField = 'entity_id') {
        $ids = [];
        if($keyField !== 'entity_id') {
            foreach($this->data as $record) {
                $ids[$record[$keyField]] = $record['entity_id'];
            }
        }
        $tableData = $this->fetchAll(
            'SELECT `' . $key . '` AS `entity_id`, `' . implode('`, `', array_keys($fields)) . '` FROM `' . $tableName . '`' .
            ' WHERE `' . $key . '` IN (' . implode(', ', (empty($ids) ? $this->entityIds : array_keys($ids))) . ')' . ($where ? ' AND ' . $where : '')
        );
        if(! empty($tableData)) {
            foreach($tableData AS $data) {
                foreach($fields AS $key => $alias) {
                    if(empty($ids)) {
                        $this->data[$data['entity_id']][$alias] = $data[$key];
                    } else {
                        $this->data[$ids[$data['entity_id']]][$alias] = $data[$key];
                    }
                }
            }
        }
    }

    /**
     * @return void
     * @throws ExceptionAlias
     */
    private function exportData() {
        if(! empty($this->getExportFields()) AND ! empty($this->data)) {
            foreach($this->data AS $data) {
                if(! $this->shouldExportRow($data)) {
                    continue;
                }
                $record = [
                    'store_id' => $this->helper->getStoreId(),
                ];
                foreach($this->getExportFields() AS $field) {
                    $record[$field] = $this->{'get' . str_replace('_', '', $field)}($data);
                }
                $this->saveToBuffer($record);
                $this->writeToFile();
            }
            $this->writeToFile(true);
        }
    }

    private function writeToFile($ignoreBufferMaxSize = false) {
        if ($this->bufferSize > 0 && ($ignoreBufferMaxSize || $this->bufferSize >= static::WRITE_LINES_BUFFER_MAX_SIZE)) {
            file_put_contents($this->getBulkFile(), implode("\n", $this->buffer) . "\n", FILE_APPEND);
            $this->bufferSize = 0;
            $this->buffer = [];
        }
    }

    /**
     * @param array $record
     */
    protected function saveToBuffer(array $record) {
        $this->bufferSize += 1;
        $this->buffer[] = json_encode($record);
    }

    /**
     * @return void
     */
    private function cleanData() {
        $this->data = null;
        gc_collect_cycles();
    }

    /**
     * @return int
     * @throws ExceptionAlias
     */
    public function getStoreId() {
        return $this->helper->getStoreId();
    }

    /**
     * @return string
     */
    protected function getKeyField() {
        return $this->useStage() ? 'row_id' : 'entity_id';
    }

    /**
     * @return bool
     */
    protected function useStage() {
        $stageModule = $this->getStageModule();
        if($stageModule) {
            $stageModule = $this->moduleManager->isEnabled($stageModule);
        }
        return $stageModule;
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getRecordId($data) {
        return $this->getPrimaryKey($data);
    }

    /**
     * @param string[] $data
     * @return bool
     */
    protected function shouldExportRow($data) {
        return true;
    }

    /**
     * @return string
     */
    protected function getStagingWhereStatement($originalWhereCondition = '', $tableAlias = '') {
        if (!empty($tableAlias)) {
            $tableAlias = "`${tableAlias}`.";
        }
        $versionId = (int) $this->helper->getStaging()->getCurrentVersionId();
        if ($this->useStage()) {
            $stagingWhereCondition = "${tableAlias}`created_in` <= ${versionId} AND ${tableAlias}updated_in > ${versionId}";
        } else {
            $stagingWhereCondition = '';
        }
        $where = '';
        if (!empty($originalWhereCondition)) {
            $where .= ($where ? ' AND ' : '') . '(' . $originalWhereCondition . ')';
        }
        if (!empty($stagingWhereCondition)) {
            $where .= ($where ? ' AND ' : '') . '(' . $stagingWhereCondition . ')';
        }
        if (!empty($where)) {
            return " WHERE ${where}";
        }
        return '';
    }
}