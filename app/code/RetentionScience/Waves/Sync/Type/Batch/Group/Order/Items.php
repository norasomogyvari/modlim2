<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/8/16
 * Time: 7:53 PM
 */

namespace RetentionScience\Waves\Sync\Type\Batch\Group\Order;


use RetentionScience\Waves\Sync\Type\Batch\GroupAbstract;

class Items extends GroupAbstract
{

    /*************
     * LOAD DATA *
     *************/

    /**
     * @return void
     * @throws \Exception
     */
    protected function loadEntityData()
    {
        $tableName = $this->getTableName('sales_order_item');
        $orderTableName = $this->getTableName('sales_order');
        $query = '
            SELECT `oi`.`item_id` AS `entity_id`, `oi`.`order_id` AS `order_record_id`, `oi`.`product_id`, `oi`.`name`, `oi`.`qty_ordered`, `oi`.`price`, `oi`.`sku`
            FROM `' . $tableName . '` AS `oi`
            INNER JOIN `' . $orderTableName . '` AS `o` ON `oi`.`order_id` = `o`.`entity_id`
            WHERE `o`.`store_id` = :store_id
            ORDER BY `oi`.`item_id` ASC
            LIMIT ' . $this->offset . ', ' . $this->getLimit();
        $this->data = $this->fetchAll($query, [
            ':store_id' => $this->getHelper()->getStoreId(),
        ]);
        $this->processedRecords += count($this->data);
        $this->entityIds = array();
        if(! empty($this->data)) {
            $sortedData = array();
            foreach($this->data AS $record) {
                if(empty($record['entity_id'])) {
                    continue;
                }
                $this->entityIds[] = $record['entity_id'];
                $sortedData[$record['entity_id']] = $record;
            }
            $this->data = $sortedData;
        }
    }

    /**
     * @return void
     * @throws \Exception
     */
    protected function loadAdditionalData()
    {
        $this->loadProductCategories();
    }

    /**
     * @return void
     * @throws \Exception
     */
    private function loadProductCategories()
    {
        if(empty($this->entityIds)) {
            return;
        }
        // Add categories to products
        $products = array();
        $productIds = array();
        if(! empty($this->data)) {
            foreach($this->data AS $row) {
                $productId = $row['product_id'];
                if(empty($productId)) {
                    continue;
                }
                if(! in_array($productId, $productIds)) {
                    $productIds[] = $productId;
                }
                if(! isset($products[$productId])) {
                    $products[$productId] = array();
                }
            }
        }
        if(! empty($productIds)) {
            $categoryProductTable = $this->getTableName('catalog_category_product');
            $cats = $this->fetchAll('
                SELECT `category_id`, `product_id` AS `entity_id` FROM `' . $categoryProductTable . '`
                WHERE `product_id` IN (' . implode(', ', $productIds) . ') ORDER BY `position` ASC
            ');
            if(! empty($cats)) {
                foreach($cats AS $cat) {
                    $entityId = $cat['entity_id'];
                    $categoryId = $cat['category_id'];
                    $products[$entityId][] = $categoryId;
                }
            }
        }
        // Fill data
        if(! empty($this->data)) {
            foreach($this->data AS $row) {
                $entityId = $row['entity_id'];
                $productId = $row['product_id'];
                if(isset($products[$productId])) {
                    $this->data[$entityId]['categories'] = $products[$productId];
                }
            }
        }
    }

    /**
     * @return int
     * @throws \Exception
     */
    protected function getTotalRecords()
    {
        $orderTableName = $this->getTableName('sales_order');
        return (int) $this
            ->fetchOne('
                SELECT COUNT(*) FROM `' . $this->getTableName('sales_order_item') . '` AS `oi`
                INNER JOIN `' . $orderTableName . '` AS `o` ON `oi`.`order_id` = `o`.`entity_id`
                WHERE `o`.`store_id` = :store_id
                ',
                [
                    ':store_id' => $this->getHelper()->getStoreId(),
                ]);
    }

    /**********
     * CONFIG *
     **********/

    /**
     * @return string
     */
    protected function getBulkFileName()
    {
        return 'order_items';
    }

    /**
     * Look at http://devdocs.magento.com/guides/v2.1/extension-dev-guide/staging/Staging.html
     * @return string|bool
     */
    protected function getStageModule()
    {
        return false;
    }

    /**
     * @return int
     */
    protected function getLimit()
    {
        return 1000;
    }

    /**
     * @return string[]
     */
    protected function getExportFields()
    {
        return [
            'order_record_id',
            'item_record_id',
            'name',
            'quantity',
            'price',
            'final_price',
            'attribute_1',
            'categories',
        ];
    }

    /**
     * @return string
     */
    public function getExportName()
    {
        return 'order_items';
    }

    /*****************
     * EXPORT FIELDS *
     *****************/

    /**
     * @param string[] $data
     * @return string
     */
    protected function getPrimaryKey($data)
    {
        return $data['entity_id'];
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getOrderRecordId($data) {
        return $data['order_record_id'];
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getItemRecordId($data) {
        return $data['product_id'];
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getName($data) {
        return $data['name'];
    }

    /**
     * @param string[] $data
     * @return int
     */
    protected function getQuantity($data) {
        return (int) $data['qty_ordered'];
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getPrice($data) {
        return number_format($data['price'], 2, '.', '');
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getFinalPrice($data) {
        return number_format($data['price'] * $data['qty_ordered'], 2, '.', '');
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getAttribute1($data) {
        return $data['sku'];
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getCategories($data) {
        if(isset($data['categories'])) {
            return implode(',', $data['categories']);
        } else {
            return '';
        }
    }
}