<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/13/16
 * Time: 7:16 PM
 */

namespace RetentionScience\Waves\Sync\Type\Batch\Group\Order;


use RetentionScience\Waves\Connection\RetentionScience\Exception;
use RetentionScience\Waves\Sync\Type\Batch\Group\Customer as AllCustomers;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Directory\Model\Config\Source\Allregion;
use Magento\Framework\Module\Manager;
use RetentionScience\Waves\Helper\Data;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Eav\Model\Config;
use RetentionScience\Waves\Connection\AWS\CloudWatch;
use RetentionScience\Waves\Sync\Type\Batch\GroupAbstract;
use RetentionScience\Waves\Sync\Type\Batch\Group\Subscriber as SubscriberProxy;
use Magento\Framework\Filesystem\Io\File as FileIo;

class Customer extends AllCustomers
{

    /**
     * @var DateTime
     */
    private $dateTime;

    /**
     * @var AllCustomers
     */
    private $customerExport;

    /**
     * Customer constructor.
     * @param SubscriberProxy $subscriberCustomer
     * @param DateTime $dateTime
     * @param Allregion $regions
     * @param Manager $moduleManager
     * @param Data $helper
     * @param ResourceConnection $resource
     * @param DirectoryList $directoryList
     * @param Config $eavConfig
     * @param CloudWatch $cloudWatch
     * @param FileIo $fileIo
     */
    public function __construct(
        SubscriberProxy $subscriberCustomer,
        DateTime $dateTime,
        Allregion $regions,
        Manager $moduleManager,
        Data $helper,
        ResourceConnection $resource,
        DirectoryList $directoryList,
        Config $eavConfig,
        CloudWatch $cloudWatch,
        FileIo $fileIo
    )
    {
        parent::__construct($this, $subscriberCustomer, $dateTime, $regions, $moduleManager, $helper, $resource, $directoryList, $eavConfig, $cloudWatch, $fileIo);
        $this->dateTime = $dateTime;
    }

    public function setCustomerExportModel(AllCustomers $customerExportModel) {
        $this->customerExport = $customerExportModel;
    }

    /************
     * OVERRIDE *
     ************/

    /**
     * @param int $timestamp
     * @return void;
     * @throws Exception
     */
    public function run($timestamp)
    {
        GroupAbstract::run($timestamp);
    }

    /**
     * @param string[] $data
     * @return bool
     */
    protected function shouldExportRow($data)
    {
        $pk = $this->getPrimaryKey($data);
        $ret = ! $this->customerExport->isExported($pk);
        $this->customerExport->addExportedId($pk);
        return $ret;
    }

    /*************
     * LOAD DATA *
     *************/

    /**
     * @return void
     * @throws \Exception
     */
    protected function loadEntityData()
    {
        $tableName = $this->getTableName('sales_order');
        $query = '
            SELECT `' . $this->getKeyField() . '` AS `entity_id`, `customer_email` AS `email`, `created_at`
            FROM `' . $tableName . '`
            WHERE `store_id` = :store_id AND `customer_is_guest` = 1
            LIMIT ' . $this->offset . ', ' . $this->getLimit();
        $this->data = $this->fetchAll($query, [
            ':store_id' => $this->getHelper()->getStoreId(),
        ]);
        $this->processedRecords += count($this->data);
        $this->entityIds = [];
        if(! empty($this->data)) {
            $sortedData = array();
            foreach($this->data AS $record) {
                if(empty($record['entity_id'])) {
                    continue;
                }
                $this->entityIds[] = $record['entity_id'];
                $sortedData[$record['entity_id']] = $record;
            }
            $this->data = $sortedData;
        }
    }

    /**
     * @return void
     * @throws \Exception
     */
    protected function loadAdditionalData()
    {
        /* Address Data */
        $this->fillTableData($this->getTableName('sales_order_address'), 'parent_id', array(
            'street'                => 'street',
            'city'                  => 'city',
            'region_id'             => 'region_id',
            'postcode'              => 'postcode',
            'country_id'            => 'country_id',
            'telephone'             => 'telephone',
            'prefix'                =>  'prefix',
            'firstname'             =>  'firstname',
            'suffix'                =>  'suffix',
            'middlename'            =>  'middlename',
            'lastname'              =>  'lastname',
        ), '`address_type` = "billing"');
        /* NEWSLETTER SUBSCRIPTION DATA */
        $this->loadNewsletterSubscriptionData();
    }

    /**
     * @return int
     * @throws \Exception
     */
    protected function getTotalRecords() {
        return (int) $this
            ->fetchOne('
                SELECT COUNT(*)
                FROM `' . $this->getTableName('sales_order') . '`
                WHERE `store_id` = :store_id AND `customer_is_guest` = 1',
                [
                    ':store_id' => $this->getHelper()->getStoreId(),
                ]);
    }

    /*****************
     * EXPORT FIELDS *
     *****************/

    /**
     * @param string[] $data
     * @return mixed
     */
    protected function getLastLogOnAt($data) {
        return $this->dateTime->date('Y-m-d', $data['created_at']);
    }

    /**
     * @param string[] $data
     * @return mixed
     */
    protected function getPrimaryKey($data) {
        return Data::md5(trim(strtolower($data['email'])));
    }

    private function loadNewsletterSubscriptionData()
    {
        $mapEmailsToIds = array();
        $bind = array();
        $where = array();
        $index = 1;
        if (!empty($this->data)) {
            foreach($this->data as $record) {
                if(empty($record['entity_id']) OR empty($record['email'])) {
                    continue;
                }
                $placeholder = ':email' . $index;
                $where[] = $placeholder;
                $bind[$placeholder] = $record['email'];
                $mapEmailsToIds[$record['email']] = $record['entity_id'];
                $index ++;
            }
        }
        if(empty($mapEmailsToIds)) {
            return;
        }
        $tableData = $this->fetchAll(
            'SELECT `subscriber_email`, `subscriber_status` FROM `' . $this->getTableName('newsletter_subscriber') . '`' .
            ' WHERE `subscriber_email` IN (' . implode(', ', $where) . ')', $bind
        );
        if (!empty($tableData)) {
            foreach($tableData as $record) {
                $email = $record['subscriber_email'];
                $status = $record['subscriber_status'];
                if (isset($mapEmailsToIds[$email])) {
                    $recordId = $mapEmailsToIds[$email];
                    if(isset($this->data[$recordId])) {
                        $this->data[$recordId]['subscriber_status'] = $status;
                    }
                }
            }
        }
    }

}