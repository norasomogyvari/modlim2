<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/8/16
 * Time: 7:52 PM
 */

namespace RetentionScience\Waves\Sync\Type\Batch\Group;


use Exception;
use Magento\Catalog\Model\Product as ProductModel;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\UrlInterface;
use Magento\GroupedProduct\Model\ResourceModel\Product\Link;
use Magento\Store\Model\StoreManagerInterface;
use RetentionScience\Waves\Connection\RetentionScience\Exception as ExceptionAlias;
use RetentionScience\Waves\Sync\Type\Batch\GroupAbstract;
use Magento\Framework\Module\Manager;
use RetentionScience\Waves\Helper\Data;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Eav\Model\Config;
use RetentionScience\Waves\Connection\AWS\CloudWatch;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Framework\Filesystem\Io\File as FileIo;

class Product extends GroupAbstract
{

    // In the future, we may choose to include grouped hierarchy
    const USE_CONFIGURABLE = true;
    const USE_GROUPED = false;
    const USE_BUNDLE = true;

    /**
     * @var array
     */
    private $configurableLinks = [];

    /**
     * @var array
     */
    private $configurablePrices = [];

    /**
     * @var array
     */
    private $groupedLinks;

    /**
     * @var array
     */
    private $bundleLinks;

    /**
     * @var string[]
     */
    private $simpleProducts = ['simple', 'virtual', 'downloadable'];

    /**
     * @var string[]
     */
    private $compositeProducts = ['configurable', 'grouped', 'bundle'];

    /**
     * @var integer[]
     */
    private $visibleProducts = [Visibility::VISIBILITY_BOTH, Visibility::VISIBILITY_IN_CATALOG];

    /**
     * @var array
     */
    private $configurableActive;

    /**
     * @var array
     */
    private $groupedActive;

    /**
     * @var array
     */
    private $simpleActive;

    /**
     * @var array
     */
    private $simplePrices;

    /**
     * @var array
     */
    private $simpleQuantity;

    /**
     * @var array
     */
    private $simpleVisible;

    /**
     * @var string
     */
    private $mediaUrl;
    /**
     * @var ProductModel
     */
    private $productModel;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var TimezoneInterface
     */
    private $localeDate;

    protected $mapEntityIdToRowId = [];
    protected $mapRowIdToEntityId = [];

    /**
     * Product constructor.
     * @param TimezoneInterface $localeDate
     * @param ProductModel $productModel
     * @param StoreManagerInterface $storeManager
     * @param Manager $moduleManager
     * @param Data $helper
     * @param ResourceConnection $resource
     * @param DirectoryList $directoryList
     * @param Config $eavConfig
     * @param CloudWatch $cloudWatch
     * @param FileIo $fileIo
     */
    public function __construct(
        TimezoneInterface $localeDate,
        ProductModel $productModel,
        StoreManagerInterface $storeManager,
        Manager $moduleManager,
        Data $helper,
        ResourceConnection $resource,
        DirectoryList $directoryList,
        Config $eavConfig,
        CloudWatch $cloudWatch,
        FileIo $fileIo)
    {
        parent::__construct($moduleManager, $helper, $resource, $directoryList, $eavConfig, $cloudWatch, $fileIo);
        $this->productModel = $productModel;
        $this->storeManager = $storeManager;
        $this->localeDate = $localeDate;
    }

    protected function setMapEntityIdToRowId($entityId, $rowId) {
        if (!$this->useStage()) {
            return;
        }
        $this->mapEntityIdToRowId[$entityId] = $rowId;
        $this->mapRowIdToEntityId[$rowId] = $entityId;
    }

    protected function getMapEntityIdToRowId($entityId) {
        if (!$this->useStage()) {
            return $entityId;
        }
        if (!isset($this->mapEntityIdToRowId[$entityId])) {
            return $entityId;
        }
        return $this->mapEntityIdToRowId[$entityId];
    }

    protected function getMapRowIdToEntityId($rowId) {
        if (!$this->useStage()) {
            return $rowId;
        }
        if (!isset($this->mapRowIdToEntityId[$rowId])) {
            return $rowId;
        }
        return $this->mapRowIdToEntityId[$rowId];
    }

    protected function isSimple($type) {
        return in_array($type, $this->simpleProducts);
    }

    /**********
     * CONFIG *
     **********/

    /**
     * @return string
     */
    protected function getBulkFileName()
    {
        return 'items';
    }

    /**
     * Look at http://devdocs.magento.com/guides/v2.1/extension-dev-guide/staging/Staging.html
     * @return string|bool
     */
    protected function getStageModule()
    {
        return 'Magento_CatalogStaging';
    }

    /**
     * @return int
     */
    protected function getLimit()
    {
        return 1000;
    }

    /**
     * @return string[]
     */
    protected function getExportFields()
    {
        return [
            'active',
            'record_id',
            'name',
            'manufacturer',
            'model',
            'quantity',
            'price',
            'image_list',
            'item_url',
            'parent_record_id',
            'attribute_1',
            'categories',
            'do_not_recommend',
            'display_name',
        ];
    }

    /**
     * @return string
     */
    public function getExportName()
    {
        return 'items';
    }

    /*************
     * LOAD DATA *
     *************/

    /**
     * @return void
     * @throws Exception
     */
    protected function loadEntityData()
    {
        $tableName = $this->getTableName('catalog_product_entity');
        $websiteTableName = $this->getTableName('catalog_product_website');
        $query = '
            SELECT `p`.`' . $this->getKeyField() . '` AS `entity_id`, `p`.`entity_id` AS `original_id`, `p`.`type_id`, `p`.`sku`
            FROM `' . $tableName . '` AS `p`
            INNER JOIN `' . $websiteTableName . '` as `pw` ON `p`.`entity_id` = `pw`.`product_id`
            ' . $this->getStagingWhereStatement('`pw`.`website_id` = :website_id', 'p') . '
            ORDER BY FIELD(`p`.`type_id`, "simple", "virtual", "downloadable", "configurable", "grouped", "bundle"), `p`.`entity_id` ASC
            LIMIT ' . $this->offset . ', ' . $this->getLimit();
        $this->data = $this->fetchAll($query, [
            ':website_id' => $this->getHelper()->getWebsiteId(),
        ]);
        $this->processedRecords += count($this->data);
        $this->entityIds = array();
        if(! empty($this->data)) {
            $sortedData = array();
            foreach($this->data AS $record) {
                if(empty($record['entity_id'])) {
                    continue;
                }
                $this->entityIds[] = $record['entity_id'];
                $sortedData[$record['entity_id']] = $record;
                $this->setMapEntityIdToRowId($record['original_id'], $record['entity_id']);
            }
            $this->data = $sortedData;
        }
    }

    /**
     * @return void
     * @throws LocalizedException
     * @throws ExceptionAlias
     * @throws Exception
     */
    protected function loadAdditionalData()
    {
        /* PRODUCT DATA */
        $this->fillAttributeData('catalog_product', 'name');
        $this->fillAttributeData('catalog_product', 'manufacturer');
        $this->fillAttributeData('catalog_product', 'price');
        $this->fillAttributeData('catalog_product', 'special_price');
        $this->fillAttributeData('catalog_product', 'special_from_date');
        $this->fillAttributeData('catalog_product', 'special_to_date');
        $this->fillAttributeData('catalog_product', 'image');
        $this->fillAttributeData('catalog_product', 'status', NULL, FALSE);
        $this->fillAttributeData('catalog_product', 'url_key');
        $this->fillAttributeData('catalog_product', 'visibility', NULL, FALSE);
        $this->fillAttributeData('catalog_product', 'resci_recommend', NULL);
        $this->fillAttributeData('catalog_product', 'resci_display_name');
        /* STOCK DATA */
        $this->fillTableData($this->getTableName('cataloginventory_stock_item'), 'product_id', array(
            'qty'                       => 'stock_qty',
            'use_config_manage_stock'   => 'stock_use_config_manage_stock',
            'manage_stock'              => 'stock_manage_stock',
            'is_in_stock'               => 'stock_is_in_stock',
        ), '', 'original_id');
        /* Media Gallery */
        $this->loadMediaGallery();
        /* Categories */
        $this->loadCategories();
        /* Configurable Links */
        $this->loadConfigurableLinks();
        /* Grouped Links */
        $this->loadGroupedLInks();
        /* Bundle links */
        $this->loadBundleLinks();
        /* Bundle options */
        $this->fillBundleOptions();
    }

    protected function fillBundleOptions() {
        if (!self::USE_BUNDLE) {
            return;
        }
        if(empty($this->entityIds)) {
            return;
        }
        $ids = $this->entityIds;
        $selectionTableName = $this->getTableName('catalog_product_bundle_selection');
        $optionTableName = $this->getTableName('catalog_product_bundle_option');
        $rows = $this->fetchAll('
            SELECT `s`.`parent_product_id` AS `entity_id`, `s`.`product_id` AS `child_id`, `o`.`option_id`, `o`.`required` FROM `' . $selectionTableName . '` AS `s`
            INNER JOIN `' . $optionTableName . '` AS `o` ON `o`.`option_id` = `s`.`option_id`
            WHERE `s`.`parent_product_id` IN (' . implode(', ', $ids) . ')
        ');
        if(! empty($rows)) {
            foreach($rows AS $row) {
                $entityId = $row['entity_id'];
                $childId = $row['child_id'];
                $optionId = $row['option_id'];
                $required = $row['required'];
                if (!isset($this->data[$entityId]['bundle_options'])) {
                    $this->data[$entityId]['bundle_options'] = array();
                    $this->data[$entityId]['bundle_selections'] = array();
                }
                $this->data[$entityId]['bundle_options'][] = array(
                    'optionId' => $optionId,
                    'required' => $required,
                );
                $this->data[$entityId]['bundle_selections'][] = array(
                    'productId' => $childId,
                    'optionId' => $optionId,
                );
            }
        }
    }

    /**
     * @return void
     * @throws Exception
     */
    private function loadBundleLinks()
    {
        if (!self::USE_BUNDLE) {
            return;
        }
        if(empty($this->entityIds)) {
            return;
        }
        if($this->useStage()) {
            $ids = [];
            foreach($this->data as $record) {
                $ids[$record['original_id']] = $record['entity_id'];
            }
        } else {
            $ids = $this->entityIds;
        }
        $linkTable = $this->getTableName('catalog_product_bundle_selection');
        $productTable = $this->getTableName('catalog_product_entity');
        $rows = $this->fetchAll('
            SELECT `p`.`entity_id` AS `parent_id`, `l`.`product_id` AS `entity_id`
            FROM `' . $linkTable . '` AS `l`
            INNER JOIN `' . $productTable . '` AS `p` ON `l`.`parent_product_id` = `p`.`' . $this->getKeyField() . '`
            ' . $this->getStagingWhereStatement('`l`.`product_id` IN (' . implode(', ', ($this->useStage() ? array_keys($ids) : $ids)) . ')', 'p') . '
        ');
        if(! empty($rows)) {
            foreach($rows AS $row) {
                if($this->useStage()) {
                    $entityId = $ids[$row['entity_id']];
                } else {
                    $entityId = $row['entity_id'];
                }
                $parentId = $row['parent_id'];
                $this->bundleLinks[$entityId] = $parentId;
            }
        }
    }

    /**
     * @return void
     * @throws Exception
     */
    private function loadGroupedLInks()
    {
        if (!self::USE_GROUPED) {
            return;
        }
        if(empty($this->entityIds)) {
            return;
        }
        if($this->useStage()) {
            $ids = [];
            foreach($this->data as $record) {
                $ids[$record['original_id']] = $record['entity_id'];
            }
        } else {
            $ids = $this->entityIds;
        }
        $linkTable = $this->getTableName('catalog_product_link');
        $rows = $this->fetchAll('
            SELECT `linked_product_id` AS `entity_id`, `product_id` AS `parent_id` FROM `' . $linkTable . '`
            WHERE `linked_product_id` IN (' . implode(', ', ($this->useStage() ? array_keys($ids) : $ids)) . ')
              AND `link_type_id` = ' . Link::LINK_TYPE_GROUPED . '
        ');
        if(! empty($rows)) {
            foreach($rows AS $row) {
                if($this->useStage()) {
                    $entityId = $ids[$row['entity_id']];
                } else {
                    $entityId = $row['entity_id'];
                }
                $parentId = $row['parent_id'];
                $this->groupedLinks[$entityId] = $parentId;
            }
        }
    }

    /**
     * @return void
     * @throws LocalizedException
     * @throws ExceptionAlias
     */
    private function loadConfigurableLinks()
    {
        if (!self::USE_CONFIGURABLE) {
            return;
        }
        if(empty($this->entityIds)) {
            return;
        }
        if($this->useStage()) {
            $ids = [];
            foreach($this->data as $record) {
                $ids[$record['original_id']] = $record['entity_id'];
            }
        } else {
            $ids = $this->entityIds;
        }
        $configurableLinkTable = $this->getTableName('catalog_product_super_link');
        $attribute = $this->getEavAttribute('catalog_product', 'url_key');
        if ($attribute->getAttributeId()) {
            $backendTable = $attribute->getBackendTable();
            $rows = $this->fetchAll('
                SELECT `cl`.`product_id` AS `entity_id`, `cl`.`parent_id`, 
                       IF(`store_value`.`value` IS NULL, `default_value`.`value`, `store_value`.`value`) AS `url_key`
                FROM `' . $configurableLinkTable . '` AS `cl`
                LEFT OUTER JOIN `' . $backendTable . '` as `default_value`
                    ON
                        `cl`.`parent_id` = `default_value`.`' . $this->getKeyField() . '` AND
                        `default_value`.`attribute_id` = ' . $attribute->getAttributeId() . ' AND
                        `default_value`.`store_id` = 0
                LEFT OUTER JOIN `' . $backendTable . '` as `store_value`
                    ON
                        `cl`.`parent_id` = `store_value`.`' . $this->getKeyField() . '` AND
                        `store_value`.`attribute_id` = ' . $attribute->getAttributeId() . ' AND
                        `store_value`.`store_id` = ' . $this->getStoreId() . '
                WHERE `cl`.`product_id` IN (' . implode(', ', ($this->useStage() ? array_keys($ids) : $ids)) . ')
            ');
        } else {
            $rows = $this->fetchAll('
                SELECT `product_id` AS `entity_id`, `parent_id` FROM `' . $configurableLinkTable . '`
                WHERE `product_id` IN (' . implode(', ', ($this->useStage() ? array_keys($ids) : $ids)) . ')
            ');
        }
        if(! empty($rows)) {
            foreach($rows AS $row) {
                if($this->useStage()) {
                    $entityId = $ids[$row['entity_id']];
                } else {
                    $entityId = $row['entity_id'];
                }
                $parentId = $row['parent_id'];
                $this->configurableLinks[$entityId] = $parentId;
                if (! empty($row['url_key'])) {
                    $this->data[$entityId]['parent_url_key'] = $row['url_key'];
                }
            }
        }
    }

    /**
     * @return void
     * @throws Exception
     */
    private function loadCategories()
    {
        if(empty($this->entityIds)) {
            return;
        }
        if($this->useStage()) {
            $ids = [];
            foreach($this->data as $record) {
                $ids[$record['original_id']] = $record['entity_id'];
            }
        } else {
            $ids = $this->entityIds;
        }
        $categoryProductTable = $this->getTableName('catalog_category_product');
        $rows = $this->fetchAll('
            SELECT `category_id`, `product_id` AS `entity_id` FROM `' . $categoryProductTable . '`
            WHERE `product_id` IN (' . implode(', ', ($this->useStage() ? array_keys($ids) : $ids)) . ') ORDER BY `position` ASC
        ');
        if(! empty($rows)) {
            foreach($rows AS $row) {
                if($this->useStage()) {
                    $entityId = $ids[$row['entity_id']];
                } else {
                    $entityId = $row['entity_id'];
                }
                $categoryId = $row['category_id'];
                if(! isset($this->data[$entityId]['categories'])) {
                    $this->data[$entityId]['categories'] = array();
                }
                $this->data[$entityId]['categories'][] = $categoryId;
            }
        }
    }

    /**
     * @return void
     * @throws ExceptionAlias
     */
    private function loadMediaGallery()
    {
        if(empty($this->entityIds)) {
            return;
        }
        $mediaGalleryTable = $this->getTableName('catalog_product_entity_media_gallery');
        $mediaGalleryValueTable = $this->getTableName('catalog_product_entity_media_gallery_value');
        $query = 'SELECT mgvt.`' . $this->getKeyField() . '` AS `entity_id`, mgt.`value` FROM `' . $mediaGalleryTable . '` AS mgt '
            . 'INNER JOIN `' . $mediaGalleryValueTable . '` AS mgvt ON mgt.`value_id` = mgvt.`value_id` '
            . 'WHERE mgvt.`' . $this->getKeyField() . '` IN (' . implode(', ', $this->entityIds) . ') AND (mgvt.`store_id` = ' . $this->getStoreId() . ' OR mgvt.`store_id` = 0) AND mgvt.`disabled` = 0'
            . ' GROUP BY mgvt.`value_id`';
        $results = $this->fetchAll($query);
        if(! empty($results)) {
            foreach($results AS $row) {
                $entityId = $row['entity_id'];
                $image = $row['value'];
                if(! isset($this->data[$entityId]['gallery'])) {
                    $this->data[$entityId]['gallery'] = array();
                }
                $this->data[$entityId]['gallery'][] = $image;
            }
        }
    }

    /**
     * @return int
     * @throws Exception
     */
    protected function getTotalRecords()
    {
        $websiteTableName = $this->getTableName('catalog_product_website');
        return (int) $this
            ->fetchOne('
                SELECT COUNT(*)
                FROM `' . $this->getTableName('catalog_product_entity') . '` AS `p`
                INNER JOIN `' . $websiteTableName . '` as `pw` ON `p`.`entity_id` = `pw`.`product_id`
                ' . $this->getStagingWhereStatement('`pw`.`website_id` = :website_id'), [
                ':website_id' => $this->getHelper()->getWebsiteId()
            ]);
    }

    /*****************
     * EXPORT FIELDS *
     *****************/

    /**
     * @param string[] $data
     * @return string
     */
    protected function getPrimaryKey($data)
    {
        return $data['original_id'];
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getName($data) {
        return isset($data['name']) ? $data['name'] : '';
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getManufacturer($data) {
        return isset($data['manufacturer']) ? $data['manufacturer'] : '';
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getModel($data) {
        return isset($data['sku']) ? $data['sku'] : '';
    }

    /**
     * @param string[] $data
     * @return int
     */
    protected function getQuantity($data) {
        $entityId = $data['entity_id'];
        $typeId = $data['type_id'];
        $qty = isset($data['stock_qty']) ? $data['stock_qty'] : 0;
        switch ($typeId) {
            case 'simple':
            case 'virtual':
            case 'downloadable':
                $this->simpleQuantity[$entityId] = (float) $qty;
                break;
            case 'bundle':
                if (isset($data['bundle_options'])) {
                    $options = array();
                    foreach ($data['bundle_options'] as $bundleOption) {
                        if ($bundleOption['required'] === '1') {
                            $options[$bundleOption['optionId']] = 0;
                        }
                    }
                    foreach ($data['bundle_selections'] as $bundleSelection) {
                        $childId = $this->getMapEntityIdToRowId($bundleSelection['productId']);
                        $optionId = $bundleSelection['optionId'];
                        if (isset($options[$optionId]) && $this->getIsSimpleActive($childId) && isset($this->simpleQuantity[$childId])) {
                            $options[$optionId] += $this->simpleQuantity[$childId];
                        }
                    }
                    if (count($options) === 1) {
                        $qty = array_shift($options);
                    } elseif (count($options) > 1) {
                        $qty = min($options);
                    }
                }
                break;
        }
        return $qty;
    }

    /**
     * @param $data
     * @return bool
     * @throws ExceptionAlias
     */
    protected function isProductSpecialPriceActive($data) {
        return $this->localeDate->isScopeDateInInterval(
            $this->getStoreId(),
            isset($data['special_from_date']) ? $data['special_from_date'] : null,
            isset($data['special_to_date']) ? $data['special_to_date'] : null
        );
    }

    /**
     * @param float[] $prices
     * @return string
     */
    protected function getMinFormattedPrice($prices) {
        if (empty($prices)) {
            $ret = 0;
        } elseif (count($prices) === 1) {
            $ret = $prices[0];
        } else {
            $ret = min($prices);
        }

        return number_format($ret, 2, '.', '');
    }

    /**
     * @param string[] $data
     * @return string
     * @throws ExceptionAlias
     */
    protected function getPrice($data) {
        $typeId = $data['type_id'];
        $entityId = $data['entity_id'];
        $prices = [];

        switch ($typeId) {
            case 'bundle':
                // We can't calculate price for bundle since it is determined by customer
                $prices = ['0.00'];
                break;
            default:
                if (isset($data['special_price'])) {
                    if ($this->isProductSpecialPriceActive($data)) {
                        $prices[] = (float) $data['special_price'];
                    }
                }

                if (isset($data['price'])) {
                    $prices[] = (float) $data['price'];
                }
                break;
        }
        $price = $this->getMinFormattedPrice($prices);

        $typeId = $data['type_id'];
        $parentId = isset($this->configurableLinks[$entityId]) ? $this->configurableLinks[$entityId] : null;

        if ($this->isSimple($typeId)) {
            $this->simplePrices[$entityId] = $price;
        }

        if ($typeId === 'simple' && self::USE_CONFIGURABLE && !empty($parentId)) {
            if ($this->getIsSimpleActive($entityId)) {
                $this->configurablePrices[$parentId] = $price;
            }
        }

        if ($typeId === 'configurable' && self::USE_CONFIGURABLE && isset($this->configurablePrices[$entityId])) {
            return $this->configurablePrices[$entityId];
        }

        return $price;
    }

    /**
     * @param string[] $data
     * @return int
     */
    protected function getActive($data) {
        $active = 1;
        $entityId = $data['entity_id'];
        $visibility = in_array((int) $data['visibility'], $this->visibleProducts) ? 1 : 0;
        if(isset($data['status'])) {
            if($data['status'] != Status::STATUS_ENABLED) {
                $active = 0;
            }
        } else {
            $active = 0;
        }
        if($active) {
            if(in_array($data['type_id'], $this->compositeProducts)) {
                switch($data['type_id']) {
                    case 'configurable':
                        return isset($this->configurableActive[$entityId]) ? $this->configurableActive[$entityId] : 0;
                        break;
                    case 'grouped':
                        return isset($this->groupedActive[$entityId]) ? $this->groupedActive[$entityId] : 0;
                        break;
                    case 'bundle':
                        if (!isset($data['bundle_options'])) {
                            $active = 0;
                        } else {
                            $options = array();
                            $salableCount = 0;
                            foreach ($data['bundle_options'] as $bundleOption) {
                                if ($bundleOption['required'] === '1') {
                                    $options[$bundleOption['optionId']] = 0;
                                }
                            }
                            foreach ($data['bundle_selections'] as $bundleSelection) {
                                if ($this->getIsSimpleActive($bundleSelection['productId'])) {
                                    $salableCount ++;
                                    if (isset($options[$bundleSelection['optionId']])) {
                                        $options[$bundleSelection['optionId']] = 1;
                                    }
                                }
                            }
                            $active = (array_sum($options) === count($options) && $salableCount)
                                ? $active
                                : 0;
                        }
                        break;
                }
                return $active;
            } else {
                // If no data - product is not salable
                if(! isset($data['stock_use_config_manage_stock'])) {
                    if($this->getHelper()->manageStock()) {
                        return 0;
                    } else {
                        return 1;
                    }
                }
                // From Mage_CatalogInventory_Model_Resource_Stock
                if(
                    ($data['stock_use_config_manage_stock'] == 0 AND $data['stock_manage_stock'] == 1 AND $data['stock_is_in_stock'] == 1)
                    OR
                    ($data['stock_use_config_manage_stock'] == 0 AND $data['stock_manage_stock'] == 0)
                    OR (
                    $this->getHelper()->manageStock() ? (
                        $data['stock_use_config_manage_stock'] == 1 AND $data['stock_is_in_stock'] == 1
                    ) : (
                        $data['stock_use_config_manage_stock'] == 1
                    )
                    )
                ) {
                    $active = 1;
                } else {
                    $active = 0;
                }
                $this->setIsSimpleActive($data['entity_id'], $active);
                $this->setIsSimpleVisible($entityId, $visibility);
                $active = $active & $visibility;
            }
        }
        return $active;
    }

    /**
     * space-delimited string of image URLs
     * @param array $data
     * @return string
     * @throws ExceptionAlias
     * @throws NoSuchEntityException
     */
    protected function getImageList($data) {
        if(is_null($this->mediaUrl)) {
            $this->mediaUrl = $this->storeManager->getStore($this->getStoreId())->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . '/catalog/product';
        }
        $imageList = array();
        if(isset($data['image']) && $data['image'] !== 'no_selection') {
            $imageList[] = $this->mediaUrl . $data['image'];
        }
        if(! empty($data['gallery']) AND is_array($data['gallery'])) {
            foreach($data['gallery'] AS $image) {
                $imageList[] = $this->mediaUrl . $image;
            }
        }
        return implode(' ', array_unique($imageList));
    }

    /**
     * @param string[] $data
     * @return string
     * @throws ExceptionAlias
     */
    protected function getItemUrl($data) {
        $entityId = $data['entity_id'];
        $this->productModel->setData($data);
        $this->productModel->setEntityId($data['original_id']);
        $this->productModel->setStoreId($this->getStoreId());
        if (self::USE_CONFIGURABLE && isset($data['parent_url_key'])) {
            $parentId = $this->getMapRowIdToEntityId($this->configurableLinks[$entityId]);
            $urlKey = $data['parent_url_key'];
            $this->productModel->addData([
                'entity_id' => $parentId,
                'url_key' => $urlKey,
            ]);
        }
        return $this->productModel->getProductUrl();
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getParentRecordId($data) {
        $entityId = $data['entity_id'];
        $parentIds = array();

        if(self::USE_CONFIGURABLE && isset($this->configurableLinks[$entityId])) {
            $parentIds[] = $this->getMapRowIdToEntityId($this->configurableLinks[$entityId]);
            $isActive = isset($this->configurableActive[$this->configurableLinks[$entityId]]) ? $this->configurableActive[$this->configurableLinks[$entityId]] : 0;
            $this->configurableActive[$this->configurableLinks[$entityId]] = $isActive ? 1 : $this->getIsSimpleActive($entityId);
        }
        if(self::USE_GROUPED && isset($this->groupedLinks[$entityId])) {
            $parentIds[] = $this->groupedLinks[$entityId];
            $isActive = isset($this->groupedActive[$this->groupedLinks[$entityId]]) ? $this->groupedActive[$this->groupedLinks[$entityId]] : 0;
            $this->groupedActive[$this->groupedLinks[$entityId]] = $isActive ? 1 : $this->getIsSimpleActive($entityId);
        }
        if(self::USE_BUNDLE && isset($this->bundleLinks[$entityId])) {
            $parentIds[] = $this->bundleLinks[$entityId];
        }
        return implode(',', $parentIds);
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getAttribute1($data) {
        return $data['type_id'];
    }

    /**
     * @param $data
     * @return bool
     */
    protected function getDoNotRecommend($data) {
        if(isset($data['resci_recommend']) && $data['resci_recommend'] === '0') {
            return true;
        }
        return false;
    }

    /**
     * @param $data
     * @return string|null
     */
    protected function getDisplayName($data) {
        return empty($data['resci_display_name']) ? '' : $data['resci_display_name'];
    }

    /**
     * comma-delimited string of category ids
     * @param string[] $data
     * @return string
     */
    protected function getCategories($data) {
        if(isset($data['categories'])) {
            return implode(',', $data['categories']);
        }
        return '';
    }

    /**
     * @param string|int $productId
     * @param bool $active
     * @return void
     */
    protected function setIsSimpleActive($productId, $active) {
        $this->simpleActive[$productId] = $active;
    }

    /**
     * @param string|int $productId
     * @param bool $visible
     * @return void
     */
    protected function setIsSimpleVisible($productId, $visible) {
        $this->simpleVisible[$productId] = $visible;
    }

    /**
     * @param string|int $productId
     * @return int
     */
    protected function getIsSimpleActive($productId) {
        if(isset($this->simpleActive[$productId])) {
            return $this->simpleActive[$productId];
        } else {
            return 0;
        }
    }

    /**
     * @param string|int $productId
     * @return int
     */
    protected function getIsSimpleVisible($productId) {
        if(isset($this->simpleVisible[$productId])) {
            return $this->simpleVisible[$productId];
        } else {
            return 0;
        }
    }
}