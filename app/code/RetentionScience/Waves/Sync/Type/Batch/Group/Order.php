<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/8/16
 * Time: 7:52 PM
 */

namespace RetentionScience\Waves\Sync\Type\Batch\Group;


use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Payment\Helper\Data as PaymentHelper;
use RetentionScience\Waves\Sync\Type\Batch\GroupAbstract;
use Magento\Framework\Module\Manager;
use RetentionScience\Waves\Helper\Data;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Eav\Model\Config;
use RetentionScience\Waves\Connection\AWS\CloudWatch;
use Magento\Framework\Filesystem\Io\File as FileIo;

class Order extends GroupAbstract
{

    /**
     * @var DateTime
     */
    private $dateTime;
    /**
     * @var PaymentHelper
     */
    private $paymentHelper;

    /**
     * Order constructor.
     * @param PaymentHelper $paymentHelper
     * @param DateTime $dateTime
     * @param Manager $moduleManager
     * @param Data $helper
     * @param ResourceConnection $resource
     * @param DirectoryList $directoryList
     * @param Config $eavConfig
     * @param CloudWatch $cloudWatch
     * @param FileIo $fileIo
     */
    public function __construct(
        PaymentHelper $paymentHelper,
        DateTime $dateTime,
        Manager $moduleManager,
        Data $helper,
        ResourceConnection $resource,
        DirectoryList $directoryList,
        Config $eavConfig,
        CloudWatch $cloudWatch,
        FileIo $fileIo
    )
    {
        parent::__construct($moduleManager, $helper, $resource, $directoryList, $eavConfig, $cloudWatch, $fileIo);
        $this->dateTime = $dateTime;
        $this->paymentHelper = $paymentHelper;
    }

    /**********
     * CONFIG *
     **********/

    /**
     * @return string
     */
    protected function getBulkFileName()
    {
        return 'orders';
    }

    /**
     * Look at http://devdocs.magento.com/guides/v2.1/extension-dev-guide/staging/Staging.html
     * @return string|bool
     */
    protected function getStageModule()
    {
        return false;
    }

    /**
     * @return int
     */
    protected function getLimit()
    {
        return 1000;
    }

    /**
     * @return string[]
     */
    protected function getExportFields()
    {
        return [
            'record_id',
            'user_record_id',
            'total_price',
            'discount_amount',
            'shipping_amount',
            'tax_amount',
            'ordered_at',
            'payment_method',
            'order_status',
        ];
    }

    /**
     * @return string
     */
    public function getExportName()
    {
        return 'orders';
    }

    /*************
     * LOAD DATA *
     *************/

    /**
     * @return void
     * @throws \Exception
     */
    protected function loadEntityData()
    {
        $tableName = $this->getTableName('sales_order');
        $query = '
            SELECT `' . $this->getKeyField() . '` AS `entity_id`, `state`, `customer_email`, `base_subtotal`, `base_discount_amount`, `shipping_amount`, `base_tax_amount`, `created_at`
            FROM `' . $tableName . '`
            WHERE `store_id` = :store_id
            ORDER BY `entity_id` ASC LIMIT ' . $this->offset . ', ' . $this->getLimit();
        $this->data = $this->fetchAll($query, [
            ':store_id' => $this->getHelper()->getStoreId(),
        ]);
        $this->processedRecords += count($this->data);
        $this->entityIds = [];
        if(! empty($this->data)) {
            $sortedData = [];
            foreach($this->data AS $record) {
                if(empty($record['entity_id'])) {
                    continue;
                }
                $this->entityIds[] = $record['entity_id'];
                $sortedData[$record['entity_id']] = $record;
            }
            $this->data = $sortedData;
        }
    }

    /**
     * @return void
     * @throws \Exception
     */
    protected function loadAdditionalData()
    {
        /* Get Payment Method Code */
        $this->fillTableData($this->getTableName('sales_order_payment'), 'parent_id', [
            'method'                => 'payment_method',
        ]);
    }

    /**
     * @return int
     * @throws \Exception
     */
    protected function getTotalRecords()
    {
        return (int) $this
            ->fetchOne('
                SELECT COUNT(*) FROM `' . $this->getTableName('sales_order') . '`
                WHERE `store_id` = :store_id', [
                ':store_id' => $this->getStoreId(),
            ]);
    }

    /*****************
     * EXPORT FIELDS *
     *****************/

    /**
     * @param string[] $data
     * @return string
     */
    protected function getPrimaryKey($data)
    {
        return $data['entity_id'];
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getUserRecordId($data) {
        return Data::md5(trim(strtolower($data['customer_email'])));
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getTotalPrice($data) {
        return number_format($data['base_subtotal'], 2, '.', '');
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getDiscountAmount($data) {
        return number_format($data['base_discount_amount'], 2, '.', '');
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getShippingAmount($data) {
        return number_format($data['shipping_amount'], 2, '.', '');
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getTaxAmount($data) {
        return number_format($data['base_tax_amount'], 2, '.', '');
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getOrderedAt($data) {
        return $this->dateTime->date('Y-m-d H:i:s', $data['created_at']);
    }

    /**
     * @param string[] $data
     * @return string
     * @TODO: to RS team - check if it return value in case of exception is correct?!
     */
    protected function getPaymentMethod($data) {
        try {
            return $this->paymentHelper->getMethodInstance($data['payment_method'])->getTitle();
        } catch (\Exception $e) {
            return 'Not Available';
        }
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getOrderStatus($data) {
        if(isset($data['state'])) {
            return $data['state'];
        } else {
            return '';
        }
    }
}