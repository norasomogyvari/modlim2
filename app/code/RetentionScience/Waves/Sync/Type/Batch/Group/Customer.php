<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/8/16
 * Time: 7:52 PM
 */

namespace RetentionScience\Waves\Sync\Type\Batch\Group;


use Magento\Directory\Model\Config\Source\Allregion;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Newsletter\Model\Subscriber;
use RetentionScience\Waves\Sync\Type\Batch\Group\Subscriber as SubscriberProxy;
use RetentionScience\Waves\Sync\Type\Batch\GroupAbstract;
use Magento\Framework\Module\Manager;
use RetentionScience\Waves\Helper\Data;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Eav\Model\Config;
use RetentionScience\Waves\Connection\AWS\CloudWatch;
use Magento\Framework\Filesystem\Io\File as FileIo;

class Customer extends GroupAbstract
{
    /**
     * @var array
     */
    private $regions;

    /**
     * @var DateTime
     */
    private $dateTime;

    /**
     * @var Order\Customer
     */
    private $orderCustomer;

    /**
     * @var array
     */
    private $exportedPks = [];
    /**
     * @var \RetentionScience\Waves\Sync\Type\Batch\Group\Subscriber
     */
    private $subscriberCustomer;

    /**
     * Customer constructor.
     * @param Order\Customer $orderCustomer
     * @param SubscriberProxy $subscriberCustomer
     * @param DateTime $dateTime
     * @param Allregion $regions
     * @param Manager $moduleManager
     * @param Data $helper
     * @param ResourceConnection $resource
     * @param DirectoryList $directoryList
     * @param Config $eavConfig
     * @param CloudWatch $cloudWatch
     * @param FileIo $fileIo
     * @internal param $ Order\Customer
     */
    public function __construct(
        Order\Customer $orderCustomer,
        SubscriberProxy $subscriberCustomer,
        DateTime $dateTime,
        Allregion $regions,
        Manager $moduleManager,
        Data $helper,
        ResourceConnection $resource,
        DirectoryList $directoryList,
        Config $eavConfig,
        CloudWatch $cloudWatch,
        FileIo $fileIo
    )
    {
        parent::__construct($moduleManager, $helper, $resource, $directoryList, $eavConfig, $cloudWatch, $fileIo);
        $this->regions = [];
        foreach($regions->toOptionArray(true) as $country) {
            foreach($country['value'] as $reg) {
                $this->regions[$reg['value']] = $reg['label'];
            }
        }
        $this->dateTime = $dateTime;
        $this->orderCustomer = $orderCustomer;
        $this->subscriberCustomer = $subscriberCustomer;
    }

    /************
     * OVERRIDE *
     ************/

    /**
     * @param string $id
     */
    public function addExportedId($id) {
        $this->exportedPks[$id] = true;
    }

    /**
     * @param string $id
     * @return bool
     */
    public function isExported($id) {
        return isset($this->exportedPks[$id]);
    }

    public function run($timestamp)
    {
        parent::run($timestamp);

        $this->orderCustomer->setCustomerExportModel($this);
        $this->orderCustomer->run($timestamp);

        $this->subscriberCustomer->setCustomerExportModel($this);
        $this->subscriberCustomer->run($timestamp);
    }

    /**********
     * CONFIG *
     **********/

    /**
     * @return string
     */
    protected function getBulkFileName()
    {
        return 'users';
    }

    /**
     * Look at http://devdocs.magento.com/guides/v2.1/extension-dev-guide/staging/Staging.html
     * @return string|bool
     */
    protected function getStageModule()
    {
        return false;
    }

    /**
     * @return int
     */
    protected function getLimit()
    {
        return 1000;
    }

    /**
     * @return string[]
     */
    protected function getExportFields()
    {
        return [
            'record_id',
            'email',
            'full_name',
            'address1',
            'city',
            'state',
            'zip',
            'country',
            'phone',
            'birth_date',
            'gender',
            'account_created_on',
            'last_logon_at',
            'subscriber_status',
        ];
    }

    /**
     * @return string
     */
    public function getExportName()
    {
        return 'users';
    }

    /*************
     * LOAD DATA *
     *************/

    /**
     * @return void
     * @throws \Exception
     */
    protected function loadEntityData()
    {
        $tableName = $this->getTableName('customer_entity');
        $query = '
            SELECT `' . $this->getKeyField() . '` AS `entity_id`, `email`, `created_at`, `updated_at`, `firstname`, `lastname`, `middlename`, `suffix`, `prefix`, `default_billing`, `dob`, `gender`
            FROM `' . $tableName . '`
            WHERE `website_id` = :website_id
            LIMIT ' . $this->offset . ', ' . $this->getLimit();
        $this->data = $this->fetchAll($query, [
            ':website_id' => $this->getHelper()->getWebsiteId(),
        ]);
        $this->processedRecords += count($this->data);
        $this->entityIds = [];
        if(! empty($this->data)) {
            $sortedData = array();
            foreach($this->data AS $record) {
                if(empty($record['entity_id'])) {
                    continue;
                }
                $this->entityIds[] = $record['entity_id'];
                $sortedData[$record['entity_id']] = $record;
            }
            $this->data = $sortedData;
        }
    }

    /**
     * @return void
     * @throws \Exception
     */
    protected function loadAdditionalData()
    {
        /* ADDRESS DATA */
        $this->loadAddressData();
        /* NEWSLETTER SUBSCRIPTION DATA */
        $this->fillTableData(
            $this->getTableName('newsletter_subscriber'),
            'customer_id',
            array('subscriber_status' => 'subscriber_status')
        );
    }

    /**
     * @return void
     * @throws \Exception
     */
    private function loadAddressData()
    {
        $addresses = [];
        foreach($this->data as $item) {
            if(! empty($item['default_billing'])) {
                $addresses[] = $item['default_billing'];
            }
        }
        if(! empty($addresses)) {
            $tableData = $this->fetchAll(
                'SELECT `entity_id` AS `address_id`, `parent_id` as `entity_id`, `street`, `city`, `region_id`, `postcode`, `country_id`, `telephone` FROM `' . $this->getTableName('customer_address_entity') . '`'
                . ' WHERE `entity_id` IN (' . implode(', ', $addresses) . ')'
            );
        }
        $copy = [
            'street',
            'city',
            'postcode',
            'country_id',
            'telephone',
            'region_id',
        ];
        if(! empty($tableData)) {
            foreach($tableData as $data) {
                $entityId = $data['entity_id'];
                if(isset($this->data[$entityId])) {
                    foreach($copy as $field) {
                        $this->data[$entityId][$field] = $data[$field];
                    }
                }
            }
        }
    }

    /**
     * @return int
     * @throws \Exception
     */
    protected function getTotalRecords()
    {
        return (int) $this
            ->fetchOne('
                SELECT COUNT(*) FROM `' . $this->getTableName('customer_entity') . '`
                WHERE `website_id` = :website_id',
                [
                    ':website_id' => $this->getHelper()->getWebsiteId(),
            ]);
    }

    /*****************
     * EXPORT FIELDS *
     *****************/

    /**
     * @param string[] $data
     * @return string
     */
    protected function getPrimaryKey($data)
    {
        $pk = Data::md5(trim(strtolower($data['email'])));
        $this->addExportedId($pk);
        return $pk;
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getEmail($data) {
        return isset($data['email']) ? $data['email'] : '';
    }

    /**
     * @param string[] $data
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getFullname($data) {
        $name = '';
        if ($this->getEavAttribute('customer', 'prefix')->getIsVisibleOnFront() && isset($data['prefix'])) {
            $name .= $data['prefix'] . ' ';
        }
        $name .= isset($data['firstname']) ? $data['firstname'] : '';
        if ($this->getEavAttribute('customer', 'middlename')->getIsVisibleOnFront() && isset($data['middlename'])) {
            $name .= ' ' . $data['middlename'];
        }
        $name .=  isset($data['lastname']) ? ' ' . $data['lastname'] : '';
        if ($this->getEavAttribute('customer', 'suffix')->getIsVisibleOnFront() && isset($data['suffix'])) {
            $name .= ' ' . $data['suffix'];
        }
        return $name;
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getAddress1($data) {
        return isset($data['street']) ? str_replace("\n", ' ', $data['street']) : '';
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getCity($data) {
        return isset($data['city']) ? $data['city'] : '';
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getState($data) {
        return isset($data['region_id']) ? (isset($this->regions[$data['region_id']]) ? $this->regions[$data['region_id']] : '') : '';
    }

    protected function getZip($data) {
        return isset($data['postcode']) ? $data['postcode'] : '';
    }

    protected function getCountry($data) {
        if(isset($data['country_id'])) {
            // shorten to US
            if ($data['country_id'] == 'United States') {
                return 'US';
            } elseif ($data['country_id'] == 'United Kingdom') {
                return 'UK';
            } else {
                return $data['country_id'];
            }

        }
        return '';
    }

    protected function getPhone($data) {
        return isset($data['telephone']) ? $data['telephone'] : '';
    }

    // @return yyyy-mm-dd
    protected function getBirthdate($data) {
        if(isset($data['dob'])) {
            $birthDateTime = explode(' ', $data['dob']);
            if($birthDateTime[0] === '0000-00-00') {
                return '';
            }
            return $birthDateTime[0];
        }
        return '';
    }

    protected function getGender($data) {
        if (isset($data['gender'])) {
            $gender = strtolower($data['gender']);
            if ($gender == 'female' || $gender == '1') {
                return 'f';
            } elseif ($gender == 'male' || $gender == '0') {
                return 'm';
            } else {
                return '';
            }
        }
        return '';
    }

    protected function getAccountCreatedOn($data) {
        return $this->dateTime->date('Y-m-d H:i:s', $data['created_at']);
    }

    protected function getLastLogOnAt($data) {
        return $this->dateTime->date('Y-m-d H:i:s', $data['updated_at']);
    }

    protected function getSubscriberStatus($data) {
        if (!isset($data['subscriber_status']) || $data['subscriber_status'] != Subscriber::STATUS_SUBSCRIBED) {
            return false;
        }
        return true;
    }
}