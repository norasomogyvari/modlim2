<?php

namespace RetentionScience\Waves\Sync\Type\Batch\Group;


use RetentionScience\Waves\Connection\RetentionScience\Exception;
use RetentionScience\Waves\Sync\Type\Batch\Group\Customer as AllCustomers;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Directory\Model\Config\Source\Allregion;
use Magento\Framework\Module\Manager;
use RetentionScience\Waves\Helper\Data;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Eav\Model\Config;
use RetentionScience\Waves\Connection\AWS\CloudWatch;
use RetentionScience\Waves\Sync\Type\Batch\GroupAbstract;
use Magento\Framework\Filesystem\Io\File as FileIo;

class Subscriber extends AllCustomers
{

    /**
     * @var DateTime
     */
    private $dateTime;

    /**
     * @var AllCustomers
     */
    private $customerExport;

    /**
     * Customer constructor.
     * @param Order\Customer $orderCustomer
     * @param DateTime $dateTime
     * @param Allregion $regions
     * @param Manager $moduleManager
     * @param Data $helper
     * @param ResourceConnection $resource
     * @param DirectoryList $directoryList
     * @param Config $eavConfig
     * @param CloudWatch $cloudWatch
     * @param FileIo $fileIo
     */
    public function __construct(
        Order\Customer $orderCustomer,
        DateTime $dateTime,
        Allregion $regions,
        Manager $moduleManager,
        Data $helper,
        ResourceConnection $resource,
        DirectoryList $directoryList,
        Config $eavConfig,
        CloudWatch $cloudWatch,
        FileIo $fileIo
    )
    {
        parent::__construct($orderCustomer, $this, $dateTime, $regions, $moduleManager, $helper, $resource, $directoryList, $eavConfig, $cloudWatch, $fileIo);
        $this->dateTime = $dateTime;
    }

    public function setCustomerExportModel(AllCustomers $customerExportModel) {
        $this->customerExport = $customerExportModel;
    }

    /************
     * OVERRIDE *
     ************/

    /**
     * @param int $timestamp
     * @return void;
     * @throws Exception
     */
    public function run($timestamp)
    {
        GroupAbstract::run($timestamp);
    }

    /**
     * @param string[] $data
     * @return bool
     */
    protected function shouldExportRow($data)
    {
        $pk = $this->getPrimaryKey($data);
        $ret = ! $this->customerExport->isExported($pk);
        $this->customerExport->addExportedId($pk);
        return $ret;
    }

    /*************
     * LOAD DATA *
     *************/

    /**
     * @return void
     * @throws \Exception
     */
    protected function loadEntityData()
    {
        $tableName = $this->getTableName('newsletter_subscriber');
        $query = '
            SELECT `subscriber_id` AS `entity_id`, `subscriber_email` AS `email`, `change_status_at` AS `created_at`, `subscriber_status`
            FROM `' . $tableName . '`
            WHERE `store_id` = :store_id
            LIMIT ' . $this->offset . ', ' . $this->getLimit();

        $this->data = $this->fetchAll($query, [
            ':store_id' => $this->getHelper()->getStoreId(),
        ]);
        $this->processedRecords += count($this->data);
        $this->entityIds = [];
        if(! empty($this->data)) {
            $sortedData = array();
            foreach($this->data AS $record) {
                if(empty($record['entity_id'])) {
                    continue;
                }
                $this->entityIds[] = $record['entity_id'];
                $sortedData[$record['entity_id']] = $record;
            }
            $this->data = $sortedData;
        }
    }

    /**
     * @return void
     */
    protected function loadAdditionalData()
    {
    }

    /**
     * @return int
     * @throws \Exception
     */
    protected function getTotalRecords() {
        return (int) $this
            ->fetchOne('
                SELECT COUNT(*) FROM `' . $this->getTableName('newsletter_subscriber') . '`
                WHERE `store_id` = :store_id
            ', [
                ':store_id' => $this->getHelper()->getStoreId(),
            ]);
    }

    /*****************
     * EXPORT FIELDS *
     *****************/

    /**
     * @param string[] $data
     * @return mixed
     */
    protected function getLastLogOnAt($data) {
        if (!empty($data['created_at'])) {
            return $this->dateTime->date('Y-m-d', $data['created_at']);
        }
        return '';
    }

    /**
     * @param string[] $data
     * @return mixed
     */
    protected function getAccountCreatedOn($data) {
        if (!empty($data['created_at'])) {
            return $this->dateTime->date('Y-m-d', $data['created_at']);
        }
        return '';
    }

    /**
     * @param string[] $data
     * @return mixed
     */
    protected function getPrimaryKey($data) {
        return Data::md5(trim(strtolower($data['email'])));
    }

}