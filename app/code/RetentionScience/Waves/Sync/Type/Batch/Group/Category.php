<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/8/16
 * Time: 7:52 PM
 */

namespace RetentionScience\Waves\Sync\Type\Batch\Group;


use Exception;
use Magento\Framework\Exception\LocalizedException;
use RetentionScience\Waves\Connection\RetentionScience\Exception as ExceptionAlias;
use RetentionScience\Waves\Sync\Type\Batch\GroupAbstract;

class Category extends GroupAbstract
{

    /**********
     * CONFIG *
     **********/

    /**
     * @return string
     */
    public function getExportName()
    {
        return 'categories';
    }

    /**
     * @return string
     */
    protected function getBulkFileName()
    {
        return 'categories';
    }

    /**
     * @return string|bool
     */
    protected function getStageModule()
    {
        return 'Magento_CatalogStaging';
    }

    /**
     * @return int
     */
    protected function getLimit()
    {
        return 1000;
    }

    /**
     * @return string[]
     */
    protected function getExportFields()
    {
        return [
            'record_id',
            'name',
            'description',
            'parent_record_id',
        ];
    }

    private function getSearchLikePath() {
        return "1/{$this->getHelper()->getRootCategoryId()}%";
    }

    /*************
     * LOAD DATA *
     *************/

    /**
     * @return void
     * @throws Exception
     */
    protected function loadEntityData() {
        $tableName = $this->getTableName('catalog_category_entity');
        $query = '
            SELECT `' . $this->getKeyField() . '` AS `entity_id`, `entity_id` as `original_id`, `parent_id`
            FROM `' . $tableName . '` ' . $this->getStagingWhereStatement('`path` LIKE :path') . '
            ORDER BY `level` ASC
            LIMIT ' . $this->offset . ', ' . $this->getLimit();
        $this->data = $this->fetchAll($query, [
            ':path' => $this->getSearchLikePath(),
        ]);
        $this->processedRecords += count($this->data);
        $this->entityIds = [];
        if(! empty($this->data)) {
            $sortedData = [];
            foreach($this->data AS $record) {
                if(empty($record['entity_id'])) {
                    continue;
                }
                $this->entityIds[] = $record['entity_id'];
                $sortedData[$record['entity_id']] = $record;
            }
            $this->data = $sortedData;
        }
    }

    /**
     * @return void
     * @throws LocalizedException
     * @throws ExceptionAlias
     */
    protected function loadAdditionalData() {
        $this->fillAttributeData('catalog_category', 'name');
        $this->fillAttributeData('catalog_category', 'description');
    }

    /**
     * @return int
     * @throws Exception
     */
    protected function getTotalRecords() {
        return (int) $this
            ->fetchOne('
                SELECT COUNT(*) FROM `' . $this->getTableName('catalog_category_entity') . '`
                ' . $this->getStagingWhereStatement('`path` LIKE :path'),
                [
                    ':path' => $this->getSearchLikePath(),
                ]);
    }

    /*****************
     * EXPORT FIELDS *
     *****************/

    /**
     * @param string[] $data
     * @return string
     */
    protected function getPrimaryKey($data) {
        return $data['original_id'];
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getName($data) {
        return isset($data['name']) ? $data['name'] : '';
    }

    /**
     * @param string[] $data
     * @return string
     */
    protected function getDescription($data) {
        return isset($data['description']) ? $data['description'] : '';
    }

    /**
     * @param string[] $data
     * @return null|string
     */
    protected function getParentRecordId($data) {
        return isset($data['parent_id']) ? $data['parent_id'] : NULL;
    }
}