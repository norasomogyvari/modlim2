<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/8/16
 * Time: 3:32 PM
 */

namespace RetentionScience\Waves\Sync\Type;


use Exception;
use Magento\Store\Model\App\Emulation;
use RetentionScience\Waves\Connection\AWS\CloudWatch;
use RetentionScience\Waves\Connection\RetentionScience\Exception as ExceptionAlias;
use RetentionScience\Waves\Helper\Data;
use RetentionScience\Waves\Sync\Logger;
use RetentionScience\Waves\Sync\Params;
use RetentionScience\Waves\Sync\TypeInterface;
use Magento\Framework\Filesystem\Io\File as FileIo;

class Batch implements TypeInterface
{

    const GROUPS_ALL = 'all';

    /**
     * @var Batch\GroupAbstract[]
     */
    private $availableGroups;

    /**
     * @var Batch\GroupAbstract[]
     */
    private $groups;

    /**
     * @var Data
     */
    private $helper;
    /**
     * @var Logger
     */
    private $logger;
    /**
     * @var Emulation
     */
    private $emulation;
    /**
     * @var CloudWatch
     */
    private $cloudWatch;
    /**
     * @var FileIo
     */
    private $fileIo;

    /**
     * Batch constructor.
     * @param Logger $logger
     * @param Data $helper
     * @param Emulation $emulation
     * @param CloudWatch $cloudWatch
     * @param FileIo $fileIo
     * @param array $availableGroups
     */
    public function __construct(
        Logger $logger,
        Data $helper,
        Emulation $emulation,
        CloudWatch $cloudWatch,
        FileIo $fileIo,
        array $availableGroups
    )
    {
        $this->availableGroups = $availableGroups;
        $this->helper = $helper;
        $this->logger = $logger;
        $this->emulation = $emulation;
        $this->cloudWatch = $cloudWatch;
        $this->fileIo = $fileIo;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'batch';
    }

    /**
     * @param string $groups
     */
    public function setGroups($groups) {
        if($groups === self::GROUPS_ALL) {
            $this->groups = $this->availableGroups;
        } else {
            $this->groups = [];
            $groups = explode(',', $groups);
            foreach($this->availableGroups as $group) {
                if(in_array($group->getExportName(), $groups)) {
                    $this->groups[$group->getExportName()] = $group;
                }
            }
        }
    }

    /**
     * @param string $groups
     * @return bool
     * @throws Exception
     */
    public function validateGroups($groups) {
        if($groups === self::GROUPS_ALL) {
            return true;
        } else {
            $groups = explode(',', $groups);
            foreach($groups as $group) {
                if(! isset($this->availableGroups[$group])) {
                    throw new Exception('Not valid export groups. Allowed groups: all or any/comma-separated of(' . implode(', ', array_keys($this->availableGroups)) . ')');
                }
            }
        }
        return true;
    }

    /**
     * @return string[]
     */
    public function getParams()
    {
        return [
            [
                'name' => 'groups',
                'required' => true,
                'description' => '; available values: (' . self::GROUPS_ALL . '|comma-separated list of ' . implode(', ', array_keys($this->availableGroups)) . '); example: groups:' . self::GROUPS_ALL,
            ],
        ];
    }

    /**
     * List of files to export
     * @param Params $params
     * @return string[]
     * @throws ExceptionAlias
     */
    public function generateFiles(Params $params)
    {
        $files = [];
        $timestamp = microtime(TRUE);
        $storeIds = $params->getStoreIds();
        if (!empty($storeIds)) {
            foreach ($storeIds as $storeId) {
                $this->helper->setStoreId($storeId);
                $message = $params->getMessage();
                if (!empty($message)) {
                    $this->cloudWatch->logMessage($params->getMessage());
                }
                $this->cloudWatch->logMessage($this->helper->getStagingInfo());
                // Export groups
                $this->emulation->startEnvironmentEmulation($this->helper->getStoreId());
                foreach($this->groups as $group) {
                    try {
                        $this->logger
                            ->setExportName($this->getName() . '_' . $group->getExportName())
                            ->setExportSource($params->getSource())
                            ->setExportProcessedRecord(0)
                            ->setExportTotalRecords(0);
                        $this->logger->logStart();
                        $this->logger->setListenErrors(true);
                        $group->run($timestamp);
                        $this->logger->setExportProcessedRecord($group->getProcessedRecords());
                        $this->logger->logSuccess();
                        if($this->fileIo->fileExists($file = $group->getBulkFile())) {
                            $files[$group->getExportName()] = $file;
                        }
                    } catch(Exception $e) {
                        $this->logger->setExportProcessedRecord($group->getProcessedRecords());
                        // Catch exception
                        $errno = $e->getCode();
                        $errstr = $e->getMessage();
                        $errfile = $e->getFile();
                        $errline = $e->getLine();
                        // Do stuff
                        $this->logger->logException($errno, $errstr, $errfile, $errline);
                    }

                    $this->logger->setListenErrors(false);
                    $this->logger->sendLogs();
                }
                $this->emulation->stopEnvironmentEmulation();
            }
        }
        return $files;
    }
}