<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/8/16
 * Time: 3:40 PM
 */

namespace RetentionScience\Waves\Sync;


interface TypeInterface
{

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string[]
     */
    public function getParams();

    /**
     * List of files to export
     * @param Params $params
     * @return \string[]
     */
    public function generateFiles(Params $params);

}