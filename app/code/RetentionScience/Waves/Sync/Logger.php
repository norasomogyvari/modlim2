<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/8/16
 * Time: 6:25 PM
 */

namespace RetentionScience\Waves\Sync;


use Magento\Framework\App\ProductMetadata;
use Magento\Framework\Stdlib\DateTime\DateTime;
use RetentionScience\Waves\Connection\AWS\CloudWatch;
use RetentionScience\Waves\Helper\Data;

class Logger
{
    /**
     * @var string
     */
    private $allocatedMemory;
    /**
     * @var bool
     */
    private $listenErrors;
    /**
     * @var array
     */
    private $posixSignals = [];
    /**
     * @var DateTime
     */
    private $dateTime;
    /**
     * @var Data
     */
    private $helper;
    /**
     * @var CloudWatch
     */
    private $cloudWatch;
    /**
     * @var string
     */
    private $guid;
    /**
     * @var int
     */
    private $startTime;
    /**
     * @var int
     */
    private $startMemory;
    /**
     * @var int
     */
    private $exportName;
    /**
     * @var int
     */
    private $exportSource;
    /**
     * @var int
     */
    private $processedRecords;
    /**
     * @var int
     */
    private $totalRecords;
    /**
     * @var ProductMetadata
     */
    private $magento;

    /**
     * Logger constructor.
     * @param DateTime $dateTime
     * @param Data $helper
     * @param CloudWatch $cloudWatch
     * @param ProductMetadata $magento
     */
    public function __construct(
        DateTime $dateTime,
        Data $helper,
        CloudWatch $cloudWatch,
        ProductMetadata $magento
    )
    {
        $this->registerShutdownFunction();
        $this->setErrorHandler();
        $this->registerPosixHandlers();
        $this->dateTime = $dateTime;
        $this->helper = $helper;
        $this->cloudWatch = $cloudWatch;
        $this->magento = $magento;
    }

    private function registerPosixHandlers() {
        if(function_exists('pcntl_signal')) {
            $this->posixSignals[SIGINT] = 'SIGINT';
            $this->posixSignals[SIGTERM] = 'SIGTERM';
            $this->posixSignals[SIGHUP] = 'SIGHUP';
            declare(ticks = 1);
            foreach($this->posixSignals AS $signo => $code) {
                pcntl_signal($signo, array($this, 'posixHandler'));
            }
        }
    }

    private function registerShutdownFunction() {
        // Save 10MB of reserved memory, because PHP not allows to do anything else if out of memory error occurred
        $this->allocatedMemory = str_repeat('*', 10 * 1024 * 1024);
        register_shutdown_function(array($this, 'shutdownHandler'));
    }

    private function setErrorHandler() {
        set_error_handler(array($this, 'errorHandler'), E_ALL | E_STRICT);
    }

    /**
     * @return bool
     */
    private function getListenErrors()
    {
        return $this->listenErrors;
    }

    /**
     * @param bool $listenErrors
     * @return $this
     */
    public function setListenErrors($listenErrors)
    {
        $this->listenErrors = !! $listenErrors;
        return $this;
    }

    public function errorHandler($errno, $errstr, $errfile, $errline) {
        if(! $this->getListenErrors()) {
            return;
        }
        $this->logRecoverableError($errno, $errstr, $errfile, $errline);
    }

    public function shutdownHandler() {
        if(! $this->getListenErrors()) {
            return;
        }
        $this->setListenErrors(false);
        // Release memory, because PHP not allows to do anything else if out of memory
        $this->allocatedMemory = null;
        // Catch non-recoverable error
        $errno   = E_CORE_ERROR;
        $errstr  = "shutdown";
        $errfile = "unknown file";
        $errline = 0;
        $error = error_get_last();
        if( $error !== null) {
            $errno   = $error["type"];
            $errfile = $error["file"];
            $errline = $error["line"];
            $errstr  = $error["message"];
        }
        // Do stuff
        $this->logUnrecoverableError($errno, $errstr, $errfile, $errline);
    }

    public function posixHandler($signo) {
        if(! $this->getListenErrors()) {
            return;
        }
        $this->logError('manual_shutdown', $signo, 'Processed was manually stopped with ' . $this->posixSignals[$signo], 'unknown file', 0);
        $this->logFail();
        $this->cloudWatch->sendLogs();
        if(! defined('TEST_ENVIRONMENT')) {
            //exit; To pass static code analysis
        }
    }

    protected function log($logStreamName, $message) {
        // There are some errors during including AWS SDK files - ignore them
        $oldListen = $this->getListenErrors();
        $this->setListenErrors(false);
        $logTime = $this->dateTime->gmtDate() . ' UTC';
        $siteId = $this->helper->getSiteId();
        $this->cloudWatch->log([
            'logGroupName'      => $this->helper->getAWSLogGroup(),
            'logStreamName'     => $this->helper->getAWSLogStream(),
            'message'           => $logTime . ' - [site_id ' . $siteId . '] ' . $this->magento->getName() . '[' . $this->magento->getEdition() . ' ' . $this->magento->getVersion() . '] [' . strtoupper($logStreamName) . ']: [' . $this->guid . '] ' . $message,
        ]);
        $this->setListenErrors($oldListen);
    }

    public function logStart() {
        $this->setStartTime(round(microtime(true)));
        $this->setStartMemory(memory_get_usage(TRUE));
        $this->setGUID(substr(Data::md5(rand()), 0, 8) . '-' . substr(Data::md5(rand()), 0, 4) . '-' . substr(Data::md5(rand()), 0, 4) . '-' . substr(Data::md5(rand()), 0, 12));
        $start = $this->getExportName() . ' export started';
        if($this->getExportSource()) {
            $start .= ' [source ' . $this->getExportSource() . ']';
        }
        $start .= ' [memstart ' . $this->getStartMemory() . ']';
        $this->log('start', $start);
    }

    public function logSuccess() {
        $memdiff = memory_get_usage(TRUE) - $this->getStartMemory();
        $processedRecords = $this->getExportProcessedRecords();
        $timediff = round(microtime(true)) - $this->getStartTime();
        $this->log('success', $this->getExportName() . ' export ended successful [processed ' . $processedRecords . '] [memdiff ' . ($memdiff > 0 ? '+' : '') . $memdiff . '] [timediff ' . $timediff . ']');
    }

    public function logFail() {
        $memdiff = memory_get_usage(TRUE) - $this->getStartMemory();
        $processedRecords = $this->getExportProcessedRecords();
        $timediff = round(microtime(true)) - $this->getStartTime();
        $totalRecords = $this->getExportTotalRecords();
        $this->log('fail', $this->getExportName() . ' export failed [processed=' . $processedRecords . '] [total ' . $totalRecords . '] [memdiff ' . ($memdiff > 0 ? '+' : '') . $memdiff . '] [timediff=' . $timediff . ']');
    }

    public function logException($errno, $errstr, $errfile, $errline) {
        $this->logError('exception', $errno, $errstr, $errfile, $errline);
        $this->logFail();
    }

    public function logRecoverableError($errno, $errstr, $errfile, $errline) {
        $this->logError('recoverable_error', $errno, $errstr, $errfile, $errline);
    }

    public function logUnrecoverableError($errno, $errstr, $errfile, $errline) {
        $this->logError('unrecoverable_error', $errno, $errstr, $errfile, $errline);
        $this->logFail();
        $this->cloudWatch->sendLogs();
    }

    protected function logError($type, $errno, $errstr, $errfile, $errline) {
        $memdiff = memory_get_usage(TRUE) - $this->getStartMemory();
        $processedRecords = $this->getExportProcessedRecords();
        $timediff = round(microtime(true)) - $this->getStartTime();
        $totalRecords = $this->getExportTotalRecords();
        $this->log($type, $this->getExportName() . ' error occured [errno ' . $errno . '] [errstr ' . $errstr . '] [errfile ' . $errfile . '] [errline ' . $errline . '] [processed ' . $processedRecords . '] [total ' . $totalRecords . '] [memdiff ' . ($memdiff > 0 ? '+' : '') . $memdiff . '] [timediff ' . $timediff . ']');
    }

    private function getStartMemory()
    {
        return $this->startMemory;
    }

    /**
     * @param int $memory
     * @return $this
     */
    private function setStartMemory($memory)
    {
        $this->startMemory = $memory;
        return $this;
    }

    /**
     * @return int
     */
    private function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @param int $timestamp
     * @return $this
     */
    private function setStartTime($timestamp)
    {
        $this->startTime = $timestamp;
        return $this;
    }

    /**
     * @param string $guid
     * @return $this
     */
    private function setGUID($guid)
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return string
     */
    private function getExportName()
    {
        return $this->exportName;
    }

    /**
     * @param string $exportName
     * @return $this
     */
    public function setExportName($exportName) {
        $this->exportName = $exportName;
        return $this;
    }

    /**
     * @return string
     */
    private function getExportSource()
    {
        return $this->exportSource;
    }

    /**
     * @param string $exportSource
     * @return $this
     */
    public function setExportSource($exportSource) {
        $this->exportSource = $exportSource;
        return $this;
    }

    /**
     * @return int
     */
    private function getExportProcessedRecords()
    {
        return $this->processedRecords;
    }

    /**
     * @param int $exportProcessedRecords
     * @return $this
     */
    public function setExportProcessedRecord($exportProcessedRecords) {
        $this->processedRecords = $exportProcessedRecords;
        return $this;
    }

    /**
     * @return int
     */
    private function getExportTotalRecords()
    {
        return $this->totalRecords;
    }

    /**
     * @param int $exportTotalRecords
     * @return $this
     */
    public function setExportTotalRecords($exportTotalRecords) {
        $this->totalRecords = $exportTotalRecords;
        return $this;
    }

    public function sendLogs() {
        $this->cloudWatch->sendLogs();
    }

}
