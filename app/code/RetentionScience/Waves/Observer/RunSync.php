<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/6/16
 * Time: 2:01 PM
 */

namespace RetentionScience\Waves\Observer;


use Magento\Framework\DataObject;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;
use RetentionScience\Waves\Helper\Data;
use RetentionScience\Waves\Sync\Logger;
use RetentionScience\Waves\Sync\Params;
use RetentionScience\Waves\Sync\Uploader;

class RunSync implements ObserverInterface
{
    /**
     * @var Data
     */
    private $helper;
    /**
     * @var Logger
     */
    private $logger;
    /**
     * @var Uploader
     */
    private $uploader;

    public function __construct(
        Uploader $uploader,
        Data $helper,
        Logger $logger
    ) {
        $this->helper = $helper;
        $this->logger = $logger;
        $this->uploader = $uploader;
    }

    /**
     * @param Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        /* @var Params $params */
        $params = $observer->getData('params');
        if(! $params->validateType()) {
            return;
        }

        $groups = $this->helper->getGroupedStoreIds();

        if (!empty($groups)) {
            foreach ($groups as $storeIds) {
                $params->setStoreIds($storeIds);
                $files = $params->getType()->generateFiles($params);
                $this->uploader
                    ->setParams($params)
                    ->processFiles($files, true);
            }
        }
    }

}