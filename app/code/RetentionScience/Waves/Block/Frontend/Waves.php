<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/21/16
 * Time: 2:22 PM
 */

namespace RetentionScience\Waves\Block\Frontend;


use Magento\Catalog\Model\Product;
use Magento\Framework\App\Request\Http as RequestInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use RetentionScience\Waves\Connection\RetentionScience\Exception;
use RetentionScience\Waves\Helper\Data;

class Waves extends Template
{

    /**
     * @var Data
     */
    private $helper;
    /**
     * @var Registry
     */
    private $registry;
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * Waves constructor.
     * @param RequestInterface $request
     * @param Registry $registry
     * @param Data $helper
     * @param Template\Context $context
     * @param array $data
     * @throws NoSuchEntityException
     */
    public function __construct(
        RequestInterface $request,
        Registry $registry,
        Data $helper,
        Template\Context $context,
        array $data)
    {
        $this->helper = $helper;
        $this->registry = $registry;
        $this->request = $request;
        parent::__construct($context, $data);
        $this->helper->setStoreId($this->_storeManager->getStore()->getId());
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getSiteId() {
        return $this->helper->getSiteId();
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function isEnabled() {
        return $this->helper->isEnabled();
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function isAjaxAddToCartEnable() {
        return $this->helper->isAjaxAddToCartEnabled();
    }

    /**
     * @return bool|int
     */
    public function getProductId() {
        if($_product = $this->registry->registry('product')) {
            /* @var Product $_product */
            return $_product->getId();
        }
        return false;
    }

    /**
     * @return false|string
     * @throws Exception
     */
    public function getJsLayout()
    {
        $json = json_decode(parent::getJsLayout());
        $json['track'] = [];
        $json['track'][] = ['_setSiteId', $this->getSiteId(), ];
        $json['cart'] = [];
        if($productId = $this->getProductId()) {
            $json['track'][] = ['_addItem', ['id' => $productId, ], ];
        }
        $json['cart']['ajax'] = $this->isAjaxAddToCartEnable();
        if(
            ($this->request->getModuleName() === 'checkout' && $this->request->getControllerName() === 'cart' && $this->request->getActionName() === 'index')
        ) {
            $json['cart']['now'] = true;
        } else {
            $json['cart']['now'] = false;
        }
        if(
            ($this->request->getModuleName() === 'checkout' && $this->request->getControllerName() === 'onepage' && $this->request->getActionName() === 'success')
        ) {
            $json['checkout_success'] = true;
        } else {
            $json['checkout_success'] = false;
        }

        $json['baseUrls'] = array_unique([
            $this->getUrl(null, ['_secure' => true]),
            $this->getUrl(null, ['_secure' => false]),
        ]);
        return json_encode($json);
    }

}