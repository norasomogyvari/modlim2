<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/6/16
 * Time: 12:28 PM
 */

namespace RetentionScience\Waves\Cron\Sync;


use Magento\Framework\Event\ManagerInterface;
use RetentionScience\Waves\Helper\Data;
use RetentionScience\Waves\Sync\Params;

class Auto
{

    /**
     * @var Data
     */
    private $helper;
    /**
     * @var ManagerInterface
     */
    private $eventManager;
    /**
     * @var Params
     */
    private $params;

    /**
     * Auto constructor.
     * @param Params $params
     * @param Data $helper
     * @param ManagerInterface $eventManager
     */
    public function __construct(
        Params $params,
        Data $helper,
        ManagerInterface $eventManager
    )
    {
        $this->helper = $helper;
        $this->eventManager = $eventManager;
        $this->params = $params;
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function execute() {
        $this->params
            ->setType('batch')
            ->setSpec(['groups:all', ])
            ->setSource('magento2_cron_auto')
            ->setMessage('Cron export() running. Kicked off from the crontab');

        $this->eventManager->dispatch('waves_run_export', ['params' => $this->params, ]);
    }

}