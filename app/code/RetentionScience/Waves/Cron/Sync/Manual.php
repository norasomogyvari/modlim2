<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/6/16
 * Time: 12:29 PM
 */

namespace RetentionScience\Waves\Cron\Sync;


use Magento\Framework\Event\ManagerInterface;
use RetentionScience\Waves\Helper\Data;
use RetentionScience\Waves\Sync\Params;

class Manual
{

    /**
     * @var Data
     */
    private $helper;
    /**
     * @var ManagerInterface
     */
    private $eventManager;
    /**
     * @var Params
     */
    private $params;

    /**
     * Manual constructor.
     * @param Params $params
     * @param Data $helper
     * @param ManagerInterface $eventManager
     */
    public function __construct(
        Params $params,
        Data $helper,
        ManagerInterface $eventManager
    )
    {
        $this->helper = $helper;
        $this->eventManager = $eventManager;
        $this->params = $params;
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function execute() {
        if ($this->helper->getIsRunManual()){
            $this->helper->setIsRunManual(false);

            $this->params
                ->setType('batch')
                ->setSpec(['groups:all', ])
                ->setSource('magento2_cron_manual')
                ->setMessage('Cron exportManual() running. Kicked off from export button push');

            $this->eventManager->dispatch('waves_run_export', ['params' => $this->params, ]);
        }
    }

}