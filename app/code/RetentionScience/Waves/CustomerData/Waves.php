<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/21/16
 * Time: 12:57 AM
 */

namespace RetentionScience\Waves\CustomerData;


use Magento\Customer\CustomerData\SectionSourceInterface;
use Magento\Customer\Model\Session as CustomerSession;
use RetentionScience\Waves\Helper\Data;

class Waves implements SectionSourceInterface
{

    /**
     * @var CustomerSession
     */
    private $customerSession;

    public function __construct(
        CustomerSession $customerSession
    )
    {
        $this->customerSession = $customerSession;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getSectionData()
    {
        $data = [
            'loggedIn' => $this->customerSession->isLoggedIn(),
        ];
        if($this->customerSession->isLoggedIn()) {
            $data['userId'] = Data::md5(trim(strtolower($this->customerSession->getCustomer()->getEmail())));
        }
        return $data;
    }
}