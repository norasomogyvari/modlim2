<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/21/16
 * Time: 12:57 AM
 */

namespace RetentionScience\Waves\CustomerData;


use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\CustomerData\SectionSourceInterface;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Sales\Model\Order\Item;

class WavesOrder implements SectionSourceInterface
{

    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    public function __construct(
        CheckoutSession $checkoutSession
    )
    {
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getSectionData()
    {
        $data = [];
        $order = $this->checkoutSession->getLastRealOrder();
        if($order && $order->getId()) {
            $items = $order->getAllVisibleItems();
            $data = [
                'items' => [],
                'id' => $order->getId(),
                'total' => $order->getGrandTotal(),
            ];
            foreach($items as $item) {
                /* @var Item $item */
                $children = $item->getChildrenItems();
                if(! empty($children)) {
                    $child = $children[0];
                } else {
                    $child = false;
                }
                $data['items'][] = [
                    'id' => $child ? $child->getProductId() : $item->getProductId(),
                    'name' => $item->getName(),
                    'price' => $item->getPrice(),
                ];
            }
        }
        return $data;
    }
}