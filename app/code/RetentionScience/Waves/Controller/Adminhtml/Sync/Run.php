<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/6/16
 * Time: 2:41 PM
 */

namespace RetentionScience\Waves\Controller\Adminhtml\Sync;


use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use RetentionScience\Waves\Helper\Data;

class Run extends Action
{

    /**
     * @var JsonFactory
     */
    private $jsonFactory;
    /**
     * @var Data
     */
    private $helper;

    public function __construct(
        Data $helper,
        JsonFactory $jsonFactory,
        Action\Context $context
    )
    {
        parent::__construct($context);
        $this->jsonFactory = $jsonFactory;
        $this->helper = $helper;
    }

    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $json = $this->jsonFactory->create();
        $this->helper->setIsRunManual(true);
        $json->setData([
            'success' => true,
            'message' => __('Sync would be run in next few minutes.'),
        ]);
        return $json;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Retention_Science::settings');
    }
}