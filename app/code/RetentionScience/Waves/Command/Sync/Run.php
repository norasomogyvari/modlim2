<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/6/16
 * Time: 3:57 PM
 */

namespace RetentionScience\Waves\Command\Sync;

use Magento\Framework\App\State;
use RetentionScience\Waves\Helper\Data;
use RetentionScience\Waves\Sync\Params;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\Event\ManagerInterface;

class Run extends Command
{

    /**
     * @var Data
     */
    private $helper;
    /**
     * @var ManagerInterface
     */
    private $eventManager;
    /**
     * @var Params
     */
    private $params;
    /**
     * @var State
     */
    private $appState;

    /**
     * Run constructor.
     * @param Params $params
     * @param ManagerInterface $eventManager
     * @param Data $helper
     * @param State $appState
     */
    public function __construct(
        Params $params,
        ManagerInterface $eventManager,
        Data $helper,
        State $appState
    )
    {
        $this->helper = $helper;
        $this->eventManager = $eventManager;
        $this->params = $params;
        parent::__construct();
        $this->appState = $appState;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure() {
        $this->setName('waves:sync:run');
        $this->setDescription('Export Data to RetentionScience.');
        $specDescription = 'Type-specific params:';
        foreach($this->params->getAvailableTypes() as $typeModel) {
            $specDescription .= "\n  {$typeModel->getName()}";
            foreach($typeModel->getParams() as $param) {
                $specDescription .= "\n    {$param['name']}[" . ((isset($param['required']) && $param['required']) ? 'required' : 'optional') . "]" . (isset($param['description']) ? $param['description'] : '');
            }
        }
        $this->setDefinition([
            new InputArgument(
                'type',
                InputArgument::REQUIRED,
                'Available Types: ' . implode(', ', $this->params->getAvailableTypeNames())
            ),
            new InputArgument(
                'spec',
                InputArgument::IS_ARRAY,
                $specDescription
            ),
        ]);
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->appState->setAreaCode('global');

        $params = $this->params;
        $params
            ->setType($input->getArgument('type'))
            ->setSpec($input->getArgument('spec'))
            ->setSource('magento2_shell_script')
            ->setMessage('Cron export() running. Kicked off from the console');

        $this->eventManager->dispatch('waves_run_export', ['params' => $params, ]);
    }

}