<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/13/17
 * Time: 2:47 PM
 */

namespace RetentionScience\Waves\Plugin\Magento\Checkout\CustomerData;


use Magento\Checkout\CustomerData\DefaultItem as Original;
use Magento\Quote\Model\Quote\Item;

class DefaultItem
{

    /**
     * @param Original $original
     * @param callable $proceed
     * @param Item $item
     * @return array
     */
    public function aroundGetItemData(Original $original, callable $proceed, Item $item) {
        $wavesProductId = $item->getProduct()->getId();
        $children = $item->getChildren();
        if(! empty($children)) {
            $wavesProductId = $children[0]->getProduct()->getId();
        }
        return \array_merge(
            ['waves_product_id' => $wavesProductId],
            $proceed($item)
        );
    }

}