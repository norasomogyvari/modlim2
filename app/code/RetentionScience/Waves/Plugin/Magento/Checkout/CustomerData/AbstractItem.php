<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/21/16
 * Time: 5:34 PM
 */

namespace RetentionScience\Waves\Plugin\Magento\Checkout\CustomerData;


use Magento\Checkout\CustomerData\AbstractItem as Original;
use Magento\Quote\Model\Quote\Item;

class AbstractItem
{

    /**
     * @param Original $abstractItem
     * @param callable $proceed
     * @param Item $item
     * @return array
     */
    public function aroundGetItemData(Original $abstractItem, callable $proceed, Item $item) {
        $return = $proceed($item);
        return \array_merge(
            $return,
            [
                'product_id' => $item->getProduct()->getId(),
            ]
        );
    }

}