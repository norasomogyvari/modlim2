<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/5/16
 * Time: 10:55 PM
 */

namespace RetentionScience\Waves\Model\Backend;


use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Value;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use RetentionScience\Waves\Connection\AWS\CloudWatch;
use RetentionScience\Waves\Connection\AWS\CloudWatch as ConnectionCloudWatch;
use RetentionScience\Waves\Connection\RetentionScience\ApiFactory;
use RetentionScience\Waves\Connection\RetentionScience\Exception;
use RetentionScience\Waves\Helper\Data;
use Zend\Json\Json;

class Credentials extends Value
{

    /**
     * @var Data
     */
    private $helper;
    /**
     * @var ApiFactory
     */
    private $apiFactory;
    /**
     * @var ManagerInterface
     */
    private $messageManager;
    /**
     * @var CloudWatch
     */
    private $cloudWatch;

    public function __construct(
        ConnectionCloudWatch $cloudWatch,
        ManagerInterface $messageManager,
        ApiFactory $apiFactory,
        Data $helper,
        Context $context,
        Registry $registry,
        ScopeConfigInterface $config,
        TypeListInterface $cacheTypeList,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = [])
    {
        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
        $this->helper = $helper;
        $this->apiFactory = $apiFactory;
        $this->messageManager = $messageManager;
        $this->cloudWatch = $cloudWatch;
    }

    public function afterSave()
    {
        static $settings = [];
        $settings[$this->getPath()] = $this->getValue();
        if(count($settings) === 4) {
            $this->helper->setStoreId($this->getScopeId());
            // abideByNewsletterSubscriber is not needed nere
            $api = $this->apiFactory->create([
                'username' => $settings[Data::WAVES_GENERAL_API_USER],
                'password' => $settings[Data::WAVES_GENERAL_API_PASSWORD],
                'testmode' => !! $settings[Data::WAVES_GENERAL_TEST_MODE],
            ]);
            $valid_aws_credentials = false;
            $valid_site_id = false;
            $aws_credentials = [];
            $site_id = [];
            try {
                $aws_credentials = Json::decode($api->get_aws_credentials(), Json::TYPE_ARRAY);
                $site_id = Json::decode($api->get_site_id(), Json::TYPE_ARRAY);

                $valid_aws_credentials = is_array($aws_credentials) && isset($aws_credentials['status']) && $aws_credentials['status'] === 'success';
                $valid_site_id = is_array($site_id) && $site_id['status'] === 'success';

                if(! $valid_aws_credentials) {
                    throw new Exception('Error 8a with API call. Please check credentials and try again.');
                }

                if(! $valid_site_id) {
                    throw new Exception('Error 8b with API call. Please check credentials and try again.');
                }
            } catch (\Exception $e) {
                $this->helper->setIsEnabled(false);
                $this->messageManager->addErrorMessage('Unable to connect to Retention Science API. Module is disabled. ' . $e->getMessage());
            }
            if($valid_aws_credentials && $valid_site_id) {
                $this->helper
                    ->setAWSAccessKeyId($aws_credentials['access_key_id'])
                    ->setAWSSecretAccessKey($aws_credentials['secret_access_key'])
                    ->setAWSLogStream($aws_credentials['log_stream'])
                    ->setAWSLogGroup($aws_credentials['log_group'])
                    ->setAWSSessionToken($aws_credentials['session_token'])
                    ->setSiteId($site_id['id']);
                try {
                    $msg = "Saved helper details: site_id " . $site_id['id'];
                    $msg .= ", access_key_id " . $aws_credentials['access_key_id'];
                    $msg .= ", secret_access_key " . $aws_credentials['secret_access_key'];
                    $msg .= ", log_group " . $aws_credentials['log_group'];
                    $msg .= ", log_stream " . $aws_credentials['log_stream'];
                    $msg .= ", session_token " . $aws_credentials['session_token'];
                    $this->cloudWatch->reinitClient();
                    $this->cloudWatch->logMessage($msg);

                } catch(Exception $e) {
                    $this->helper->setIsEnabled(false);
                    $this->messageManager->addErrorMessage('Error: ' . $e->getMessage());
                }
            }
        }
        return parent::afterSave();
    }

}