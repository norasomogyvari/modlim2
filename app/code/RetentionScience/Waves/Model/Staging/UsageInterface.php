<?php
/**
 * Created by PhpStorm.
 * User: arybitskiy
 * Date: 2/28/19
 * Time: 6:54 PM
 */

namespace RetentionScience\Waves\Model\Staging;

interface UsageInterface
{
    /**
     * @return boolean
     */
    public function isEnabled();

    /**
     * @return int
     */
    public function getCurrentVersionId();
}