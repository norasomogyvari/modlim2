<?php
/**
 * Created by PhpStorm.
 * User: arybitskiy
 * Date: 2/28/19
 * Time: 6:58 PM
 */

namespace RetentionScience\Waves\Model\Staging;

class Factory
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    private $moduleManager;
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * UsageFactory constructor.
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Framework\ObjectManagerInterface $objectManager
    )
    {
        $this->moduleManager = $moduleManager;
        $this->objectManager = $objectManager;
    }

    /**
     * @param array $data
     * @return UsageInterface
     */
    public function create(array $data = []) {
        if ($this->moduleManager->isEnabled('Magento_Staging')) {
            $instanceName = 'RetentionScience\Waves\Model\Staging\Exists';
        } else {
            $instanceName = 'RetentionScience\Waves\Model\Staging\NotExists';
        }
        return $this->objectManager->create($instanceName, $data);
    }
}