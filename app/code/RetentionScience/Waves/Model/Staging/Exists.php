<?php
/**
 * Created by PhpStorm.
 * User: arybitskiy
 * Date: 2/28/19
 * Time: 6:56 PM
 */

namespace RetentionScience\Waves\Model\Staging;

class Exists implements UsageInterface
{
    /**
     * @var \Magento\Staging\Model\VersionManager
     */
    private $versionManager;

    /**
     * Exists constructor.
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager
    )
    {
        $this->versionManager = $objectManager->create('Magento\Staging\Model\VersionManager');
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return true;
    }

    /**
     * @return int
     */
    public function getCurrentVersionId()
    {
        return $this->versionManager->getCurrentVersion()->getId();
    }
}