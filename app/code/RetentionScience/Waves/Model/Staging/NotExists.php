<?php
/**
 * Created by PhpStorm.
 * User: arybitskiy
 * Date: 2/28/19
 * Time: 6:56 PM
 */

namespace RetentionScience\Waves\Model\Staging;

class NotExists implements UsageInterface
{

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return false;
    }

    /**
     * @return int
     */
    public function getCurrentVersionId()
    {
        return 1;
    }
}