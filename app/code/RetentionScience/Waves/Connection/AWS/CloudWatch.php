<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/6/16
 * Time: 12:44 AM
 */

namespace RetentionScience\Waves\Connection\AWS;


use Aws\CloudWatchLogs\CloudWatchLogsClient;
use Aws\CloudWatchLogs\Exception\CloudWatchLogsException;
use Magento\Framework\App\ProductMetadata;
use Magento\Framework\Stdlib\DateTime\DateTime;
use RetentionScience\Waves\Helper\Data;

class CloudWatch
{

    const AWS_DEFAULT_REGION = 'us-east-1';

    const AWS_VERSION = '2014-03-28';

    const MAX_LOG_EVENTS = 10000;

    const MAX_BATCH_SIZE = 1048576;

    const EVENT_ADDITIONAL_SIZE = 26;

    protected $_logs = array();

    protected $_logSize = array();

    protected $_logBatches = array();
    /**
     * @var Data
     */
    private $helper;
    /**
     * @var CloudWatchLogsClient
     */
    protected $client;
    /**
     * @var DateTime
     */
    private $dateTime;
    /**
     * @var string[]
     */
    private $sequenceTokens;
    /**
     * @var ProductMetadata
     */
    private $magento;

    /**
     * CloudWatch constructor.
     * @param DateTime $dateTime
     * @param Data $helper
     * @param ProductMetadata $magento
     * @internal param array $data
     */
    public function __construct(
        DateTime $dateTime,
        Data $helper,
        ProductMetadata $magento
    ) {
        $this->helper = $helper;
        $this->dateTime = $dateTime;

        $this->magento = $magento;
    }

    public function reinitClient() {
        $this->setClient(new CloudWatchLogsClient(array(
            'region' => self::AWS_DEFAULT_REGION,
            'version' => self::AWS_VERSION,
            'credentials' => array(
                'key' => $this->helper->getAWSAccessKeyId(),
                'secret' => $this->helper->getAWSSecretAccessKey(),
                'token' => $this->helper->getAWSSessionToken(),
            ),
        )));
    }

    /**
     *
     * Log one message
     * Example:
     * $this->log_message("hello testing");
     * @param string $msg
     * @param array $options
     */
    public function logMessage($msg, array $options = []) {

        $siteId = $this->helper->getSiteId();
        $prefix = $this->dateTime->gmtDate('Y-m-d H:i:s'). ' UTC - [site_id ' . $siteId . '] ' . $this->magento->getName() . '[' . $this->magento->getEdition() . ' ' . $this->magento->getVersion() . '] ';

        $this->log(array_merge(array(
            'send' => TRUE,
            'logGroupName' => $this->helper->getAWSLogGroup(),
            'logStreamName' => $this->helper->getAWSLogStream(),
            'message' => $prefix . $msg
        ), $options));
    }

    /**
     *
     * Log event
     * Example:
     * $this->log(array(
     *      //Required
     *      'logGroupName' => 'test',
     *      // Required
     *      'logStreamName' => 'test',
     *      // Required
     *      'message' => 'qwerty',
     *      // Optional
     *      'timestamp' => round(microtime(true) * 1000),
     *      // Optional
     *      'send' => TRUE || FALSE, // If set tu true - log will be send to AWS CloudWatchLogs immediately
     * ));
     * @param array $log
     */
    public function log(array $log) {
        if(! isset($log['logGroupName']) OR ! isset($log['logStreamName']) OR ! isset($log['message'])) {
            return;
        }
        if(! isset($log['timestamp'])) {
            $log['timestamp'] = round(microtime(true) * 1000);
        }

        $newBatch = FALSE;
        if(isset($log['send']) AND $log['send']) {
            $location = array(
                'logGroupName' => $log['logGroupName'],
                'logStreamName' => $log['logStreamName'],
            );
            $location['logGroupName'] = $this->getLogGroup($location['logGroupName']);
            $location['logStreamName'] = $this->getLogStream($location['logStreamName']);
            $this->putLogEvents($location, array(
                array('timestamp' => $log['timestamp'], 'message' => $log['message'],),
            ));
        } else {
            $logGroupName = $log['logGroupName'];
            $logStreamName = $log['logStreamName'];
            if(! isset($this->_logs[$logGroupName])) {
                $this->_logs[$logGroupName] = array();
                $this->_logSize[$logGroupName] = array();
                $this->_logBatches[$logGroupName] = array();
            }
            if(! isset($this->_logs[$logGroupName][$logStreamName])) {
                $this->_logs[$logGroupName][$logStreamName] = array();
                $this->_logSize[$logGroupName][$logStreamName] = 0;
                $this->_logBatches[$logGroupName][$logStreamName] = 0;
            }
            $batch = $this->_logBatches[$logGroupName][$logStreamName];
            if(! isset($this->_logs[$logGroupName][$logStreamName][$batch])) {
                $this->_logs[$logGroupName][$logStreamName][$batch] = array();
            }
            if(
                (strlen($log['message']) + self::EVENT_ADDITIONAL_SIZE + $this->_logSize[$logGroupName][$logStreamName] > self::MAX_BATCH_SIZE)
                OR
                (count($this->_logs[$logGroupName][$logStreamName][$batch])+1 > self::MAX_LOG_EVENTS)
            ) {
                $this->_logSize[$logGroupName][$logStreamName] = 0;
                $this->_logBatches[$logGroupName][$logStreamName] ++;
                $batch = $this->_logBatches[$logGroupName][$logStreamName];
                // If batch is full - send and clean - if there would be a lot of errors - to save memory
                $this->sendLogs();
                gc_collect_cycles();
            }
            $this->_logSize[$logGroupName][$logStreamName] += strlen($log['message']) + self::EVENT_ADDITIONAL_SIZE;
            $this->_logs[$logGroupName][$logStreamName][$batch][] = array(
                'timestamp' => $log['timestamp'],
                'message' => $log['message'],
            );
        }
    }

    /**
     * Send all logs to AWS CloudWatchLogs if 'send' wasn't set to TRUE in $this->log() method
     */
    public function sendLogs() {
        if(empty($this->_logs)) {
            return;
        }
        foreach($this->_logs AS $logGroupName => $logStreams) {
            $location = array(
                'logGroupName' => $logGroupName,
            );
            $location['logGroupName'] = $this->getLogGroup($location['logGroupName']);
            foreach($logStreams AS $logStreamName => $logEventsBatches) {
                $location['logStreamName'] = $this->getLogStream($logStreamName);
                foreach($logEventsBatches AS $logEvents) {
                    $this->putLogEvents($location, $logEvents);
                }
            }
        }
        $this->_logs = array();
    }

    public function getReadableEventLog($logGroupName, $logStreamNames = '') {
        $ret = '';
        $ret .= 'LogGroupName: ' . $logGroupName . "\n";
        try {
            if (empty($logStreamNames)) {
                $streams = $this->getClient()->describeLogStreams(array(
                    'logGroupName' => $logGroupName,
                ))->get('logStreams');
                $logStreamNames = array();
                if (!empty($streams)) {
                    foreach ($streams AS $stream) {
                        $logStreamNames[] = $stream['logStreamName'];
                    }
                }
            } else {
                $logStreamNames = array($logStreamNames);
            }
            if (empty($logStreamNames)) {
                $ret .= "\tNo streams found\n";
            } else {
                foreach ($logStreamNames AS $logStreamName) {
                    $ret .= "\tLogStreamName: {$logStreamName}\n";
                    $logEvents = $this->getClient()->getLogEvents(array(
                        'logGroupName' => $logGroupName,
                        'logStreamName' => $logStreamName,
                    ))->get('events');
                    if (empty($logEvents)) {
                        $ret .= "\t\tNo log events found\n";
                    } else {
                        foreach ($logEvents AS $logEvent) {
                            $ret .= "\t\t" . $logEvent['timestamp'] . " {$logEvent['message']}\n";
                        }
                    }
                }
            }
        } catch(\Exception $e) {
            $ret .= "\t" . $e->getMessage() . "\n";
        }
        return $ret;
    }

    /**
     * Get/Create log group
     * @param $logGroupName
     * @return mixed
     */
    protected function getLogGroup($logGroupName) {
        $logGroupName = preg_replace('#[^a-zA-Z0-9_\-\/\.]#', '', $logGroupName);
        return $logGroupName;
    }

    /**
     * Get/Create log stream
     * @param $logStreamName
     * @return mixed
     */
    protected function getLogStream($logStreamName) {
        $logStreamName = preg_replace('#[:]#', '', $logStreamName);
        return $logStreamName;
    }

    /**
     * Send log events
     * @param array $location
     * @param array $events
     * @param bool $catchException
     * @return \Aws\Result|void
     * @throws \Exception
     */
    protected function putLogEvents(array $location, array $events = array(), $catchException = TRUE) {
        if(empty($events)) {
            return null;
        }
        $sequenceToken = $this->getSequenceToken($location);
        $request = $location;
        $request['logEvents'] = $events;
        if(! is_null($sequenceToken)) {
            $request['sequenceToken'] = $sequenceToken;
        }
        try {
            $result = $this->getClient()->putLogEvents($request);
            $this->setSequenceToken($location, $result->get('nextSequenceToken'));
//            foreach($request['logEvents'] as $e) {
//                echo "{$e['message']}\n";
//            }
            return $result;
        } catch(CloudWatchLogsException $e) {
            if($e->getAwsErrorCode() === 'InvalidSequenceTokenException') {
                $result = $this->getClient()->describeLogStreams(array(
                    'logGroupName' => $location['logGroupName'],
                    'logStreamNamePrefix' => $location['logStreamName'],
                ));
                $logStreams = $result->get('logStreams');
                if(! empty($logStreams)) {
                    $logStream = array_shift($logStreams);
                    $this->setSequenceToken($location, $logStream['uploadSequenceToken']);
                    return $this->putLogEvents($location, $events);
                }
            } else {
                if(! $catchException) {
                    throw $e;
                }
                $this->helper->updateAWSCredentials();
                $this->reinitClient();
                $this->putLogEvents($location, $events, FALSE);
            }
        } catch(\Exception $e) {
            if(! $catchException) {
                throw $e;
            }
            $this->helper->updateAWSCredentials();
            $this->reinitClient();
            $this->putLogEvents($location, $events, FALSE);
        }
        return null;
    }

    /**
     * Get Sequence Token
     * @param array $location
     * @return mixed
     */
    protected function getSequenceToken(array $location) {
        $tokens = $this->getSequenceTokens();
        $key = $location['logGroupName'] . '::' . $location['logStreamName'];
        if(isset($tokens[$key])) {
            return $tokens[$key];
        }
        return null;
    }

    /**
     * Set Sequence Token
     * @param array $location
     * @param $sequenceToken
     */
    protected function setSequenceToken(array $location, $sequenceToken) {
        $tokens = $this->getSequenceTokens();
        if(! is_array($tokens)) {
            $tokens = array();
        }
        $key = $location['logGroupName'] . '::' . $location['logStreamName'];
        $tokens[$key] = $sequenceToken;
        $this->setSequenceTokens($tokens);
    }

    /**
     * @param CloudWatchLogsClient $client
     * @return $this
     */
    protected function setClient($client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return CloudWatchLogsClient
     */
    private function getClient()
    {
        if(is_null($this->client)) {
            $this->reinitClient();
        }
        return $this->client;
    }

    private function getSequenceTokens()
    {
        return $this->sequenceTokens;
    }

    private function setSequenceTokens($tokens)
    {
        $this->sequenceTokens = $tokens;
        return $this;
    }

}