<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/6/16
 * Time: 12:13 AM
 */

namespace RetentionScience\Waves\Connection\RetentionScience;


use Magento\Framework\ObjectManagerInterface;

class ApiFactory
{
    /**
     * Object Manager
     *
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * Construct
     *
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(ObjectManagerInterface $objectManager)
    {
        $this->_objectManager = $objectManager;
    }

    /**
     * @param array $data
     * @return Api
     */
    public function create(array $data = []) {
        return $this->_objectManager->create('RetentionScience\Waves\Connection\RetentionScience\Api', $data);
    }
}