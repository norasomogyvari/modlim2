<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/6/16
 * Time: 12:09 AM
 */

namespace RetentionScience\Waves\Connection\RetentionScience;


use Magento\Framework\App\ObjectManager;
use Magento\Framework\HTTP\Client\Curl;

class Api
{

    const API_TEST_URL = 'https://api.retentionsandbox.com';
    const API_URL = 'https://api.retentionscience.com';
    const API_PORT = 443;
    const API_VERSION = '1';

    private $password;
    private $time_out = 60;
    private $username;
    private $testmode;
    private $abideByNewsletterSubscriber;
    /**
     * @var Curl
     */
    private $curl;

    // class methods

    /**
     * Api constructor.
     * @param Curl $curl
     * @param null $username
     * @param null $password
     * @param bool $testmode
     * @param bool $abideByNewsletterSubscriber
     */
    public function __construct(Curl $curl, $username = null, $password = null, $testmode=false, $abideByNewsletterSubscriber = false) {
        $this->curl = $curl;
        if(is_array($username) AND isset($username['username']) AND isset($username['password'])) {
            $password = $username['password'];
            $testmode = isset($username['testmode']) ? $username['testmode'] : FALSE;
            $abideByNewsletterSubscriber = isset($username['abideByNewsletterSubscriber']) ? $username['abideByNewsletterSubscriber'] : $abideByNewsletterSubscriber;
            $username = $username['username'];
        }
        if($username !== null) $this->set_username($username);
        if($password !== null) $this->set_password($password);
        if($testmode) $this->set_testmode($testmode);
        $this->set_abideByNewsletterSubscriber($abideByNewsletterSubscriber);
    }

    /**
     * @param $url
     * @param array $params
     * @param bool $authenticate
     * @param bool $use_post
     * @return array
     * @throws \Exception
     */
    protected function perform_call($url, $params = array(), $authenticate = false, $use_post = true) {
        // redefine
        $url = (string) $url;
        $authenticate = (bool) $authenticate;
        $use_post = (bool) $use_post;

        if ($this->get_testmode()) {
            $url = self::API_TEST_URL .'/' . $url;
        } else {
            $url = self::API_URL .'/' . $url;
        }

        // validate needed authentication
        if($authenticate && ($this->get_username() == '' || $this->get_password() == '')) {
            throw new \Exception('No username or password was set.');
        }

        // build GET URL if not using post
        if(!empty($params) && !$use_post){
            $url .= '?'. http_build_query( $params );
        }

        // set options
        $this->curl->setOption(CURLOPT_URL, $url);
        $this->curl->setOption(CURLOPT_PORT, self::API_PORT);

        $this->curl->setOption(CURLOPT_USERAGENT, $this->get_user_agent());
        // follow on only if allowed - 20120221
        if (ini_get('open_basedir') == '' && ini_get('safe_mode' == 'Off')){
            $this->curl->setOption(CURLOPT_FOLLOWLOCATION, true);
        }
        $this->curl->setOption(CURLOPT_RETURNTRANSFER, true);
        $this->curl->setOption(CURLOPT_TIMEOUT, $this->time_out);

        // HTTP basic auth
        if($authenticate) {
            $this->curl->setOption(CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            $this->curl->setOption(CURLOPT_USERPWD, $this->get_username() .':'. $this->get_password());
        }

        try {
            if ($use_post) {
                $this->curl->setOption(CURLOPT_POST, 1);
                $this->curl->setOption(CURLOPT_POSTFIELDS, $params);
                $this->curl->setOption(CURLOPT_HTTPGET, 0);
            }
            $this->curl->get($url);
            $response = $this->curl->getBody();
        } catch (\Exception $e) {
            $response = 'CURL ERROR: ' . $e->getMessage();
        }
        $http_status_code = $this->curl->getStatus();
        return array('response_code' => $http_status_code,
            'response' => $response);
    }

    protected function handle_response($response){
        // decode the returned json
        if ($response['response_code'] == 200 || $response['response_code'] == 201 ){
            return $response['response'];
        } else {
            throw new \Exception($response['response_code'] . ' - ' . $response['response']);
        }
    }

    private function get_magento_version() {
        static $version;
        if(is_null($version)) {
            $objectManager = ObjectManager::getInstance();
            $productMetadata = $objectManager->get('Magento\Framework\App\ProductMetadataInterface');
            $version = $productMetadata->getVersion();
        }
        return $version;
    }


    // Getters
    private function get_password(){
        return (string) $this->password;
    }
    private function get_user_agent(){
        return (string) 'Retention Science Magento (' . $this->get_magento_version() . ') PHP API Client / v'. self::API_VERSION;
    }
    private function get_username(){
        return (string) $this->username;
    }
    private function get_testmode(){
        return (boolean) $this->testmode;
    }
    private function get_abideByNewsletterSubscriber() {
        return $this->abideByNewsletterSubscriber;
    }

    // Setters
    private function set_username($username){
        $this->username = (string) $username;
    }
    private function set_password($password){
        $this->password = (string) $password;
    }
    private function set_testmode($testmode){
        $this->testmode = (boolean) $testmode;
    }
    private function set_abideByNewsletterSubscriber($abideByNewsletterSubscriber) {
        $this->abideByNewsletterSubscriber = (boolean) $abideByNewsletterSubscriber;
}


    /* log_credentials resource */
    public function get_aws_credentials() {
        $url = 'magento/log_credentials';
        $response = $this->perform_call($url, array(), true, false);
        return $this->handle_response($response);
    }

    /* site_id */
    public function get_site_id() {
        $url = 'sites/id';
        $response = $this->perform_call($url, array(), true, false);
        return $this->handle_response($response);
    }

    /* Data Sync */
    public function sync_data($file_hash) {
        $url = 'bulk_import/import';

        $upload_files = array(
            'import_type' => 'magento',
            'abide_by_marketing_flag' => $this->get_abideByNewsletterSubscriber() ? 'true' : 'false',
        );
        foreach ($file_hash as $type => $file) {
            if(! \class_exists('\CURLFile')) {
                $upload_files[$type] = "@$file";
            } else {
                $upload_files[$type] = new \CURLFile($file);
            }
        }
        $response = $this->perform_call($url, $upload_files, true, true);
        return $this->handle_response($response);
    }

}
