define([
    'jquery',
    'underscore',
    'Magento_Customer/js/customer-data'
], function($, _, customerData) {
    "use strict";

    $.widget('rs.waves', {
        sentCartItems : [],
        ajaxCartUrls : [
            {
                isUrlMatches: function(url) {
                    if (decodeURIComponent(url.replace(/\+/g)).match(/update_section_id=false/)) {
                        return false;
                    }
                    var parsed = decodeURIComponent(url.replace(/\+/g)).match(/\?(.+)/);
                    if(!parsed || !parsed.length) {
                        return false;
                    }

                    var queryString = parsed[1];
                    var queryGroups = queryString.split('&');

                    var hasCart = false;

                    if(queryGroups && queryGroups.length) {
                        queryGroups.forEach(function(groupString) {
                            var groupParsed = groupString.split('=');
                            if(!groupParsed || !groupParsed.length) {
                                return;
                            }

                            var name = groupParsed[0];
                            var valueString = groupParsed[1];

                            if(name !== 'sections') {
                                return;
                            }

                            var sections = valueString.split(',');
                            if(sections.indexOf('cart') !== -1) {
                                hasCart = true;
                            }
                        });
                    }

                    return hasCart;
                },
                methods : /get/i,
                delayed : false
            },
            {
                isUrlMatches: function(url) {
                    if (decodeURIComponent(url.replace(/\+/g)).match(/update_section_id=false/)) {
                        return false;
                    }
                    var parsed = decodeURIComponent(url.replace(/\+/g)).match(/customer\/section\/load\/\?_=\d+/);
                    return !!parsed;
                },
                methods : /get/i,
                delayed : false
            }
        ],
        _create : function() {
            /**
             * Init Ajax Cart Handlers
             */
            this.options.cart.ajax && this.bindAjaxCart();
            /**
             * Make sure proper customer data loaded
             */
            this.loadCustomerData((function() {
                this.track();
            }).bind(this));
        },
        bindAjaxCart : function() {
            $(document).on('ajaxComplete', (function (event, xhr, settings) {
                for(var i = 0, l = this.ajaxCartUrls.length; i < l; i++) {
                    var params = this.ajaxCartUrls[i];
                    if(! settings.type.match(params.methods)) {
                        continue;
                    }
                    if(! params.isUrlMatches(settings.url)) {
                        continue;
                    }
                    if (!xhr.responseJSON || !('cart' in xhr.responseJSON)) {
                        continue;
                    }
                    if(params.delayed) {
                        var subscription = customerData.get('cart').subscribe((function() {
                            subscription.dispose();
                            var _rsq = window._rsq = window._rsq || [];
                            this.trackCustomerId(_rsq);
                            this.trackCartItems(_rsq);
                            _rsq.push(['_track']);
                        }).bind(this));
                    } else {
                        ((function() {
                            var _rsq = window._rsq = window._rsq || [];
                            this.trackCustomerId(_rsq);
                            this.trackCartItems(_rsq);
                            _rsq.push(['_track']);
                        }).bind(this)) ();
                    }
                }
            }).bind(this));
        },
        track : function() {
            var waves = customerData.get('waves')();
            var _rsq = window._rsq = window._rsq || [];
            if(_.isArray(this.options.track)) {
                _.each(this.options.track, function(el) {
                    _rsq.push(el);
                });
            }
            this.trackCustomerId(_rsq);
            /**
             * Cart Items
             */
            this.options.cart.now && this.trackCartItems(_rsq);
            /**
             * Checkout Success
             */
            if(this.options.checkout_success) {
                var wavesOrder = customerData.get('waves-order')();
                if(('id' in wavesOrder) && wavesOrder.id) {
                    _.each(wavesOrder.items, function(item) {
                        _rsq.push(['_addItem', item]);
                    });
                    _rsq.push(['_addOrder', {
                        id : wavesOrder.id,
                        total : wavesOrder.total
                    }]);
                    _rsq.push(['_setAction', 'checkout_success']);
                }
            }
            _rsq.push(['_track']);
            this.loadRS();
        },
        trackCustomerId : function(_rsq) {
            var waves = customerData.get('waves')();
            if(('loggedIn' in waves) && waves.loggedIn) {
                _rsq.push(['_setUserId', waves.userId]);
            }
        },
        trackCartItems : function(_rsq) {
            /**
             * Here we check if we have already sent such items on current page
             * To prevent duplicate requests
             */
            var cart = customerData.get('cart')(), newCartItems = [], hasDiff;
            if(! cart || ! ('items' in cart) || ! cart.items.length) {
                return;
            }
            _.each(cart.items, function(item) {
                newCartItems.push(item.item_id + ' ' + item.qty);
            });
            hasDiff = ! (_.intersection(this.sentCartItems, newCartItems).length === this.sentCartItems.length && this.sentCartItems.length === newCartItems.length);
            if(hasDiff) {
                _.each(cart.items, function(item) {
                    _rsq.push(['_addItem', {
                        id : item.waves_product_id,
                        name : item.product_name,
                        price : item.product_price_value
                    }]);
                });
                _rsq.push(['_setAction', 'shopping_cart']);
                this.sentCartItems = newCartItems;
            }
        },
        loadCustomerData : function(cb) {
            var sections = ['waves'], sectionsToLoad;
            this.options.cart.now && sections.push('cart');
            sectionsToLoad = $.grep(sections, function(section) {
                return ! ('data_id' in customerData.get(section)());
            });
            this.options.checkout_success && sectionsToLoad.push('waves-order');
            if(sectionsToLoad.length) {
                if(cb) {
                    customerData.reload(sectionsToLoad, 1).done(cb);
                } else {
                    customerData.reload(sectionsToLoad, 1);
                }
            } else {
                cb && cb();
            }
        },
        loadRS : function() {
            var rScix = document.createElement('script');
            rScix.type = 'text/javascript';
            rScix.async = true;
            rScix.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'd1stxfv94hrhia.cloudfront.net/waves/v2/w.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(rScix);
        }
    });

    return $.rs.waves;
});