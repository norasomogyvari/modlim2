<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/5/16
 * Time: 11:13 PM
 */

namespace RetentionScience\Waves\Helper;

use Exception;
use Magento\CatalogInventory\Model\Configuration;
use Magento\Framework\App\Cache\Type\Config;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use RetentionScience\Waves\Connection\RetentionScience\Api;
use RetentionScience\Waves\Connection\RetentionScience\ApiFactory;
use RetentionScience\Waves\Connection\RetentionScience\Exception as ExceptionAlias;
use RetentionScience\Waves\Model\Staging\Factory;
use RetentionScience\Waves\Model\Staging\UsageInterface;
use Zend\Json\Json;

class Data extends AbstractHelper
{

    const WAVES_CONFIG_START = 'waves';
    const WAVES_GENERAL_ENABLED = 'waves/general/enabled';
    const WAVES_GENERAL_API_USER = 'waves/general/api_user';
    const WAVES_GENERAL_API_PASSWORD = 'waves/general/api_password';
    const WAVES_GENERAL_SITE_ID = 'waves/general/site_id';
    const WAVES_GENERAL_TEST_MODE = 'waves/general/test_mode';
    const WAVES_GENERAL_AJAX_ADDTOCART_ENABLE = 'waves/general/ajax_addtocart_enable';
    const WAVES_GENERAL_STORE_ID = 'waves/general/store_id';
    const WAVES_GENERAL_ABIDE_BY_NEWSLETTER_SUBSCRIBER = 'waves/general/abide_by_newsletter_subscriber';

    const WAVES_AWS_ACCESS_KEY_ID = 'waves/aws/access_key_id';
    const WAVES_AWS_SECRET_ACCESS_KEY = 'waves/aws/secret_access_key';
    const WAVES_AWS_LOG_STREAM = 'waves/aws/log_stream';
    const WAVES_AWS_LOG_GROUP = 'waves/aws/log_group';
    const WAVES_AWS_SESSION_TOKEN = 'waves/aws/session_token';

    const WAVES_SYNC_RUN_MANUAL = 'waves/sync/run_manual';
    const WAVES_SYNC_BULK_COMPRESSION_ENABLE = 'waves/sync/bulk_compression_enable';

    /**
     * @var WriterInterface
     */
    private $writer;
    /**
     * @var TypeListInterface
     */
    private $cacheTypeList;
    /**
     * @var Api[]
     */
    private $api = [];
    /**
     * @var ApiFactory
     */
    private $apiFactory;
    /**
     * @var array
     */
    private $config = [];
    /**
     * @var UsageInterface
     */
    private $staging;
    /**
     * @var integer
     */
    private $storeId;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * Data constructor.
     * @param ApiFactory $apiFactory
     * @param TypeListInterface $cacheTypeList
     * @param WriterInterface $writer
     * @param Context $context
     * @param Factory $stagingFactory
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ApiFactory $apiFactory,
        TypeListInterface $cacheTypeList,
        WriterInterface $writer,
        Context $context,
        Factory $stagingFactory,
        StoreManagerInterface $storeManager
    )
    {
        $this->writer = $writer;
        $this->cacheTypeList = $cacheTypeList;
        $this->apiFactory = $apiFactory;
        $this->staging = $stagingFactory->create();
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }

    /**
     * md5 function is not used in this extension for encryption, but just to create a hash
     * This is done to pass magento 2 coding standard
     * @param string $str
     * @return string
     */
    public static function md5($str) {
        return \Zend_Crypt::hash('md5', $str);
    }

    /**
     * @return int
     * @throws ExceptionAlias
     */
    public function getStoreId() {
        if (!is_numeric($this->storeId)) {
            throw new ExceptionAlias('Store is not selected');
        }

        return $this->storeId;
    }

    /**
     * @param $storeId
     * @return $this
     */
    public function setStoreId($storeId) {
        $this->storeId = $storeId;
        return $this;
    }

    /**
     * @throws ExceptionAlias
     * @throws NoSuchEntityException
     * @return integer
     */
    public function getWebsiteId() {
        return $this->storeManager->getStore($this->getStoreId())->getWebsiteId();
    }

    /**
     * @return int
     * @throws ExceptionAlias
     * @throws NoSuchEntityException
     */
    public function getRootCategoryId() {
        $storeGroupId = $this->storeManager->getStore($this->getStoreId())->getStoreGroupId();
        return $this->storeManager->getGroup($storeGroupId)->getRootCategoryId();
    }

        /**
     * @param integer|false $storeId
     * @return string
     */
    private static function getScopeCode($storeId = false) {
        if ($storeId === false) {
            return ScopeConfigInterface::SCOPE_TYPE_DEFAULT;
        }

        return "store_{$storeId}";
    }

    /**
     * @param integer|false $storeId
     * @return bool
     */
    private static function isStoreId($storeId = false) {
        return $storeId !== false;
    }

    /**
     * @return UsageInterface
     */
    public function getStaging() {
        return $this->staging;
    }

    /**
     * @return string
     */
    public function getStagingInfo() {
        $info = $this->getStaging()->isEnabled() ? 'enabled' : 'disabled';
        return 'Staging: ' . $info;
    }

    /**
     * @return bool
     * @throws ExceptionAlias
     */
    public function isEnabled() {
        return $this->isSetFlag(self::WAVES_GENERAL_ENABLED, $this->getStoreId());
    }

    /**
     * @param bool $enabled
     * @return $this
     * @throws ExceptionAlias
     */
    public function setIsEnabled($enabled) {
        return $this->setConfigValue(self::WAVES_GENERAL_ENABLED, $enabled ? '1' : '0', $this->getStoreId());
    }

    /**
     * @return string
     * @throws ExceptionAlias
     */
    public function getApiUser() {
        return (string) $this->getValue(self::WAVES_GENERAL_API_USER, $this->getStoreId());
    }

    /**
     * @param string $apiUser
     * @return $this
     * @throws ExceptionAlias
     */
    public function setApiUser($apiUser) {
        return $this->setConfigValue(self::WAVES_GENERAL_API_USER, (string) $apiUser, $this->getStoreId());
    }

    /**
     * @return string
     * @throws ExceptionAlias
     */
    public function getApiPassword() {
        return (string) $this->getValue(self::WAVES_GENERAL_API_PASSWORD, $this->getStoreId());
    }

    /**
     * @param string $apiPassword
     * @return $this
     * @throws ExceptionAlias
     */
    public function setApiPassword($apiPassword) {
        return $this->setConfigValue(self::WAVES_GENERAL_API_PASSWORD, (string) $apiPassword, $this->getStoreId());
    }

    /**
     * @return string
     * @throws ExceptionAlias
     */
    public function getSiteId() {
        return (string) $this->getValue(self::WAVES_GENERAL_SITE_ID, $this->getStoreId());
    }

    /**
     * @param string $siteId
     * @return $this
     * @throws ExceptionAlias
     */
    public function setSiteId($siteId) {
        return $this->setConfigValue(self::WAVES_GENERAL_SITE_ID, (string) $siteId, $this->getStoreId());
    }

    /**
     * @return bool
     * @throws ExceptionAlias
     */
    public function isTestMode() {
        return $this->isSetFlag(self::WAVES_GENERAL_TEST_MODE, $this->getStoreId());
    }

    /**
     * @param bool $testMode
     * @return $this
     * @throws ExceptionAlias
     */
    public function setIsTestMode($testMode) {
        return $this->setConfigValue(self::WAVES_GENERAL_TEST_MODE, $testMode ? '1' : '0', $this->getStoreId());
    }

    /**
     * @return bool
     * @throws ExceptionAlias
     */
    public function isAjaxAddToCartEnabled() {
        return $this->isSetFlag(self::WAVES_GENERAL_AJAX_ADDTOCART_ENABLE, $this->getStoreId());
    }

    /**
     * @param bool $isAjaxAddToCartEnabled
     * @return $this
     * @throws ExceptionAlias
     */
    public function setIsAjaxAddToCartEnabled($isAjaxAddToCartEnabled) {
        return $this->setConfigValue(self::WAVES_GENERAL_AJAX_ADDTOCART_ENABLE, $isAjaxAddToCartEnabled ? '1' : '0', $this->getStoreId());
    }

    /**
     * @return string
     * @throws ExceptionAlias
     */
    public function getAbideByNewsletterSubscriber() {
        return ((string) $this->getValue(self::WAVES_GENERAL_ABIDE_BY_NEWSLETTER_SUBSCRIBER, $this->getStoreId())) === '1' ? true : false;
    }

    /**
     * @return string
     * @throws ExceptionAlias
     */
    public function getAWSAccessKeyId() {
        return (string) $this->getValue(self::WAVES_AWS_ACCESS_KEY_ID, $this->getStoreId());
    }

    /**
     * @param string $awsAccessKeyId
     * @return $this
     * @throws ExceptionAlias
     */
    public function setAWSAccessKeyId($awsAccessKeyId) {
        return $this->setConfigValue(self::WAVES_AWS_ACCESS_KEY_ID, (string) $awsAccessKeyId, $this->getStoreId());
    }

    /**
     * @return string
     * @throws ExceptionAlias
     */
    public function getAWSSecretAccessKey() {
        return (string) $this->getValue(self::WAVES_AWS_SECRET_ACCESS_KEY, $this->getStoreId());
    }

    /**
     * @param string $awsSecretAccessKey
     * @return $this
     * @throws ExceptionAlias
     */
    public function setAWSSecretAccessKey($awsSecretAccessKey) {
        return $this->setConfigValue(self::WAVES_AWS_SECRET_ACCESS_KEY, (string) $awsSecretAccessKey, $this->getStoreId());
    }

    /**
     * @return string
     * @throws ExceptionAlias
     */
    public function getAWSLogStream() {
        return (string) $this->getValue(self::WAVES_AWS_LOG_STREAM, $this->getStoreId());
    }

    /**
     * @param string $awsLogStream
     * @return $this
     * @throws ExceptionAlias
     */
    public function setAWSLogStream($awsLogStream) {
        return $this->setConfigValue(self::WAVES_AWS_LOG_STREAM, (string) $awsLogStream, $this->getStoreId());
    }

    /**
     * @return string
     * @throws ExceptionAlias
     */
    public function getAWSLogGroup() {
        return (string) $this->getValue(self::WAVES_AWS_LOG_GROUP, $this->getStoreId());
    }

    /**
     * @param string $awsLogGroup
     * @return $this
     * @throws ExceptionAlias
     */
    public function setAWSLogGroup($awsLogGroup) {
        return $this->setConfigValue(self::WAVES_AWS_LOG_GROUP, (string) $awsLogGroup, $this->getStoreId());
    }

    /**
     * @return string
     * @throws ExceptionAlias
     */
    public function getAWSSessionToken() {
        return (string) $this->getValue(self::WAVES_AWS_SESSION_TOKEN, $this->getStoreId());
    }

    /**
     * @param string $awsSessionToken
     * @return $this
     * @throws ExceptionAlias
     */
    public function setAWSSessionToken($awsSessionToken) {
        return $this->setConfigValue(self::WAVES_AWS_SESSION_TOKEN, (string) $awsSessionToken, $this->getStoreId());
    }

    /**
     * @return bool
     */
    public function getIsRunManual() {
        return $this->isSetFlag(self::WAVES_SYNC_RUN_MANUAL);
    }

    /**
     * @param bool $runManual
     * @return $this
     */
    public function setIsRunManual($runManual) {
        return $this->setConfigValue(self::WAVES_SYNC_RUN_MANUAL, $runManual ? '1' : '0');
    }

    /**
     * @return bool
     */
    public function isBulkEnabled() {
        return $this->isSetFlag(self::WAVES_SYNC_BULK_COMPRESSION_ENABLE);
    }

    /**
     * @param bool $isBulkEnabled
     * @return $this
     */
    public function setIsBulkEnabled($isBulkEnabled) {
        return $this->setConfigValue(self::WAVES_SYNC_BULK_COMPRESSION_ENABLE, $isBulkEnabled ? '1' : '0');
    }

    /**
     * @param string $path
     * @param integer|false $storeId
     * @return bool
     */
    private function isSetFlag($path, $storeId = false) {
        $scopeCode = self::getScopeCode($storeId);

        if(isset($this->config[$scopeCode][$path])) {
            return $this->config[$scopeCode][$path];
        }

        if (self::isStoreId($storeId)) {
            return $this->scopeConfig->isSetFlag($path, ScopeInterface::SCOPE_STORES, $storeId);
        }

        return $this->scopeConfig->isSetFlag($path);
    }

    /**
     * @param string $path
     * @param bool $storeId
     * @return mixed
     */
    private function getValue($path, $storeId = false) {
        $scopeCode = self::getScopeCode($storeId);

        if(isset($this->config[$scopeCode][$path])) {
            return $this->config[$scopeCode][$path];
        }

        if (self::isStoreId($storeId)) {
            return $this->scopeConfig->getValue($path, ScopeInterface::SCOPE_STORES, $storeId);
        }

        return $this->scopeConfig->getValue($path);
    }

    /**
     * @param string $path
     * @param string $value
     * @return $this
     */
    private function setConfigValue($path, $value, $storeId = false)
    {
        $scopeCode = self::getScopeCode($storeId);

        $this->config[$scopeCode][$path] = $value;

        if (self::isStoreId($storeId)) {
            $this->writer->save($path, $value, ScopeInterface::SCOPE_STORES, $storeId);
        } else {
            $this->writer->save($path, $value);
        }

        $this->cacheTypeList->invalidate(Config::TYPE_IDENTIFIER);
        $this->cacheTypeList->cleanType(Config::TYPE_IDENTIFIER);

        return $this;
    }

    /**
     * @return Api
     * @throws ExceptionAlias
     */
    public function getApi() {
        if(is_null($this->api[$this->getStoreId()])) {
            $this->api[$this->getStoreId()] = $this->apiFactory->create([
                'username' => $this->getApiUser($this->getStoreId()),
                'password' => $this->getApiPassword($this->getStoreId()),
                'testmode' => $this->isTestMode($this->getStoreId()),
                'abideByNewsletterSubscriber' => $this->getAbideByNewsletterSubscriber($this->getStoreId()),
            ]); 
        }
        return $this->api[$this->getStoreId()];
    }

    /**
     * @return bool
     */
    public function updateAWSCredentials()
    {
        try {
            $aws_credentials = $this->getApi($this->getStoreId())->get_aws_credentials();
            $aws_credentials = Json::decode($aws_credentials, Json::TYPE_ARRAY);
            if(is_array($aws_credentials) && isset($aws_credentials['status']) && $aws_credentials['status'] === 'success') {
                $this->setAWSAccessKeyId($aws_credentials['access_key_id'], $this->getStoreId());
                $this->setAWSSecretAccessKey($aws_credentials['secret_access_key'], $this->getStoreId());
                $this->setAWSLogStream($aws_credentials['log_stream'], $this->getStoreId());
                $this->setAWSLogGroup($aws_credentials['log_group'], $this->getStoreId());
                $this->setAWSSessionToken($aws_credentials['session_token'], $this->getStoreId());
            }
            return TRUE;
        } catch(Exception $e) {
            return FALSE;
        }
    }

    /**
     * @return bool
     */
    public function manageStock() {
        return $this->getValue(Configuration::XML_PATH_MANAGE_STOCK);
    }

    /**
     * @return bool
     */
    public function isBulkCompressionEnabled()
    {
        $enabled = (bool) $this->getValue(self::WAVES_SYNC_BULK_COMPRESSION_ENABLE);
        if($enabled) {
            if(! function_exists('gzopen') OR ! function_exists('gzwrite') OR ! function_exists('gzclose')) {
                $enabled = false;
            }
        }
        return $enabled;
    }

    /**
     * Returns array of store groups
     * @return integer[][]
     */
    public function getGroupedStoreIds() {
        $helper = $this;

        $storesData = array_map(function(StoreInterface $store) use ($helper) {
            $helper->setStoreId($store->getId());
            $data = [
                'id' => $store->getId(),
                'isEnabled' => $helper->isEnabled(),
                'hash' => self::md5("{$helper->getApiUser()}_{$helper->getApiPassword()}_{$helper->isTestMode()}"),
            ];

            return $data;
        }, $this->storeManager->getStores());

        $enabledStoresData = array_filter($storesData, function($data) {
            return $data['isEnabled'];
        });

        $groupedStores = [];
        if (!empty($enabledStoresData)) {
            foreach($enabledStoresData as $data) {
                if (!isset($groupedStores[$data['hash']])) {
                    $groupedStores[$data['hash']] = [];
                }
                $groupedStores[$data['hash']][] = $data['id'];
            }
        }

        return array_values($groupedStores);
    }
}