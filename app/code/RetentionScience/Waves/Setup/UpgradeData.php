<?php
/**
 * Created by PhpStorm.
 * User: arybitskiy
 * Date: 1/9/18
 * Time: 7:05 PM
 */

namespace RetentionScience\Waves\Setup;


use Exception;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Model\Entity\Attribute\Source\Boolean;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Eav\Setup\EavSetup;
use Zend_Validate_Exception;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var ConfigMigration
     */
    private $configMigration;

    /**
     * UpgradeData constructor.
     * @param EavSetupFactory $eavSetupFactory
     * @param ConfigMigration $configMigration
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        ConfigMigration $configMigration
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->configMigration = $configMigration;
    }

    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     * @throws Exception
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if(version_compare($context->getVersion(), '1.0.11') < 0) {
            $this->upgradeToVersion_1_0_11($setup);
        }

        if(version_compare($context->getVersion(), '1.0.13') < 0) {
            $this->upgradeToVersion_1_0_13();
        }

        if(version_compare($context->getVersion(), '1.0.15') < 0) {
            $this->upgradeToVersion_1_0_15($setup);
        }

        $setup->endSetup();
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @return void
     * @throws LocalizedException
     * @throws Zend_Validate_Exception
     */
    private function upgradeToVersion_1_0_11(ModuleDataSetupInterface $setup)
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(
            Product::ENTITY,
            'resci_recommend',[
                'group' => 'General',
                'type' => 'int',
                'backend' => '',
                'frontend' => '',
                'label' => 'ReSci Recommend',
                'input' => 'boolean',
                'class' => '',
                'source' => Boolean::class,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '1',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'apply_to' => 'simple,configurable,virtual,bundle,downloadable'
            ]
        );
    }

    /**
     * @return void
     * @throws Exception
     */
    private function upgradeToVersion_1_0_13()
    {
        $this->configMigration->migrate();
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @return void
     * @throws LocalizedException
     * @throws Zend_Validate_Exception
     */
    private function upgradeToVersion_1_0_15(ModuleDataSetupInterface $setup)
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(
            Product::ENTITY,
            'resci_display_name',[
                'group' => 'General',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'ReSci Display Name',
                'input' => 'text',
                'class' => '',
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'apply_to' => 'simple,configurable,virtual,bundle,downloadable'
            ]
        );
    }
}