<?php


namespace RetentionScience\Waves\Setup;

use Magento\Config\Model\ResourceModel\Config\Data\CollectionFactory as ConfigCollectionFactory;
use Magento\Config\Model\ResourceModel\Config\Data\Collection as ConfigCollection;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Value;
use Magento\Store\Model\ScopeInterface;
use RetentionScience\Waves\Helper\Data as Helper;

class ConfigMigration
{
    const DEFAULT_SCOPE_ID = 0;

    /**
     * @var ConfigCollectionFactory
     */
    private $configCollectionFactory;

    /**
     * ConfigMigration constructor.
     * @param ConfigCollectionFactory $configCollectionFactory
     */
    public function __construct(
        ConfigCollectionFactory $configCollectionFactory
    )
    {
        $this->configCollectionFactory = $configCollectionFactory;
    }

    /**
     * @throws \Exception
     */
    public function migrate() {
        $oldConfig = $this->getOldConfig();
        $storeId = $this->getStoreIdFromOldConfig($oldConfig);
        if ($storeId !== false) {
            $this->moveOldConfigToNewConfig($storeId, $oldConfig);
        }
    }

    /**
     * @return Value[]
     */
    private function getOldConfig() {
        /** @var ConfigCollection $configCollection */
        $configCollection = $this->configCollectionFactory->create();
        $configCollection->addScopeFilter(ScopeConfigInterface::SCOPE_TYPE_DEFAULT, self::DEFAULT_SCOPE_ID, Helper::WAVES_CONFIG_START);
        $config = [];
        foreach ($configCollection as $configData) {
            /** @var Value $configData */
            $config[] = $configData;
        }
        return $config;
    }

    /**
     * @param Value[] $config
     * @return integer|false
     */
    private function getStoreIdFromOldConfig(array $config) {
        if (!empty($config)) {
            foreach ($config as $configData) {
                if ($configData->getPath() === Helper::WAVES_GENERAL_STORE_ID) {
                    return (int) $configData->getValue();
                }
            }
        }

        return false;
    }

    /**
     * @param integer $targetStoreId
     * @param Value[] $config
     * @throws \Exception
     */
    private function moveOldConfigToNewConfig($targetStoreId, array $config) {
        if (!empty($config)) {
            foreach ($config as $configData) {
                if ($configData->getPath() === Helper::WAVES_GENERAL_STORE_ID) {
                    $configData->delete();
                } else {
                    $configData->setScope(ScopeInterface::SCOPE_STORES);
                    $configData->setScopeId($targetStoreId);
                    $configData->save();
                }
            }
        }
    }
}