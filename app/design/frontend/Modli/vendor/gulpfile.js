let gulp = require('gulp');

let sass = require('gulp-sass');
let sourcemaps = require('gulp-sourcemaps');
let notify = require('gulp-notify');
let plumber = require('gulp-plumber');
let beep = require('beepbeep');
let livereload = require('gulp-livereload');
const cleanCSS = require('gulp-clean-css');

let onError = function(err) {
    notify.onError({
        title: "Error in " + err.plugin,
        message: "\n\nFilename: " + err.fileName + "\nMessage: " + err.message + "\n\nLine number: " + err.line + " (col: " + err.column + ")\n" + "\nCode: " + err.extract + "\n",
    })(err);

    beep(2);
    this.emit('end');

};

let compileStyles = (input, output) => {
    return gulp.src(input)
    .pipe(plumber({
        errorHandler: onError
    }))
    .pipe(sourcemaps.init({
        loadMaps: true,
        largeFile: true
    }))
    .pipe(sass())
    //.pipe(cleanCSS())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(output))
    .pipe(livereload())
};

gulp.task(
    'styles',
    compileStyles.bind(undefined, './Unirgy_Dropship/web/scss/udropship.scss', './Unirgy_Dropship/web/css/')
);

gulp.task(
    'watch',function() {
        livereload.listen();
        gulp.watch('./Unirgy_Dropship/web/scss/**/*.scss', compileStyles.bind(undefined, './Unirgy_Dropship/web/scss/udropship.scss', './Unirgy_Dropship/web/css/'));
    }
);
