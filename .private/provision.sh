#!/usr/bin/env bash
# vi: ft=sh expandtab smarttab tabstop=4 shiftwidth=4 softtabstop=4
#===============================================================================
#          FILE:  provision.sh
#
#         USAGE:  ./provision.sh
#
#   DESCRIPTION:  This script is going to set up a vagrant based 
#                 development environment on CentOS 7 or Ubuntu 16.04
#        AUTHOR:  mixe3y (Janos Miko), janos.miko@itegration.com
#       COMPANY:  ITegration
#       VERSION:  0.1
#===============================================================================
#===============================================================================
# Global Variables
#===============================================================================
# debug level
# 0: off
# 1: application debug
# 2: application debug + bash debug
DEBUGLEVEL=0

PROVISION_DIR="/usr/local/env-setup"
PLAYBOOK_DIR="$PROVISION_DIR/ansible"

LOGFILE="/var/log/provision.log"
LOCKFILE="/var/run/`basename $0`.lock"


#===============================================================================
# Bash environment
#===============================================================================
# Exit on Error
set -o errexit
# If any part of a pipe fails the pipe returns error
set -o pipefail
# No unbound variables
#set -o nounset
# Debug mode
if [ ${DEBUGLEVEL} -ge 2 ]; then
    set -o xtrace
fi

#===============================================================================
# Command Line Args
#===============================================================================
if [ $# -ne 0 ]; then
    ARGS="$@"
    if [[ $ARGS = *"--force"* ]]; then
        ARGS=$(sed -e 's/--force//g' <<< ${ARGS})
        FORCE=1
    else
        FORCE=0
    fi
    ARGS=$(sed -e 's/\ /,/g' <<< ${ARGS})
fi

#===============================================================================
# Functions
#===============================================================================
# Locking
LOCKFD=99
# Private
# Call flock on lockfd with params
# u: unlock, xn: exclusive nonblocking, s: shared, x: exclusive
function _lock() {
    flock -$1 $LOCKFD; 
}
# unlock and remove lockfile
function _no_more_locking()  {
    _lock u
    _lock xn && rm -f $LOCKFILE; 
}
# start the trap
function _prepare_locking()  {
    eval "exec $LOCKFD>\"$LOCKFILE\""
    trap _no_more_locking EXIT
}
# Public
# Obtain an exclusive lock or fail
function exlock_now() {
    _lock xn
}

# Obtain an exclusive lock
function exlock() {
    _lock x
}

# Obtain a shared lock
function shlock() { 
    _lock s
}

# Unlock
function unlock() { 
    _lock u
}


# Logging
function _prepare_logging {
    exec > >(tee -i ${LOGFILE})
    exec 2>&1
}
# This function logs the arguments it get
function log {
    echo "[$(date --rfc-3339=seconds)]: $*"
}
# Log and instant exit
function log_and_error {
    echo "[$(date --rfc-3339=seconds)]: ERROR: $*"
    exit 1
}

# Determine the OS Family and Version
function determine_os {
if [ -f /etc/os-release ]; then
    # freedesktop.org and systemd
    . /etc/os-release
    OS=$NAME
    VER=$VERSION_ID
elif type lsb_release >/dev/null 2>&1; then
    # linuxbase.org
    OS=$(lsb_release -si)
    VER=$(lsb_release -sr)
elif [ -f /etc/lsb-release ]; then
    # For some versions of Debian/Ubuntu without lsb_release command
    . /etc/lsb-release
    OS=$DISTRIB_ID
    VER=$DISTRIB_RELEASE
else
    # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
    OS=$(uname -s)
    VER=$(uname -r)
fi
}

#===============================================================================
### MAIN
#===============================================================================
# Configure Logging
_prepare_logging
# Configure Lock
_prepare_locking

exlock_now || log_and_error "Cannot Obtain Lock"
log "PROVISION START"

# If the provision directory exists, the system was already provisioned
# so the base packages should not be installed again.
if [ -d ${PROVISION_DIR} ]; then
    cd ${PROVISION_DIR}
    git fetch
    UPSTREAM='@{u}'
    LOCAL=$(git rev-parse @{0})
    REMOTE=$(git rev-parse "$UPSTREAM")
    BASE=$(git merge-base @{0} "$UPSTREAM")
    if [ $LOCAL = $REMOTE ]; then
        if [[ $FORCE == "1" ]]; then
            cd ${PLAYBOOK_DIR}
            if [ ! -z "${ARGS}" ]; then
                ansible-playbook -l $(hostname | cut -d"." -f1)-localdev localdev.yml -t ${ARGS}
            else
                ansible-playbook -l $(hostname | cut -d"." -f1)-localdev localdev.yml
            fi
        elif [ ! -z "${ARGS}" ]; then
            cd ${PLAYBOOK_DIR}
            ansible-playbook -l $(hostname | cut -d"." -f1)-localdev localdev.yml -t ${ARGS}
        else
            log "No changes on the remote, We don't run the provision"
        fi
    elif [ $LOCAL = $BASE ]; then
        log "Pull and rerun the provision"
        git pull
        git submodule update --recursive --remote
        cd ${PLAYBOOK_DIR}
        if [ ! -z "${ARGS}" ]; then
            ansible-playbook -l $(hostname | cut -d"." -f1)-localdev localdev.yml -t ${ARGS}
        else
            ansible-playbook -l $(hostname | cut -d"." -f1)-localdev localdev.yml
        fi
    fi
else
    # Call the determine_os function and check if the OS Family and Version is supported
    determine_os

    if [[ ${OS} =~ ^CentOS ]] && [[ ${VER} == 7 ]]; then
        # disable deltarpm
        grep -q '^deltarpm' /etc/yum.conf && sed -i 's/^deltarpm.*/deltarpm=0/' /etc/yum.conf || echo 'deltarpm=0' >> /etc/yum.conf

        # Install the most important packages for set up the ansible privisioning
        if [ ${DEBUGLEVEL} -ge 1 ]; then
            yum update --assumeyes --verbose --debuglevel 6
            yum install --assumeyes --verbose --debuglevel 6 epel-release
            yum install --assumeyes --verbose --debuglevel 6 ansible cowsay git curl
        else
            yum update --assumeyes --quiet --errorlevel 0 --debuglevel 0
            yum install --assumeyes --quiet --errorlevel 0 --debuglevel 0 epel-release
            yum install --assumeyes --quiet --errorlevel 0 --debuglevel 0 ansible cowsay git curl
        fi
        ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts
        git clone --recursive git@bitbucket.org:team-itegration/magento-environments.git \
        ${PROVISION_DIR}
        # Set an "OS_SUPPORTED"
        OS_SUPPORTED=1
    elif [[ ${OS} =~ ^Ubuntu ]] && [[ ${VER} == "16.04" ]]; then
        # Install the most important packages for set up the ansible privisioning
        if [ ${DEBUGLEVEL} -ge 1 ]; then
            apt-add-repository -m ppa:ansible/ansible
            apt-get update -y && apt-get upgrade -y 
            apt-get install -y git ansible cowsay curl
        else
            apt-add-repository ppa:ansible/ansible
            apt-get --quiet=2 update && apt-get --quiet=2 upgrade
            apt-get --quiet=2 install git ansible cowsay curl
        fi
        ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts
        git clone --recurse-submodules -j8 git@bitbucket.org:team-itegration/magento-environments.git \
        ${PROVISION_DIR}
        # Set an "OS_SUPPORTED"
        OS_SUPPORTED=1
    fi

    if [ $OS_SUPPORTED == 1 ]; then
        # run playbook
        cd ${PLAYBOOK_DIR}
        ansible-playbook -l $(hostname | cut -d"." -f1)-localdev localdev.yml
    else
        echo -e "Your Operating System is not supported!"
    fi
fi

# UNLOCK
unlock