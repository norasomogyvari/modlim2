#!/bin/bash

CONFIG=".private/config"

if [ -f "${CONFIG}" ]; then
  source "${CONFIG}"
else
  echo "Configuration File missing. Exiting."
  exit 1
fi

source "${CONFIG}"

if ! grep -iq Darwin <(uname -a); then
  if [[ "$(sysctl -n fs.inotify.max_user_watches)" -lt 524288 ]]; then
  
    if ! grep -q '^fs.inotify.max_user_watches' /etc/sysctl.conf /etc/sysctl.d/inotify_userwatches.conf ; then
      echo "fs.inotify.max_user_watches = 524288" | sudo tee /etc/sysctl.d/inotify_userwatches.conf > /dev/null 
    fi
    
    sudo sysctl --system
  fi
fi

rm -fr ~/.unison/

UNISON_PORT=${UNISON_PORT}
VAGRANT_MACHINE=${PROJECT_IP}

LOCAL_ROOT=./
REMOTE_ROOT="socket://$VAGRANT_MACHINE:$UNISON_PORT///var/www/magento/www/"

declare -a IGNORE

IGNORE+=( -ignore 'Path var/cache' )
IGNORE+=( -ignore 'Path var/composer_home' )
IGNORE+=( -ignore 'Path var/page_cache' )
IGNORE+=( -ignore 'Path var/session' )
IGNORE+=( -ignore 'Path var/tmp' )
IGNORE+=( -ignore 'Path var/.setup_cronjob_status' )
IGNORE+=( -ignore 'Path var/.update_cronjob_status' )
IGNORE+=( -ignore 'Path dev/' )
#IGNORE+=( -ignore 'Path media' )
#IGNORE+=( -ignore 'Path pub/media' )
IGNORE+=( -ignore 'Path .gitattributes' )
IGNORE+=( -ignore 'Path .magento' )
IGNORE+=( -ignore 'Path .vagrant/' )
IGNORE+=( -ignore 'Path .private/' )
IGNORE+=( -ignore 'Name {.*.swp}' )
IGNORE+=( -ignore 'Name {.unison.*}' )

declare -a UNISONARGS
UNISONARGS=($LOCAL_ROOT $REMOTE_ROOT -prefer $LOCAL_ROOT -auto -batch)

if [ ! -d  $LOCAL_ROOT/vendor ]; then
  unison ${UNISONARGS[@]} "${IGNORE[@]}"
fi

while true; do
  unison ${UNISONARGS[@]} -repeat watch -fastcheck true "${IGNORE[@]}"
  sleep 5
done
