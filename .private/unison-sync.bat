@ECHO OFF

for /f "delims=" %%x in (.private/config) do (SET "%%x")

rmdir /S /Q "%USERPROFILE%\.unison\"
del /F /S /Q "%USERPROFILE%\unison"

SET UNISON_PORT=%UNISON_PORT%
SET VAGRANT_MACHINE=%PROJECT_IP%

@SET LOCAL_ROOT=./
@SET REMOTE_ROOT=socket://%VAGRANT_MACHINE%:%UNISON_PORT%///var/www/magento/www/

@SET IGNORE=

rem Magento files not worth pulling locally.
@SET IGNORE=%IGNORE% -ignore "Path var/cache"
@SET IGNORE=%IGNORE% -ignore "Path var/composer_home"
@SET IGNORE=%IGNORE% -ignore "Path var/page_cache"
@SET IGNORE=%IGNORE% -ignore "Path var/session"
@SET IGNORE=%IGNORE% -ignore "Path var/tmp"
@SET IGNORE=%IGNORE% -ignore "Path var/.setup_cronjob_status"
@SET IGNORE=%IGNORE% -ignore "Path var/.update_cronjob_status"
@SET IGNORE=%IGNORE% -ignore "Path dev/"
rem @SET IGNORE=%IGNORE% -ignore "Path media"
rem @SET IGNORE=%IGNORE% -ignore "Path pub/media"
@SET IGNORE=%IGNORE% -ignore "Path .gitattributes"
@SET IGNORE=%IGNORE% -ignore "Path .magento"
@SET IGNORE=%IGNORE% -ignore "Path .vagrant/"
@SET IGNORE=%IGNORE% -ignore "Path .private/"
@SET IGNORE=%IGNORE% -ignore "Name {.*.swp}"
@SET IGNORE=%IGNORE% -ignore "Name {.unison.*}"

@set UNISONARGS=%LOCAL_ROOT% %REMOTE_ROOT% -prefer %LOCAL_ROOT% -preferpartial "Path var -> %REMOTE_ROOT%" -auto -batch %IGNORE%

IF NOT EXIST  %LOCAL_ROOT%/vendor (
   unison.exe %UNISONARGS%
)

:loop_sync
    unison.exe %UNISONARGS% -repeat watch -fastcheck true
    timeout 5
    @GOTO loop_sync
PAUSE
