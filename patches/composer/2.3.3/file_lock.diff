diff --git a/App/ObjectManagerFactory.php b/App/ObjectManagerFactory.php
index 1c05c5847360..b781a92b4714 100644
--- a/App/ObjectManagerFactory.php
+++ b/App/ObjectManagerFactory.php
@@ -106,7 +106,10 @@ public function __construct(DirectoryList $directoryList, DriverPool $driverPool
     public function create(array $arguments)
     {
         $writeFactory = new \Magento\Framework\Filesystem\Directory\WriteFactory($this->driverPool);
-        $generatedFiles = new GeneratedFiles($this->directoryList, $writeFactory);
+        /** @var \Magento\Framework\Filesystem\Driver\File $fileDriver */
+        $fileDriver = $this->driverPool->getDriver(DriverPool::FILE);
+        $lockManager = new \Magento\Framework\Lock\Backend\FileLock($fileDriver, BP);
+        $generatedFiles = new GeneratedFiles($this->directoryList, $writeFactory, $lockManager);
         $generatedFiles->cleanGeneratedFiles();
 
         $deploymentConfig = $this->createDeploymentConfig($this->directoryList, $this->configFilePool, $arguments);
diff --git a/Code/GeneratedFiles.php b/Code/GeneratedFiles.php
index bc44b361c57e..28301f7a1fb8 100644
--- a/Code/GeneratedFiles.php
+++ b/Code/GeneratedFiles.php
@@ -3,24 +3,36 @@
  * Copyright © Magento, Inc. All rights reserved.
  * See COPYING.txt for license details.
  */
+
 namespace Magento\Framework\Code;
 
-use Magento\Framework\App\DeploymentConfig\Writer\PhpFormatter;
 use Magento\Framework\App\Filesystem\DirectoryList;
-use Magento\Framework\Config\File\ConfigFilePool;
+use Magento\Framework\Exception\FileSystemException;
+use Magento\Framework\Exception\RuntimeException;
 use Magento\Framework\Filesystem\Directory\WriteFactory;
 use Magento\Framework\Filesystem\Directory\WriteInterface;
+use Magento\Framework\Lock\LockManagerInterface;
 
 /**
- * Regenerates generated code and DI configuration
+ * Clean generated code, DI configuration and cache folders
  */
 class GeneratedFiles
 {
     /**
-     * Separator literal to assemble timer identifier from timer names
+     * Regenerate flag file name
      */
     const REGENERATE_FLAG = '/var/.regenerate';
 
+    /**
+     * Regenerate lock file name
+     */
+    const REGENERATE_LOCK = self::REGENERATE_FLAG . '.lock';
+
+    /**
+     * Acquire regenerate lock timeout
+     */
+    const REGENERATE_LOCK_TIMEOUT = 5;
+
     /**
      * @var DirectoryList
      */
@@ -32,19 +44,39 @@ class GeneratedFiles
     private $write;
 
     /**
-     * Constructor
+     * @var LockManagerInterface
+     */
+    private $lockManager;
+
+    /**
+     * GeneratedFiles constructor.
      *
      * @param DirectoryList $directoryList
      * @param WriteFactory $writeFactory
+     * @param LockManagerInterface $lockManager
      */
-    public function __construct(DirectoryList $directoryList, WriteFactory $writeFactory)
-    {
+    public function __construct(
+        DirectoryList $directoryList,
+        WriteFactory $writeFactory,
+        LockManagerInterface $lockManager
+    ) {
         $this->directoryList = $directoryList;
         $this->write = $writeFactory->create(BP);
+        $this->lockManager = $lockManager;
     }
 
     /**
-     * Clean generated code and DI configuration
+     * Create flag for cleaning up generated content
+     *
+     * @return void
+     */
+    public function requestRegeneration()
+    {
+        $this->write->touch(self::REGENERATE_FLAG);
+    }
+
+    /**
+     * Clean generated code, generated metadata and cache directories
      *
      * @return void
      *
@@ -57,156 +89,75 @@ public function regenerate()
     }
 
     /**
-     * Clean generated/code, generated/metadata and var/cache
+     * Clean generated code, generated metadata and cache directories
      *
      * @return void
      */
     public function cleanGeneratedFiles()
     {
-        if ($this->write->isExist(self::REGENERATE_FLAG)) {
-            $enabledCacheTypes = [];
-
-            //TODO: to be removed in scope of MAGETWO-53476
-            $deploymentConfig = $this->directoryList->getPath(DirectoryList::CONFIG);
-            $configPool = new ConfigFilePool();
-            $envPath = $deploymentConfig . '/' . $configPool->getPath(ConfigFilePool::APP_ENV);
-            if ($this->write->isExist($this->write->getRelativePath($envPath))) {
-                $enabledCacheTypes = $this->getEnabledCacheTypes();
-                $this->disableAllCacheTypes();
-            }
-            //TODO: Till here
-
-            $cachePath = $this->write->getRelativePath($this->directoryList->getPath(DirectoryList::CACHE));
-            $generationPath = $this->write->getRelativePath(
-                $this->directoryList->getPath(DirectoryList::GENERATED_CODE)
-            );
-            $diPath = $this->write->getRelativePath($this->directoryList->getPath(DirectoryList::GENERATED_METADATA));
-
-            // Clean generated/code dir
-            if ($this->write->isDirectory($generationPath)) {
-                $this->write->delete($generationPath);
-            }
-
-            // Clean generated/metadata
-            if ($this->write->isDirectory($diPath)) {
-                $this->write->delete($diPath);
+        if ($this->isCleanGeneratedFilesAllowed() && $this->acquireLock()) {
+            try {
+                $this->write->delete(self::REGENERATE_FLAG);
+                $this->deleteFolder(DirectoryList::GENERATED_CODE);
+                $this->deleteFolder(DirectoryList::GENERATED_METADATA);
+                $this->deleteFolder(DirectoryList::CACHE);
+            } catch (FileSystemException $exception) {
+                // A filesystem error occurred, possible concurrency error while trying
+                // to delete a generated folder being used by another process.
+                // Request regeneration for the next and unlock
+                $this->requestRegeneration();
+            } finally {
+                $this->lockManager->unlock(self::REGENERATE_LOCK);
             }
-
-            // Clean var/cache
-            if ($this->write->isDirectory($cachePath)) {
-                $this->write->delete($cachePath);
-            }
-            $this->write->delete(self::REGENERATE_FLAG);
-            $this->enableCacheTypes($enabledCacheTypes);
         }
     }
 
     /**
-     * Create flag for cleaning up generated/code, generated/metadata and var/cache directories for subsequent
-     * regeneration of this content
+     * Clean generated files is allowed if requested and not locked
      *
-     * @return void
+     * @return bool
      */
-    public function requestRegeneration()
+    private function isCleanGeneratedFilesAllowed(): bool
     {
-        $this->write->touch(self::REGENERATE_FLAG);
-    }
-
-    /**
-     * Reads Cache configuration from env.php and returns indexed array containing all the enabled cache types.
-     *
-     * @return string[]
-     */
-    private function getEnabledCacheTypes()
-    {
-        $enabledCacheTypes = [];
-        $envPath = $this->getEnvPath();
-        if ($this->write->isReadable($this->write->getRelativePath($envPath))) {
-            $envData = include $envPath;
-            if (isset($envData['cache_types'])) {
-                $cacheStatus = $envData['cache_types'];
-                $enabledCacheTypes = array_filter($cacheStatus, function ($value) {
-                    return $value;
-                });
-                $enabledCacheTypes = array_keys($enabledCacheTypes);
-            }
+        try {
+            $isAllowed = $this->write->isExist(self::REGENERATE_FLAG)
+                && !$this->lockManager->isLocked(self::REGENERATE_LOCK);
+        } catch (FileSystemException | RuntimeException $e) {
+            // Possible filesystem problem
+            $isAllowed = false;
         }
-        return $enabledCacheTypes;
-    }
 
-    /**
-     * Returns path to env.php file
-     *
-     * @return string
-     * @throws \Exception
-     */
-    private function getEnvPath()
-    {
-        $deploymentConfig = $this->directoryList->getPath(DirectoryList::CONFIG);
-        $configPool = new ConfigFilePool();
-        $envPath = $deploymentConfig . '/' . $configPool->getPath(ConfigFilePool::APP_ENV);
-        return $envPath;
+        return $isAllowed;
     }
 
     /**
-     * Disables all cache types by updating env.php.
+     * Acquire lock for performing operations
      *
-     * @return void
+     * @return bool
      */
-    private function disableAllCacheTypes()
+    private function acquireLock(): bool
     {
-        $envPath = $this->getEnvPath();
-        if ($this->write->isWritable($this->write->getRelativePath($envPath))) {
-            $envData = include $envPath;
-
-            if (isset($envData['cache_types'])) {
-                $cacheTypes = array_keys($envData['cache_types']);
-
-                foreach ($cacheTypes as $cacheType) {
-                    $envData['cache_types'][$cacheType] = 0;
-                }
-
-                $formatter = new PhpFormatter();
-                $contents = $formatter->format($envData);
-
-                $this->write->writeFile($this->write->getRelativePath($envPath), $contents);
-                if (function_exists('opcache_invalidate')) {
-                    opcache_invalidate(
-                        $this->write->getAbsolutePath($envPath)
-                    );
-                }
-            }
+        try {
+            $lockAcquired = $this->lockManager->lock(self::REGENERATE_LOCK, self::REGENERATE_LOCK_TIMEOUT);
+        } catch (RuntimeException $exception) {
+            // Lock not acquired due to possible filesystem problem
+            $lockAcquired = false;
         }
+
+        return $lockAcquired;
     }
 
     /**
-     * Enables appropriate cache types in app/etc/env.php based on the passed in $cacheTypes array
-     * TODO: to be removed in scope of MAGETWO-53476
+     * Delete folder by path
      *
-     * @param string[] $cacheTypes
+     * @param string $pathType
      * @return void
      */
-    private function enableCacheTypes($cacheTypes)
+    private function deleteFolder(string $pathType): void
     {
-        if (empty($cacheTypes)) {
-            return;
-        }
-        $envPath = $this->getEnvPath();
-        if ($this->write->isReadable($this->write->getRelativePath($envPath))) {
-            $envData = include $envPath;
-            foreach ($cacheTypes as $cacheType) {
-                if (isset($envData['cache_types'][$cacheType])) {
-                    $envData['cache_types'][$cacheType] = 1;
-                }
-            }
-
-            $formatter = new PhpFormatter();
-            $contents = $formatter->format($envData);
-
-            $this->write->writeFile($this->write->getRelativePath($envPath), $contents);
-            if (function_exists('opcache_invalidate')) {
-                opcache_invalidate($this->write->getAbsolutePath($envPath));
-            }
+        $relativePath = $this->write->getRelativePath($this->directoryList->getPath($pathType));
+        if ($this->write->isDirectory($relativePath)) {
+            $this->write->delete($relativePath);
         }
     }
 }
diff --git a/Code/Test/Unit/GeneratedFilesTest.php b/Code/Test/Unit/GeneratedFilesTest.php
index 31462fad1457..9b901005622d 100644
--- a/Code/Test/Unit/GeneratedFilesTest.php
+++ b/Code/Test/Unit/GeneratedFilesTest.php
@@ -8,110 +8,390 @@

 use Magento\Framework\App\Filesystem\DirectoryList;
 use Magento\Framework\Code\GeneratedFiles;
+use Magento\Framework\Exception\FileSystemException;
+use Magento\Framework\Exception\RuntimeException;
+use Magento\Framework\Filesystem\Directory\WriteFactory;
+use Magento\Framework\Filesystem\Directory\WriteInterface;
+use Magento\Framework\Lock\Backend\FileLock;

+/**
+ * Class GeneratedFilesTest
+ */
 class GeneratedFilesTest extends \PHPUnit\Framework\TestCase
 {
     /**
-     * @var \Magento\Framework\App\Filesystem\DirectoryList | \PHPUnit_Framework_MockObject_MockObject
+     * @var DirectoryList|\PHPUnit_Framework_MockObject_MockObject
      */
     private $directoryList;

     /**
-     * @var \Magento\Framework\Filesystem\Directory\WriteInterface | \PHPUnit_Framework_MockObject_MockObject
+     * @var WriteInterface|\PHPUnit_Framework_MockObject_MockObject
      */
     private $writeInterface;

+    /**
+     * @var WriteFactory|\PHPUnit_Framework_MockObject_MockObject
+     */
+    private $writeFactory;
+
+    /**
+     * @var FileLock|\PHPUnit_Framework_MockObject_MockObject
+     */
+    private $lockManager;
+
     /**
      * @var \Magento\Framework\Code\GeneratedFiles
      */
     private $model;

+    /**
+     * @var string
+     */
+    private $pathGeneratedCode = '/var/www/magento/generated/code';
+
+    /**
+     * @var string
+     */
+    private $pathGeneratedMetadata = '/var/www/magento/generated/metadata';
+
+    /**
+     * @var string
+     */
+    private $pathVarCache = '/var/www/magento/generated/var/cache';
+
+    /**
+     * Setup mocks for tests
+     *
+     * @return void
+     */
     protected function setUp()
     {
-        $this->directoryList =
-            $this->createPartialMock(\Magento\Framework\App\Filesystem\DirectoryList::class, ['getPath']);
-        $writeFactory = $this->createMock(\Magento\Framework\Filesystem\Directory\WriteFactory::class);
-        $this->writeInterface = $this->getMockBuilder(\Magento\Framework\Filesystem\Directory\WriteInterface::class)
-            ->setMethods(['getPath', 'delete'])
-            ->getMockForAbstractClass();
-        $writeFactory->expects($this->once())->method('create')->willReturn($this->writeInterface);
-        $this->model = new GeneratedFiles($this->directoryList, $writeFactory);
+        $this->directoryList = $this->createMock(DirectoryList::class);
+        $this->writeFactory = $this->createMock(WriteFactory::class);
+        $this->lockManager = $this->createMock(FileLock::class);
+        $this->writeInterface = $this->getMockForAbstractClass(WriteInterface::class);
+
+        $this->directoryList->expects($this->any())->method('getPath')->willReturnMap(
+            [
+                [DirectoryList::GENERATED_CODE, $this->pathGeneratedCode],
+                [DirectoryList::GENERATED_METADATA, $this->pathGeneratedMetadata],
+                [DirectoryList::CACHE, $this->pathVarCache],
+            ]
+        );
+        $this->writeInterface->expects($this->any())->method('getRelativePath')->willReturnMap(
+            [
+                [$this->pathGeneratedCode, $this->pathGeneratedCode],
+                [$this->pathGeneratedMetadata, $this->pathGeneratedMetadata],
+                [$this->pathVarCache, $this->pathVarCache],
+            ]
+        );
+        $this->writeInterface->expects($this->any())->method('isDirectory')->willReturnMap(
+            [
+                [$this->pathGeneratedCode, true],
+                [$this->pathGeneratedMetadata, true],
+                [$this->pathVarCache, true],
+            ]
+        );
+
+        $this->writeFactory->expects($this->once())->method('create')->willReturn($this->writeInterface);
+
+        $this->model = new GeneratedFiles(
+            $this->directoryList,
+            $this->writeFactory,
+            $this->lockManager
+        );
     }

     /**
-     * @param array $getPathMap
-     * @param array $isDirectoryMap
-     * @param array $deleteMap
-     * @dataProvider cleanGeneratedFilesDataProvider
+     * Expect regeneration requested
+     *
+     * @param int $times
+     * @return void
      */
-    public function testCleanGeneratedFiles($getPathMap, $isDirectoryMap, $deleteMap)
+    private function expectRegenerationRequested(int $times): void
     {
+        $this->writeInterface->expects($this->exactly($times))->method('touch')->with(GeneratedFiles::REGENERATE_FLAG);
+    }

-        $this->writeInterface
-            ->expects($this->any())
+    /**
+     * Expect delete not requested
+     *
+     * @return void
+     */
+    private function expectDeleteNotRequested(): void
+    {
+        $this->writeInterface->expects($this->never())->method('delete');
+    }
+
+    /**
+     * Expect flag present
+     *
+     * @param int $times
+     * @param bool $flagPresent
+     * @return void
+     */
+    private function expectFlagPresent(int $times, bool $flagPresent): void
+    {
+        $this->writeInterface->expects($this->exactly($times))
             ->method('isExist')
-            ->with()
-            ->willReturnMap([
-                [GeneratedFiles::REGENERATE_FLAG, true],
-                ['path/to/di', false]
-            ]);
-        $this->directoryList->expects($this->any())->method('getPath')->willReturnMap($getPathMap);
-        $this->writeInterface->expects($this->any())->method('getRelativePath')->willReturnMap($getPathMap);
-        $this->writeInterface->expects($this->any())->method('isDirectory')->willReturnMap($isDirectoryMap);
-        $this->writeInterface->expects($this->exactly(1))->method('delete')->willReturnMap($deleteMap);
+            ->with(GeneratedFiles::REGENERATE_FLAG)
+            ->willReturn($flagPresent);
+    }
+
+    /**
+     * Expect process locked
+     *
+     * @param int $times
+     * @param bool|null $processLocked
+     * @return void
+     */
+    private function expectProcessLocked(int $times, bool $processLocked = null): void
+    {
+        $this->lockManager->expects($this->exactly($times))
+            ->method('isLocked')
+            ->with(GeneratedFiles::REGENERATE_LOCK)
+            ->willReturn($processLocked);
+
+        if ($processLocked) {
+            $this->expectLockOperation(0);
+            $this->expectUnlockOperation(0);
+        }
+    }
+
+    /**
+     * Expect lock operation
+     *
+     * @param int $times
+     * @param bool|null $lockResult
+     * @return void
+     */
+    private function expectLockOperation(int $times, bool $lockResult = null): void
+    {
+        $invocationMocker = $this->lockManager->expects($this->exactly($times))
+            ->method('lock')
+            ->with(GeneratedFiles::REGENERATE_LOCK, GeneratedFiles::REGENERATE_LOCK_TIMEOUT);
+
+        if (null !== $lockResult) {
+            $invocationMocker->willReturn($lockResult);
+        }
+    }
+
+    /**
+     * Expect unlock operation
+     *
+     * @param int $times
+     * @param bool|null $unlockResult
+     * @return void
+     */
+    private function expectUnlockOperation(int $times, bool $unlockResult = null): void
+    {
+        $invocationMocker = $this->lockManager->expects($this->exactly($times))
+            ->method('unlock')
+            ->with(GeneratedFiles::REGENERATE_LOCK);
+
+        if (null !== $unlockResult) {
+            $invocationMocker->willReturn($unlockResult);
+        }
+    }
+
+    /**
+     * Expect no action performed, it does not execute any statement inside if condition
+     *
+     * @return void
+     */
+    private function expectNoActionPerformed(): void
+    {
+        $this->expectDeleteNotRequested();
+        $this->expectRegenerationRequested(0);
+        $this->expectUnlockOperation(0);
+    }
+
+    /**
+     * Test request regeneration
+     *
+     * @test
+     * @return void
+     */
+    public function itRequestsRegenerationProperly()
+    {
+        $this->expectRegenerationRequested(1);
+        $this->model->requestRegeneration();
+    }
+
+    /**
+     * It does not clean generated files if no flag is present
+     *
+     * @test
+     * @return void
+     */
+    public function itDoesNotCleanGeneratedFilesIfNoFlagIsPresent()
+    {
+        $this->expectFlagPresent(1, false);
+        $this->expectProcessLocked(0);
+        $this->expectNoActionPerformed();
         $this->model->cleanGeneratedFiles();
     }

     /**
-     * @return array
+     * It does not clean generated files if process is locked
+     *
+     * @test
+     * @return void
      */
-    public function cleanGeneratedFilesDataProvider()
+    public function itDoesNotCleanGeneratedFilesIfProcessIsLocked()
     {
-        $pathToGeneration = 'path/to/generation';
-        $pathToDi = 'path/to/di';
-        $pathToCache = 'path/to/di';
-        $pathToConfig = 'path/to/config';
+        $this->expectFlagPresent(1, true);
+        $this->expectProcessLocked(1, true);
+        $this->expectNoActionPerformed();
+        $this->model->cleanGeneratedFiles();
+    }

-        $getPathMap =     [
-            [DirectoryList::GENERATED_CODE, $pathToGeneration],
-            [DirectoryList::GENERATED_METADATA, $pathToDi],
-            [DirectoryList::CACHE, $pathToCache],
-            [DirectoryList::CONFIG, $pathToConfig],
-        ];
+    /**
+     * It does not clean generated files when checking flag exists due to exceptions
+     *
+     * @test
+     * @param string $exceptionClassName
+     * @return void
+     *
+     * @dataProvider itDoesNotCleanGeneratedFilesDueToExceptionsDataProvider
+     */
+    public function itDoesNotCleanGeneratedFilesWhenCheckingFlagExistsDueToExceptions(
+        string $exceptionClassName
+    ) {
+        // Configure write interface to throw exception upon flag existence check
+        $this->writeInterface->expects($this->any())
+            ->method('isExist')
+            ->with(GeneratedFiles::REGENERATE_FLAG)
+            ->willThrowException(new $exceptionClassName(__('Some error has occurred.')));

-        $deleteMap = [[BP . '/' . $pathToGeneration, true],
-            [BP . '/' . $pathToDi, true],
-            [BP . GeneratedFiles::REGENERATE_FLAG, true],
-        ];
+        $this->expectProcessLocked(0);
+        $this->expectNoActionPerformed();
+        $this->model->cleanGeneratedFiles();
+    }
+
+    /**
+     * It does not clean generated files when checking process lock due to exceptions
+     *
+     * @test
+     * @param string $exceptionClassName
+     * @return void
+     *
+     * @dataProvider itDoesNotCleanGeneratedFilesDueToExceptionsDataProvider
+     */
+    public function itDoesNotCleanGeneratedFilesWhenCheckingProcessLockDueToExceptions(
+        string $exceptionClassName
+    ) {
+        $this->expectFlagPresent(1, true);

+        // Configure lock to throw exception upon process lock check
+        $this->lockManager->expects($this->any())
+            ->method('isLocked')
+            ->with(GeneratedFiles::REGENERATE_LOCK)
+            ->willThrowException(new $exceptionClassName(__('Some error has occurred.')));
+
+        $this->expectNoActionPerformed();
+        $this->model->cleanGeneratedFiles();
+    }
+
+    /**
+     * It does not clean generated files due to exceptions in allowed check data provider
+     *
+     * @return array
+     */
+    public function itDoesNotCleanGeneratedFilesDueToExceptionsDataProvider()
+    {
         return [
-            'runAll' => [ $getPathMap, [[BP . '/' . $pathToGeneration, true],
-                [BP . '/' . $pathToDi, true]], $deleteMap ],
-            'noDIfolder' => [ $getPathMap, [[BP . '/' . $pathToGeneration, true],
-                [BP . '/' . $pathToDi, false]], $deleteMap],
-            'noGenerationfolder' => [$getPathMap, [[BP . '/' . $pathToGeneration, false],
-                [BP . '/' . $pathToDi, true]], $deleteMap],
-            'nofolders' => [ $getPathMap, [[BP . '/' . $pathToGeneration, false],
-                [BP . '/' . $pathToDi, false]], $deleteMap],
+            RuntimeException::class => [RuntimeException::class],
+            FileSystemException::class => [FileSystemException::class],
         ];
     }

-    public function testCleanGeneratedFilesWithNoFlag()
+    /**
+     * It does not clean generated files if process lock is not acquired
+     *
+     * @test
+     * @return void
+     */
+    public function itDoesNotCleanGeneratedFilesIfProcessLockIsNotAcquired()
+    {
+        $this->expectFlagPresent(1, true);
+        $this->expectProcessLocked(1, false);
+
+        // Expect lock manager try to lock, but fail without exception
+        $this->lockManager->expects($this->once())->method('lock')->with(
+            GeneratedFiles::REGENERATE_LOCK,
+            GeneratedFiles::REGENERATE_LOCK_TIMEOUT
+        )->willReturn(false);
+
+        $this->expectNoActionPerformed();
+        $this->model->cleanGeneratedFiles();
+    }
+
+    /**
+     * It does not clean generated files if process lock is not acquired due to exception
+     *
+     * @test
+     * @return void
+     */
+    public function itDoesNotCleanGeneratedFilesIfProcessLockIsNotAcquiredDueToException()
     {
-        $this->writeInterface
-            ->expects($this->once())
-            ->method('isExist')
-            ->with(GeneratedFiles::REGENERATE_FLAG)
-            ->willReturn(false);
-        $this->directoryList->expects($this->never())->method('getPath');
-        $this->writeInterface->expects($this->never())->method('getPath');
-        $this->writeInterface->expects($this->never())->method('delete');
+        $this->expectFlagPresent(1, true);
+        $this->expectProcessLocked(1, false);
+
+        // Expect lock manager try to lock, but fail with runtime exception
+        $this->lockManager->expects($this->once())->method('lock')->with(
+            GeneratedFiles::REGENERATE_LOCK,
+            GeneratedFiles::REGENERATE_LOCK_TIMEOUT
+        )->willThrowException(new RuntimeException(__('Cannot acquire a lock.')));
+
+        $this->expectNoActionPerformed();
+        $this->model->cleanGeneratedFiles();
+    }
+
+    /**
+     * It cleans generated files properly, when no errors or exceptions raised
+     *
+     * @test
+     * @return void
+     */
+    public function itCleansGeneratedFilesProperly()
+    {
+        $this->expectFlagPresent(1, true);
+        $this->expectProcessLocked(1, false);
+        $this->expectLockOperation(1, true);
+
+        $this->writeInterface->expects($this->exactly(4))->method('delete')->withConsecutive(
+            [GeneratedFiles::REGENERATE_FLAG],
+            [$this->pathGeneratedCode],
+            [$this->pathGeneratedMetadata],
+            [$this->pathVarCache]
+        );
+
+        $this->expectRegenerationRequested(0);
+        $this->expectUnlockOperation(1, true);
+
         $this->model->cleanGeneratedFiles();
     }

-    public function testRequestRegeneration()
+    /**
+     * It requests regeneration and unlock upon FileSystemException
+     *
+     * @test
+     * @return void
+     */
+    public function itRequestsRegenerationAndUnlockUponFileSystemException()
     {
-        $this->writeInterface->expects($this->once())->method("touch");
-        $this->model->requestRegeneration();
+        $this->expectFlagPresent(1, true);
+        $this->expectProcessLocked(1, false);
+        $this->expectLockOperation(1, true);
+
+        $this->writeInterface->expects($this->any())->method('delete')->willThrowException(
+            new FileSystemException(__('Some error has occurred.'))
+        );
+
+        $this->expectRegenerationRequested(1);
+        $this->expectUnlockOperation(1, true);
+
+        $this->model->cleanGeneratedFiles();
     }
 }
