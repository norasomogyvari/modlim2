# -*- mode: ruby -*-
# vi: set ft=ruby :
CONFIG = File.join(File.dirname(__FILE__), ".private/config")

# Global Variables
project_name = "itegration"
alias_domains = ["www.itegration.test"]
project_ip = "192.168.56.10"
mem_size = 4096*2
cpu_num = 6

if File.exist?(CONFIG)
  load CONFIG
  project_name = PROJECT_NAME
  project_ip = PROJECT_IP
  alias_domains = ALIAS_DOMAINS
end

# 1.) Set the hostname
# 2.) Update /etc/hosts with hostname
# 3.) Disable dns override by NetworkManager in /etc/resolv.conf
# 4.) Add external nameserver to /etc/resolv.conf
networkProvision = <<SCRIPT
  hostnamectl set-hostname "${_PROJECTNAME}"
  if ! grep "${_PROJECTNAME}" &>/dev/null; then sed -ie "/127\.0\./ s/$/ ${_PROJECTNAME}/" /etc/hosts; fi
  if [ -f /etc/NetworkManager/NetworkManager.conf ]; then if ! grep "dns=none" /etc/NetworkManager/NetworkManager.conf &>/dev/null; then sed -ire '/\[main\]/a dns=none' /etc/NetworkManager/NetworkManager.conf; fi; fi
  if ! grep "nameserver 1.1.1.1" &>/dev/null; then sed -ie "/nameserver/i nameserver 1.1.1.1" /etc/resolv.conf; fi
SCRIPT

syncProvision = <<SCRIPT
  # create /var/www folder
  install -vdo root -g root -m 0755 /var/www

  # create magento user
  groupadd -g 4999 magento
  useradd -d /var/www/magento -c "magento" -mg magento -s /bin/bash -u 4999 magento
  install -vdo magento -g magento /var/www/magento/www

  # install unison
  cd /usr/bin
  curl -o unison -sSL https://github.com/mixe3y/unison/raw/master/unison-2.51.2-linux && \
  curl -o unison-fsmonitor -sSL https://github.com/mixe3y/unison/raw/master/unison-fsmonitor-2.51.2-linux && \
  chmod +x unison*

  if [[ "$(sysctl -n fs.inotify.max_user_watches)" -lt 524288 ]]; then

    if ! grep -q '^fs.inotify.max_user_watches' /etc/sysctl.conf /etc/sysctl.d/inotify_userwatches.conf ; then
      echo "fs.inotify.max_user_watches = 524288" | sudo tee /etc/sysctl.d/inotify_userwatches.conf > /dev/null 
    fi
    if ! grep -q '^net.ipv6.conf.all.disable_ipv6' /etc/sysctl.conf /etc/sysctl.d/ipv6_disable.conf ; then
      echo -e "net.ipv6.conf.all.disable_ipv6 = 1\nnet.ipv6.conf.default.disable_ipv6 = 1\nnet.ipv6.conf.eth0.disable_ipv6 = 1\nnet.ipv6.conf.lo.disable_ipv6 = 1" | sudo tee /etc/sysctl.d/ipv6_disable.conf > /dev/null
    fi

    sudo sysctl --system
  fi

SCRIPT

environmentProvision = <<SCRIPT
  cp -f /tmp/.private/provision.sh /usr/local/bin/provision.sh
  chmod +x /usr/local/bin/provision.sh
  touch /usr/local/etc/localdev
  install -vdo root -g root -m 700 /root/.ssh/
  install -vo root -g root -m 600 /tmp/.private/access-key /root/.ssh/id_rsa

  # Run provision.sh
  /usr/local/bin/provision.sh
SCRIPT

# localcommand
module LocalCommand
  class Config < Vagrant.plugin("2", :config)
      attr_accessor :command
  end
  class Plugin < Vagrant.plugin("2")
      name "local_shell"
      config(:local_shell, :provisioner) do
          Config
      end
      provisioner(:local_shell) do
          Provisioner
      end
  end
  class Provisioner < Vagrant.plugin("2", :provisioner)
      def provision
          result = system "#{config.command}"
      end
  end
end

Vagrant.configure("2") do |config|
  required_plugins = %w( vagrant-hostmanager )
  required_plugins.each do |plugin|
    system "vagrant plugin install #{plugin}" unless Vagrant.has_plugin? plugin
  end

  config.hostmanager.enabled = true
  config.hostmanager.manage_host = true
  config.hostmanager.manage_guest = false

  # CentOS 7 box
  config.vm.define "centos" do |centos|
    centos.vm.box = "centos/7"
    #centos.vm.box = "mixe3y/sandbox-centos-virtualbox"
    #centos.vm.box_url = "https://vagrant:itgvagrant@management.itg.cloud/sandbox-centos-virtualbox.json"
    centos.vm.hostname = project_name << ".test"
    centos.hostmanager.aliases = alias_domains
    centos.vm.network :private_network, ip: project_ip
    centos.vm.provider "hyperv" do |hv|
      # VMName = centos.projectname
      hv.vmname = "#{project_name}"
      hv.memory = mem_size
      hv.cpus = cpu_num
      hv.vm_integration_services = {
        guest_service_interface: true,
        heartbeat: true,
        key_value_pair_exchange: true,
        shutdown: true,
        time_synchronization: true,
        vss: true
      }
    end
    centos.vm.provider :virtualbox do |v|
      v.name = "#{project_name}"
      v.customize [
          "modifyvm", :id,
          "--name", "#{project_name}",
          "--memory", mem_size,
          "--natdnshostresolver1", "on",
          "--cpus", cpu_num,
      ]
    end
    centos.vm.synced_folder ".", "/vagrant", type: "rsync", disabled: true

    # Network and Virtual Machine Provisioning
    centos.vm.provision "shell", env: {"_PROJECTNAME" => "#{project_name}"}, inline: networkProvision

    # Unison Sync Provisioning
    centos.vm.provision "shell", inline: syncProvision
    centos.vm.provision "Starting Unison on the Guest Machine", type: 'shell', :inline => 'echo "starting unison" && cd /var/www/magento/www && rm -fr /var/www/magento/.unison/ && sudo -u magento nohup /usr/bin/unison -owner magento -group magento -socket 5000 2>&1 > /dev/null & sleep 1', :run => 'always' 
    if Vagrant::Util::Platform.windows?
      centos.vm.provision "Starting Unison Sync on the Host Machine (Windows)", type: "local_shell", command: "START .private\\unison-sync.bat", :run => 'always'
    else
      centos.vm.provision "Starting Unison Sync on the Host Machine (Linux)", type: "local_shell", command: "xterm -hold -e bash .private/unison-sync.sh &", :run => 'always'
    end

    # Environment Provisioning
    centos.vm.synced_folder ".private/", "/tmp/.private/", type: "rsync"
    centos.vm.provision "shell", inline: environmentProvision
  end

  #config.vm.define "ubuntu" do |ubuntu|
  #  ubuntu.vm.box = "bento/ubuntu-16.04"
  #  ubuntu.hostmanager.aliases = alias_domains
  #  ubuntu.vm.network :private_network, ip: env_ip2
  #  ubuntu.ssh.forward_agent = true
  #  ubuntu.vm.provider "hyperv" do |hv|
  #    # VMName = ubuntu.projectname
  #    hv.vmname = "ubuntu.#{project_name}"
  #    hv.memory = mem_size
  #    hv.cpus = cpu_num
  #    hv.vm_integration_services = {
  #      guest_service_interface: true,
  #      heartbeat: true,
  #      key_value_pair_exchange: true,
  #      shutdown: true,
  #      time_synchronization: true,
  #      vss: true
  #    }
  #  end
  #  ubuntu.vm.provider :virtualbox do |v|
  #    v.name = "ubuntu.#{project_name}"
  #    v.customize [
  #        "modifyvm", :id,
  #        "--name", "ubuntu.#{project_name}",
  #        "--memory", mem_size,
  #        "--natdnshostresolver1", "on",
  #        "--cpus", cpu_num,
  #    ]
  #  end
  #  # Rsync off
  #  ubuntu.vm.synced_folder ".", "/vagrant", type: "rsync", disabled: true
  #  # Provisioning
  #  ubuntu.vm.provision "shell", env: {"_PROJECTNAME" => "ubuntu.#{project_name}.local"}, inline: networkProvision
  #  ubuntu.vm.provision "file", source: ".private", destination: "/tmp/.private"
  #  ubuntu.vm.provision "shell", inline: environmentProvision
  #end

  config.hostmanager.aliases = alias_domains

  #config.vm.box = "centos/7"
  #config.vm.hostname = $project_name << ".local"

  #config.vm.network "private_network", ip: $env_ip
  #config.vm.network "public_network"

  #config.vm.provider :virtualbox do |vb|
		#vb.gui = false
  #  vb.vmname = $project_name
		#vb.memory = $mem_size
		#vb.cpus = $cpu_num
  #  vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
  #end

  #config.vm.provider "hyperv" do |hv|
  #  hv.vmname = $project_name
  #  hv.memory = $mem_size
  #  hv.cpus = $cpu_num
  #  hv.vm_integration_services = {
  #    guest_service_interface: true,
  #    heartbeat: true,
  #    key_value_pair_exchange: true,
  #    shutdown: true,
  #    time_synchronization: true,
  #    vss: true
  #  }
  #end

  # Rsync Sync
  # config.vm.synced_folder ".", "/vagrant", type: "rsync
  
  # Box Provision
  #config.vm.provision "file", source: ".provision/provision.sh", destination: "/tmp/provision.sh"
  #config.vm.provision "shell", inline: <<-SHELL
  #  mv -f /tmp/provision.sh /usr/local/bin/provision.sh
  #  chmod +x /usr/local/bin/provision.sh
  #  touch /usr/local/etc/localdev

    ## Run provision.sh
  #  /usr/local/bin/provision.sh
  #SHELL

end
